package br.com.produban.openbus.avro;

import br.com.produban.openbus.avro.schemaregistry.AvroLocalSchemaRegistry;
import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import br.com.produban.openbus.avro.schemaregistry.SchemaRegistry;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.log4j.Logger;

import java.nio.ByteBuffer;
import java.text.MessageFormat;
import java.util.Map;

public class AvroDecoder {
	private final static Logger LOGGER = Logger.getLogger(AvroDecoder.class);

	private static final byte MAGIC_BYTE = 0x0;

	private SchemaRegistry schemaRegistry;
	private DecoderFactory decoderFactory;

	@SuppressWarnings("unchecked")
	public AvroDecoder() {
		Config appConfig = ConfigFactory.load().getConfig(SchemaRegistry.APP_CONFIG_ROOT);
		init(appConfig);
	}

	public AvroDecoder(Config appConfig) {
		init(appConfig);
	}

	private void init(Config appConfig) {
		if (appConfig == null) {
			throw new IllegalArgumentException("Application config for Schema Registry wasn't found.");
		}

		String schemaPackage = appConfig.getString(SchemaRegistry.APP_CONFIG_SCHEMA_PACKAGE);
		if (schemaPackage == null || schemaPackage.isEmpty()) {
			throw new IllegalArgumentException("Properties for Schema Registry cannot be null.");
		}

		if (!appConfig.hasPath(SchemaRegistry.APP_CONFIG_TOOLS) ||
				appConfig.getConfig(SchemaRegistry.APP_CONFIG_TOOLS).entrySet().isEmpty()) {
			throw new IllegalArgumentException("Application config for Schema Registry wasn't found.");
		}

		schemaRegistry = new AvroLocalSchemaRegistry();
		schemaRegistry.init(appConfig);

		decoderFactory = DecoderFactory.get();
	}

	public AvroTool toRecord(byte[] byteArray) {

		// check & discard MAGIC_BYTE
		ByteBuffer buffer = getByteBuffer(byteArray);
		if (buffer == null) {
			return null;
		}

		// read ID
		int id = buffer.getInt();

		try {
			SchemaRegistry.SchemaRegister register = schemaRegistry.getToolById(id);

			if (register == null)
				throw new IllegalArgumentException(MessageFormat.format("Schema ID {0} not found", id));
			else {
				Class schemaClass = schemaRegistry.getClassBySchema(register.getSchema());
				DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>(register.getSchema());
				Decoder decoder = decoderFactory.binaryDecoder(buffer.array(), buffer.position(), buffer.remaining(), null);
				final GenericRecord record = reader.read(schemaClass != null ? (GenericRecord) schemaClass.newInstance() : null, decoder);

				return new AvroTool(register.getTool(), record);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return null;
	}

	private ByteBuffer getByteBuffer(byte[] payload) {
		ByteBuffer buffer = ByteBuffer.wrap(payload);
		if (buffer.get() != MAGIC_BYTE) {
			LOGGER.error("Unknown magic byte!");
			return null;
		}
		return buffer;
	}
}
