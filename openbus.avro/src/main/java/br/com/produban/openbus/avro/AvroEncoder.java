package br.com.produban.openbus.avro;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AvroEncoder {

	private final static Logger LOGGER = Logger.getLogger(AvroEncoder.class);
	
	public static byte[] toByteArray(String tool, GenericRecord genericRecord) {

		Schema schema = genericRecord.getSchema();

		String resolvedTool = tool == null || tool.isEmpty() ? schema.getName() : tool;

		GenericDatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>();
		writer.setSchema(schema);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		ByteBuffer bufferSchemaId = FingerprintUtils.crc32AsByteBuffer(resolvedTool, schema);

		try {
			// Writes magic byte
			outputStream.write(0x0);

			// Writes Schema Id based on the CRC32 Schema
			outputStream.write(bufferSchemaId.array());

			Encoder encoder = EncoderFactory.get().binaryEncoder(outputStream, null);

			writer.write(genericRecord, encoder);

			encoder.flush();
		} catch (IOException e) {
			LOGGER.log(Level.ERROR, e.getMessage(), e);
			return null;
		}

		return outputStream.toByteArray();

	}

}
