package br.com.produban.openbus.avro;

import org.apache.avro.Schema;
import org.apache.log4j.Logger;

import java.nio.ByteBuffer;
import java.text.MessageFormat;
import java.util.zip.CRC32;

public class FingerprintUtils {

    private final static Logger LOGGER = Logger.getLogger(FingerprintUtils.class);

    private static final String toolIdTemplate = "{0}|{1}";

    public static ByteBuffer crc32AsByteBuffer(String tool, Schema avroSchema) {
        ByteBuffer fingerprintBuffer = ByteBuffer.allocate(4);
        fingerprintBuffer.putInt(crc32AsInt(tool, avroSchema));
        return fingerprintBuffer;
    }

    public static int crc32AsInt(String tool, Schema avroSchema) {
        CRC32 crc32 = new CRC32();
        crc32.reset();
        crc32.update(MessageFormat.format(toolIdTemplate,tool,avroSchema.toString()).getBytes());

        int schemaId = (int) crc32.getValue();

        if (LOGGER.isDebugEnabled())
            LOGGER.debug(
                    MessageFormat.format("Expected schema ID to be read for tool {0} and schema name {1} is: {2}",
                            tool,
                            avroSchema.getName(),
                            schemaId));

        return schemaId;
    }

}
