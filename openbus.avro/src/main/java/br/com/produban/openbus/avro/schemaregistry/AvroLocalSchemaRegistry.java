package br.com.produban.openbus.avro.schemaregistry;

import br.com.produban.openbus.avro.FingerprintUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import org.apache.avro.Schema;
import org.apache.avro.specific.AvroGenerated;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class AvroLocalSchemaRegistry implements SchemaRegistry, Serializable {

	private static final Logger LOGGER = Logger.getLogger(AvroLocalSchemaRegistry.class);

	private static final long serialVersionUID = -4125035311988019557L;
	
	private final Map<Integer, SchemaRegister> schemasById;

    protected List<Class<?>> classes;
	private Map<Schema, Class> classPerSchema;
	
	public AvroLocalSchemaRegistry() {
		schemasById = new ConcurrentHashMap<>();
		classPerSchema = new ConcurrentHashMap<>();
	}

	@SuppressWarnings("rawtypes")
	public void init(Config appConfig) {
		// classpath scanning
		String schemaPackage = appConfig.getString(APP_CONFIG_SCHEMA_PACKAGE);
		Logger.getLogger(this.getClass()).info("Schema Package:" + schemaPackage);
		
		classes = scan(schemaPackage);

        if (classes == null || classes.isEmpty())
            throw new IllegalStateException("Avro schema classes not found. Please verify your classpath!");

        // Register every Record's Schema along with available tools
		for (Class<?> clazz : classes) {
			try {
				SpecificRecordBase genericRecord = (SpecificRecordBase) clazz.newInstance();
				for (String tool : appConfig.getStringList(APP_CONFIG_TOOLS + "." + genericRecord.getSchema().getName())) {
					register(tool, genericRecord.getSchema(), clazz);
				}
			} catch (InstantiationException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (IllegalAccessException e) {
				LOGGER.error(e.getMessage(), e);
			} catch (ConfigException e) {
				LOGGER.warn("Fail to register schema",e);
			}
		}
	}

	public void register(String tool, Schema schema, Class clazz) {
		int id = FingerprintUtils.crc32AsInt(tool,schema);
		
		// register Schema
		schemasById.put(id, new SchemaRegister(tool,schema));
		classPerSchema.put(schema, clazz);

		LOGGER.info(MessageFormat.format("Schema registry: New schema [ID: {0} - Tool: {1} - Schema: {2}]",
				id,
				tool,
				schema.getName()));
	}

	@Override
	public SchemaRegister getToolById(Integer id) {
		return schemasById.get(id);
	}

	@Override
	public Class getClassBySchema(Schema schema) {
		return classPerSchema.get(schema);
	}

	public List<Class<?>> scan(String packageName) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forClassLoader())
            .filterInputsBy(new FilterBuilder().includePackage(packageName)));
        return new ArrayList<>(reflections.getTypesAnnotatedWith(AvroGenerated.class));
    }
}
