package br.com.produban.openbus.avro.schemaregistry;

import org.apache.avro.generic.GenericRecord;

import java.io.Serializable;

public class AvroTool implements Serializable {

    private String toolName;
    private GenericRecord record;

    public AvroTool() {
    }

    public AvroTool(String toolName, GenericRecord record) {
        this.toolName = toolName;
        this.record = record;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public GenericRecord getRecord() {
        return record;
    }

    public void setRecord(GenericRecord record) {
        this.record = record;
    }
}
