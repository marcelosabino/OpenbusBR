package br.com.produban.openbus.avro.schemaregistry;

import com.typesafe.config.Config;
import org.apache.avro.Schema;

import java.util.Objects;
import java.util.Properties;

public interface SchemaRegistry {

    String APP_CONFIG_ROOT = "openbus";
    String APP_CONFIG_TOOLS = "tools";
    String APP_CONFIG_SCHEMA_PACKAGE = "schemaPackage";

    void init(Config config);
    SchemaRegister getToolById(Integer id);
    Class getClassBySchema(Schema schema);

    class SchemaRegister {
        private String tool;
        private Schema schema;

        public SchemaRegister(String tool, Schema schema) {
            this.tool = tool;
            this.schema = schema;
        }

        public String getTool() {
            return tool;
        }

        public void setTool(String tool) {
            this.tool = tool;
        }

        public Schema getSchema() {
            return schema;
        }

        public void setSchema(Schema schema) {
            this.schema = schema;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof SchemaRegister)) return false;
            SchemaRegister that = (SchemaRegister) o;
            return Objects.equals(getTool(), that.getTool()) &&
                    Objects.equals(getSchema(), that.getSchema());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getTool(), getSchema());
        }
    }
}
