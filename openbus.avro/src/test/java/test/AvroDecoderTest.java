package test;

import br.com.produban.openbus.avro.AvroDecoder;
import br.com.produban.openbus.avro.AvroEncoder;
import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import br.com.produban.openbus.model.avro.Eventlog;
import br.com.produban.openbus.model.avro.Syslog5424;
import br.com.produban.openbus.model.avro.ZabbixAgentData;
import org.apache.avro.generic.GenericRecord;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class AvroDecoderTest {

    private static AvroDecoder decoder;

    private byte[] byteArray;

    @BeforeClass
    public static void beforeClass() {
        decoder = new AvroDecoder();
    }

    @Test
    public void testDecodingZabbixAgentData() {

        String tool = "Zabbix";

        GenericRecord avro = ZabbixAgentData.newBuilder()
                .setValue("valueTest")
                .setTimestamp("timestamp")
                .setClock("clock")
                .setHost("host")
                .setKey("key")
                .setHostMetadata(null)
                .setLastlogsize(null)
                .setEventid(null)
                .setMtime(null)
                .setNs(null)
                .setSeverity(null)
                .setSource(null)
                .setState(null)
                .build();

        byteArray = AvroEncoder.toByteArray(tool, avro);

        AvroTool decodedMessage = decoder.toRecord(byteArray);

        assertTrue((decodedMessage.getRecord() instanceof ZabbixAgentData));
        assertEquals(tool,decodedMessage.getToolName());
        assertEquals("valueTest", ((ZabbixAgentData) decodedMessage.getRecord()).getValue());
    }

    @Test
    public void testDecodingSyslog5424() {

        String tool = "Syslog";

        GenericRecord avro = Syslog5424.newBuilder()
                .setApplicationName("application")
                .setHeader("header")
                .setHostname("hostname")
                .setMessage("messageTest")
                .setMessageId("messageId")
                .setPriority("priority")
                .setProcessId("processId")
                .setStructuredData("structuredData")
                .setSyslogMessage("syslogMessage")
                .setTimestamp("timestamp")
                .setVersion(1)
                .build();

        byteArray = AvroEncoder.toByteArray(tool, avro);

        AvroTool decodedMessage = decoder.toRecord(byteArray);

        assertTrue((decodedMessage.getRecord() instanceof Syslog5424));
        assertEquals(tool,decodedMessage.getToolName());
        assertEquals("messageTest", ((Syslog5424) decodedMessage.getRecord()).getMessage());
    }

    @Test
    public void testDecodingEventlog() {

        String tool = "WindowsEventLog";

        Eventlog avro = new Eventlog();
        avro.setMessage("mensagem de teste");

        byteArray = AvroEncoder.toByteArray(tool, avro);

        AvroTool decodedMessage = decoder.toRecord(byteArray);

        assertTrue((decodedMessage.getRecord() instanceof Eventlog));
        assertEquals(tool,decodedMessage.getToolName());
        assertEquals("mensagem de teste", ((Eventlog) decodedMessage.getRecord()).getMessage());
    }

    @Test
    public void testSchemaClassesNotFound() throws Exception {
        decoder = new AvroDecoder();
    }
}
