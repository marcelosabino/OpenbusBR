package test;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.junit.Test;

import java.util.Map;

public class ConfigTest {

    @Test
    public void testConfiguration() throws Exception {
        Config appConfig = ConfigFactory.load().getConfig("openbus.tools");

        for (Map.Entry<String, ConfigValue> entry : appConfig.entrySet()) {
            System.out.println(String.format("Schema: %s",entry.getKey()));

            for (String tool : appConfig.getStringList(entry.getKey()))
                System.out.println(String.format("  Tool: %s",tool));
        }
    }

}
