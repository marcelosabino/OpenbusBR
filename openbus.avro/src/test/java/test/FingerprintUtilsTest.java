package test;

import br.com.produban.openbus.avro.FingerprintUtils;
import br.com.produban.openbus.model.avro.ZabbixAgentData;
import org.apache.avro.generic.GenericRecord;
import org.junit.Test;

public class FingerprintUtilsTest {

    @Test
    public void testToolId() throws Exception {
        String tool = "Robots";

        GenericRecord avro = ZabbixAgentData.newBuilder()
                .setValue("123123")
                .setTimestamp("timestamp")
                .setClock("1442603622")
                .setHost("BSBRSP7654")
                .setKey("service_monitoring.latency")
                .setHostMetadata(null)
                .setLastlogsize(null)
                .setEventid(null)
                .setMtime(null)
                .setNs(null)
                .setSeverity(null)
                .setSource(null)
                .setState(null)
                .build();

        System.out.println(FingerprintUtils.crc32AsInt(tool,avro.getSchema()));
    }

}
