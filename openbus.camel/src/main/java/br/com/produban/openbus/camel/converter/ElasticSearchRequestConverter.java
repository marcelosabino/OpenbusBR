package br.com.produban.openbus.camel.converter;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by renato on 14/10/15.
 */
public class ElasticSearchRequestConverter implements Processor {

    public void process(Exchange exchange) throws Exception {
        String indexes = exchange.getIn().getHeader("indexes", String.class);
        String types = exchange.getIn().getHeader("types", String.class);
        String aggr = exchange.getIn().getHeader("aggr", String.class);
        String interval = exchange.getIn().getHeader("interval", String.class);
        String query = exchange.getIn().getHeader("query", String.class);

        indexes = URLDecoder.decode(indexes, "utf-8");
        types = URLDecoder.decode(types, "utf-8");
        aggr = URLDecoder.decode(aggr, "utf-8");
        interval = URLDecoder.decode(interval, "utf-8");
        QueryStringQueryBuilder queryBuilder = new QueryStringQueryBuilder(URLDecoder.decode(query, "utf-8"));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(queryBuilder);

        searchSourceBuilder.from(0);
        searchSourceBuilder.size(100000);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(indexes);
        searchRequest.types(types);

        exchange.getIn().setBody(searchRequest);
    }
}
