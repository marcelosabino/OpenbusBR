package br.com.produban.openbus.camel.converter;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.PlainActionFuture;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

/**
 * Created by renato on 14/10/15.
 */
public class ElasticSearchResponseConverter implements Processor {

    public void process(Exchange exchange) throws Exception {

        SearchResponse searchResponse = (SearchResponse) exchange.getIn().getBody();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[\n");
        SearchHits searchHits = searchResponse.getHits();
        int lengthHits = searchHits.hits().length;
        for (int c = 0; c < lengthHits; c++) {
            stringBuilder.append(searchHits.getAt(c).getSourceAsString());
            if (c != lengthHits -1) {
                stringBuilder.append(",\n");
            }
        }
        stringBuilder.append("\n\n]");

        exchange.getIn().setBody(stringBuilder);

    }
}
