#!/bin/bash
echo "------------------------------------------"
echo "running job ..."

date +"Begin: %Y-%m-%d %H:%M:%S"

/usr/bin/hadoop jar /produtos/openbus.camus/openbus.camus-0.0.1-SNAPSHOT-shaded.jar com.linkedin.camus.etl.kafka.CamusJob -libjars '/produtos/openbus.camus/schemas/openbus.model-0.0.2-SNAPSHOT.jar' -P /produtos/openbus.camus/camus.properties

date +"End: %Y-%m-%d %H:%M:%S"

/usr/bin/hdfs dfs -ls -h -R /openbus/data/metrics.ZabbixAgentData/hourly/`date +"%Y/%m/%d"`| grep "\.avro" | awk '{print $7"\t"$8"\t"$5" "$6"\t"$9}' | sort -u | tail -n2
/usr/bin/hdfs dfs -ls -h -R /openbus/data/logs.Eventlog/hourly/`date +"%Y/%m/%d"`| grep "\.avro" | awk '{print $7"\t"$8"\t"$5" "$6"\t"$9}' | sort -u | tail -n2
/usr/bin/hdfs dfs -ls -h -R /openbus/data/logs.Syslog5424/hourly/`date +"%Y/%m/%d"`| grep "\.avro" | awk '{print $7"\t"$8"\t"$5" "$6"\t"$9}' | sort -u | tail -n2
echo ""
echo ""
