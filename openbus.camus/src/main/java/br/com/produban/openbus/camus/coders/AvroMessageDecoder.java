package br.com.produban.openbus.camus.coders;

import br.com.produban.openbus.avro.AvroDecoder;
import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import br.com.produban.openbus.model.factories.DataBeanFactory;
import com.linkedin.camus.coders.CamusWrapper;
import com.linkedin.camus.coders.MessageDecoder;
import com.linkedin.camus.etl.kafka.common.KafkaMessage;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class AvroMessageDecoder extends MessageDecoder<KafkaMessage, GenericRecord> {

    private final static Logger LOGGER = LoggerFactory.getLogger(AvroMessageDecoder.class);

    protected AvroDecoder avroDecoder;

    @Override
    public void init(Properties props, String topicName) {
        super.init(props, topicName);
        avroDecoder = new AvroDecoder();
    }

    @Override
    public CamusWrapper<GenericRecord> decode(KafkaMessage message) {

        AvroTool avroTool = avroDecoder.toRecord(message.getPayload());
        if(avroTool == null) {
            LOGGER.info("Record is null.");
            return null;
        }
        CamusWrapper<GenericRecord> wrapper = new CamusWrapper(avroTool.getRecord());
        wrapper.put(new Text("schemaName"), new Text(avroTool.getToolName()));
        try {
            Long timestamp = DataBeanFactory.toBean(wrapper.getRecord()).getTimestamp();
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(timestamp));
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            timestamp = cal.getTimeInMillis();
            wrapper.put(new Text("timestamp"), new Text(String.valueOf(timestamp)));
        } catch (Exception e) {

            LOGGER.error("Fail to get message timestamp: " + wrapper.getRecord(), e);
        }
        return wrapper;
    }

}