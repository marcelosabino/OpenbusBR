package br.com.produban.openbus.camus.partitioners;

import com.linkedin.camus.etl.IEtlKey;
import com.linkedin.camus.etl.kafka.partitioner.DefaultPartitioner;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.JobContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpenbusAvroSchemaPartitioner extends DefaultPartitioner {

    private final static Logger LOGGER = LoggerFactory.getLogger(OpenbusAvroSchemaPartitioner.class);

	@Override
	public String encodePartition(JobContext context, IEtlKey key) {
        String schemaName = key.getPartitionMap().get(new Text("schemaName")).toString();
        String timestamp = key.getPartitionMap().get(new Text("timestamp")) != null ?
                key.getPartitionMap().get(new Text("timestamp")).toString() : super.encodePartition(context, key);
        return timestamp +"-"+ schemaName;
	}

    @Override
    public String generateFileName(JobContext context, String topic, String brokerId, int partitionId, int count, long offset, String encodedPartition) {
        String[] splittedPartition = encodedPartition.split("-");
        return super.generateFileName(context, topic, brokerId, partitionId, count, offset, splittedPartition[0]);
    }

    @Override
    public String generatePartitionedPath(JobContext context, String topic, String encodedPartition) {
        String[] enrichedPartition = encodedPartition.split("-");
        return super.generatePartitionedPath(context, topic+"."+enrichedPartition[1], enrichedPartition[0]);
    }
}