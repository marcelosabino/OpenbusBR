package br.com.produban.openbus.camus;

import br.com.produban.openbus.avro.AvroEncoder;
import br.com.produban.openbus.model.avro.Eventlog;
import br.com.produban.openbus.model.avro.ZabbixAgentData;
import com.google.gson.Gson;
import com.linkedin.camus.etl.kafka.CamusJob;
import com.linkedin.camus.etl.kafka.mapred.EtlInputFormat;
import com.linkedin.camus.etl.kafka.mapred.EtlMultiOutputFormat;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import kafka.serializer.StringEncoder;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.FileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.mapred.FsInput;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.core.Is.is;

public class CamusJobTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(CamusJobTest.class);

    private static final Random RANDOM = new Random();

    private static final String BASE_PATH = "/camus";
    private static final String DESTINATION_PATH = BASE_PATH + "/destination";
    private static final String EXECUTION_BASE_PATH = BASE_PATH + "/execution";
    private static final String EXECUTION_HISTORY_PATH = EXECUTION_BASE_PATH + "/history";

    private static final String TOPIC_1 = "topic_1";
//    private static final String TOPIC_2 = "topic_2";
//    private static final String TOPIC_3 = "topic_3";

    private static final String[] SCHEMAS = {"Eventlog", "ZabbixAgentData"};

    private static KafkaCluster cluster;
    private static FileSystem fs;
    private static Gson gson;
    private static Map<String, List<byte[]>> messagesWritten;

    @BeforeClass
    public static void beforeClass() throws IOException {
        cluster = new KafkaCluster();
        fs = FileSystem.get(new Configuration());
        gson = new Gson();

        // You can't delete messages in Kafka so just writing a set of known messages that can be used for testing
        messagesWritten = new HashMap<>();
        messagesWritten.put(TOPIC_1, writeKafka(TOPIC_1, 15));
//        messagesWritten.put(TOPIC_2, writeKafka(TOPIC_2, 15));
//        messagesWritten.put(TOPIC_3, writeKafka(TOPIC_3, 15));
    }

    @AfterClass
    public static void afterClass() {
        cluster.shutdown();
    }

    private Properties props;
    private CamusJob job;
    private TemporaryFolder folder;
    private String destinationPath;

    @Before
    public void before() throws IOException, NoSuchFieldException, IllegalAccessException {

        resetCamus();

        folder = new TemporaryFolder();
        folder.create();

        String path = folder.getRoot().getAbsolutePath();

        props = cluster.getProps();

        Properties prop = new Properties();
        InputStream input = new FileInputStream(Paths.get("").toAbsolutePath() +"/src/test/resources/camus.properties");
        prop.load(input);
        props.putAll(prop);

//        props.setProperty(EtlMultiOutputFormat.ETL_DESTINATION_PATH, destinationPath);
//        props.setProperty(CamusJob.ETL_EXECUTION_BASE_PATH, path + EXECUTION_BASE_PATH);
//        props.setProperty(CamusJob.ETL_EXECUTION_HISTORY_PATH, path + EXECUTION_HISTORY_PATH);

//        props.setProperty(EtlInputFormat.CAMUS_MESSAGE_DECODER_CLASS, JsonStringMessageDecoder.class.getName());
//        props.setProperty(EtlMultiOutputFormat.ETL_RECORD_WRITER_PROVIDER_CLASS, SequenceFileRecordWriterProvider.class.getName());

//        props.setProperty(EtlMultiOutputFormat.ETL_RUN_TRACKING_POST, Boolean.toString(false));
//        props.setProperty(CamusJob.KAFKA_CLIENT_NAME, "Camus");

        props.setProperty(CamusJob.KAFKA_BROKERS, props.getProperty("metadata.broker.list"));

        // Run Map/Reduce tests in process for hadoop2
        props.setProperty("mapreduce.framework.name", "local");
        // Run M/R for Hadoop1
        props.setProperty("mapreduce.jobtracker.address", "local");

        props.setProperty(EtlMultiOutputFormat.ETL_DESTINATION_PATH, path + props.getProperty("etl.destination.path"));
        props.setProperty(CamusJob.ETL_EXECUTION_BASE_PATH, path + props.getProperty("etl.execution.base.path"));
        props.setProperty(CamusJob.ETL_EXECUTION_HISTORY_PATH, path + props.getProperty("etl.execution.history.path"));

//        props.setProperty("camus.message.decoder.class", "br.com.produban.openbus.camus.coders.AvroMessageDecoder");
//        props.setProperty("etl.partitioner.class", "br.com.produban.openbus.camus.partitioners.OpenbusAvroSchemaPartitioner");
//        props.setProperty("kafka.registry.schemaPackage", "br.com.produban.openbus.model.avro");

        destinationPath = props.getProperty("etl.destination.path");
        job = new CamusJob(props);
    }

    @After
    public void after() throws IOException, NoSuchFieldException, SecurityException, IllegalArgumentException,
            IllegalAccessException {
        // Delete all camus data
        folder.delete();
        Field field = EtlMultiOutputFormat.class.getDeclaredField("committer");
        field.setAccessible(true);
        field.set(null, null);
    }

    @Test
    public void runJob() throws Exception {
        job.run();

        assertCamusContains(TOPIC_1);
//        assertCamusContains(TOPIC_2);
//        assertCamusContains(TOPIC_3);

        // Run a second time (no additional messages should be found)
//        job = new CamusJob(props);
//        job.run();

//        assertCamusContains(TOPIC_1);
//        assertCamusContains(TOPIC_2);
//        assertCamusContains(TOPIC_3);
    }

    @Test
    public void runJobWithoutErrorsAndFailOnErrors() throws Exception {
        props.setProperty(CamusJob.ETL_FAIL_ON_ERRORS, Boolean.TRUE.toString());
        job = new CamusJob(props);
        runJob();
    }

    @Test(expected = RuntimeException.class)
    public void runJobWithErrorsAndFailOnErrors() throws Exception {
        props.setProperty(CamusJob.ETL_FAIL_ON_ERRORS, Boolean.TRUE.toString());
        props.setProperty(EtlInputFormat.CAMUS_MESSAGE_DECODER_CLASS, FailDecoder.class.getName());
//        props.setProperty(CamusJob.ETL_MAX_PERCENT_SKIPPED_OTHER, "100.0");
        job = new CamusJob(props);
        job.run();
    }

    private void assertCamusContains(String topic) throws InstantiationException, IllegalAccessException, IOException {
        for(String schema : SCHEMAS) {
            assertCamusContains(topic +"."+ schema, messagesWritten.get(topic));
        }
    }

    private void assertCamusContains(String topic, List<byte[]> messages) throws InstantiationException,
            IllegalAccessException, IOException {
//        List<Message> readMessages = readMessages(topic);
//        assertThat(readMessages.size(), is(messages.size()));
        assertTrue(readMessages(topic).containsAll(messages));
    }

    private static List<byte[]> writeKafka(String topic, int numOfMessages) {

        List<byte[]> messages = new ArrayList<>();
        List<KeyedMessage<String, byte[]>> kafkaMessages = new ArrayList<>();
        for (int i = 0; i < numOfMessages; i++) {

            int rand = new Random().nextInt((3 - 1) + 1) + 1;
            switch(rand) {
                case 1:
                    createZabbixAgentData(topic, kafkaMessages, i);
                    break;
                case 2:
                    createEventLog(topic, kafkaMessages, i);
                    break;
                default:
                    createZabbixAgentData(topic, kafkaMessages, i);
            }
        }

        Properties producerProps = cluster.getProps();

//        producerProps.setProperty("serializer.class", StringEncoder.class.getName());
        producerProps.setProperty("key.serializer.class", StringEncoder.class.getName());

        Producer<String, byte[]> producer = new Producer<>(new ProducerConfig(producerProps));

        try {
            producer.send(kafkaMessages);
        } finally {
            producer.close();
        }

        return messages;
    }

    private static void createZabbixAgentData(String topic, List<KeyedMessage<String, byte[]>> kafkaMessages, int i) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, ((new Random().nextInt() % 2 == 0) ? -2 : (new Random().nextInt() % 2 == 0) ? -3 : -4));

        ZabbixAgentData zbx = new ZabbixAgentData();
        zbx.setHost("localhost"+ i);
        zbx.setHostMetadata("host_metadata");
        zbx.setKey("key");
        zbx.setValue("value");
        zbx.setLastlogsize("lastlogsize");
        zbx.setMtime("mtime");
        zbx.setTimestamp(String.valueOf(c.getTimeInMillis()));
        zbx.setSource("source");
        zbx.setSeverity("severity");
        zbx.setEventid("eventiId");
        zbx.setClock(String.valueOf(c.getTimeInMillis()));
        zbx.setNs(String.valueOf(c.getTimeInMillis()));

        byte[] msg = AvroEncoder.toByteArray("Zabbix",zbx);
        kafkaMessages.add(new KeyedMessage<String, byte[]>(topic,msg));
    }

    private static void createEventLog(String topic, List<KeyedMessage<String, byte[]>> kafkaMessages, int i) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, ((RANDOM.nextInt() % 2 == 0) ? -2 : (RANDOM.nextInt() % 2 == 0) ? -3 : -4));

        Eventlog evl = new Eventlog();
        evl.setComputerName("localhost"+ i);
        evl.setActivityID("activit" + i);
        evl.setAccountName("account" + i);
        evl.setCategory(Long.valueOf(i));
        evl.setCategoryString("category" + i);
        evl.setData("data");
        evl.setDomain("br.com.produban." + i);
        evl.setEventCode(Long.valueOf(i));
        evl.setSeverity("severity");
        evl.setEventIdentifier("eventiId" + i);
        evl.setEventType(i);
        evl.setLogfile("logfile" + i);
        evl.setMessage("message" + i);
        evl.setOpCode("opCode" + i);
        evl.setOpcodeValue("opCodeValue" + i);
        evl.setProcessID("process" + i);
        evl.setProviderGuid("provider" + i);
        evl.setRecordNumber(Long.valueOf(i));
        evl.setSid("Sid" + i);
        evl.setSidType("SidType" + i);
        evl.setSourceName("source");
        evl.setTask("task" + i);
        evl.setTaskCategory("taskCategory" + i);
        evl.setTimeGenerated(String.valueOf(c.getTimeInMillis()));
        evl.setTimeWritten(String.valueOf(c.getTimeInMillis()));
        evl.setType("type");
        evl.setUser("user");
        evl.setVersion("version"+ i);

        byte[] msg = AvroEncoder.toByteArray("Eventlog",evl);
        kafkaMessages.add(new KeyedMessage<String, byte[]>(topic,msg));
    }

    private List<Message> readMessages(String topic) throws IOException, InstantiationException, IllegalAccessException {
        return readMessages(new Path(destinationPath, topic));
    }

    private List<Message> readMessages(Path path) throws IOException, InstantiationException, IllegalAccessException {
        List<Message> messages = new ArrayList<>();

        try {

            for (FileStatus file : fs.listStatus(path)) {

                if (file.isDir()) {
                    messages.addAll(readMessages(file.getPath()));
                } else {
//                    SequenceFile.Reader reader = new SequenceFile.Reader(fs, file.getPath(), new Configuration());
                    Configuration config = new Configuration(); // make this your Hadoop env config
                    SeekableInput input = new FsInput(new Path(file.getPath().toString()), config);
                    DatumReader<GenericRecord> reader = new GenericDatumReader<>();
                    FileReader<GenericRecord> fileReader = DataFileReader.openReader(input, reader);
                    try {

                        LOGGER.info(file.getPath().toString());
                        for (GenericRecord datum : fileReader) {
                            LOGGER.info(datum.toString());
                            messages.add(gson.fromJson(datum.toString(), Message.class));
                        }

                        /*LongWritable key = (LongWritable) reader.getKeyClass().newInstance();
                        Text value = (Text) reader.getValueClass().newInstance();

                        while (reader.next(key, value)) {
                            messages.add(gson.fromJson(value.toString(), Message.class));
                        }*/
                    } finally {
//                        reader.close();
                        fileReader.close();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("No camus messages were found in [" + path + "]");
        }

        return messages;
    }

    private static class Message {

        private int number;

        public Message(int number) {
            this.number = number;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof Message))
                return false;

            Message other = (Message) obj;

            return number == other.number;
        }
    }

    private static void resetCamus() throws NoSuchFieldException, IllegalAccessException {
        // The EtlMultiOutputFormat has a static private field called committer which is only created if null. The problem is this
        // writes the Camus metadata meaning the first execution of the camus job defines where all committed output goes causing us
        // problems if you want to run Camus again using the meta data (i.e. what offsets we processed). Setting it null here forces
        // it to re-instantiate the object with the appropriate output path

        Field field = EtlMultiOutputFormat.class.getDeclaredField("committer");
        field.setAccessible(true);
        field.set(null, null);
    }

}