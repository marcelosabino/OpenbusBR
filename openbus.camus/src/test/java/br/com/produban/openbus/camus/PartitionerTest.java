package br.com.produban.openbus.camus;

import br.com.produban.openbus.camus.partitioners.OpenbusAvroSchemaPartitioner;
import com.linkedin.camus.etl.IEtlKey;
import com.linkedin.camus.etl.Partitioner;
import com.linkedin.camus.etl.kafka.common.EtlKey;
import com.linkedin.camus.etl.kafka.mapred.EtlMultiOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobContext;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PartitionerTest {

    private Partitioner subject;
    private IEtlKey etlKey = new EtlKey();

    private JobContext jobContext;
    private Configuration configuration;

    @Before
    public void setup() {
        jobContext = mock(JobContext.class);
        configuration = mock(Configuration.class);

        when(jobContext.getConfiguration()).thenReturn(configuration);
        when(configuration.getInt(EtlMultiOutputFormat.ETL_OUTPUT_FILE_TIME_PARTITION_MINS, 60)).thenReturn(10);
        when(configuration.get(EtlMultiOutputFormat.ETL_DESTINATION_PATH_TOPIC_SUBDIRECTORY, "hourly")).thenReturn("hourly");

        subject = new OpenbusAvroSchemaPartitioner();
        etlKey.put(new Text("schemaName"), new Text("Zabbix"));

        subject.setConf(configuration);
    }

    @Test
    public void testEncodePartition() {
        String encodedPartition = subject.encodePartition(jobContext, etlKey);
        assertEquals("Zabbix",encodedPartition.split("-")[1]);
    }

    @Test
    public void testGeneratePartitionedPath() {
        String generatedPath = subject.generatePartitionedPath(jobContext, "topic", subject.encodePartition(jobContext, etlKey));

        assertTrue(generatedPath.contains("topic.Zabbix"));
    }

    @Test
    public void testGenerateFileName() {
        String encodedPartition = subject.encodePartition(jobContext, etlKey);

        String generatedFileName = subject.generateFileName(jobContext, "topic", "0", 0, 12345, 67890L, encodedPartition);

        assertFalse(generatedFileName.contains("\tZabbix"));
    }

}
