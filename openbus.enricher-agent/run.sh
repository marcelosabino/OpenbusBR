#!/usr/bin/env bash

java -server \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.port=8170 \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Djava.rmi.server.hostname=srvbigpvlbr10 \
    -Xms128m -Xmx256m \
    -classpath "openbus.enricher-agent.jar:lib/*:conf" \
    br.com.produban.openbus.enricher.agent.Launcher &