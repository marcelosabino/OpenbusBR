package br.com.produban.openbus.enricher.agent

import _root_.kafka.serializer.DefaultDecoder
import akka.actor.{Props, ActorSystem}
import br.com.produban.openbus.enricher.agent.actors.AvroRouter
import br.com.produban.openbus.enricher.agent.kafka.KafkaAvroDecoder
import com.sclasen.akka.kafka.{AkkaConsumer, AkkaConsumerProps}
import com.typesafe.config.ConfigFactory

object Launcher extends App {

  Class.forName("org.neo4j.jdbc.Driver")

  val actorSystem = ActorSystem("test")
  val avroRouter = actorSystem.actorOf(Props[AvroRouter])
  val kafkaConfig = ConfigFactory.load().getConfig("kafka")
  
  val consumerProps = AkkaConsumerProps.forSystem(
    system = actorSystem,
    zkConnect = kafkaConfig.getString("zkConnect"),
    topic = kafkaConfig.getString("topic"),
    group = "consumer-openbus-enricher-agent",
    streams = 1, //one per partition
    keyDecoder = new DefaultDecoder(),
    msgDecoder = new KafkaAvroDecoder(),
    receiver = avroRouter
  )

  val consumer = new AkkaConsumer(consumerProps)

  consumer.start()
}