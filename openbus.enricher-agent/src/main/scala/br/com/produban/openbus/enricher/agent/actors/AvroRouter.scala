package br.com.produban.openbus.enricher.agent.actors

import akka.actor._
import br.com.produban.openbus.enricher.agent.kafka.AvroMessage
import br.com.produban.openbus.model.avro.enrichment._
import com.sclasen.akka.kafka.StreamFSM

class AvroRouter extends Actor with ActorLogging {

  val childs : Map[String,ActorRef] = Map(
    LogicalServerData.getClassSchema.getName -> context.actorOf(Props[LogicalServerPersister]),
    HyperData.getClassSchema.getName -> context.actorOf(Props[HyperPersister]),
    ComputerSystemData.getClassSchema.getName -> context.actorOf(Props[ComputerSystemPersister]),
    CmdbWas.getClassSchema.getName -> context.actorOf(Props[WasPersister]),
    CmdbDatabase.getClassSchema.getName -> context.actorOf(Props[DatabasePersister]),
    InfoApplication.getClassSchema.getName -> context.actorOf(Props[InfoApplicationPersister]),
    InfoRobots.getClassSchema.getName -> context.actorOf(Props[InfoRobotsPersister]),
    VCenter.getClassSchema.getName -> context.actorOf(Props[VCenterPersister]),
    Services.getClassSchema.getName -> context.actorOf(Props[ServicesPersister])
  )

  override def receive = {
    case message:AvroMessage if message.avro.isDefined => {
      childs get(message.avro.get.getRecord.getSchema.getName) match {
        case Some(actor) => actor forward message
        case None => sender ! StreamFSM.Processed
      }
    }
    case _ => sender ! StreamFSM.Processed
  }
  
}
