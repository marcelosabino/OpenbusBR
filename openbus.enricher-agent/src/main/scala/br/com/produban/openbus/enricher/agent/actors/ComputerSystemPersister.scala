package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.ComputerSystemData
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class ComputerSystemPersister extends PersisterNeo4JBase[ComputerSystemData] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.computerSystem")

  override def buildRequest(cs:ComputerSystemData) : Request = {
    Request(Array(registerStatement(cs,cypherConfig,"upsert.query","upsert.fields")))
  }

  override def cast(avro: GenericRecord): Option[ComputerSystemData] = {
    Some(avro.asInstanceOf[ComputerSystemData])
  }
}
