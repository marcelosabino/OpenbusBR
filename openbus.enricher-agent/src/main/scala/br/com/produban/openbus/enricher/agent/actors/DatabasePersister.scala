package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.CmdbDatabase
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class DatabasePersister extends PersisterNeo4JBase[CmdbDatabase] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.database")

  override def cast(avro: GenericRecord): Option[CmdbDatabase] = Some(avro.asInstanceOf[CmdbDatabase])

  override def buildRequest(avro: CmdbDatabase): Request = {
    var statements = Array(registerStatement(avro,cypherConfig,"upsert.query","upsert.fields"))

    Option(avro.getDbiLServer) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(avro,cypherConfig,"logicalServer.query","logicalServer.fields")
    }
    
    Option(avro.getScAssetid) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(avro,cypherConfig,"serviceComponent.query","serviceComponent.fields")
    }    

    Request(statements)
  }
}
