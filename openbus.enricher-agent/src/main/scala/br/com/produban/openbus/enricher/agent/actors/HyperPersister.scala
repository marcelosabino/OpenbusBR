package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.HyperData
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class HyperPersister extends PersisterNeo4JBase[HyperData] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.hyper")

  override def buildRequest(hyp:HyperData) : Request = {

    var statements = Array(registerStatement(hyp,cypherConfig,"upsert.query","upsert.fields"))

    Option(hyp.getComputerSystemName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(hyp,cypherConfig,"computerSystem.query","computerSystem.fields")
    }

    Request(statements)
  }

  override def cast(avro: GenericRecord): Option[HyperData] = {
    Some(avro.asInstanceOf[HyperData])
  }
}
