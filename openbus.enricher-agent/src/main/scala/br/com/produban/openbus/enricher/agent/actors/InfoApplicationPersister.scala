package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.PersisterRedisBase
import br.com.produban.openbus.model.avro.enrichment.InfoApplication
import org.apache.avro.generic.GenericRecord

class InfoApplicationPersister extends PersisterRedisBase[InfoApplication] {

  override def cast(avro: GenericRecord): Option[InfoApplication] = Some(avro.asInstanceOf[InfoApplication])

  override def execute(avro: InfoApplication) = {
    redis(avro.getTenant).hset(InfoApplicationPersister.INFO_APP_KEY,avro.getServerIp,avro.getServerName)
  }

}

object InfoApplicationPersister {
  val INFO_APP_KEY = "IpToHostname"
}
