package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.InfoRobots
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class InfoRobotsPersister extends PersisterNeo4JBase[InfoRobots] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.infoRobots")

  override def cast(avro: GenericRecord): Option[InfoRobots] = Some(avro.asInstanceOf[InfoRobots])

  override def buildRequest(avro: InfoRobots): Request = {
    var statements = Array(registerStatement(avro,cypherConfig,"upsert.query","upsert.fields"))
    
    Option(avro.getScName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(avro,cypherConfig,"scToRobot.query","scToRobot.fields")
    }    
    
    Request(statements)
  }
}
