package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.LogicalServerData
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class LogicalServerPersister extends PersisterNeo4JBase[LogicalServerData] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.logicalServer")

  override def buildRequest(ls:LogicalServerData) : Request = {

    var statements = Array(registerStatement(ls,cypherConfig,"upsert.query","upsert.fields"))

    Option(ls.getHyperName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(ls,cypherConfig,"hyper.query","hyper.fields")
    }

    Option(ls.getComputerSystemName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(ls,cypherConfig,"computerSystem.query","computerSystem.fields")
    }

    Request(statements)
  }

  override def cast(avro: GenericRecord): Option[LogicalServerData] = {
    Some(avro.asInstanceOf[LogicalServerData])
  }
}