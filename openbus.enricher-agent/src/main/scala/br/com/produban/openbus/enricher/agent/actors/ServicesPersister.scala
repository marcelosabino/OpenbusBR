package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.PersisterNeo4JBase
import br.com.produban.openbus.model.avro.enrichment.Services
import org.apache.avro.generic.GenericRecord
import br.com.produban.openbus.enricher.agent.base.Request
import com.typesafe.config.ConfigFactory

class ServicesPersister extends PersisterNeo4JBase[Services] {
  
  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.services")

  override def cast(avro: GenericRecord): Option[Services] = Some(avro.asInstanceOf[Services])

  override def buildRequest(srv: Services): Request = {
    var statements = Array(registerStatement(srv,cypherConfig,"businessService.query","businessService.fields"))
    
    Option(srv.getTsAssetid) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(srv,cypherConfig,"technicalService.query","technicalService.fields")  
    }
    
    Option(srv.getScAssetid) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(srv,cypherConfig,"serviceComponent.query","serviceComponent.fields")  
    }
    
    Option(srv.getTsAssetid) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(srv,cypherConfig,"bsToTs.query","bsToTs.fields")  
    }
    
    Option(srv.getScAssetid) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(srv,cypherConfig,"tsToSc.query","tsToSc.fields")  
    }
    
    Option(srv.getScName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(srv,cypherConfig,"scToLs.query","scToLs.fields")  
    }
    
    Option(srv.getScName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(srv,cypherConfig,"scToWs.query","scToLs.fields")  
    }

    Request(statements)    
  }  
  
}