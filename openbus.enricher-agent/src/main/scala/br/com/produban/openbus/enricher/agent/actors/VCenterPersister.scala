package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.VCenter
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class VCenterPersister extends PersisterNeo4JBase[VCenter] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.vCenter")

  override def cast(avro: GenericRecord): Option[VCenter] = Some(avro.asInstanceOf[VCenter])

  override def buildRequest(avro: VCenter): Request = {
    def extractFromFQDN(name:String,value:String) : String = {
      name match {
        case "hyper" | "hostname" => value.split("\\.")(0)
        case _ => value
      }
    }

    Request(Array(registerStatement(avro,cypherConfig,"update.query","update.fields",extractFromFQDN)))
  }
}
