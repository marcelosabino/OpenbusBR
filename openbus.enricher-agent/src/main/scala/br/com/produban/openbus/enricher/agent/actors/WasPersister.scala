package br.com.produban.openbus.enricher.agent.actors

import br.com.produban.openbus.enricher.agent.base.{PersisterNeo4JBase, Request}
import br.com.produban.openbus.model.avro.enrichment.CmdbWas
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord

class WasPersister extends PersisterNeo4JBase[CmdbWas] {

  val cypherConfig = ConfigFactory.load("cypher").getConfig("cypher.was")

  override def cast(avro: GenericRecord): Option[CmdbWas] = Some(avro.asInstanceOf[CmdbWas])

  override def buildRequest(was: CmdbWas): Request = {
    var statements = Array(registerStatement(was,cypherConfig,"upsert.query","upsert.fields"))

    Option(was.getLogicalServerName) filterNot(_.isEmpty) foreach { _ =>
      statements = statements :+ registerStatement(was,cypherConfig,"logicalServer.query","logicalServer.fields")
    }
    
    Request(statements)
  }
}
