package br.com.produban.openbus.enricher.agent.base

import akka.actor.{Actor, ActorLogging}
import br.com.produban.openbus.enricher.agent.kafka.AvroMessage
import com.sclasen.akka.kafka.StreamFSM
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.avro.generic.GenericRecord
import spray.json.{DefaultJsonProtocol, pimpAny, pimpString}

import scala.collection.JavaConversions._
import scalaj.http.Http

case class Statement(statement:String,parameters:Map[String,String])
case class Request(statements:Array[Statement])

case class Error(code:String,message:String)
case class Response(errors:Array[Error])

object Neo4JProtocol extends DefaultJsonProtocol {
  implicit val statementFormat = jsonFormat2(Statement)
  implicit val requestFormat = jsonFormat1(Request)
  implicit val errorFormat = jsonFormat2(Error)
  implicit val responseFormat = jsonFormat1(Response)
}

import br.com.produban.openbus.enricher.agent.base.Neo4JProtocol._

abstract class PersisterNeo4JBase[T <: GenericRecord] extends Actor with ActorLogging {

  val appConfig = ConfigFactory.load.getConfig("neo4j")

  override def receive = {
    case message : AvroMessage => {
      cast(message.avro.get.getRecord) foreach(execute)
      sender ! StreamFSM.Processed
    }
  }

  protected def registerStatement(avro : GenericRecord,
                                  config : Config,
                                  queryKey : String,
                                  fieldListKey: String,
                                  transformer : (String,String) => String = null) = {

    val query = config.getString(queryKey)
    val fields = config.getStringList(fieldListKey).toList

    val parameters = fields.zipWithIndex.map(_.swap).toMap.map {
      case (index, fieldName) => {
        val fieldValue = Option(avro.get(fieldName)).map(_.toString).getOrElse("")
        index.toString -> Option(transformer).map( f => f(fieldName,fieldValue) ).getOrElse(fieldValue)
      }
    }

    Statement(query,parameters)
  }

  private def execute(avro : T): Unit = {

    val requestBody = buildRequest(avro).toJson.compactPrint

    if (log isDebugEnabled)
      log debug("Request body: {}", requestBody)

    val httpResponse = Http(appConfig.getString("url"))
                      .auth(appConfig.getString("username"),appConfig.getString("password"))
                      .headers(Map(
                        "Content-Type" -> "application/json",
                        "Accept" -> "application/json; charset=UTF-8"
                      ))
                      .timeout(60000, 60000)
                      .postData(requestBody)
                      .asString

    if (log isDebugEnabled)
      log debug("Response body: {}", httpResponse.body)

    Option(httpResponse.body.parseJson.convertTo[Response].errors) filterNot(_.isEmpty) foreach {
      errors => log error("Fail to execute request: {}",errors.mkString)
    }
  }

  def buildRequest(avro : T) : Request
  def cast(avro : GenericRecord) : Option[T]
}
