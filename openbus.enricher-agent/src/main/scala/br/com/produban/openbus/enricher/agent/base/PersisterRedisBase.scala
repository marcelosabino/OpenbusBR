package br.com.produban.openbus.enricher.agent.base

import java.net.URI

import akka.actor.{Actor, ActorLogging}
import br.com.produban.openbus.enricher.agent.kafka.AvroMessage
import com.sclasen.akka.kafka.StreamFSM
import com.typesafe.config.ConfigFactory
import org.apache.avro.generic.GenericRecord
import redis.clients.jedis.Jedis

import scala.collection.JavaConversions._

abstract class PersisterRedisBase[T <: GenericRecord] extends Actor with ActorLogging {

  val appConfig = ConfigFactory.load.getConfig("redis")

  val redis = appConfig.getConfigList("tenants").map { config =>
    val password = appConfig.getString("password")
    val databaseIndex = config.getInt("databaseIndex")
    val uri = new URI(
      "redis",
      s"openbus:$password",
      appConfig.getString("host"),
      appConfig.getInt("port"),
      s"/$databaseIndex",
      null,
      null)

    (config.getString("id"), new Jedis(uri,appConfig.getInt("timeout")))
  }.toMap[String,Jedis]

  override def receive = {
    case message : AvroMessage => {
      cast(message.avro.get.getRecord) foreach(execute)
      sender ! StreamFSM.Processed
    }
  }

  def execute(avro : T): Unit
  def cast(avro : GenericRecord) : Option[T]

}
