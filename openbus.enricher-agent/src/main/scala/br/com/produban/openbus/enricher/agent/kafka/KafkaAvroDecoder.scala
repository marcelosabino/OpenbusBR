package br.com.produban.openbus.enricher.agent.kafka

import java.util.Properties

import br.com.produban.openbus.avro.AvroDecoder
import br.com.produban.openbus.avro.schemaregistry.{AvroTool, AvroLocalSchemaRegistry}
import com.typesafe.config.ConfigFactory
import kafka.serializer.Decoder
import org.apache.avro.generic.GenericRecord

case class AvroMessage(avro:Option[AvroTool])

class KafkaAvroDecoder extends Decoder[AvroMessage] {
  val decoder : AvroDecoder = new AvroDecoder
  override def fromBytes(bytes: Array[Byte]): AvroMessage = AvroMessage(Option(decoder.toRecord(bytes)))
}
