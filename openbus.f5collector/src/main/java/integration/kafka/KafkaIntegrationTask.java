package integration.kafka;

import br.com.openbus.publisher.Publisher;
import br.com.openbus.publisher.kafka.KafkaAvroPublisher;
import br.com.produban.openbus.model.avro.BigIp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class KafkaIntegrationTask implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaIntegrationTask.class);

    private static final ThreadLocal<Publisher<BigIp>> publisherThreadLocal = new ThreadLocal<>();

    private String kafkaBrokerList;
    private String topicName;
    private KafkaMessage message;

    public KafkaIntegrationTask(String kafkaBrokerList, String topicName) {
        this.kafkaBrokerList = kafkaBrokerList;
        this.topicName = topicName;
    }

    public void setMessage(KafkaMessage message) {
        this.message = message;
    }

    private Publisher<BigIp> getPublisher() {
        Publisher<BigIp> publisher = publisherThreadLocal.get();

        if (publisher == null) {
            publisher = new KafkaAvroPublisher<BigIp>(kafkaBrokerList,false,false,0);
            publisherThreadLocal.set(publisher);
        }

        return publisher;
    }

    @Override
    public void run() {
        if (LOGGER.isInfoEnabled())
            LOGGER.info("Running kafka integration task for [BigIP: {} | Pool: {}]",
                    message.getVirtualServer().getBalancerIp(),
                    message.getPool().getName());

        try {
            List<BigIp> avroList = message.toAvroList();
            if (LOGGER.isInfoEnabled())
                LOGGER.info("Sending {} messages to Kafka [BigIP: {} | Pool: {}]",
                        avroList.size(),
                        message.getVirtualServer().getBalancerIp(),
                        message.getPool().getName());

            if (LOGGER.isDebugEnabled())
                LOGGER.debug("Avro list: {}",avroList.toString());

            getPublisher().publishAll(message.getTool(), avroList, topicName);
        } catch (Exception e) {
            LOGGER.error("Fail to send collected data to Kafka",e);
            return;
        }

        if (LOGGER.isInfoEnabled())
            LOGGER.info("Running kafka integration task done!");
    }
}
