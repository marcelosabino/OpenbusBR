package br.com.produban.openbus.introscope.sender;

import java.util.List;

import br.com.produban.openbus.introscope.util.properties.ResourceUtils;
import br.com.produban.openbus.introscope.util.queue.Queues;
import br.com.produban.openbus.model.avro.Introscope;

public abstract class ISender {

	protected Queues queues;
	protected ResourceUtils resourceUtils;
	
	public ISender(Queues queues, ResourceUtils resourceUtils) {
		this.queues = queues;
		this.resourceUtils = resourceUtils;
	}
	
	protected abstract void send(List<Introscope> Introscopes);
	
	protected abstract void close();
}
