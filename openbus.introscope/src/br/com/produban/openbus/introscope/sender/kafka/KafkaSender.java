package br.com.produban.openbus.introscope.sender.kafka;

import br.com.openbus.publisher.Publisher;
import br.com.openbus.publisher.kafka.KafkaAvroPublisher;
import br.com.produban.openbus.introscope.sender.ISender;
import br.com.produban.openbus.introscope.util.properties.ResourceUtils;
import br.com.produban.openbus.introscope.util.queue.Queues;
import br.com.produban.openbus.model.avro.Introscope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class KafkaSender extends ISender {

	private Logger LOG = LoggerFactory.getLogger(this.getClass());

	private Publisher<Introscope> publisher;

	private final String brokerList;
	private final boolean requiredAcks;
	private final String topicName;

	public KafkaSender(Queues queues, ResourceUtils resourceUtils) {
		super(queues, resourceUtils);

		brokerList = resourceUtils.getProperty(ResourceUtils.Constants.KAFKA_BROKER_LIST);
		requiredAcks = Boolean.valueOf(resourceUtils.getProperty(ResourceUtils.Constants.KAFKA_REQUIRED_ACKS));
		topicName = resourceUtils.getProperty(ResourceUtils.Constants.KAFKA_TOPIC_NAME);

		publisher = new KafkaAvroPublisher<Introscope>(brokerList,requiredAcks,false,0);
	}

	public void send(List<Introscope> introscopeList) {
		try {
			if (introscopeList.size() > 0)
				LOG.debug(Thread.currentThread().getName() + " processing " + introscopeList.size() + " messages.");
			publisher.publishAll(introscopeList, topicName);
		} catch (Exception e) {
			LOG.error("Error in kafka publisher.", e);
		}
	}

	@Override
	protected void close() {
		publisher.closePublisher();
	}

}
