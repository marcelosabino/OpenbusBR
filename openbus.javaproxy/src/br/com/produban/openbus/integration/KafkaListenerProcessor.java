package br.com.produban.openbus.integration;

import br.com.openbus.publisher.Publisher;
import br.com.openbus.publisher.kafka.KafkaAvroPublisher;
import br.com.produban.openbus.model.PreRequest;
import br.com.produban.openbus.model.Request;
import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.utils.MessageTransformer;
import br.com.produban.openbus.utils.ZabbixAgentDataValidator;
import com.github.terma.javaniotcpproxy.TcpProxyRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.LinkedList;

public class KafkaListenerProcessor implements ListenerProcessor {

	private final static Logger LOGGER = LoggerFactory.getLogger(KafkaListenerProcessor.class);
	
	private final Publisher<ZabbixAgentData> publisher;
	
	private final IntegrationQueue<ByteBuffer> kafkaIntegrationQueue;
    private final IntegrationQueue<ZabbixAgentData> zabbixIntegrationQueue;
    private final String topicName;
	private final String resolvedTool;

	public KafkaListenerProcessor(IntegrationQueue<ByteBuffer> kafkaIntegrationQueue,
                                  IntegrationQueue<ZabbixAgentData> zabbixIntegrationQueue,
                                  String topicName) {
		this.kafkaIntegrationQueue = kafkaIntegrationQueue;
        this.zabbixIntegrationQueue = zabbixIntegrationQueue;
        this.topicName = topicName;
		publisher = new KafkaAvroPublisher<ZabbixAgentData>(TcpProxyRunner.getPublisherConfig().getIntegrationBrokerList()
				, false, true, 300, new ZabbixAgentDataValidator());
		this.resolvedTool = TcpProxyRunner.getPublisherConfig().getToolName();
	}

    @Override
	public void compute() {
		LinkedList<ByteBuffer> list = kafkaIntegrationQueue.getAll(100);
		for (ByteBuffer byteBuffer : list) {

			PreRequest preRequest;
			preRequest = MessageTransformer.parseByteBuffer(byteBuffer);

			if (preRequest == null) {
				continue;
			}
			
			LOGGER.debug("Final Message:  '" + preRequest.getRequest() + "'");
			Request request = MessageTransformer.parseStringToRequest(preRequest.getRequest());
			
			if (request != null) {
				if (request.getRequest().equals("agent data")) {
                    for (ZabbixAgentData data : request.getData()) {
                        if (data.getValue().contains("{#")) {
                            if (LOGGER.isDebugEnabled())
                                LOGGER.debug("This data is going to Zabbix: " + data);
                            zabbixIntegrationQueue.insert(data);
                        } else {
                            if (LOGGER.isDebugEnabled())
                                LOGGER.debug("This data is going to Kafka: " + data);
							publisher.publish(resolvedTool, data, topicName);
                        }
                    }
				} else if (request.getRequest().equals("sender data")) {
					for (ZabbixAgentData agentData : request.getData()) {
						if (agentData.getClock() == null || agentData.getNs() == null) {
							String clock = String.valueOf(System.currentTimeMillis());
							String ns = "000000";
							agentData.setClock(clock);
							agentData.setNs(ns);
						}
						publisher.publish(resolvedTool, agentData, topicName);
					}
				}
			}
		}
	}
}