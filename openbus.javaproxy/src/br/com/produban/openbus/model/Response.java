package br.com.produban.openbus.model;

import br.com.produban.openbus.model.avro.ZabbixAgentData;

public class Response {

	private String response;
	private ZabbixAgentData data;
	private String info;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public ZabbixAgentData getData() {
		return data;
	}

	public void setData(ZabbixAgentData data) {
		this.data = data;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
