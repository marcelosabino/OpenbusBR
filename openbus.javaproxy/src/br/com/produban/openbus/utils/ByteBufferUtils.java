package br.com.produban.openbus.utils;

import kafka.message.ByteBufferBackedInputStream;

import java.io.*;
import java.nio.ByteBuffer;

public class ByteBufferUtils {

    public static String parseContent(ByteBuffer buffer, int offset, int length) throws IOException {
        InputStream inputStream = new ByteBufferBackedInputStream(buffer);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "ISO-8859-1");
        BufferedReader bufferedReader = null;
        char[] start = new char[length];
        try {
            bufferedReader = new BufferedReader(inputStreamReader);
            bufferedReader.read(start, offset, length);
            return String.valueOf(start);
        } finally {
            buffer.rewind();
            bufferedReader.close();
        }
    }

}
