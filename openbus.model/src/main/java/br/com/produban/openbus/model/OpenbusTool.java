package br.com.produban.openbus.model;

import br.com.produban.openbus.model.avro.*;
import br.com.produban.openbus.model.pojo.*;
import org.apache.avro.generic.GenericRecord;

public enum OpenbusTool {

    FIREWALL(Firewall.getClassSchema().getName(), FirewallBean.class, ToolType.LOGS),
    RADIUS(Radius.getClassSchema().getName(), RadiusBean.class, ToolType.LOGS),
    EVENTLOG(Eventlog.getClassSchema().getName(), EventlogBean.class, ToolType.LOGS),
    ACTIVEDIRECTORY(ActiveDirectory.getClassSchema().getName(), ActiveDirectoryBean.class, ToolType.LOGS),
    FIREEYE(FireEye.getClassSchema().getName(), FireEyeBean.class, ToolType.LOGS),
    IPS(br.com.produban.openbus.model.avro.IPS.getClassSchema().getName(), IPSBean.class, ToolType.LOGS),
    PROXY(Proxy.getClassSchema().getName(), ProxyBean.class, ToolType.LOGS),
    SNMPTRAP(SNMPTrap.getClassSchema().getName(), SNMPTrapBean.class, ToolType.LOGS),
    SYSLOG5424(Syslog5424.getClassSchema().getName(), Syslog5424Bean.class, ToolType.LOGS),
    IDM(br.com.produban.openbus.model.avro.IDM.getClassSchema().getName(), IDMBean.class, ToolType.LOGS),
    TACACS(Tacacs.getClassSchema().getName(), TacacsBean.class, ToolType.LOGS),
    ZABBIX(ZabbixAgentData.getClassSchema().getName(), ZabbixAgentDataBean.class, ToolType.METRICS),
    ROBOT(Robot.getClassSchema().getName(), RobotBean.class, ToolType.METRICS),
    BIGIP_POOL_MEMBERS(BigIp.getClassSchema().getName(), BigIpBean.class, ToolType.METRICS),
    INTROSCOPE(Introscope.getClassSchema().getName(), IntroscopeBean.class, ToolType.METRICS),
    ORACLE(Oracle.getClassSchema().getName(), OracleBean.class, ToolType.METRICS),
    SQLSERVER(SQLServer.getClassSchema().getName(), SQLServerBean.class, ToolType.METRICS),
    WEBSPHERE(WebSphere.getClassSchema().getName(), WebsphereBean.class, ToolType.METRICS),
    COMM_ELEMENT(CommElement.getClassSchema().getName(), CommElementBean.class, ToolType.METRICS),
    WEBSPHERE_MQ(WebsphereMQ.getClassSchema().getName(), WebsphereMQBean.class, ToolType.METRICS),
    DB2(br.com.produban.openbus.model.avro.DB2.getClassSchema().getName(), DB2Bean.class, ToolType.METRICS),
    STORAGE(br.com.produban.openbus.model.avro.Storage.getClassSchema().getName(), StorageBean.class, ToolType.METRICS);

    public static OpenbusTool resolveTool(GenericRecord avro) {
        for (OpenbusTool tool : values()) {
            if (tool.getAvroSchemaName().equals(avro.getSchema().getName()))
                return tool;
        }

        return null;
    }

    public enum ToolType {
        METRICS,LOGS;
    }

    private String avroSchemaName;
    private Class<? extends DataBean> dataBeanClass;
    private ToolType toolType;

    OpenbusTool(String avroSchemaName, Class<? extends DataBean> dataBeanClass, ToolType toolType) {
        this.avroSchemaName = avroSchemaName;
        this.dataBeanClass = dataBeanClass;
        this.toolType = toolType;
    }

    public String getAvroSchemaName() {
        return avroSchemaName;
    }

    public ToolType getToolType() {
        return toolType;
    }

    public Class<? extends DataBean> getDataBeanClass() {
        return dataBeanClass;
    }
}
