/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package br.com.produban.openbus.model.avro;  
@SuppressWarnings("all")
/** Communication element message based on SNMP Generic Model. */
@org.apache.avro.specific.AvroGenerated
public class CommElement extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"CommElement\",\"namespace\":\"br.com.produban.openbus.model.avro\",\"doc\":\"Communication element message based on SNMP Generic Model.\",\"fields\":[{\"name\":\"info\",\"type\":{\"type\":\"map\",\"values\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"avro.java.string\":\"String\"}},{\"name\":\"objectId\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"propertyName\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"propertyValue\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"timestamp\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.util.Map<java.lang.String,java.lang.String> info;
  @Deprecated public java.lang.String objectId;
  @Deprecated public java.lang.String propertyName;
  @Deprecated public java.lang.String propertyValue;
  @Deprecated public java.lang.String timestamp;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>. 
   */
  public CommElement() {}

  /**
   * All-args constructor.
   */
  public CommElement(java.util.Map<java.lang.String,java.lang.String> info, java.lang.String objectId, java.lang.String propertyName, java.lang.String propertyValue, java.lang.String timestamp) {
    this.info = info;
    this.objectId = objectId;
    this.propertyName = propertyName;
    this.propertyValue = propertyValue;
    this.timestamp = timestamp;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return info;
    case 1: return objectId;
    case 2: return propertyName;
    case 3: return propertyValue;
    case 4: return timestamp;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: info = (java.util.Map<java.lang.String,java.lang.String>)value$; break;
    case 1: objectId = (java.lang.String)value$; break;
    case 2: propertyName = (java.lang.String)value$; break;
    case 3: propertyValue = (java.lang.String)value$; break;
    case 4: timestamp = (java.lang.String)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'info' field.
   */
  public java.util.Map<java.lang.String,java.lang.String> getInfo() {
    return info;
  }

  /**
   * Sets the value of the 'info' field.
   * @param value the value to set.
   */
  public void setInfo(java.util.Map<java.lang.String,java.lang.String> value) {
    this.info = value;
  }

  /**
   * Gets the value of the 'objectId' field.
   */
  public java.lang.String getObjectId() {
    return objectId;
  }

  /**
   * Sets the value of the 'objectId' field.
   * @param value the value to set.
   */
  public void setObjectId(java.lang.String value) {
    this.objectId = value;
  }

  /**
   * Gets the value of the 'propertyName' field.
   */
  public java.lang.String getPropertyName() {
    return propertyName;
  }

  /**
   * Sets the value of the 'propertyName' field.
   * @param value the value to set.
   */
  public void setPropertyName(java.lang.String value) {
    this.propertyName = value;
  }

  /**
   * Gets the value of the 'propertyValue' field.
   */
  public java.lang.String getPropertyValue() {
    return propertyValue;
  }

  /**
   * Sets the value of the 'propertyValue' field.
   * @param value the value to set.
   */
  public void setPropertyValue(java.lang.String value) {
    this.propertyValue = value;
  }

  /**
   * Gets the value of the 'timestamp' field.
   */
  public java.lang.String getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the value of the 'timestamp' field.
   * @param value the value to set.
   */
  public void setTimestamp(java.lang.String value) {
    this.timestamp = value;
  }

  /** Creates a new CommElement RecordBuilder */
  public static br.com.produban.openbus.model.avro.CommElement.Builder newBuilder() {
    return new br.com.produban.openbus.model.avro.CommElement.Builder();
  }
  
  /** Creates a new CommElement RecordBuilder by copying an existing Builder */
  public static br.com.produban.openbus.model.avro.CommElement.Builder newBuilder(br.com.produban.openbus.model.avro.CommElement.Builder other) {
    return new br.com.produban.openbus.model.avro.CommElement.Builder(other);
  }
  
  /** Creates a new CommElement RecordBuilder by copying an existing CommElement instance */
  public static br.com.produban.openbus.model.avro.CommElement.Builder newBuilder(br.com.produban.openbus.model.avro.CommElement other) {
    return new br.com.produban.openbus.model.avro.CommElement.Builder(other);
  }
  
  /**
   * RecordBuilder for CommElement instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<CommElement>
    implements org.apache.avro.data.RecordBuilder<CommElement> {

    private java.util.Map<java.lang.String,java.lang.String> info;
    private java.lang.String objectId;
    private java.lang.String propertyName;
    private java.lang.String propertyValue;
    private java.lang.String timestamp;

    /** Creates a new Builder */
    private Builder() {
      super(br.com.produban.openbus.model.avro.CommElement.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(br.com.produban.openbus.model.avro.CommElement.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.info)) {
        this.info = data().deepCopy(fields()[0].schema(), other.info);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.objectId)) {
        this.objectId = data().deepCopy(fields()[1].schema(), other.objectId);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.propertyName)) {
        this.propertyName = data().deepCopy(fields()[2].schema(), other.propertyName);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.propertyValue)) {
        this.propertyValue = data().deepCopy(fields()[3].schema(), other.propertyValue);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[4].schema(), other.timestamp);
        fieldSetFlags()[4] = true;
      }
    }
    
    /** Creates a Builder by copying an existing CommElement instance */
    private Builder(br.com.produban.openbus.model.avro.CommElement other) {
            super(br.com.produban.openbus.model.avro.CommElement.SCHEMA$);
      if (isValidValue(fields()[0], other.info)) {
        this.info = data().deepCopy(fields()[0].schema(), other.info);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.objectId)) {
        this.objectId = data().deepCopy(fields()[1].schema(), other.objectId);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.propertyName)) {
        this.propertyName = data().deepCopy(fields()[2].schema(), other.propertyName);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.propertyValue)) {
        this.propertyValue = data().deepCopy(fields()[3].schema(), other.propertyValue);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[4].schema(), other.timestamp);
        fieldSetFlags()[4] = true;
      }
    }

    /** Gets the value of the 'info' field */
    public java.util.Map<java.lang.String,java.lang.String> getInfo() {
      return info;
    }
    
    /** Sets the value of the 'info' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder setInfo(java.util.Map<java.lang.String,java.lang.String> value) {
      validate(fields()[0], value);
      this.info = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'info' field has been set */
    public boolean hasInfo() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'info' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder clearInfo() {
      info = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'objectId' field */
    public java.lang.String getObjectId() {
      return objectId;
    }
    
    /** Sets the value of the 'objectId' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder setObjectId(java.lang.String value) {
      validate(fields()[1], value);
      this.objectId = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'objectId' field has been set */
    public boolean hasObjectId() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'objectId' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder clearObjectId() {
      objectId = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /** Gets the value of the 'propertyName' field */
    public java.lang.String getPropertyName() {
      return propertyName;
    }
    
    /** Sets the value of the 'propertyName' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder setPropertyName(java.lang.String value) {
      validate(fields()[2], value);
      this.propertyName = value;
      fieldSetFlags()[2] = true;
      return this; 
    }
    
    /** Checks whether the 'propertyName' field has been set */
    public boolean hasPropertyName() {
      return fieldSetFlags()[2];
    }
    
    /** Clears the value of the 'propertyName' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder clearPropertyName() {
      propertyName = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /** Gets the value of the 'propertyValue' field */
    public java.lang.String getPropertyValue() {
      return propertyValue;
    }
    
    /** Sets the value of the 'propertyValue' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder setPropertyValue(java.lang.String value) {
      validate(fields()[3], value);
      this.propertyValue = value;
      fieldSetFlags()[3] = true;
      return this; 
    }
    
    /** Checks whether the 'propertyValue' field has been set */
    public boolean hasPropertyValue() {
      return fieldSetFlags()[3];
    }
    
    /** Clears the value of the 'propertyValue' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder clearPropertyValue() {
      propertyValue = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /** Gets the value of the 'timestamp' field */
    public java.lang.String getTimestamp() {
      return timestamp;
    }
    
    /** Sets the value of the 'timestamp' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder setTimestamp(java.lang.String value) {
      validate(fields()[4], value);
      this.timestamp = value;
      fieldSetFlags()[4] = true;
      return this; 
    }
    
    /** Checks whether the 'timestamp' field has been set */
    public boolean hasTimestamp() {
      return fieldSetFlags()[4];
    }
    
    /** Clears the value of the 'timestamp' field */
    public br.com.produban.openbus.model.avro.CommElement.Builder clearTimestamp() {
      timestamp = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    @Override
    public CommElement build() {
      try {
        CommElement record = new CommElement();
        record.info = fieldSetFlags()[0] ? this.info : (java.util.Map<java.lang.String,java.lang.String>) defaultValue(fields()[0]);
        record.objectId = fieldSetFlags()[1] ? this.objectId : (java.lang.String) defaultValue(fields()[1]);
        record.propertyName = fieldSetFlags()[2] ? this.propertyName : (java.lang.String) defaultValue(fields()[2]);
        record.propertyValue = fieldSetFlags()[3] ? this.propertyValue : (java.lang.String) defaultValue(fields()[3]);
        record.timestamp = fieldSetFlags()[4] ? this.timestamp : (java.lang.String) defaultValue(fields()[4]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
