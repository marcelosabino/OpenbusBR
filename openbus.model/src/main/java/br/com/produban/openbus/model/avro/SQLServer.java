/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package br.com.produban.openbus.model.avro;  
@SuppressWarnings("all")
/** MS SQL Server schema. */
@org.apache.avro.specific.AvroGenerated
public class SQLServer extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"SQLServer\",\"namespace\":\"br.com.produban.openbus.model.avro\",\"doc\":\"MS SQL Server schema.\",\"fields\":[{\"name\":\"instanceId\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"dbName\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"timestamp\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"value\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"metricUnit\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"metricName\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.lang.String instanceId;
  @Deprecated public java.lang.String dbName;
  @Deprecated public java.lang.String timestamp;
  @Deprecated public java.lang.String value;
  @Deprecated public java.lang.String metricUnit;
  @Deprecated public java.lang.String metricName;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>. 
   */
  public SQLServer() {}

  /**
   * All-args constructor.
   */
  public SQLServer(java.lang.String instanceId, java.lang.String dbName, java.lang.String timestamp, java.lang.String value, java.lang.String metricUnit, java.lang.String metricName) {
    this.instanceId = instanceId;
    this.dbName = dbName;
    this.timestamp = timestamp;
    this.value = value;
    this.metricUnit = metricUnit;
    this.metricName = metricName;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return instanceId;
    case 1: return dbName;
    case 2: return timestamp;
    case 3: return value;
    case 4: return metricUnit;
    case 5: return metricName;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: instanceId = (java.lang.String)value$; break;
    case 1: dbName = (java.lang.String)value$; break;
    case 2: timestamp = (java.lang.String)value$; break;
    case 3: value = (java.lang.String)value$; break;
    case 4: metricUnit = (java.lang.String)value$; break;
    case 5: metricName = (java.lang.String)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'instanceId' field.
   */
  public java.lang.String getInstanceId() {
    return instanceId;
  }

  /**
   * Sets the value of the 'instanceId' field.
   * @param value the value to set.
   */
  public void setInstanceId(java.lang.String value) {
    this.instanceId = value;
  }

  /**
   * Gets the value of the 'dbName' field.
   */
  public java.lang.String getDbName() {
    return dbName;
  }

  /**
   * Sets the value of the 'dbName' field.
   * @param value the value to set.
   */
  public void setDbName(java.lang.String value) {
    this.dbName = value;
  }

  /**
   * Gets the value of the 'timestamp' field.
   */
  public java.lang.String getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the value of the 'timestamp' field.
   * @param value the value to set.
   */
  public void setTimestamp(java.lang.String value) {
    this.timestamp = value;
  }

  /**
   * Gets the value of the 'value' field.
   */
  public java.lang.String getValue() {
    return value;
  }

  /**
   * Sets the value of the 'value' field.
   * @param value the value to set.
   */
  public void setValue(java.lang.String value) {
    this.value = value;
  }

  /**
   * Gets the value of the 'metricUnit' field.
   */
  public java.lang.String getMetricUnit() {
    return metricUnit;
  }

  /**
   * Sets the value of the 'metricUnit' field.
   * @param value the value to set.
   */
  public void setMetricUnit(java.lang.String value) {
    this.metricUnit = value;
  }

  /**
   * Gets the value of the 'metricName' field.
   */
  public java.lang.String getMetricName() {
    return metricName;
  }

  /**
   * Sets the value of the 'metricName' field.
   * @param value the value to set.
   */
  public void setMetricName(java.lang.String value) {
    this.metricName = value;
  }

  /** Creates a new SQLServer RecordBuilder */
  public static br.com.produban.openbus.model.avro.SQLServer.Builder newBuilder() {
    return new br.com.produban.openbus.model.avro.SQLServer.Builder();
  }
  
  /** Creates a new SQLServer RecordBuilder by copying an existing Builder */
  public static br.com.produban.openbus.model.avro.SQLServer.Builder newBuilder(br.com.produban.openbus.model.avro.SQLServer.Builder other) {
    return new br.com.produban.openbus.model.avro.SQLServer.Builder(other);
  }
  
  /** Creates a new SQLServer RecordBuilder by copying an existing SQLServer instance */
  public static br.com.produban.openbus.model.avro.SQLServer.Builder newBuilder(br.com.produban.openbus.model.avro.SQLServer other) {
    return new br.com.produban.openbus.model.avro.SQLServer.Builder(other);
  }
  
  /**
   * RecordBuilder for SQLServer instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<SQLServer>
    implements org.apache.avro.data.RecordBuilder<SQLServer> {

    private java.lang.String instanceId;
    private java.lang.String dbName;
    private java.lang.String timestamp;
    private java.lang.String value;
    private java.lang.String metricUnit;
    private java.lang.String metricName;

    /** Creates a new Builder */
    private Builder() {
      super(br.com.produban.openbus.model.avro.SQLServer.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(br.com.produban.openbus.model.avro.SQLServer.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.instanceId)) {
        this.instanceId = data().deepCopy(fields()[0].schema(), other.instanceId);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.dbName)) {
        this.dbName = data().deepCopy(fields()[1].schema(), other.dbName);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[2].schema(), other.timestamp);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.value)) {
        this.value = data().deepCopy(fields()[3].schema(), other.value);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.metricUnit)) {
        this.metricUnit = data().deepCopy(fields()[4].schema(), other.metricUnit);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.metricName)) {
        this.metricName = data().deepCopy(fields()[5].schema(), other.metricName);
        fieldSetFlags()[5] = true;
      }
    }
    
    /** Creates a Builder by copying an existing SQLServer instance */
    private Builder(br.com.produban.openbus.model.avro.SQLServer other) {
            super(br.com.produban.openbus.model.avro.SQLServer.SCHEMA$);
      if (isValidValue(fields()[0], other.instanceId)) {
        this.instanceId = data().deepCopy(fields()[0].schema(), other.instanceId);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.dbName)) {
        this.dbName = data().deepCopy(fields()[1].schema(), other.dbName);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[2].schema(), other.timestamp);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.value)) {
        this.value = data().deepCopy(fields()[3].schema(), other.value);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.metricUnit)) {
        this.metricUnit = data().deepCopy(fields()[4].schema(), other.metricUnit);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.metricName)) {
        this.metricName = data().deepCopy(fields()[5].schema(), other.metricName);
        fieldSetFlags()[5] = true;
      }
    }

    /** Gets the value of the 'instanceId' field */
    public java.lang.String getInstanceId() {
      return instanceId;
    }
    
    /** Sets the value of the 'instanceId' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder setInstanceId(java.lang.String value) {
      validate(fields()[0], value);
      this.instanceId = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'instanceId' field has been set */
    public boolean hasInstanceId() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'instanceId' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder clearInstanceId() {
      instanceId = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'dbName' field */
    public java.lang.String getDbName() {
      return dbName;
    }
    
    /** Sets the value of the 'dbName' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder setDbName(java.lang.String value) {
      validate(fields()[1], value);
      this.dbName = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'dbName' field has been set */
    public boolean hasDbName() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'dbName' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder clearDbName() {
      dbName = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /** Gets the value of the 'timestamp' field */
    public java.lang.String getTimestamp() {
      return timestamp;
    }
    
    /** Sets the value of the 'timestamp' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder setTimestamp(java.lang.String value) {
      validate(fields()[2], value);
      this.timestamp = value;
      fieldSetFlags()[2] = true;
      return this; 
    }
    
    /** Checks whether the 'timestamp' field has been set */
    public boolean hasTimestamp() {
      return fieldSetFlags()[2];
    }
    
    /** Clears the value of the 'timestamp' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder clearTimestamp() {
      timestamp = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /** Gets the value of the 'value' field */
    public java.lang.String getValue() {
      return value;
    }
    
    /** Sets the value of the 'value' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder setValue(java.lang.String value) {
      validate(fields()[3], value);
      this.value = value;
      fieldSetFlags()[3] = true;
      return this; 
    }
    
    /** Checks whether the 'value' field has been set */
    public boolean hasValue() {
      return fieldSetFlags()[3];
    }
    
    /** Clears the value of the 'value' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder clearValue() {
      value = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /** Gets the value of the 'metricUnit' field */
    public java.lang.String getMetricUnit() {
      return metricUnit;
    }
    
    /** Sets the value of the 'metricUnit' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder setMetricUnit(java.lang.String value) {
      validate(fields()[4], value);
      this.metricUnit = value;
      fieldSetFlags()[4] = true;
      return this; 
    }
    
    /** Checks whether the 'metricUnit' field has been set */
    public boolean hasMetricUnit() {
      return fieldSetFlags()[4];
    }
    
    /** Clears the value of the 'metricUnit' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder clearMetricUnit() {
      metricUnit = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /** Gets the value of the 'metricName' field */
    public java.lang.String getMetricName() {
      return metricName;
    }
    
    /** Sets the value of the 'metricName' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder setMetricName(java.lang.String value) {
      validate(fields()[5], value);
      this.metricName = value;
      fieldSetFlags()[5] = true;
      return this; 
    }
    
    /** Checks whether the 'metricName' field has been set */
    public boolean hasMetricName() {
      return fieldSetFlags()[5];
    }
    
    /** Clears the value of the 'metricName' field */
    public br.com.produban.openbus.model.avro.SQLServer.Builder clearMetricName() {
      metricName = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    @Override
    public SQLServer build() {
      try {
        SQLServer record = new SQLServer();
        record.instanceId = fieldSetFlags()[0] ? this.instanceId : (java.lang.String) defaultValue(fields()[0]);
        record.dbName = fieldSetFlags()[1] ? this.dbName : (java.lang.String) defaultValue(fields()[1]);
        record.timestamp = fieldSetFlags()[2] ? this.timestamp : (java.lang.String) defaultValue(fields()[2]);
        record.value = fieldSetFlags()[3] ? this.value : (java.lang.String) defaultValue(fields()[3]);
        record.metricUnit = fieldSetFlags()[4] ? this.metricUnit : (java.lang.String) defaultValue(fields()[4]);
        record.metricName = fieldSetFlags()[5] ? this.metricName : (java.lang.String) defaultValue(fields()[5]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
