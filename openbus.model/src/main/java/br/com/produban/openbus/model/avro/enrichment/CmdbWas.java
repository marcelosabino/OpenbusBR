/**
 * Autogenerated by Avro
 * 
 * DO NOT EDIT DIRECTLY
 */
package br.com.produban.openbus.model.avro.enrichment;  
@SuppressWarnings("all")
/** Information for cmdb was enrichment. */
@org.apache.avro.specific.AvroGenerated
public class CmdbWas extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"CmdbWas\",\"namespace\":\"br.com.produban.openbus.model.avro.enrichment\",\"doc\":\"Information for cmdb was enrichment.\",\"fields\":[{\"name\":\"tenant\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"timestamp\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasName\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasCompany\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasDelivery\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasEnvironment\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasStatus\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasItem\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasVDC\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasDistributionNetwork\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasManufacturer\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasProductName\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasModelVersion\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasServiceComponent\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasTechnicalService\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"wasBusinessService\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"logicalServerName\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }
  @Deprecated public java.lang.String tenant;
  @Deprecated public java.lang.String timestamp;
  @Deprecated public java.lang.String wasName;
  @Deprecated public java.lang.String wasCompany;
  @Deprecated public java.lang.String wasDelivery;
  @Deprecated public java.lang.String wasEnvironment;
  @Deprecated public java.lang.String wasStatus;
  @Deprecated public java.lang.String wasItem;
  @Deprecated public java.lang.String wasVDC;
  @Deprecated public java.lang.String wasDistributionNetwork;
  @Deprecated public java.lang.String wasManufacturer;
  @Deprecated public java.lang.String wasProductName;
  @Deprecated public java.lang.String wasModelVersion;
  @Deprecated public java.lang.String wasServiceComponent;
  @Deprecated public java.lang.String wasTechnicalService;
  @Deprecated public java.lang.String wasBusinessService;
  @Deprecated public java.lang.String logicalServerName;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>. 
   */
  public CmdbWas() {}

  /**
   * All-args constructor.
   */
  public CmdbWas(java.lang.String tenant, java.lang.String timestamp, java.lang.String wasName, java.lang.String wasCompany, java.lang.String wasDelivery, java.lang.String wasEnvironment, java.lang.String wasStatus, java.lang.String wasItem, java.lang.String wasVDC, java.lang.String wasDistributionNetwork, java.lang.String wasManufacturer, java.lang.String wasProductName, java.lang.String wasModelVersion, java.lang.String wasServiceComponent, java.lang.String wasTechnicalService, java.lang.String wasBusinessService, java.lang.String logicalServerName) {
    this.tenant = tenant;
    this.timestamp = timestamp;
    this.wasName = wasName;
    this.wasCompany = wasCompany;
    this.wasDelivery = wasDelivery;
    this.wasEnvironment = wasEnvironment;
    this.wasStatus = wasStatus;
    this.wasItem = wasItem;
    this.wasVDC = wasVDC;
    this.wasDistributionNetwork = wasDistributionNetwork;
    this.wasManufacturer = wasManufacturer;
    this.wasProductName = wasProductName;
    this.wasModelVersion = wasModelVersion;
    this.wasServiceComponent = wasServiceComponent;
    this.wasTechnicalService = wasTechnicalService;
    this.wasBusinessService = wasBusinessService;
    this.logicalServerName = logicalServerName;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call. 
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return tenant;
    case 1: return timestamp;
    case 2: return wasName;
    case 3: return wasCompany;
    case 4: return wasDelivery;
    case 5: return wasEnvironment;
    case 6: return wasStatus;
    case 7: return wasItem;
    case 8: return wasVDC;
    case 9: return wasDistributionNetwork;
    case 10: return wasManufacturer;
    case 11: return wasProductName;
    case 12: return wasModelVersion;
    case 13: return wasServiceComponent;
    case 14: return wasTechnicalService;
    case 15: return wasBusinessService;
    case 16: return logicalServerName;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }
  // Used by DatumReader.  Applications should not call. 
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: tenant = (java.lang.String)value$; break;
    case 1: timestamp = (java.lang.String)value$; break;
    case 2: wasName = (java.lang.String)value$; break;
    case 3: wasCompany = (java.lang.String)value$; break;
    case 4: wasDelivery = (java.lang.String)value$; break;
    case 5: wasEnvironment = (java.lang.String)value$; break;
    case 6: wasStatus = (java.lang.String)value$; break;
    case 7: wasItem = (java.lang.String)value$; break;
    case 8: wasVDC = (java.lang.String)value$; break;
    case 9: wasDistributionNetwork = (java.lang.String)value$; break;
    case 10: wasManufacturer = (java.lang.String)value$; break;
    case 11: wasProductName = (java.lang.String)value$; break;
    case 12: wasModelVersion = (java.lang.String)value$; break;
    case 13: wasServiceComponent = (java.lang.String)value$; break;
    case 14: wasTechnicalService = (java.lang.String)value$; break;
    case 15: wasBusinessService = (java.lang.String)value$; break;
    case 16: logicalServerName = (java.lang.String)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'tenant' field.
   */
  public java.lang.String getTenant() {
    return tenant;
  }

  /**
   * Sets the value of the 'tenant' field.
   * @param value the value to set.
   */
  public void setTenant(java.lang.String value) {
    this.tenant = value;
  }

  /**
   * Gets the value of the 'timestamp' field.
   */
  public java.lang.String getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the value of the 'timestamp' field.
   * @param value the value to set.
   */
  public void setTimestamp(java.lang.String value) {
    this.timestamp = value;
  }

  /**
   * Gets the value of the 'wasName' field.
   */
  public java.lang.String getWasName() {
    return wasName;
  }

  /**
   * Sets the value of the 'wasName' field.
   * @param value the value to set.
   */
  public void setWasName(java.lang.String value) {
    this.wasName = value;
  }

  /**
   * Gets the value of the 'wasCompany' field.
   */
  public java.lang.String getWasCompany() {
    return wasCompany;
  }

  /**
   * Sets the value of the 'wasCompany' field.
   * @param value the value to set.
   */
  public void setWasCompany(java.lang.String value) {
    this.wasCompany = value;
  }

  /**
   * Gets the value of the 'wasDelivery' field.
   */
  public java.lang.String getWasDelivery() {
    return wasDelivery;
  }

  /**
   * Sets the value of the 'wasDelivery' field.
   * @param value the value to set.
   */
  public void setWasDelivery(java.lang.String value) {
    this.wasDelivery = value;
  }

  /**
   * Gets the value of the 'wasEnvironment' field.
   */
  public java.lang.String getWasEnvironment() {
    return wasEnvironment;
  }

  /**
   * Sets the value of the 'wasEnvironment' field.
   * @param value the value to set.
   */
  public void setWasEnvironment(java.lang.String value) {
    this.wasEnvironment = value;
  }

  /**
   * Gets the value of the 'wasStatus' field.
   */
  public java.lang.String getWasStatus() {
    return wasStatus;
  }

  /**
   * Sets the value of the 'wasStatus' field.
   * @param value the value to set.
   */
  public void setWasStatus(java.lang.String value) {
    this.wasStatus = value;
  }

  /**
   * Gets the value of the 'wasItem' field.
   */
  public java.lang.String getWasItem() {
    return wasItem;
  }

  /**
   * Sets the value of the 'wasItem' field.
   * @param value the value to set.
   */
  public void setWasItem(java.lang.String value) {
    this.wasItem = value;
  }

  /**
   * Gets the value of the 'wasVDC' field.
   */
  public java.lang.String getWasVDC() {
    return wasVDC;
  }

  /**
   * Sets the value of the 'wasVDC' field.
   * @param value the value to set.
   */
  public void setWasVDC(java.lang.String value) {
    this.wasVDC = value;
  }

  /**
   * Gets the value of the 'wasDistributionNetwork' field.
   */
  public java.lang.String getWasDistributionNetwork() {
    return wasDistributionNetwork;
  }

  /**
   * Sets the value of the 'wasDistributionNetwork' field.
   * @param value the value to set.
   */
  public void setWasDistributionNetwork(java.lang.String value) {
    this.wasDistributionNetwork = value;
  }

  /**
   * Gets the value of the 'wasManufacturer' field.
   */
  public java.lang.String getWasManufacturer() {
    return wasManufacturer;
  }

  /**
   * Sets the value of the 'wasManufacturer' field.
   * @param value the value to set.
   */
  public void setWasManufacturer(java.lang.String value) {
    this.wasManufacturer = value;
  }

  /**
   * Gets the value of the 'wasProductName' field.
   */
  public java.lang.String getWasProductName() {
    return wasProductName;
  }

  /**
   * Sets the value of the 'wasProductName' field.
   * @param value the value to set.
   */
  public void setWasProductName(java.lang.String value) {
    this.wasProductName = value;
  }

  /**
   * Gets the value of the 'wasModelVersion' field.
   */
  public java.lang.String getWasModelVersion() {
    return wasModelVersion;
  }

  /**
   * Sets the value of the 'wasModelVersion' field.
   * @param value the value to set.
   */
  public void setWasModelVersion(java.lang.String value) {
    this.wasModelVersion = value;
  }

  /**
   * Gets the value of the 'wasServiceComponent' field.
   */
  public java.lang.String getWasServiceComponent() {
    return wasServiceComponent;
  }

  /**
   * Sets the value of the 'wasServiceComponent' field.
   * @param value the value to set.
   */
  public void setWasServiceComponent(java.lang.String value) {
    this.wasServiceComponent = value;
  }

  /**
   * Gets the value of the 'wasTechnicalService' field.
   */
  public java.lang.String getWasTechnicalService() {
    return wasTechnicalService;
  }

  /**
   * Sets the value of the 'wasTechnicalService' field.
   * @param value the value to set.
   */
  public void setWasTechnicalService(java.lang.String value) {
    this.wasTechnicalService = value;
  }

  /**
   * Gets the value of the 'wasBusinessService' field.
   */
  public java.lang.String getWasBusinessService() {
    return wasBusinessService;
  }

  /**
   * Sets the value of the 'wasBusinessService' field.
   * @param value the value to set.
   */
  public void setWasBusinessService(java.lang.String value) {
    this.wasBusinessService = value;
  }

  /**
   * Gets the value of the 'logicalServerName' field.
   */
  public java.lang.String getLogicalServerName() {
    return logicalServerName;
  }

  /**
   * Sets the value of the 'logicalServerName' field.
   * @param value the value to set.
   */
  public void setLogicalServerName(java.lang.String value) {
    this.logicalServerName = value;
  }

  /** Creates a new CmdbWas RecordBuilder */
  public static br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder newBuilder() {
    return new br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder();
  }
  
  /** Creates a new CmdbWas RecordBuilder by copying an existing Builder */
  public static br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder newBuilder(br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder other) {
    return new br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder(other);
  }
  
  /** Creates a new CmdbWas RecordBuilder by copying an existing CmdbWas instance */
  public static br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder newBuilder(br.com.produban.openbus.model.avro.enrichment.CmdbWas other) {
    return new br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder(other);
  }
  
  /**
   * RecordBuilder for CmdbWas instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<CmdbWas>
    implements org.apache.avro.data.RecordBuilder<CmdbWas> {

    private java.lang.String tenant;
    private java.lang.String timestamp;
    private java.lang.String wasName;
    private java.lang.String wasCompany;
    private java.lang.String wasDelivery;
    private java.lang.String wasEnvironment;
    private java.lang.String wasStatus;
    private java.lang.String wasItem;
    private java.lang.String wasVDC;
    private java.lang.String wasDistributionNetwork;
    private java.lang.String wasManufacturer;
    private java.lang.String wasProductName;
    private java.lang.String wasModelVersion;
    private java.lang.String wasServiceComponent;
    private java.lang.String wasTechnicalService;
    private java.lang.String wasBusinessService;
    private java.lang.String logicalServerName;

    /** Creates a new Builder */
    private Builder() {
      super(br.com.produban.openbus.model.avro.enrichment.CmdbWas.SCHEMA$);
    }
    
    /** Creates a Builder by copying an existing Builder */
    private Builder(br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.tenant)) {
        this.tenant = data().deepCopy(fields()[0].schema(), other.tenant);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[1].schema(), other.timestamp);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.wasName)) {
        this.wasName = data().deepCopy(fields()[2].schema(), other.wasName);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.wasCompany)) {
        this.wasCompany = data().deepCopy(fields()[3].schema(), other.wasCompany);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.wasDelivery)) {
        this.wasDelivery = data().deepCopy(fields()[4].schema(), other.wasDelivery);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.wasEnvironment)) {
        this.wasEnvironment = data().deepCopy(fields()[5].schema(), other.wasEnvironment);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.wasStatus)) {
        this.wasStatus = data().deepCopy(fields()[6].schema(), other.wasStatus);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.wasItem)) {
        this.wasItem = data().deepCopy(fields()[7].schema(), other.wasItem);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.wasVDC)) {
        this.wasVDC = data().deepCopy(fields()[8].schema(), other.wasVDC);
        fieldSetFlags()[8] = true;
      }
      if (isValidValue(fields()[9], other.wasDistributionNetwork)) {
        this.wasDistributionNetwork = data().deepCopy(fields()[9].schema(), other.wasDistributionNetwork);
        fieldSetFlags()[9] = true;
      }
      if (isValidValue(fields()[10], other.wasManufacturer)) {
        this.wasManufacturer = data().deepCopy(fields()[10].schema(), other.wasManufacturer);
        fieldSetFlags()[10] = true;
      }
      if (isValidValue(fields()[11], other.wasProductName)) {
        this.wasProductName = data().deepCopy(fields()[11].schema(), other.wasProductName);
        fieldSetFlags()[11] = true;
      }
      if (isValidValue(fields()[12], other.wasModelVersion)) {
        this.wasModelVersion = data().deepCopy(fields()[12].schema(), other.wasModelVersion);
        fieldSetFlags()[12] = true;
      }
      if (isValidValue(fields()[13], other.wasServiceComponent)) {
        this.wasServiceComponent = data().deepCopy(fields()[13].schema(), other.wasServiceComponent);
        fieldSetFlags()[13] = true;
      }
      if (isValidValue(fields()[14], other.wasTechnicalService)) {
        this.wasTechnicalService = data().deepCopy(fields()[14].schema(), other.wasTechnicalService);
        fieldSetFlags()[14] = true;
      }
      if (isValidValue(fields()[15], other.wasBusinessService)) {
        this.wasBusinessService = data().deepCopy(fields()[15].schema(), other.wasBusinessService);
        fieldSetFlags()[15] = true;
      }
      if (isValidValue(fields()[16], other.logicalServerName)) {
        this.logicalServerName = data().deepCopy(fields()[16].schema(), other.logicalServerName);
        fieldSetFlags()[16] = true;
      }
    }
    
    /** Creates a Builder by copying an existing CmdbWas instance */
    private Builder(br.com.produban.openbus.model.avro.enrichment.CmdbWas other) {
            super(br.com.produban.openbus.model.avro.enrichment.CmdbWas.SCHEMA$);
      if (isValidValue(fields()[0], other.tenant)) {
        this.tenant = data().deepCopy(fields()[0].schema(), other.tenant);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.timestamp)) {
        this.timestamp = data().deepCopy(fields()[1].schema(), other.timestamp);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.wasName)) {
        this.wasName = data().deepCopy(fields()[2].schema(), other.wasName);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.wasCompany)) {
        this.wasCompany = data().deepCopy(fields()[3].schema(), other.wasCompany);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.wasDelivery)) {
        this.wasDelivery = data().deepCopy(fields()[4].schema(), other.wasDelivery);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.wasEnvironment)) {
        this.wasEnvironment = data().deepCopy(fields()[5].schema(), other.wasEnvironment);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.wasStatus)) {
        this.wasStatus = data().deepCopy(fields()[6].schema(), other.wasStatus);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.wasItem)) {
        this.wasItem = data().deepCopy(fields()[7].schema(), other.wasItem);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.wasVDC)) {
        this.wasVDC = data().deepCopy(fields()[8].schema(), other.wasVDC);
        fieldSetFlags()[8] = true;
      }
      if (isValidValue(fields()[9], other.wasDistributionNetwork)) {
        this.wasDistributionNetwork = data().deepCopy(fields()[9].schema(), other.wasDistributionNetwork);
        fieldSetFlags()[9] = true;
      }
      if (isValidValue(fields()[10], other.wasManufacturer)) {
        this.wasManufacturer = data().deepCopy(fields()[10].schema(), other.wasManufacturer);
        fieldSetFlags()[10] = true;
      }
      if (isValidValue(fields()[11], other.wasProductName)) {
        this.wasProductName = data().deepCopy(fields()[11].schema(), other.wasProductName);
        fieldSetFlags()[11] = true;
      }
      if (isValidValue(fields()[12], other.wasModelVersion)) {
        this.wasModelVersion = data().deepCopy(fields()[12].schema(), other.wasModelVersion);
        fieldSetFlags()[12] = true;
      }
      if (isValidValue(fields()[13], other.wasServiceComponent)) {
        this.wasServiceComponent = data().deepCopy(fields()[13].schema(), other.wasServiceComponent);
        fieldSetFlags()[13] = true;
      }
      if (isValidValue(fields()[14], other.wasTechnicalService)) {
        this.wasTechnicalService = data().deepCopy(fields()[14].schema(), other.wasTechnicalService);
        fieldSetFlags()[14] = true;
      }
      if (isValidValue(fields()[15], other.wasBusinessService)) {
        this.wasBusinessService = data().deepCopy(fields()[15].schema(), other.wasBusinessService);
        fieldSetFlags()[15] = true;
      }
      if (isValidValue(fields()[16], other.logicalServerName)) {
        this.logicalServerName = data().deepCopy(fields()[16].schema(), other.logicalServerName);
        fieldSetFlags()[16] = true;
      }
    }

    /** Gets the value of the 'tenant' field */
    public java.lang.String getTenant() {
      return tenant;
    }
    
    /** Sets the value of the 'tenant' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setTenant(java.lang.String value) {
      validate(fields()[0], value);
      this.tenant = value;
      fieldSetFlags()[0] = true;
      return this; 
    }
    
    /** Checks whether the 'tenant' field has been set */
    public boolean hasTenant() {
      return fieldSetFlags()[0];
    }
    
    /** Clears the value of the 'tenant' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearTenant() {
      tenant = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /** Gets the value of the 'timestamp' field */
    public java.lang.String getTimestamp() {
      return timestamp;
    }
    
    /** Sets the value of the 'timestamp' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setTimestamp(java.lang.String value) {
      validate(fields()[1], value);
      this.timestamp = value;
      fieldSetFlags()[1] = true;
      return this; 
    }
    
    /** Checks whether the 'timestamp' field has been set */
    public boolean hasTimestamp() {
      return fieldSetFlags()[1];
    }
    
    /** Clears the value of the 'timestamp' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearTimestamp() {
      timestamp = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /** Gets the value of the 'wasName' field */
    public java.lang.String getWasName() {
      return wasName;
    }
    
    /** Sets the value of the 'wasName' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasName(java.lang.String value) {
      validate(fields()[2], value);
      this.wasName = value;
      fieldSetFlags()[2] = true;
      return this; 
    }
    
    /** Checks whether the 'wasName' field has been set */
    public boolean hasWasName() {
      return fieldSetFlags()[2];
    }
    
    /** Clears the value of the 'wasName' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasName() {
      wasName = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /** Gets the value of the 'wasCompany' field */
    public java.lang.String getWasCompany() {
      return wasCompany;
    }
    
    /** Sets the value of the 'wasCompany' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasCompany(java.lang.String value) {
      validate(fields()[3], value);
      this.wasCompany = value;
      fieldSetFlags()[3] = true;
      return this; 
    }
    
    /** Checks whether the 'wasCompany' field has been set */
    public boolean hasWasCompany() {
      return fieldSetFlags()[3];
    }
    
    /** Clears the value of the 'wasCompany' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasCompany() {
      wasCompany = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /** Gets the value of the 'wasDelivery' field */
    public java.lang.String getWasDelivery() {
      return wasDelivery;
    }
    
    /** Sets the value of the 'wasDelivery' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasDelivery(java.lang.String value) {
      validate(fields()[4], value);
      this.wasDelivery = value;
      fieldSetFlags()[4] = true;
      return this; 
    }
    
    /** Checks whether the 'wasDelivery' field has been set */
    public boolean hasWasDelivery() {
      return fieldSetFlags()[4];
    }
    
    /** Clears the value of the 'wasDelivery' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasDelivery() {
      wasDelivery = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /** Gets the value of the 'wasEnvironment' field */
    public java.lang.String getWasEnvironment() {
      return wasEnvironment;
    }
    
    /** Sets the value of the 'wasEnvironment' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasEnvironment(java.lang.String value) {
      validate(fields()[5], value);
      this.wasEnvironment = value;
      fieldSetFlags()[5] = true;
      return this; 
    }
    
    /** Checks whether the 'wasEnvironment' field has been set */
    public boolean hasWasEnvironment() {
      return fieldSetFlags()[5];
    }
    
    /** Clears the value of the 'wasEnvironment' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasEnvironment() {
      wasEnvironment = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /** Gets the value of the 'wasStatus' field */
    public java.lang.String getWasStatus() {
      return wasStatus;
    }
    
    /** Sets the value of the 'wasStatus' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasStatus(java.lang.String value) {
      validate(fields()[6], value);
      this.wasStatus = value;
      fieldSetFlags()[6] = true;
      return this; 
    }
    
    /** Checks whether the 'wasStatus' field has been set */
    public boolean hasWasStatus() {
      return fieldSetFlags()[6];
    }
    
    /** Clears the value of the 'wasStatus' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasStatus() {
      wasStatus = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    /** Gets the value of the 'wasItem' field */
    public java.lang.String getWasItem() {
      return wasItem;
    }
    
    /** Sets the value of the 'wasItem' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasItem(java.lang.String value) {
      validate(fields()[7], value);
      this.wasItem = value;
      fieldSetFlags()[7] = true;
      return this; 
    }
    
    /** Checks whether the 'wasItem' field has been set */
    public boolean hasWasItem() {
      return fieldSetFlags()[7];
    }
    
    /** Clears the value of the 'wasItem' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasItem() {
      wasItem = null;
      fieldSetFlags()[7] = false;
      return this;
    }

    /** Gets the value of the 'wasVDC' field */
    public java.lang.String getWasVDC() {
      return wasVDC;
    }
    
    /** Sets the value of the 'wasVDC' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasVDC(java.lang.String value) {
      validate(fields()[8], value);
      this.wasVDC = value;
      fieldSetFlags()[8] = true;
      return this; 
    }
    
    /** Checks whether the 'wasVDC' field has been set */
    public boolean hasWasVDC() {
      return fieldSetFlags()[8];
    }
    
    /** Clears the value of the 'wasVDC' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasVDC() {
      wasVDC = null;
      fieldSetFlags()[8] = false;
      return this;
    }

    /** Gets the value of the 'wasDistributionNetwork' field */
    public java.lang.String getWasDistributionNetwork() {
      return wasDistributionNetwork;
    }
    
    /** Sets the value of the 'wasDistributionNetwork' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasDistributionNetwork(java.lang.String value) {
      validate(fields()[9], value);
      this.wasDistributionNetwork = value;
      fieldSetFlags()[9] = true;
      return this; 
    }
    
    /** Checks whether the 'wasDistributionNetwork' field has been set */
    public boolean hasWasDistributionNetwork() {
      return fieldSetFlags()[9];
    }
    
    /** Clears the value of the 'wasDistributionNetwork' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasDistributionNetwork() {
      wasDistributionNetwork = null;
      fieldSetFlags()[9] = false;
      return this;
    }

    /** Gets the value of the 'wasManufacturer' field */
    public java.lang.String getWasManufacturer() {
      return wasManufacturer;
    }
    
    /** Sets the value of the 'wasManufacturer' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasManufacturer(java.lang.String value) {
      validate(fields()[10], value);
      this.wasManufacturer = value;
      fieldSetFlags()[10] = true;
      return this; 
    }
    
    /** Checks whether the 'wasManufacturer' field has been set */
    public boolean hasWasManufacturer() {
      return fieldSetFlags()[10];
    }
    
    /** Clears the value of the 'wasManufacturer' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasManufacturer() {
      wasManufacturer = null;
      fieldSetFlags()[10] = false;
      return this;
    }

    /** Gets the value of the 'wasProductName' field */
    public java.lang.String getWasProductName() {
      return wasProductName;
    }
    
    /** Sets the value of the 'wasProductName' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasProductName(java.lang.String value) {
      validate(fields()[11], value);
      this.wasProductName = value;
      fieldSetFlags()[11] = true;
      return this; 
    }
    
    /** Checks whether the 'wasProductName' field has been set */
    public boolean hasWasProductName() {
      return fieldSetFlags()[11];
    }
    
    /** Clears the value of the 'wasProductName' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasProductName() {
      wasProductName = null;
      fieldSetFlags()[11] = false;
      return this;
    }

    /** Gets the value of the 'wasModelVersion' field */
    public java.lang.String getWasModelVersion() {
      return wasModelVersion;
    }
    
    /** Sets the value of the 'wasModelVersion' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasModelVersion(java.lang.String value) {
      validate(fields()[12], value);
      this.wasModelVersion = value;
      fieldSetFlags()[12] = true;
      return this; 
    }
    
    /** Checks whether the 'wasModelVersion' field has been set */
    public boolean hasWasModelVersion() {
      return fieldSetFlags()[12];
    }
    
    /** Clears the value of the 'wasModelVersion' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasModelVersion() {
      wasModelVersion = null;
      fieldSetFlags()[12] = false;
      return this;
    }

    /** Gets the value of the 'wasServiceComponent' field */
    public java.lang.String getWasServiceComponent() {
      return wasServiceComponent;
    }
    
    /** Sets the value of the 'wasServiceComponent' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasServiceComponent(java.lang.String value) {
      validate(fields()[13], value);
      this.wasServiceComponent = value;
      fieldSetFlags()[13] = true;
      return this; 
    }
    
    /** Checks whether the 'wasServiceComponent' field has been set */
    public boolean hasWasServiceComponent() {
      return fieldSetFlags()[13];
    }
    
    /** Clears the value of the 'wasServiceComponent' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasServiceComponent() {
      wasServiceComponent = null;
      fieldSetFlags()[13] = false;
      return this;
    }

    /** Gets the value of the 'wasTechnicalService' field */
    public java.lang.String getWasTechnicalService() {
      return wasTechnicalService;
    }
    
    /** Sets the value of the 'wasTechnicalService' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasTechnicalService(java.lang.String value) {
      validate(fields()[14], value);
      this.wasTechnicalService = value;
      fieldSetFlags()[14] = true;
      return this; 
    }
    
    /** Checks whether the 'wasTechnicalService' field has been set */
    public boolean hasWasTechnicalService() {
      return fieldSetFlags()[14];
    }
    
    /** Clears the value of the 'wasTechnicalService' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasTechnicalService() {
      wasTechnicalService = null;
      fieldSetFlags()[14] = false;
      return this;
    }

    /** Gets the value of the 'wasBusinessService' field */
    public java.lang.String getWasBusinessService() {
      return wasBusinessService;
    }
    
    /** Sets the value of the 'wasBusinessService' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setWasBusinessService(java.lang.String value) {
      validate(fields()[15], value);
      this.wasBusinessService = value;
      fieldSetFlags()[15] = true;
      return this; 
    }
    
    /** Checks whether the 'wasBusinessService' field has been set */
    public boolean hasWasBusinessService() {
      return fieldSetFlags()[15];
    }
    
    /** Clears the value of the 'wasBusinessService' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearWasBusinessService() {
      wasBusinessService = null;
      fieldSetFlags()[15] = false;
      return this;
    }

    /** Gets the value of the 'logicalServerName' field */
    public java.lang.String getLogicalServerName() {
      return logicalServerName;
    }
    
    /** Sets the value of the 'logicalServerName' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder setLogicalServerName(java.lang.String value) {
      validate(fields()[16], value);
      this.logicalServerName = value;
      fieldSetFlags()[16] = true;
      return this; 
    }
    
    /** Checks whether the 'logicalServerName' field has been set */
    public boolean hasLogicalServerName() {
      return fieldSetFlags()[16];
    }
    
    /** Clears the value of the 'logicalServerName' field */
    public br.com.produban.openbus.model.avro.enrichment.CmdbWas.Builder clearLogicalServerName() {
      logicalServerName = null;
      fieldSetFlags()[16] = false;
      return this;
    }

    @Override
    public CmdbWas build() {
      try {
        CmdbWas record = new CmdbWas();
        record.tenant = fieldSetFlags()[0] ? this.tenant : (java.lang.String) defaultValue(fields()[0]);
        record.timestamp = fieldSetFlags()[1] ? this.timestamp : (java.lang.String) defaultValue(fields()[1]);
        record.wasName = fieldSetFlags()[2] ? this.wasName : (java.lang.String) defaultValue(fields()[2]);
        record.wasCompany = fieldSetFlags()[3] ? this.wasCompany : (java.lang.String) defaultValue(fields()[3]);
        record.wasDelivery = fieldSetFlags()[4] ? this.wasDelivery : (java.lang.String) defaultValue(fields()[4]);
        record.wasEnvironment = fieldSetFlags()[5] ? this.wasEnvironment : (java.lang.String) defaultValue(fields()[5]);
        record.wasStatus = fieldSetFlags()[6] ? this.wasStatus : (java.lang.String) defaultValue(fields()[6]);
        record.wasItem = fieldSetFlags()[7] ? this.wasItem : (java.lang.String) defaultValue(fields()[7]);
        record.wasVDC = fieldSetFlags()[8] ? this.wasVDC : (java.lang.String) defaultValue(fields()[8]);
        record.wasDistributionNetwork = fieldSetFlags()[9] ? this.wasDistributionNetwork : (java.lang.String) defaultValue(fields()[9]);
        record.wasManufacturer = fieldSetFlags()[10] ? this.wasManufacturer : (java.lang.String) defaultValue(fields()[10]);
        record.wasProductName = fieldSetFlags()[11] ? this.wasProductName : (java.lang.String) defaultValue(fields()[11]);
        record.wasModelVersion = fieldSetFlags()[12] ? this.wasModelVersion : (java.lang.String) defaultValue(fields()[12]);
        record.wasServiceComponent = fieldSetFlags()[13] ? this.wasServiceComponent : (java.lang.String) defaultValue(fields()[13]);
        record.wasTechnicalService = fieldSetFlags()[14] ? this.wasTechnicalService : (java.lang.String) defaultValue(fields()[14]);
        record.wasBusinessService = fieldSetFlags()[15] ? this.wasBusinessService : (java.lang.String) defaultValue(fields()[15]);
        record.logicalServerName = fieldSetFlags()[16] ? this.logicalServerName : (java.lang.String) defaultValue(fields()[16]);
        return record;
      } catch (Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }
}
