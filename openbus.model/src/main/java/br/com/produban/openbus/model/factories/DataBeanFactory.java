package br.com.produban.openbus.model.factories;

import br.com.produban.openbus.model.OpenbusTool;
import br.com.produban.openbus.model.pojo.DataBean;
import org.apache.avro.generic.GenericRecord;

import java.lang.reflect.Constructor;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class DataBeanFactory {

    private static Map<String,Constructor<? extends DataBean>> schemaMap;

    private static Map<String,Constructor<? extends DataBean>> loadMap() throws NoSuchMethodException {
        Map<String,Constructor<? extends DataBean>> detectedSchemas = new HashMap<>();
        for (OpenbusTool tool : OpenbusTool.values()) {
            Constructor<? extends DataBean> dataBeanConstructor = tool.getDataBeanClass()
                    .getConstructor(GenericRecord.class);
            detectedSchemas.put(tool.getAvroSchemaName(), dataBeanConstructor);
        }

        return detectedSchemas;
    }

    public static DataBean toBean(GenericRecord avro) throws Exception {

        try {
            if (schemaMap==null) {
                synchronized (DataBeanFactory.class) {
                    if (schemaMap==null)
                        schemaMap = loadMap();
                }
            }

            Constructor<? extends DataBean> constructor = schemaMap.get(avro.getSchema().getName());

            if (constructor==null)
                throw new NoSuchMethodException("Constructor not found.");

            return constructor.newInstance(avro);
        } catch (Exception e) {
            String message = MessageFormat.format("Fail to build data bean: {0}",avro.getSchema().getName());
            throw new Exception(message,e);
        }
    }

}
