package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.ActiveDirectory;

public class ActiveDirectoryBean extends SuperEventlogBean {
	private static final long serialVersionUID = 4733942553484626494L;

	private ActiveDirectory activeDirectory;

	public ActiveDirectoryBean() {
		beanPrefix = "EventIdentifier.";
	}
	
	public ActiveDirectoryBean(GenericRecord eventLog) {
		setBean(ActiveDirectory.newBuilder((ActiveDirectory) eventLog).build());
		beanPrefix = "EventIdentifier.";
	}

	@Override
	public ActiveDirectory getBean() {
		return activeDirectory;
	}

	@Override
	public void setBean(SpecificRecordBase eventLog) {
		this.activeDirectory = (ActiveDirectory) eventLog;
	}

	@Override
	public String getHostname() {
		return activeDirectory.getComputerName();
	}
	
	@Override
	public void setHostname(String hostname) {
		activeDirectory.setComputerName(hostname);
	}

	@Override
	public String getRawKey(){
		return activeDirectory.getEventIdentifier();
	}

	@Override
	public void setRawKey(String key) {
		activeDirectory.setEventIdentifier(key);
	}
	
	@Override
	public Long getTimestamp() {
		return format(activeDirectory.getTimeGenerated());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		activeDirectory.setTimeGenerated(timestamp.toString());
	}

	@Override
	protected String getMessage() {
		return activeDirectory.getMessage();
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.ACTIVEDIRECTORY;
//	}
}
