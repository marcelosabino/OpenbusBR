package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.BigIp;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.util.Map;

public class BigIpBean extends DataBean {

    private BigIp bean;

    public BigIpBean() {
    }

    public BigIpBean(GenericRecord bigIp) {
        setBean(BigIp.newBuilder((BigIp) bigIp).build());
        beanPrefix = "balancers.";
    }

    @Override
    public SpecificRecordBase getBean() {
        return bean;
    }

    @Override
    public void setBean(SpecificRecordBase bean) {
        this.bean = (BigIp) bean;
    }

    @Override
    public String getHostname() {
        return ((BigIp)getBean()).getMemberAddress().toString();
    }

    @Override
    public void setHostname(String hostname) {
        ((BigIp)getBean()).setMemberAddress(hostname);
    }

    @Override
    public String getRawKey() {
        return beanPrefix + getBean().get("metricName");
    }

    @Override
    public void setRawKey(String key) {}

    @Override
    public Long getTimestamp() throws ParseException {
        if (bean.getTimestamp().length() == 13)
            return Long.parseLong(bean.getTimestamp().toString());
        else {
            return Long.parseLong(bean.getTimestamp().toString().substring(0,10)) * 1000L;
        }
    }

    @Override
    public void setTimestamp(Long timestamp) {}

    @Override
    public Map<String, String> parseFields() {
        return null;
    }

//    @Override
//    public OpenbusTool getTool() {
//        return OpenbusTool.BIGIP_POOL_MEMBERS;
//    }
}
