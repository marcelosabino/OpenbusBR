package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.CommElement;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.util.Map;

public class CommElementBean extends DataBean {

    private CommElement bean;

    public CommElementBean() {
        beanPrefix = "CommElement.";
    }

    public CommElementBean(GenericRecord commElement) {
        setBean(CommElement.newBuilder((CommElement) commElement).build());
        beanPrefix = "CommElement.";
    }

    @Override
    public SpecificRecordBase getBean() {
        return bean;
    }

    @Override
    public void setBean(SpecificRecordBase bean) {
        this.bean = CommElement.class.cast(bean);
    }

    @Override
    public String getHostname() {
        return null;
    }

    @Override
    public void setHostname(String hostname) {
    }

    @Override
    public String getRawKey() {
        return "lookupKey";
    }

    @Override
    public void setRawKey(String key) {
    }

    @Override
    public Long getTimestamp() throws ParseException {
        return Long.valueOf(bean.getTimestamp());
    }

    @Override
    public void setTimestamp(Long timestamp) {
    }

    @Override
    public Map<String, String> parseFields() {
        return null;
    }

//    @Override
//    public OpenbusTool getTool() {
//        return OpenbusTool.COMM_ELEMENT;
//    }
}
