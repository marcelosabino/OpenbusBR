package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.DB2;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.util.Map;

public class DB2Bean extends DataBean {

    private DB2 bean;

    public DB2Bean() {
        beanPrefix = "DB2.";
    }

    public DB2Bean(GenericRecord bean) {
        setBean(DB2.newBuilder((DB2) bean).build());
        beanPrefix = "DB2.";
    }

    @Override
    public SpecificRecordBase getBean() {
        return bean;
    }

    @Override
    public void setBean(SpecificRecordBase bean) {
        this.bean = DB2.class.cast(bean);
    }

    @Override
    public String getHostname() {
        return bean.getInstanceId();
    }

    @Override
    public void setHostname(String hostname) {
        this.bean.setInstanceId(hostname);
    }

    @Override
    public String getRawKey() {
        return "lookupKey";
    }

    @Override
    public void setRawKey(String key) {

    }

    @Override
    public Long getTimestamp() throws ParseException {
        return Long.valueOf(bean.getTimestamp());
    }

    @Override
    public void setTimestamp(Long timestamp) {
        this.bean.setTimestamp(timestamp.toString());
    }

    @Override
    public Map<String, String> parseFields() {
        return null;
    }
}
