package br.com.produban.openbus.model.pojo;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.Schema.Field;
import org.apache.avro.specific.SpecificRecordBase;

@SuppressWarnings("serial")
public abstract class DataBean implements Serializable {

	private Map<String, String> extraFields = new HashMap<String, String>();
	protected String beanPrefix = "";
	private String tool;

	public String getTool() {
		return tool;
	}

	public void setTool(String tool) {
		this.tool = tool;
	}

	public abstract SpecificRecordBase getBean();

	public abstract void setBean(SpecificRecordBase bean);

	public abstract String getHostname();

	public abstract void setHostname(String hostname);

	public abstract String getRawKey();

	public String getKey() {
		return beanPrefix + getRawKey();
	}

	public abstract void setRawKey(String key);

	public abstract Long getTimestamp() throws ParseException;

	public abstract void setTimestamp(Long timestamp);

	public abstract Map<String, String> parseFields();

	/**
	 * Exposes the internal map containing enriched fields (exploded fields)
	 * @return
	 */
	public Map<String, String> getExtraFields() {
		return extraFields;
	}
	
	public void addAllToExtraFields(Map<String, String> map){
		if (map != null) {
			this.extraFields.putAll(map);
		}
	}
	
	public String getExtraField(String fieldName) {
		return extraFields.get(fieldName);
	}
	
	public void addExtraField(String fieldName, String fieldValue) {
		this.extraFields.put(fieldName, fieldValue);
	}
	
	public String removeExtraField(String fieldName) {
		return this.extraFields.remove(fieldName);
	}
	
	/**
	 * Return a new Map containing Extra fields
	 * @return
	 */
	public Map<String, String> getExtraFieldsCopy() {
		HashMap<String, String> map = new HashMap<String, String>();
		
		// mapa dos campos adicionais
		for (Entry<String, String> entry : extraFields.entrySet()) {
			map.put(entry.getKey(), entry.getValue());
		}

		return map;
	}
	
	/**
	 * Return a new Map containing Avro fields
	 * @return
	 */
	public Map<String, String> getAvroMapCopy() {
		HashMap<String, String> map = new HashMap<String, String>();
		
		// mapa do Avro
		for (Field field : getBean().getSchema().getFields()) {
			Object value = getBean().get(field.pos());
			if (value != null && (value instanceof Map)) {
				map.putAll((Map<? extends String, ? extends String>) value);
			} else {
				map.put(field.name(), getBean().get(field.pos()) == null ? null : value.toString());
			}
		}
		
		return map;
	}
	
	/**
	 * Return a new Map containing Avro and Extra fields
	 * @return
	 */
	public Map<String, String> getCompleteDataBeanMapCopy() {
		Map<String, String> map = getAvroMapCopy();
		map.putAll(getExtraFieldsCopy());
		
		return map;
	}

	/**
	 * If there is an avro field AND an extra field with the same name,
	 * extra field value will be returned. *Special case* for hostname
	 * @param fieldName
	 * @return
	 */
	public String getField(String fieldName) {
		String value = null;

		// special case for "host" ("host", "hostname", "computerName", etc)
		if ("host".equalsIgnoreCase(fieldName)) {
			return this.getHostname();
		} else {
			SpecificRecordBase record = this.getBean();
			Field avroField = record.getSchema().getField(fieldName);
			if (avroField != null) {
				// lookup corresponds to an Avro Field
				Object fieldValue = record.get(avroField.pos());
				if (fieldValue != null)
					value = fieldValue.toString();
			}

			// lookup may correspond to an additional Field
			String extraValue = this.getExtraField(fieldName);
			if (extraValue != null && !extraValue.isEmpty()) {
				value = extraValue;
			}
		}

		return value;
	}

	@Override
	public String toString() {
		return "DataBean{" +
				"beanPrefix='" + beanPrefix + '\'' +
				", tool='" + tool + '\'' +
				'}';
	}
}
