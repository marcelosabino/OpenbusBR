package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Eventlog;

public class EventlogBean extends SuperEventlogBean {
	private static final long serialVersionUID = 4733942553484626494L;

	private Eventlog eventLog;

	public EventlogBean() {
		beanPrefix = "EventIdentifier.";
	}
	
	public EventlogBean(GenericRecord eventLog) {
		setBean(Eventlog.newBuilder((Eventlog) eventLog).build());
		beanPrefix = "EventIdentifier.";
	}

	@Override
	public Eventlog getBean() {
		return eventLog;
	}

	@Override
	public void setBean(SpecificRecordBase eventLog) {
		this.eventLog = (Eventlog) eventLog;
	}

	@Override
	public String getHostname() {
		return eventLog.getComputerName();
	}

	@Override
	public void setHostname(String hostname) {
		eventLog.setComputerName(hostname);
	}
	
	@Override
	public String getRawKey() {
		return eventLog.getEventIdentifier();
	}

	@Override
	public void setRawKey(String key) {
		eventLog.setEventIdentifier(key);
	}
	
	@Override
	public Long getTimestamp() {
		return format(eventLog.getTimeGenerated());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		eventLog.setTimeGenerated(timestamp.toString());
	}

	@Override
	protected String getMessage() {
		return eventLog.getMessage();
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.EVENTLOG;
//	}
}
