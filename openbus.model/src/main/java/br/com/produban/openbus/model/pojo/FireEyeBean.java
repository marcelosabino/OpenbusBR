package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.FireEye;

import java.text.ParseException;

public class FireEyeBean extends SuperSyslog5424Bean {
	private static final long serialVersionUID = 4423295265585078859L;

	private FireEye fireEye;

	public FireEyeBean() {
		beanPrefix = "FireEye.";
	}
	
	public FireEyeBean(GenericRecord fireEye) {
		setBean(FireEye.newBuilder((FireEye) fireEye).build());
		beanPrefix = "FireEye.";
	}

	@Override
	public FireEye getBean() {
		return fireEye;
	}

	@Override
	public void setBean(SpecificRecordBase fireEye) {
		this.fireEye = (FireEye) fireEye;
	}

	@Override
	public String getHostname() {
		return fireEye.getHostname();
	}

	@Override
	public void setHostname(String hostname) {
		fireEye.setHostname(hostname);
	}
	
	@Override
	public String getRawKey() {
		return fireEye.getApplicationName();
	}
	
	@Override
	public void setRawKey(String key) {
		fireEye.setApplicationName(key);
	}
	
	@Override
	public String getKey() {
		return super.getKey() + "_" + fireEye.getPriority();
	}

	@Override
	public Long getTimestamp() throws ParseException {
		return parseTimestamp(fireEye.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		fireEye.setTimestamp(timestamp.toString());
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.FIREEYE;
//	}
}
