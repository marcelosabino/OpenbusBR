package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Firewall;

public class FirewallBean extends DataBean {

	private static final long serialVersionUID = -7353663861195736948L;
	
	private static final String OPSEC_FORMAT = "ddMMMyyyy HH:mm:ss";
	private static final String GMT0 = "GMT+0";
	
	private Firewall firewall;
	
	public FirewallBean() {
		beanPrefix = "Firewall.";
	}
	
	public FirewallBean(GenericRecord firewall) {
		setBean(Firewall.newBuilder((Firewall) firewall).build());
		beanPrefix = "Firewall.";
	}
	
	@Override
	public Firewall getBean() {
		return firewall;
	}

	@Override
	public void setBean(SpecificRecordBase firewall) {
		this.firewall = (Firewall) firewall;
	}

	@Override
	public String getHostname() {
		return firewall.getOrigin();
	}

	@Override
	public void setHostname(String hostname) {
		firewall.setOrigin(hostname);
	}
	
	@Override
	public String getRawKey() {
		return firewall.getProduct();
	}

	@Override
	public void setRawKey(String key) {
		firewall.setProduct(key);
	}
	
	@Override
	public Long getTimestamp() throws ParseException {
		return format(firewall.getTime());
	}
	
	private Long format(String timestamp) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(OPSEC_FORMAT, Locale.ENGLISH);
		sdf.setTimeZone(TimeZone.getTimeZone(GMT0));

		Long tsLong = sdf.parse(timestamp).getTime();

		return tsLong;
	}

	@Override
	public void setTimestamp(Long timestamp) {
		firewall.setTime(timestamp.toString());
	}

	@Override
	public Map<String, String> parseFields() {
		return null;
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.FIREWALL;
//	}
}
