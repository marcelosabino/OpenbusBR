package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.IDM;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class IDMBean extends DataBean {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss z";

    private IDM idm;

    public IDMBean() {
        beanPrefix = "idm.";
    }

    public IDMBean(GenericRecord idm) {
        setBean(IDM.newBuilder((IDM) idm).build());
        beanPrefix = "idm.";
    }

    @Override
    public SpecificRecordBase getBean() {
        return idm;
    }

    @Override
    public void setBean(SpecificRecordBase bean) {
        idm = (IDM) bean;
    }

    @Override
    public String getHostname() {
        return idm.getHostname();
    }

    @Override
    public void setHostname(String hostname) {
        idm.setHostname(hostname);
    }

    @Override
    public String getRawKey() {
        return idm.getLogin();
    }

    @Override
    public void setRawKey(String key) {
        idm.setLogin(key);
    }

    @Override
    public Long getTimestamp() throws ParseException {
        return parseTimestamp(idm.getTimestamp());
    }

    @Override
    public void setTimestamp(Long timestamp) {
        idm.setTimestamp(timestamp.toString());
    }

    private Long parseTimestamp(String ts) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        Long tsLong = sdf.parse(ts).getTime();

        return tsLong;
    }

    @Override
    public Map<String, String> parseFields() {
        return null;
    }

//    @Override
//    public OpenbusTool getTool() {
//        return OpenbusTool.IDM;
//    }
}
