package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.IPS;

import java.text.ParseException;

public class IPSBean extends SuperSyslog5424Bean {
	private static final long serialVersionUID = 4423295265585078859L;

	private IPS ips;

	public IPSBean() {
		beanPrefix = "ips.";
	}
	
	public IPSBean(GenericRecord ips) {
		setBean(IPS.newBuilder((IPS) ips).build());
		beanPrefix = "ips.";
	}

	@Override
	public IPS getBean() {
		return ips;
	}

	@Override
	public void setBean(SpecificRecordBase ips) {
		this.ips = (IPS) ips;
	}

	@Override
	public String getHostname() {
		return ips.getHostname();
	}
	
	@Override
	public void setHostname(String hostname) {
		ips.setHostname(hostname);
	}

	@Override
	public String getRawKey() {
		return ips.getApplicationName();
	}
	
	@Override
	public String getKey() {
		return super.getKey() + "_" + ips.getPriority();
	}
	
	@Override
	public void setRawKey(String key) {
		ips.setApplicationName(key);
	}

	@Override
	public Long getTimestamp() throws ParseException {
		return parseTimestamp(ips.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		ips.setTimestamp(timestamp.toString());
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.IPS;
//	}
}
