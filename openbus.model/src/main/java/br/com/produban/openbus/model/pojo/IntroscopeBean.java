package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.util.Map;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Introscope;

public class IntroscopeBean extends DataBean {

	private static final long serialVersionUID = 2127157284393136396L;

//	private static final String GMT0 = "GMT+0";
	
	private Introscope introscope;
	
	public IntroscopeBean(GenericRecord introscope) {
		setBean(Introscope.newBuilder((Introscope) introscope).build());
		beanPrefix = "Introscope.";
	}
	
	public IntroscopeBean() {
		beanPrefix = "Introscope.";
	}
	
	@Override
	public SpecificRecordBase getBean() {
		return introscope;
	}

	@Override
	public void setBean(SpecificRecordBase bean) {
		introscope = (Introscope) bean;
	}

	@Override
	public String getHostname() {
		return introscope.getHost();
	}

	@Override
	public void setHostname(String hostname) {
		introscope.setHost(hostname);
	}

	@Override
	public String getRawKey() {
		return introscope.getResource();
	}

	@Override
	public void setRawKey(String key) {
		introscope.setResource(key);
	}

	@Override
	public Long getTimestamp() throws ParseException {
		if (introscope.getTimestamp().length() == 13)
			return Long.parseLong(introscope.getTimestamp());
		else {
			return Long.parseLong(introscope.getTimestamp().substring(0,10)) * 1000L;
		}
	}

	@Override
	public void setTimestamp(Long timestamp) {
		introscope.setTimestamp(timestamp.toString());
	}
	
	@Override
	public Map<String, String> parseFields() {
		return null;
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.INTROSCOPE;
//	}
}
