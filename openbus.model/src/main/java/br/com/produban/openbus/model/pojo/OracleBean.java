package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Oracle;

public class OracleBean extends DataBean {

	private static final long serialVersionUID = -954771016774656473L;
	
	private Oracle oracle;
	
	public OracleBean() {
		beanPrefix = "";
	}
	
	public OracleBean(GenericRecord oracle) {
		setBean(Oracle.newBuilder((Oracle) oracle).build());
		beanPrefix = "";
	}
	
	@Override
	public Oracle getBean() {
		return oracle;
	}

	@Override
	public void setBean(SpecificRecordBase oracle) {
		this.oracle = (Oracle) oracle;
	}

	@Override
	public String getHostname() {
		return oracle.getInstanceId();
	}

	@Override
	public void setHostname(String hostname) {
		oracle.setInstanceId(hostname);
	}
	
	@Override
	public String getRawKey() {
		return "oracle.lookupKey";
	}

	@Override
	public void setRawKey(String key) {
	}
	
	@Override
	public Long getTimestamp() throws ParseException {
		if (oracle.getTimestamp().length() == 10)
			return Long.valueOf(oracle.getTimestamp()) * 1000;
		else
			return Long.valueOf(oracle.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
	}

	@Override
	public Map<String, String> parseFields() {
		return null;
	}
}
