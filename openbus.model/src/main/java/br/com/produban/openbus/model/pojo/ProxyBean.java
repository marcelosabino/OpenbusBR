package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Proxy;

import java.text.ParseException;

public class ProxyBean extends SuperProxyBean {
	private static final long serialVersionUID = 4423295265585078859L;

	private Proxy proxy;

	public ProxyBean() {
		beanPrefix = "Proxy.";
	}
	
	public ProxyBean(GenericRecord proxy) {
		setBean(Proxy.newBuilder((Proxy) proxy).build());
		beanPrefix = "Proxy.";
	}

	@Override
	public Proxy getBean() {
		return proxy;
	}

	@Override
	public void setBean(SpecificRecordBase proxy) {
		this.proxy = (Proxy) proxy;
	}

	@Override
	public String getHostname() {
		return proxy.getHostname();
	}

	@Override
	public void setHostname(String hostname) {
		proxy.setHostname(hostname);
	}
	
	@Override
	public String getRawKey() {
		return proxy.getApplicationName();
	}
	
	@Override
	public String getKey() {
		return super.getKey() + "_" + proxy.getPriority();
	}
	
	@Override
	public void setRawKey(String key) {
		proxy.setApplicationName(key);
	}

	@Override
	public Long getTimestamp() throws ParseException {
		return parseTimestamp(proxy.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		proxy.setTimestamp(timestamp.toString());
	}

	@Override
	protected String getMessage() {
		return proxy.getMessage();
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.PROXY;
//	}
}
