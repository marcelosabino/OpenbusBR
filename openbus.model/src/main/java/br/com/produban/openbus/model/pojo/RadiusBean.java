package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Radius;

import java.text.ParseException;

public class RadiusBean extends SuperSyslog5424Bean {
	private static final long serialVersionUID = 4423295265585078859L;

	private Radius radius;

	public RadiusBean() {
		beanPrefix = "radius.";
	}
	
	public RadiusBean(GenericRecord radius) {
		setBean(Radius.newBuilder((Radius) radius).build());
		beanPrefix = "radius.";
	}

	@Override
	public Radius getBean() {
		return radius;
	}

	@Override
	public void setBean(SpecificRecordBase syslog) {
		this.radius = (Radius) syslog;
	}

	@Override
	public String getHostname() {
		return radius.getHostname();
	}
	
	@Override
	public void setHostname(String hostname) {
		radius.setHostname(hostname);
	}

	@Override
	public String getRawKey() {
		return radius.getApplicationName();
	}
	
	@Override
	public String getKey() {
		return super.getKey() + "_" + radius.getPriority();
	}
	
	@Override
	public void setRawKey(String key) {
		radius.setApplicationName(key);
	}

	@Override
	public Long getTimestamp() throws ParseException {
		return parseTimestamp(radius.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		radius.setTimestamp(timestamp.toString());
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.RADIUS;
//	}
}
