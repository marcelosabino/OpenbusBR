package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.Robot;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.util.Map;

public class RobotBean extends DataBean {

    private Robot robot;

    public RobotBean() {
        beanPrefix = "robot.";
    }

    public RobotBean(GenericRecord robot) {
        beanPrefix = "robot.";
        setBean(Robot.newBuilder((Robot) robot).build());
    }

    @Override
    public SpecificRecordBase getBean() {
        return robot;
    }

    @Override
    public void setBean(SpecificRecordBase bean) {
        this.robot = (Robot) bean;
    }

    @Override
    public String getHostname() {
        return null;
    }

    @Override
    public void setHostname(String hostname) {
    }

    @Override
    public String getRawKey() {
        return null;
    }

    @Override
    public void setRawKey(String key) {
    }

    @Override
    public Long getTimestamp() throws ParseException {
        if (robot.getTimestamp().length() == 13)
            return Long.parseLong(robot.getTimestamp());
        else {
            return Long.parseLong(robot.getTimestamp().substring(0,10)) * 1000L;
        }
    }

    @Override
    public void setTimestamp(Long timestamp) {
    }

    @Override
    public Map<String, String> parseFields() {
        return null;
    }
}
