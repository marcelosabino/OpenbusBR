package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.SNMPTrap;

public class SNMPTrapBean extends DataBean {

	private static final long serialVersionUID = -7880490058942938663L;
	
	private static final String SNMP_TRAP_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String GMT0 = "GMT+0";
	
	private SNMPTrap snmpTrap;
	
	public SNMPTrapBean() {
		beanPrefix = "SNMP.";
	}
	
	public SNMPTrapBean(GenericRecord snmpTrap) {
		setBean(SNMPTrap.newBuilder((SNMPTrap) snmpTrap).build());
		beanPrefix = "SNMP.";
	}
	
	@Override
	public SNMPTrap getBean() {
		return snmpTrap;
	}

	@Override
	public void setBean(SpecificRecordBase snmpTrap) {
		this.snmpTrap = (SNMPTrap) snmpTrap;
	}

	@Override
	public String getHostname() {
		return snmpTrap.getSourceIP();
	}

	@Override
	public void setHostname(String hostname) {
		snmpTrap.setSourceIP(hostname);
	}
	
	@Override
	public String getRawKey() {
		return snmpTrap.getTrapOID();
	}

	@Override
	public void setRawKey(String key) {
		snmpTrap.setTrapOID(key);
	}
	
	@Override
	public Long getTimestamp() throws ParseException {
		return format(snmpTrap.getTimestamp());
	}
	
	private Long format(String timestamp) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(SNMP_TRAP_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone(GMT0));

		String timeStr = timestamp.substring(0, 19);

		Long tsLong = sdf.parse(timeStr).getTime();

		return tsLong;
	}

	@Override
	public void setTimestamp(Long timestamp) {
		snmpTrap.setTimestamp(timestamp.toString());
	}

	@Override
	public Map<String, String> parseFields() {
		if (snmpTrap.getVariableBindings()==null || snmpTrap.getVariableBindings().isEmpty())
			return null;

		Scanner scanner = new Scanner(snmpTrap.getVariableBindings());
		HashMap<String, String> map = new HashMap<String, String>();
		while (scanner.hasNext()) {
			String next = scanner.nextLine();
			if (next.matches("(.*):\\t\\t(.*)")) {
				String[] line = next.split(":\\t\\t");
				map.put(line[0].trim(), line.length == 1 ? "" : line[1].trim());
			}
		}
		scanner.close();
		return map;
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.SNMPTRAP;
//	}
}
