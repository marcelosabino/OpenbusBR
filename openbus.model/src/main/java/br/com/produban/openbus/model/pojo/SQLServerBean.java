package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.SQLServer;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class SQLServerBean extends DataBean {

    private static final long serialVersionUID = -954771016774656473L;

    private SQLServer sqlserver;

    public SQLServerBean() {
        beanPrefix = "";
    }

    public SQLServerBean(GenericRecord sqlserver) {
        setBean(SQLServer.newBuilder((SQLServer) sqlserver).build());
        beanPrefix = "";
    }

    @Override
    public SQLServer getBean() {
        return sqlserver;
    }

    @Override
    public void setBean(SpecificRecordBase sqlserver) {
        this.sqlserver = (SQLServer) sqlserver;
    }

    @Override
    public String getHostname() {
        return sqlserver.getInstanceId();
    }

    @Override
    public void setHostname(String hostname) {
        sqlserver.setInstanceId(hostname);
    }

    @Override
    public String getRawKey() {
        return "sqlserver.lookupKey";
    }

    @Override
    public void setRawKey(String key) {

    }

    @Override
    public Long getTimestamp() throws ParseException {
        if (sqlserver.getTimestamp().length() == 10)
            return Long.valueOf(sqlserver.getTimestamp()) * 1000;
        else
            return Long.valueOf(sqlserver.getTimestamp());
    }

    @Override
    public void setTimestamp(Long timestamp) {

    }

    @Override
    public Map<String, String> parseFields() {
        return null;
    }
}
