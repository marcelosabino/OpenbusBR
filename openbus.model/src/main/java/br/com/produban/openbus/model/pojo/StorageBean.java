package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.util.Map;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Storage;

public class StorageBean extends DataBean {

	private static final long serialVersionUID = 6495346183461355629L;

	private Storage storage;
	
	public StorageBean() {
		beanPrefix = "";
	}
	
	public StorageBean(GenericRecord storage) {
		setBean(Storage.newBuilder((Storage) storage).build());
		beanPrefix = "";
	}
	
	@Override
	public SpecificRecordBase getBean() {
		return storage;
	}

	@Override
	public void setBean(SpecificRecordBase bean) {
		this.storage = (Storage) bean;
	}

	@Override
	public String getHostname() {
		return ((Storage) getBean()).getStorage();
	}

	@Override
	public void setHostname(String hostname) {
	}

	@Override
	public String getRawKey() {
		return null;
	}

	@Override
	public void setRawKey(String key) {
	}

	@Override
	public Long getTimestamp() throws ParseException {
		if (storage.getTimestamp().length() == 13)
            return Long.parseLong(storage.getTimestamp());
        else {
            return Long.parseLong(storage.getTimestamp().substring(0,10)) * 1000L;
        }
	}

	@Override
	public void setTimestamp(Long timestamp) {
	}

	@Override
	public Map<String, String> parseFields() {
		return null;
	}

}
