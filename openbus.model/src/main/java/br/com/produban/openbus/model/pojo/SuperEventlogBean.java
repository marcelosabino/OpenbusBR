package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;

public abstract class SuperEventlogBean extends DataBean {
	private static final long serialVersionUID = 4733942553484626494L;

	protected Long format(String date) {
		return Long.valueOf(date);
	}

	@Override
	public Map<String, String> parseFields() {
		if (getMessage()==null || getMessage().isEmpty())
			return null;

		Scanner scanner = new Scanner(getMessage());
		HashMap<String, String> map = new HashMap<String, String>();
		while (scanner.hasNext()) {
			String next = scanner.nextLine();
			if (next.matches("(.*):\\s(.*)")) {
				String[] line = next.split(":\\s");
				map.put(line[0].trim(), line.length == 1 ? "" : line[1].trim());
			}
		}
		scanner.close();
		return map;
	}

	protected abstract String getMessage();
	
}
