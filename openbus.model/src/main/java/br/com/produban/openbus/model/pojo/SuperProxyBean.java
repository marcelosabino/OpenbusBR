package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;

public abstract class SuperProxyBean extends DataBean {
	
	private static final long serialVersionUID = 7646817155409565692L;
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String UTC = "UTC";

	protected Long parseTimestamp(String ts) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone(UTC));

		Long tsLong = sdf.parse(ts).getTime();

		return tsLong;
	}

	@Override
	public Map<String, String> parseFields() {
		Scanner scanner = new Scanner(getMessage());
		HashMap<String, String> map = new HashMap<String, String>();
		while (scanner.hasNext()) {
			String next = scanner.nextLine();
			if (next.matches("(.*):\t(.*)")) {
				String[] line = next.split(":\t");
				map.put(line[0].trim(), line.length == 1 ? "" : line[1].trim());
			}
		}
		scanner.close();
		return map;
	}

	protected abstract String getMessage();
}
