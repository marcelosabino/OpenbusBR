package br.com.produban.openbus.model.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

public abstract class SuperSyslog5424Bean extends DataBean {
	private final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";
	private final String GMT0 = "GMT+0";

	protected Long parseTimestamp(String ts) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone(GMT0));

		String firstPart = ts.substring(0, 19);
		String lastPart = ts.substring(ts.length() - 6, ts.length());

		Long tsLong = sdf.parse(firstPart + lastPart).getTime();

		return tsLong;
	}

	@Override
	public Map<String, String> parseFields() {
		return null;
	}
}
