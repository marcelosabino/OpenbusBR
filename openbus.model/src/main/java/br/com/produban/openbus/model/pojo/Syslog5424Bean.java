package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.Syslog5424;

import java.text.ParseException;

public class Syslog5424Bean extends SuperSyslog5424Bean {
	private static final long serialVersionUID = 4423295265585078859L;

	private Syslog5424 syslog;

	public Syslog5424Bean() {
		beanPrefix = "syslog.";
	}
	
	public Syslog5424Bean(GenericRecord syslog) {
		setBean(Syslog5424.newBuilder((Syslog5424) syslog).build());
		beanPrefix = "syslog.";
	}

	@Override
	public Syslog5424 getBean() {
		return syslog;
	}

	@Override
	public void setBean(SpecificRecordBase syslog) {
		this.syslog = (Syslog5424) syslog;
	}

	@Override
	public String getHostname() {
		return syslog.getHostname();
	}
	
	@Override
	public void setHostname(String hostname) {
		syslog.setHostname(hostname);
	}

	@Override
	public String getRawKey() {
		return syslog.getApplicationName();
	}
	
	@Override
	public String getKey() {
		return super.getKey() + "_" + syslog.getPriority();
	}
	
	@Override
	public void setRawKey(String key) {
		syslog.setApplicationName(key);
	}

	@Override
	public Long getTimestamp() throws ParseException {
		return parseTimestamp(syslog.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		syslog.setTimestamp(timestamp.toString());
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.SYSLOG5424;
//	}
}
