package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.Tacacs;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;

public class TacacsBean extends SuperSyslog5424Bean {
	private static final long serialVersionUID = 1L;

	private Tacacs tacacs;

	public TacacsBean() {
		beanPrefix = "tacacs.";
	}

	public TacacsBean(GenericRecord tacacs) {
		setBean(Tacacs.newBuilder((Tacacs) tacacs).build());
		beanPrefix = "tacacs.";
	}

	@Override
	public Tacacs getBean() {
		return tacacs;
	}

	@Override
	public void setBean(SpecificRecordBase tacacs) {
		this.tacacs = (Tacacs) tacacs;
	}

	@Override
	public String getHostname() {
		return tacacs.getHostname();
	}
	
	@Override
	public void setHostname(String hostname) {
		tacacs.setHostname(hostname);
	}

	@Override
	public String getRawKey() {
		return tacacs.getApplicationName();
	}
	
	@Override
	public String getKey() {
		return super.getKey() + "_" + tacacs.getPriority();
	}
	
	@Override
	public void setRawKey(String key) {
		tacacs.setApplicationName(key);
	}

	@Override
	public Long getTimestamp() throws ParseException {
		return parseTimestamp(tacacs.getTimestamp());
	}

	@Override
	public void setTimestamp(Long timestamp) {
		tacacs.setTimestamp(timestamp.toString());
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.TACACS;
//	}
}
