package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.WebSphere;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.util.Map;

public class WebsphereBean extends DataBean {

	private static final long serialVersionUID = -6012545028851460685L;
	
	private WebSphere webSphere;
	
	public WebsphereBean() {
		beanPrefix = "";
	}
	
	public WebsphereBean(GenericRecord webSphere) {
		setBean(WebSphere.newBuilder((WebSphere) webSphere).build());
		beanPrefix = "";
	}
	
	@Override
	public SpecificRecordBase getBean() {
		return webSphere;
	}

	@Override
	public void setBean(SpecificRecordBase bean) {
		this.webSphere = (WebSphere) bean;
	}

	@Override
	public String getHostname() {
		return webSphere.getAppServerName();
	}

	@Override
	public void setHostname(String hostname) {
		webSphere.setAppServerName(hostname);
	}

	@Override
	public String getRawKey() {
		return "webSphere.lookupKey";
	}

	@Override
	public void setRawKey(String key) {}

	@Override
	public Long getTimestamp() throws ParseException {
		if (webSphere.getTimestamp().length() == 13)
			return Long.parseLong(webSphere.getTimestamp());
		else {
			return Long.parseLong(webSphere.getTimestamp().substring(0,10)) * 1000L;
		}
	}

	@Override
	public void setTimestamp(Long timestamp) {
		webSphere.setTimestamp(timestamp.toString());
	}

	@Override
	public Map<String, String> parseFields() {
		return null;
	}

//	@Override
//	public OpenbusTool getTool() {
//		return OpenbusTool.WEBSPHERE;
//	}
}
