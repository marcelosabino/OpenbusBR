package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.WebsphereMQ;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.text.ParseException;
import java.util.Map;

public class WebsphereMQBean extends DataBean {

    private WebsphereMQ bean;

    public WebsphereMQBean() {
        beanPrefix = "WebsphereMQ.";
    }

    public WebsphereMQBean(GenericRecord bean) {
        setBean(WebsphereMQ.newBuilder((WebsphereMQ) bean).build());
        beanPrefix = "WebsphereMQ.";
    }

    @Override
    public SpecificRecordBase getBean() {
        return bean;
    }

    @Override
    public void setBean(SpecificRecordBase bean) {
        this.bean = WebsphereMQ.class.cast(bean);
    }

    @Override
    public String getHostname() {
        return bean.getHostname();
    }

    @Override
    public void setHostname(String hostname) {
        this.bean.setHostname(hostname);
    }

    @Override
    public String getRawKey() {
        return "lookupKey";
    }

    @Override
    public void setRawKey(String key) {

    }

    @Override
    public Long getTimestamp() throws ParseException {
        return Long.valueOf(bean.getTimestamp());
    }

    @Override
    public void setTimestamp(Long timestamp) {
    }

    @Override
    public Map<String, String> parseFields() {
        return null;
    }

//    @Override
//    public OpenbusTool getTool() {
//        return OpenbusTool.WEBSPHERE_MQ;
//    }
}
