package br.com.produban.openbus.model.pojo;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.OpenbusTool;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.util.Map;

public class ZabbixAgentDataBean extends DataBean {
	private static final long serialVersionUID = 837610019632965440L;

	private ZabbixAgentData zabbixAgentData;

	public ZabbixAgentDataBean() {
		beanPrefix = "";
	}
	
	public ZabbixAgentDataBean(GenericRecord zabbixAgentData){
		setBean(ZabbixAgentData.newBuilder((ZabbixAgentData) zabbixAgentData).build());
		beanPrefix = "";
	}
	
	@Override
	public ZabbixAgentData getBean() {
		return zabbixAgentData;
	}
	
	@Override
	public void setBean(SpecificRecordBase zabbixAgentData) {
		this.zabbixAgentData = (ZabbixAgentData) zabbixAgentData;
	}

	@Override
	public String getHostname() {
		return zabbixAgentData.getHost();
	}
	
	@Override
	public void setHostname(String hostname) {
		zabbixAgentData.setHost(hostname);
	}

	@Override
	public String getRawKey() {
		return zabbixAgentData.getKey();
	}
	
	@Override
	public void setRawKey(String key) {
		zabbixAgentData.setKey(key);
	}
	
	@Override
	public Long getTimestamp() {
		if (zabbixAgentData.getClock().length() == 13)
			return Long.parseLong(zabbixAgentData.getClock());
		else {
			return Long.parseLong(zabbixAgentData.getClock().substring(0,10)) * 1000L;
		}
	}

	@Override
	public void setTimestamp(Long timestamp) {
		zabbixAgentData.setClock(timestamp.toString());
	}

	@Override
	public Map<String, String> parseFields() {
		return null;
	}

//	@Override
//	public OpenbusTool getTool() {
//		return getRawKey().startsWith("service_monitoring") ? OpenbusTool.SERVICE_MONITORING : OpenbusTool.ZABBIX;
//	}
}
