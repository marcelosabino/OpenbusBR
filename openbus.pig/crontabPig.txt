####################################################################
#
# Jobs de Load: Local FS -> HDFS
#
####################################################################
00 04 * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/cmdb_runMultipleCopies.sh" hdfs
05 04 * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/robos_runMultipleCopies.sh" hdfs
10 04 * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/ipToHost_runMultipleCopies.sh" hdfs


####################################################################
#
# Jobs de Load do Pig: Hdfs -> Redis
#
####################################################################
15 04 * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/cmdb_runUpdateRedis.sh" hdfs
20 04 * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/robos_runUpdateRedis.sh" hdfs
25 04 * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/ipToHost_runUpdateRedis.sh" hdfs


####################################################################
#
# Jobs de Load do Pig: Hdfs -> Hdfs
#
####################################################################
15 * * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/splunk_runMultipleFormatters.sh" hdfs


####################################################################
#
# Jobs de Threshold: Pig: hdfs -> hbase
#
####################################################################
#10 * * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; /produtos/openbus.pig/scripts/threshold_runMultipleAggregateMetrics.sh" hdfs
33 * * * * su --shell=/bin/bash --session-command="export JAVA_HOME=/usr/java/jdk1.7.0_51; export HBASE_HOME=/usr/lib/hbase; /produtos/openbus.pig/scripts/threshold_runMultipleAggregateMetrics.sh" hdfs
