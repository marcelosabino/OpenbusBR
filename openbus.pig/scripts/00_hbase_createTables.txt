# ==================================================
# Tabelas do HBase
# *valido atualmente
# ==================================================

disable 'PROCESSED_METRICS'
drop 'PROCESSED_METRICS'
create 'PROCESSED_METRICS', {NAME => 'metadata'}, {NAME => 'values'}

disable 'LINEAR_REGRESSION'
drop 'LINEAR_REGRESSION'
create 'LINEAR_REGRESSION', {NAME => 'metadata'}, {NAME => 'coefficients'}

disable 'PROJECTION'
drop 'PROJECTION'
create 'PROJECTION', {NAME => 'metadata'}, {NAME => 'values'}



# ==================================================
# historico, nao aplicavel
# ==================================================


#====================
# PROCESSED_METRICS:
#                 | metada:           | values:      |
# ----------------------------------------------------
# TSFiltrado_Hash | hostname -> valor | TS0 -> Valor |
#                 | metrica -> valor  | TS1 -> Valor |
#                 |                   | ...          |


#====================
# LINEAR_REGRESSION:
#          | system.cpu.   | system.mem.   | system.disk.  | system.cpu.   | system.mem.   | system.disk.  | system.cpu.   | system.mem.   | system.disk.  |
#          |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |
#          |  _60DAYS      |  _60DAYS      |  _60DAYS      |  _30DAYS      |  _30DAYS      |  _30DAYS      |  _10DAYS      |  _10DAYS      |  _10DAYS      |
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# host0.TS | b -> Valor    | b -> Valor    | b -> Valor    | b -> Valor    | b -> Valor    | b -> Valor    | b -> Valor    | b -> Valor    | b -> Valor    |
#          | m -> Valor    | m -> Valor    | m -> Valor    | m -> Valor    | m -> Valor    | m -> Valor    | m -> Valor    | m -> Valor    | m -> Valor    |
#          | ...           | ...           | ...           | ...           | ...           | ...           | ...           | ...           | ...           |

disable 'LINEAR_REGRESSION'
drop 'LINEAR_REGRESSION'
create 'LINEAR_REGRESSION', {NAME => 'system.cpu.utilization_60DAYS'}, {NAME => 'system.mem.utilization_60DAYS'}, {NAME => 'system.disk.utilization_60DAYS'}, {NAME => 'system.cpu.utilization_30DAYS'}, {NAME => 'system.mem.utilization_30DAYS'}, {NAME => 'system.disk.utilization_30DAYS'}, {NAME => 'system.cpu.utilization_10DAYS'}, {NAME => 'system.mem.utilization_10DAYS'}, {NAME => 'system.disk.utilization_10DAYS'}

#====================
# PROJECTION:
#          | system.cpu.   | system.mem.   | system.disk.  | system.cpu.   | system.mem.   | system.disk.  | system.cpu.   | system.mem.   | system.disk.  |
#          |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |  utilization  |
#          |  _6DAYS       |  _6DAYS       |  _6DAYS       |  _3DAYS       |  _3DAYS       |  _3DAYS       |  _1DAYS       |  _1DAYS       |  _1DAYS       |
# ----------------------------------------------------------------------------------------------------------------------------------------------------------
# host0.TS | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  | TS0 -> Valor  |
#          | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  | TS1 -> Valor  |
#          | ...           | ...           | ...           | ...           | ...           | ...           | ...           | ...           | ...           |

disable 'PROJECTION'
drop 'PROJECTION'
create 'PROJECTION', {NAME => 'system.cpu.utilization_6DAYS'}, {NAME => 'system.mem.utilization_6DAYS'}, {NAME => 'system.disk.utilization_6DAYS'}, {NAME => 'system.cpu.utilization_3DAYS'}, {NAME => 'system.mem.utilization_3DAYS'}, {NAME => 'system.disk.utilization_3DAYS'}, {NAME => 'system.cpu.utilization_1DAYS'}, {NAME => 'system.mem.utilization_1DAYS'}, {NAME => 'system.disk.utilization_1DAYS'}

