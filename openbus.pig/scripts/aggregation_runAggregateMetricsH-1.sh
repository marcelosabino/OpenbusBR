startTS=`date +"%s"`
#####
# Rodar: ./aggregation_runAggregateMetricsH-1.sh
echo "> Running: ./aggregation_runAggregateMetricsH-1.sh $*"
echo ""
if [ "$1" == "" ]
then
  tenant='BR'
else
  tenant=$1
fi
topicPath=`echo $tenant"_metrics.ZabbixAgentData"`
script=aggregation_scriptAggregateMetricsH-1.pig
tabela=METRICS_AGGREGATED
if [ "$2" == "" ]
then
  clock=`date "+%s"`
else
  clock=$2
fi

#####
#
echo "HDFS -->"
echo "   "$topicPath
echo ""
echo "--> HBase Table"
echo "   "$tabela
echo ""


#####
# calculo do "Anterior"
t1=$(( $clock - 3600 ))
t2=`date -d @$t1 "+%Y-%m-%d %H:00:00"`
inicio=`date -d "$t2" "+%s"`
pastaAnterior=`date -d @$t1 "+%Y/%m/%d/%H"`
#
echo Anterior
echo "   Pasta: "$pastaAnterior
echo "   "$inicio
echo ""


#####
# calculo do "Atual"
t1=$clock
t2=`date -d @$t1 "+%Y-%m-%d %H:00:00"`
fim=`date -d "$t2" "+%s"`
pastaAtual=`date -d @$fim "+%Y/%m/%d/%H"`
#
echo Atual
echo "   Pasta: "$pastaAtual
echo "   "$fim
echo ""


#####
# PIG
export PIG_USER_CLASSPATH_FIRST=true
/produtos/openbus.pig/bin/pig -P /produtos/openbus.pig/scripts/custom.properties \
    -param tenant=$tenant \
    -param topicSchema=$topicPath \
    -param pastaAnterior=$pastaAnterior \
    -param pastaAtual=$pastaAtual \
    -param inicio=$inicio \
    -param fim=$fim \
    -param tabela=$tabela \
    -x mapreduce /produtos/openbus.pig/scripts/$script

endTS=`date +"%s"`
echo "Tempo de processamento: "$(($endTS - $startTS))" segundos"

