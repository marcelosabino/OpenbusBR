SET mapreduce.job.user.classpath.first true;
SET mapreduce.fileoutputcommitter.marksuccessfuljobs false;

/* Define UDF */
DEFINE Generate10DigitClock br.com.produban.openbus.pig.udf.eval.Generate10DigitClock();
DEFINE FilterKey br.com.produban.openbus.pig.udf.filter.FilterKeys('$tenant');
DEFINE FormatterZabbix br.com.produban.openbus.pig.udf.eval.FormatterZabbixAgentData('$tenant');
DEFINE P50 datafu.pig.stats.Quantile('0.5');
DEFINE P75 datafu.pig.stats.Quantile('0.75');
DEFINE P85 datafu.pig.stats.Quantile('0.85');
DEFINE P95 datafu.pig.stats.Quantile('0.95');
DEFINE VAR datafu.pig.stats.VAR();
DEFINE BagConcat datafu.pig.bags.BagConcat();  

/* Var */
%declare HDFS_PATH '/openbus/data/$topicSchema/hourly/$currentFolder/*.avro'

/* Script */
avro = LOAD '$HDFS_PATH' USING AvroStorage();
-- DUMP avro;
 DESCRIBE avro;


avroSeconds = FOREACH avro GENERATE
   host,
   Generate10DigitClock((long)clock) as clock:long,
   key,
   value;
-- DUMP avroSeconds;
 DESCRIBE avroSeconds;


/* NUMERIC:
 *    - somente valor numerico
 *    - com Timestamp (clock) dentro do intervalo
 *    - FilterKey: chaves que tem regras de transformacao
 *    - IsValidHour: filtra hora valida pelo perfil do servidor
 *
numeric = FILTER avroSeconds BY
    NOT (value MATCHES '.*[a-zA-Z].*') AND NOT (value MATCHES '.*\\..*\\..*')
AND (host IS NOT NULL) AND (clock IS NOT NULL) AND (key IS NOT NULL) AND (value IS NOT NULL)
AND ((long)clock >= (long)$inicio) AND ((long)clock < (long)$fim)
AND FilterKey(key)
;*/
-- DUMP numeric;
-- DESCRIBE numeric;


/* Normalizar: */
normalized = FOREACH avroSeconds {
    temp = FormatterZabbix(host, (chararray)(long)(FLOOR((long)clock/3600L)*3600L), key, value);
    GENERATE 
        '$tenant' as tenant,
        FLATTEN(temp) as (uid:chararray, key:chararray, hour:chararray, value:float, tags:bag{T:tuple(tagK:chararray,tagV:chararray)} );
}
-- DUMP normalized;
 DESCRIBE normalized;

floatToDouble = FOREACH normalized GENERATE
   tenant,
   uid,
   key,
   hour,
   (double)value as value:double,
   tags;
-- DUMP floatToDouble;
 DESCRIBE floatToDouble;


/* Filtrar uid's nulos */
filtered = FILTER floatToDouble BY uid IS NOT NULL;
-- DUMP filtered;
 DESCRIBE filtered;


/* Agregar valores */
all_metrics_per_uid = GROUP filtered BY (tenant, uid, key, hour);
-- DUMP all_metrics_per_uid;
 DESCRIBE all_metrics_per_uid;


aggregate_per_uid = FOREACH all_metrics_per_uid {
    sorted = ORDER filtered BY value;
    GENERATE
       CONCAT(CONCAT(CONCAT((chararray)group.hour, (chararray)'_'), CONCAT((chararray)group.tenant, '_')), (chararray)group.uid) as rowKey,
       group.tenant as tenant, 
       group.uid as uid,
       group.key as key,
       CONCAT('',(chararray)group.hour) as hour,
       BagConcat(sorted.tags) as tags,
       MAX(sorted.value) as vMax:double,
       MIN(sorted.value) as vMin:double,
       AVG(sorted.value) as vAvg:double,
       COUNT(sorted.value) as vCount:long,
       FLATTEN(P50(sorted.value)) as vP50:double,
       FLATTEN(P75(sorted.value)) as vP75:double,
       FLATTEN(P85(sorted.value)) as vP85:double,
       FLATTEN(P95(sorted.value)) as vP95:double,
       SQRT(ABS(VAR(sorted.value))) as vDev:double;
}
-- DUMP aggregate_per_uid;
 DESCRIBE aggregate_per_uid;


flatTags = FOREACH aggregate_per_uid GENERATE
   rowKey,
   tenant,
   uid,
   key,
   hour,
   FLATTEN(tags),
   vMax,
   vMin,
   vAvg,
   vCount,
   vP50,
   vP75,
   vP85,
   vP95,
   vDev;
-- DUMP flatTags;
 DESCRIBE flatTags;


distinctTags = DISTINCT flatTags;
-- DUMP distinctTags;
 DESCRIBE distinctTags;



/* STORE:
 *    - Salvar em HBase:THRESHOLD_TEMP
 *       TS_
 *
 */
map_agg = FOREACH distinctTags
   GENERATE
      rowKey as rowKey,
      tenant as tenant,
      uid as uid,
      key as key,
      hour as hour,
      TOMAP(tags::tagK, tags::tagV) as tag,
      vMax as vMax,
      vMin as vMin,
      vAvg as vAvg,
      vCount as vCount,
      vP50 as vP50,
      vP75 as vP75,
      vP85 as vP85,
      vP95 as vP95,
      vDev as vDev;
 -- DUMP map_agg;
 DESCRIBE map_agg;

/* Store */
STORE map_agg INTO 'hbase://$HBaseTable' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:tenant metadata:uid metadata:key metadata:hour tags:* aggregations:max aggregations:min aggregations:avg aggregations:count aggregations:p50 aggregations:p75 aggregations:p85 aggregations:p95 aggregations:dev');