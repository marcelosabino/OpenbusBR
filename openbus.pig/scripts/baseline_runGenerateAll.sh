echo "--------------------------------------"  > /produtos/openbus.pig/logs/baseline_generate_$1.log
echo "   Baseline baseado no dia da semana"   >> /produtos/openbus.pig/logs/baseline_generate_$1.log
echo "--------------------------------------" >> /produtos/openbus.pig/logs/baseline_generate_$1.log
/produtos/openbus.pig/scripts/baseline_runGenerateBaseline.sh $1 >> /produtos/openbus.pig/logs/baseline_generate_$1.log 2>&1

echo "--------------------------------------" >> /produtos/openbus.pig/logs/baseline_generate_$1.log
echo "   Baseline baseado no N dia util"      >> /produtos/openbus.pig/logs/baseline_generate_$1.log
echo "--------------------------------------" >> /produtos/openbus.pig/logs/baseline_generate_$1.log
/produtos/openbus.pig/scripts/baseline_runGenerateNthBusinessDay.sh $1 5 >> /produtos/openbus.pig/logs/baseline_generate_$1.log 2>&1

echo "--------------------------------------" >> /produtos/openbus.pig/logs/baseline_generate_$1.log
