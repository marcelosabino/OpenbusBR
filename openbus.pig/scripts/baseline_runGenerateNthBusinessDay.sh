startTS=`date +"%s"`
#####
# Rodar: ./baseline_runGenerateNthBusinessDay.sh
echo "> Running: ./baseline_runGenerateNthBusinessDay.sh $*"
echo ""
#
EnesimoDiaUtil=5
percentSuperiorWarning=75
percentSuperiorCritical=150
# inferior < 100
percentInferiorWarning=50
percentInferiorCritical=75
#
script=baseline_scriptGenerateNthBusinessDay.pig
hbaseTable=METRICS_AGGREGATED
hbaseBaseline=METRICS_BASELINE
zkURI=srvbigpvlbr04:2181:/hbase-0.98
zkBatchSize=5000

if [ "$1" == "" ]
then
  tenant='BR'
else
  tenant=$1
fi

if [ "$2" == "" ]
then
  EnesimoDiaUtil=5
else
  EnesimoDiaUtil=$2
fi


h2Schema=`echo $tenant"_BASELINE_DB"`
h2URI=`echo "jdbc:h2:tcp://180.239.144.185:9092/repository/database/"$h2Schema";DB_CLOSE_ON_EXIT=FALSE"`
h2User=openbus
h2Pswd=openbus

if [ "$3" == "" ]
then
  today=`date "+%s"`
else
  today=$3
fi


#####
#
echo "HBase Table Source -->"
echo "   "$hbaseTable
echo ""
echo "--> HBase Table Destiny"
echo "   "$hbaseBaseline
echo "--> H2 Destiny"
echo "   "$h2URI
echo "   "$h2User"/"$h2Pswd
echo "   "$h2Schema
echo ""


#####
# PIG
/produtos/openbus.pig/bin/pig -P /produtos/openbus.pig/scripts/custom.properties \
    -param tenant=$tenant \
    -param hbaseTable=$hbaseTable \
    -param hbaseBaseline=$hbaseBaseline \
    -param zkURI=$zkURI \
    -param zkBatchSize=$zkBatchSize \
    -param today=$today \
    -param diaUtil=$EnesimoDiaUtil \
    -param pSupWarning=$percentSuperiorWarning \
    -param pSupCritical=$percentSuperiorCritical \
    -param pInfWarning=$percentInferiorWarning \
    -param pInfCritical=$percentInferiorCritical \
    -param h2URI=$h2URI \
    -param h2User=$h2User \
    -param h2Pswd=$h2Pswd \
    -param h2Schema=$h2Schema \
    -x mapreduce /produtos/openbus.pig/scripts/$script

endTS=`date +"%s"`
echo "Tempo de processamento: "$(($endTS - $startTS))" segundos"


