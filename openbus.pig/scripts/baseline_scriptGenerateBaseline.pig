/* ********** LIBs ********** */
REGISTER '/produtos/openbus.pig/lib/zookeeper-3.4.5.jar'
REGISTER '/produtos/openbus.pig/lib/h2/*.jar'
REGISTER '/produtos/openbus.pig/contrib/piggybank/java/piggybank.jar'
REGISTER '/produtos/openbus.pig/scripts/datafu-1.2.0.jar'
REGISTER '/produtos/openbus.pig/scripts/openbus.model-0.0.2-SNAPSHOT.jar'
REGISTER '/produtos/openbus.pig/scripts/openbus.pig-0.0.1-SNAPSHOT-shaded.jar'
REGISTER '/produtos/phoenix/phoenix-4.3.1-client.jar'
REGISTER '/produtos/openbus.pig/scripts/h2-1.2.140.jar'


/* ********** UDFs ********** */
DEFINE IsSameWeekDay        br.com.produban.openbus.pig.udf.filter.IsSameWeekDay();
DEFINE FormatDate           br.com.produban.openbus.pig.udf.eval.FormatDate();
DEFINE TruncateDate         br.com.produban.openbus.pig.udf.eval.TruncateDate();
DEFINE P85                  datafu.pig.stats.Quantile('0.85');


/* *********************************** */
/* ********** LOAD & FILTER ********** */


hbase = LOAD 'hbase://$hbaseTable'
   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:tenant metadata:uid metadata:key metadata:hour tags:* aggregations:max aggregations:min aggregations:avg aggregations:p50 aggregations:dev', '-loadKey true -regex=$tenant.*')
   as (rowKey, tenant:chararray, uid:chararray, key:chararray, hour:long, tags:map[], max:double, min:double, avg:double, p50:double, dev:double);
-- DUMP hbase;
-- DESCRIBE hbase;



/* Filter service_monitoring.latency AND same weekday*/
filtered = FILTER hbase BY 
   (key == 'service_monitoring.latency')
   AND IsSameWeekDay($today,hour);
-- DUMP filtered;
-- DESCRIBE filtered;


hourly = FOREACH filtered GENERATE
   tenant as tenant:chararray,
   uid as uid:chararray,
   hour as clock,
   FormatDate('HH',hour) as hour:chararray,
   p50 as value:double
   ;
 DUMP hourly;
 DESCRIBE hourly;


groupHour = GROUP hourly BY (tenant, uid, hour);
-- DUMP groupHour;
-- DESCRIBE groupHour;


base = FOREACH groupHour GENERATE
   group.tenant as tenant:chararray,
   group.uid as uid:chararray,
   group.hour as hour:chararray,
   (TruncateDate('yyyy-MM-dd', (long)$today) + ((long)group.hour * 3600)) as startTS:long,
   (TruncateDate('yyyy-MM-dd', (long)$today) + (((long)group.hour + 1) * 3600)) as endTS:long,
   FLATTEN(P85(hourly.value)) as baseValue:double;
-- DUMP base;
-- DESCRIBE base;


baseline = FOREACH base GENERATE
   tenant as tenant:chararray,
   uid as uid:chararray,
   hour as hour:chararray,
   (startTS * (long)1000) as startTS:long,
   (endTS * (long)1000) as endTS:long,
   (baseValue * (100 + $pSupCritical) / 100) as supCritical:double,
   (baseValue * (100 + $pSupWarning) / 100) as supWarning:double,
   baseValue as baseValue:double,
   (baseValue * (100 - $pInfWarning) / 100) as infWarning:double,
   (baseValue * (100 - $pInfCritical) / 100) as infCritical:double
;
-- DUMP baseline;
-- DESCRIBE baseline;


/* STORE */

STORE baseline INTO 'hbase://$hbaseBaseline'
   USING org.apache.phoenix.pig.PhoenixHBaseStorage('$zkURI', '-batchSize $zkBatchSize');


STORE baseline INTO 'unused' 
   USING org.apache.pig.piggybank.storage.DBStorage(
      'org.h2.Driver',
      '$h2URI',
      'openbus',
      'openbus',
      'MERGE INTO $h2Schema.BASELINE KEY(tenant, uid, hour) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
   );



