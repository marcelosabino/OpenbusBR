export HBASE_HOME=/produtos/hbase

now=`date "+%s"`

for (( i=1; i<=$1; i++ ))
do
  echo "iteracao: $i"
  echo "./aggregation_runAggregateMetricsH-1.sh $2 $(( $now - $i * 3600 ))"
  ./aggregation_runAggregateMetricsH-1.sh $2 $(( $now - $i * 3600 ))
done


