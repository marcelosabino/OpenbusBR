startTS=`date +"%s"`
#####
# Rodar: ./capacity_runGenerateCapacity.sh
echo "> Running: ./capacity_runGenerateCapacity.sh $*"
echo ""
regression=90
projection=90

script=capacity_scriptGenerateCapacity.pig
hbaseTable=METRICS_AGGREGATED
hbaseInfo=METRICS_INFO
hbaseThresholds=METRICS_THRESHOLDS
hbaseLinReg=METRICS_LINREG
hbaseAlerts=METRICS_ALERTS
zkURI=srvbigpvlbr04:2181:/hbase-0.98
zkBatchSize=5000

if [ "$1" == "" ]
then
  tenant='BR'
else
  tenant=$1
fi


#####
#
echo "HBase Table Source -->"
echo "   "$hbaseTable
echo ""
echo "--> HBase Table Destinies"
echo "   "$hbaseInfo
echo "   "$hbaseThresholds
echo "   "$hbaseLinReg
echo "   "$hbaseAlerts
echo ""
echo "ZooKeeper URI:
echo "   "$zkURI
echo "   Batch: "$zkBatchSize
echo ""

#####
clock=`date "+%s"`

# calculo de T-0 e Today

#####
# T-0
t1=$(( $clock - $(($regression * 24 * 3600)) ))
t2=`date -d @$t1 "+%Y-%m-%d"`
t_0=$(( `date -d "$t2" "+%s"` ))
#
echo T-0
echo "   "`date -d @$t_0 +"%Y/%m/%d"`
echo "   "$t_0
echo ""


#####
# Today
t1=$clock
t2=`date -d @$t1 "+%Y-%m-%d"`
today=`date -d "$t2" "+%s"`
#
echo Today
echo "   "`date -d @$today +"%Y/%m/%d"`
echo "   "$today
echo ""

#####
# Days
echo "   Regression: "$regression" days"
echo "   Projection: "$projection" days"
echo ""


#####
# PIG
/produtos/openbus.pig/bin/pig -P /produtos/openbus.pig/scripts/custom.properties \
    -param tenant=$tenant \
    -param hbaseTable=$hbaseTable \
    -param hbaseInfo=$hbaseInfo \
    -param hbaseThresholds=$hbaseThresholds \
    -param hbaseLinReg=$hbaseLinReg \
    -param hbaseAlerts=$hbaseAlerts \
    -param zkURI=$zkURI \
    -param zkBatchSize=$zkBatchSize \
    -param t_0=$t_0 \
    -param today=$today \
    -param days=$projection \
    -x mapreduce /produtos/openbus.pig/scripts/$script

endTS=`date +"%s"`
echo "Tempo de processamento: "$(($endTS - $startTS))" segundos"


