/* UDF's */
REGISTER '$basePath/contrib/piggybank.jar'

/* Avro + Json */
REGISTER '$basePath/json-simple-1.1.jar'
REGISTER '$basePath/guava-15.0.jar'

/* HBase */
REGISTER '$basePath/zookeeper-3.4.5.jar'
REGISTER '$basePath/hbase-server-0.98.6-hadoop2.jar'

/* My UDFs */
REGISTER '$basePath/datafu-1.2.0.jar'
REGISTER '$basePath/openbus.model-0.0.2-SNAPSHOT.jar'
REGISTER '$basePath/openbus.pig-0.1.0-SNAPSHOT-shaded.jar'

/* ********** LIBs ********** */
REGISTER '$basePath/phoenix-4.3.1-client.jar'


/* ********** UDFs ********** */
DEFINE IsValidHour          br.com.produban.openbus.pig.udf.filter.IsValidHour('$tenant');
DEFINE BagToMap             br.com.produban.openbus.pig.udf.eval.BagToMap();
DEFINE CalculateThresholds  br.com.produban.openbus.pig.udf.eval.CalculateThresholds('$tenant');
DEFINE GenerateAlerts       br.com.produban.openbus.pig.udf.eval.GenerateAlerts();
DEFINE LinReg               br.com.produban.openbus.pig.udf.eval.LinRegLongDouble('$tenant');
DEFINE MapToBagLD           br.com.produban.openbus.pig.udf.eval.MapToBagLongDouble();
DEFINE MapToBag             br.com.produban.openbus.pig.udf.eval.MapToBag();
DEFINE ProjectValues        br.com.produban.openbus.pig.udf.eval.ProjectValues();
DEFINE TruncateDate         br.com.produban.openbus.pig.udf.eval.TruncateDate();
DEFINE BagConcat            datafu.pig.bags.BagConcat();
DEFINE Intersect            datafu.pig.sets.SetIntersect();


/* *********************************** */
/* ********** LOAD & FILTER ********** */

-- USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:tenant metadata:uid metadata:key metadata:hour tags:* aggregations:max aggregations:min aggregations:avg aggregations:p50 aggregations:dev', '-loadKey true -regex=$tenant.*')
hbase = LOAD 'hbase://$hbaseTable'
   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:tenant metadata:uid metadata:key metadata:hour tags:* aggregations:max aggregations:min aggregations:avg aggregations:p50 aggregations:p75 aggregations:p85 aggregations:p95 aggregations:dev', '-loadKey true -gte $tenant')
   as (rowKey, tenant:chararray, uid:chararray, key:chararray, hour:long, tags:map[], max:double, min:double, avg:double, p50:double, dev:double);
-- DUMP hbase;
 DESCRIBE hbase;


/* Filter invalid hours */
filtered = FILTER hbase BY IsValidHour(key, hour, tags);
-- DUMP filtered;
DESCRIBE filtered;


/* ******************************** */
/* ********** INFO EXTRA ********** */

repeatedInfo = FOREACH filtered GENERATE
   CONCAT(CONCAT(tenant, '_'), uid) as rowKey,
   tenant,
   uid,
   key,
   tags;
-- DUMP repeatedInfo;
 DESCRIBE repeatedInfo;

saveInfo = DISTINCT repeatedInfo;
-- DUMP saveInfo;
 DESCRIBE saveInfo;


/* STORE */

--STORE saveInfo INTO 'hbase://$hbaseTableDst'
--   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:tenant metadata:uid metadata:key tags:*');


flatInfo = FOREACH saveInfo GENERATE
   rowKey,
   uid,
   key,
   FLATTEN(MapToBag(tags)) as (tagK:chararray,tagV:chararray);
-- DUMP flatInfo;
 DESCRIBE flatInfo;

phoenixInfo = FOREACH flatInfo GENERATE
   rowKey,
   uid,
   key,
   CONCAT('',tagK) as tagK:chararray,
   CONCAT('',tagV) as tagV:chararray;
 DUMP phoenixInfo;
 DESCRIBE phoenixInfo;

/* ******************************** */
/* ********** THRESHOLDS ********** */

/* Map To Bag */
tags_mapToBag = FOREACH filtered GENERATE
   tenant,
   uid,
   key,
   hour,
   max,
   min,
   avg,
   p50,
   dev
;
-- DUMP tags_mapToBag;
-- DESCRIBE tags_mapToBag;


/* Aggregate values */
grouped = GROUP tags_mapToBag BY (tenant, uid, key);

agg = FOREACH grouped GENERATE
   group.tenant as tenant,
   group.uid as uid,
   group.key as key,
   MAX(tags_mapToBag.max) as max,
   MIN(tags_mapToBag.min) as min,
   AVG(tags_mapToBag.avg) as avg,
   AVG(tags_mapToBag.p50) as p50,
   AVG(tags_mapToBag.dev) as dev
;
-- DUMP agg;
-- DESCRIBE agg;


flatTags = FOREACH (
              JOIN agg BY (tenant, uid),
			  saveInfo BY (tenant, uid)
           ) GENERATE
   agg::tenant as tenant,
   agg::uid as uid,
   agg::key as key,
   MapToBag(tags) as tags:bag{T:tuple(tagK:chararray,tagV:chararray)},
   agg::max as max,
   agg::min as min,
   agg::avg as avg,
   agg::p50 as p50,
   agg::dev as dev
;
-- DUMP flatTags;
-- DESCRIBE flatTags;


/* create thresholds */
thresholds = FOREACH flatTags GENERATE
   FLATTEN(CalculateThresholds(CONCAT(tenant,'_',uid), key, tags, max, min, avg, p50, dev))
   AS (
      rowKey:chararray,
      key:chararray,
      type:chararray,
      values:bag{
         T:tuple(
            tName:chararray,
            tValue:double
         )
      }
   );
-- DUMP thresholds;
-- DESCRIBE thresholds;

--saveThresholds = FOREACH thresholds GENERATE
--   rowKey as rowKey:chararray,
--   type as type:chararray,
--   BagToMap(values) as tMap:map[];
-- DUMP saveThresholds;
-- DESCRIBE saveThresholds;datafu.pig.bags.BagConcat


/* STORE */

--STORE saveThresholds INTO 'hbase://$hbaseTableDst'
--   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:type thresholds:*');


phoenixThresholds = FOREACH thresholds GENERATE
   rowKey as rowKey:chararray,
   type as type:chararray,
   FLATTEN(values) as (level:chararray, value:double);
 DUMP phoenixThresholds;
 DESCRIBE phoenixThresholds;

STORE phoenixThresholds INTO 'hbase://$hbaseThresholds'
   USING org.apache.phoenix.pig.PhoenixHBaseStorage('$zkURI', '-batchSize $zkBatchSize');


/* *************************************** */
/* ********** LINEAR REGRESSION ********** */

/* Create bag of timestamp+AVG(values), where timestamp is "day based"  */

prepareToGroup = FOREACH filtered GENERATE
   CONCAT(tenant, '_', uid) as rowKey:chararray,
   key as key:chararray,
   TruncateDate('yyyy-MM-dd', hour) as date:long,
   avg as value:double;
-- DUMP prepareToGroup;
-- DESCRIBE prepareToGroup;

grouped = GROUP prepareToGroup BY (rowKey, key, date);


/* AVG by Day  */

avgDay = FOREACH grouped GENERATE
      group.rowKey as rowKey:chararray,
      group.key as key:chararray,
      TOBAG( TOTUPLE(group.date,AVG(prepareToGroup.value)) )
        as singleValue:bag{T:tuple(date:long, value:double)}
;
-- DUMP avgDay;
-- DESCRIBE avgDay;


groupedAvg = GROUP avgDay BY (rowKey, key);
-- DUMP groupedAvg;
-- DESCRIBE groupedAvg;


bagAvg = FOREACH groupedAvg GENERATE
   group.rowKey as rowKey:chararray,
   group.key as key:chararray,
   BagConcat(avgDay.singleValue) as values:bag{T:tuple(date:long, value:double)}
;
-- DUMP bagAvg;
-- DESCRIBE bagAvg;



/* Linear Regression */

lrCoef = FOREACH bagAvg GENERATE
   CONCAT((chararray)rowKey, '') as rowKey:chararray,
   CONCAT((chararray)key, '') as key:chararray,
   FLATTEN(LinReg(values)) as (m:double, b:double);
-- DUMP lrCoef;
-- DESCRIBE lrCoef;


saveLinReg = FOREACH lrCoef GENERATE
   rowKey as rowKey:chararray,
   m as m:double,
   b as b:double;
 DUMP saveLinReg;
 DESCRIBE saveLinReg;

 
/* Store */

--STORE saveLinReg INTO 'hbase://$hbaseTableDst'
--   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('coefficients:m coefficients:b');

STORE saveLinReg INTO 'hbase://$hbaseLinReg'
   USING org.apache.phoenix.pig.PhoenixHBaseStorage('$zkURI', '-batchSize $zkBatchSize');


/* **************************** */
/* ********** ALERTS ********** */

/* Projection */
proj = FOREACH lrCoef GENERATE rowKey, key, ProjectValues((long)$today, (long)$days, m, b) as values:bag{T:tuple(date:long, value:double)};
-- DUMP proj;
-- DESCRIBE proj;


/* compare thresholds */

joined = FOREACH (
            JOIN
               proj BY (rowKey, key),
               thresholds BY (rowKey, key)
         ) GENERATE
   proj::rowKey as rowKey,
   proj::key as key,
   thresholds::type as type:chararray,
   thresholds::values as tKV:bag{T:tuple(level:chararray, value:double)},
   proj::values as projValues:bag{T:tuple(date:long, value:double)};
-- DUMP joined;
-- DESCRIBE joined;


alerts = FOREACH joined GENERATE
   rowKey as rowKey:chararray,
   key as key:chararray,
   type as type:chararray,
   GenerateAlerts(type, tKV, projValues) as alertList:bag{T:tuple(level:chararray, date:long)};
   -- BagToMap(GenerateAlerts(type, tKV, projValues)) as alertList:map[];
-- DUMP alerts;
-- DESCRIBE alerts;


validAlerts = FILTER alerts BY NOT IsEmpty(alertList);
-- DUMP validAlerts;
-- DESCRIBE validAlerts;

 
--saveAlerts = FOREACH validAlerts GENERATE
--   rowKey as validAlerts:chararray,
--   alertList as alertList:map[];
-- DUMP saveAlerts;
-- DESCRIBE saveAlerts;

 
/* Store */

--STORE saveAlerts INTO 'hbase://$hbaseTableDst'
--   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('alerts:*');


phoenixAlerts = FOREACH validAlerts GENERATE
   rowKey as rowKey:chararray,
   FLATTEN(alertList) as (level:chararray, date:long);
 DUMP phoenixAlerts;
 DESCRIBE phoenixAlerts;

STORE phoenixAlerts INTO 'hbase://$hbaseAlerts'
   USING org.apache.phoenix.pig.PhoenixHBaseStorage('$zkURI', '-batchSize $zkBatchSize');


