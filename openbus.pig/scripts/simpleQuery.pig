/* UDF's */
REGISTER '/produtos/openbus.pig/contrib/piggybank/java/piggybank.jar'

/* Avro + Json */
REGISTER '/produtos/openbus.pig/lib/json-simple-1.1.jar'
REGISTER '/produtos/openbus.pig/lib/avro-1.7.5.jar'
REGISTER '/produtos/openbus.pig/lib/avro-tools-1.7.5-nodeps.jar'
REGISTER '/produtos/openbus.pig/lib/guava-11.0.jar'

/* HBase */
REGISTER '/produtos/openbus.pig/lib/zookeeper-3.4.5.jar'
REGISTER '/produtos/openbus.pig/lib/h2/*.jar'

/* My UDFs */
REGISTER '/produtos/openbus.pig/scripts/openbus.model-0.0.2-SNAPSHOT.jar'
REGISTER '/produtos/openbus.pig/scripts/openbus.pig-0.0.1-SNAPSHOT-shaded.jar'
DEFINE IsValidKey   br.com.produban.openbus.pig.udf.filter.FilterKeys('$tenantId');
DEFINE FormatZabbix br.com.produban.openbus.pig.udf.eval.FormatterZabbixTupleToBag('$tenantId');
DEFINE AvroWriter org.apache.pig.builtin.AvroStorage('ZabbixAgentData', '-schemafile /openbus/avro_schema/zabbix-schema.avsc');
SET mapred.output.compress true
SET avro.output.codec deflate



/* AVRO */
avro = LOAD '/openbus/data/$tenantPrefix/hourly/{$pastas}/*.avro' USING AvroStorage();
-- DUMP avro;
-- DESCRIBE avro;


/* NUMERIC */
filteredByTS = FILTER avro BY
    NOT (value MATCHES '.*[a-zA-Z].*') AND NOT (value MATCHES '.*\\..*\\..*')
AND (host IS NOT NULL) AND (clock IS NOT NULL) AND (key IS NOT NULL) AND (value IS NOT NULL)
AND (key MATCHES '$filterKey')
;
-- DUMP filteredByTS;
-- DESCRIBE filteredByTS;


printRawAvro = FOREACH filteredByTS GENERATE
   'raw' as type,
   host,
   key,
   value,
   clock
;
 DUMP printRawAvro;
 DESCRIBE printRawAvro;




/* FORMATTED AVRO */
formattedAvro = FOREACH filteredByTS GENERATE FLATTEN( FormatZabbix(
		host,
		host_metadata,
		key,
		value,
		lastlogsize,
		mtime,
		timestamp,
		source,
		severity,
		eventid,
		state,
		clock,
		ns) ) AS (
                host          : chararray,
                host_metadata : chararray,
                key           : chararray,
                value         : chararray,
                lastlogsize   : chararray,
                mtime         : chararray,
                timestamp     : chararray,
                source        : chararray,
                severity      : chararray,
                eventid       : chararray,
                state         : chararray,
                clock         : chararray,
                ns            : chararray
		);
-- DUMP formattedAvro;
-- DESCRIBE formattedAvro;


printNewAvro = FOREACH formattedAvro GENERATE
   'new' as type,
   host,
   key,
   value,
   clock
;
 DUMP printNewAvro;
 DESCRIBE printNewAvro;


