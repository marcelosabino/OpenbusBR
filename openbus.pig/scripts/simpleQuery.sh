#####
# Rodar: ./simpleQuery.sh
echo "> Running: ./simpleQuery.sh"
echo ""
script=simpleQuery.pig
tenantId=$1
hdfsDateFolders=$2
filterKey="$3"
#
tenantPrefix="$1_metrics.ZabbixAgentData"


echo "HDFS - prefixo:"
echo "   "$tenantId
echo "   "$tenantPrefix

echo "HDFS - pastas de data:"
echo "   "$hdfsDateFolders


#####
# PIG
export PIG_USER_CLASSPATH_FIRST=true
/produtos/openbus.pig/bin/pig -P /produtos/openbus.pig/scripts/custom.properties \
    -4 /produtos/openbus.pig/conf/log4j.properties \
    -param tenantId=$tenantId \
    -param tenantPrefix=$tenantPrefix \
    -param pastas=$hdfsDateFolders \
    -param filterKey="$filterKey" \
    -x mapreduce /produtos/openbus.pig/scripts/$script 

