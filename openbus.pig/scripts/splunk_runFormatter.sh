startTS=`date +"%s"`
#####
# Rodar: ./splunk_runFormatter.sh
echo "> Running: ./splunk_runFormatter.sh"
echo ""
script=splunk_scriptFormatter.pig
tenantId=$1
#
if [ "$1" == "Corporativo" ] || [ "$1" == "" ]
then
   tenantId="Corporativo"
   tenantPrefix="metrics.ZabbixAgentData" 
else
   tenantPrefix="$1"
   tenantPrefix="$1_metrics.ZabbixAgentData"
fi
echo $tenantId
echo $tenantPrefix

####
#inputDate="2015-02-12 17:01"
inputDate=`date +"%Y-%m-%d %H:%M"`
now=`date -d "$inputDate" +%s`


#####
# calculo do "Anterior"
t1=$(( $now - 3600 ))
t2=`date -d @$t1 "+%Y-%m-%d %H:00:00"`
pastaAnterior=`date -d @$t1 "+%Y/%m/%d/%H"`
#
echo Anterior
echo "   Pasta: "$pastaAnterior


#####
# PIG
export PIG_USER_CLASSPATH_FIRST=true
/produtos/openbus.pig/bin/pig -P /produtos/openbus.pig/scripts/custom.properties \
    -4 /produtos/openbus.pig/conf/log4j.properties \
    -param tenantId=$tenantId \
    -param tenantPrefix=$tenantPrefix \
    -param pastaAnterior=$pastaAnterior \
    -x mapreduce /produtos/openbus.pig/scripts/$script 

endTS=`date +"%s"`
echo ""
echo "> Tempo de processamento: "$(($endTS - $startTS))" segundos"
