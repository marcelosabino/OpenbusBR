SET mapreduce.task.classpath.user.precedence true;
SET mapreduce.job.user.classpath.first true;
SET mapred.output.compress true
SET avro.output.codec deflate

/* UDF's */
REGISTER '$basePath/contrib/piggybank.jar'

/* Avro + Json */
REGISTER '$basePath/json-simple-1.1.jar'
REGISTER '$basePath/guava-15.0.jar'

/* My UDFs */
REGISTER '$basePath/openbus.model-0.0.2-SNAPSHOT.jar'
REGISTER '$basePath/openbus.pig-0.1.0-SNAPSHOT-shaded.jar'
DEFINE IsValidKey   br.com.produban.openbus.pig.udf.filter.FilterKeys('$tenantId');
DEFINE FormatZabbix br.com.produban.openbus.pig.udf.eval.FormatterZabbixTupleToBag('$tenantId');
DEFINE AvroWriter org.apache.pig.builtin.AvroStorage('ZabbixMetric', '-schemafile /openbus/avro_schema/zabbix_metric.avsc');

/* AVRO */
avro = LOAD '/openbus/data/$topicSchema/hourly/$currentFolder/*.avro' USING AvroStorage();
-- DUMP avro;
DESCRIBE avro;

/* FORMATTED AVRO */
formattedAvro = FOREACH avro GENERATE FLATTEN( FormatZabbix(
		host,
		host_metadata,
		key,
		value,
		lastlogsize,
		mtime,
		timestamp,
		source,
		severity,
		eventid,
		state,
		clock,
		ns) ) AS (
                host          : chararray,
                key           : chararray,
                value         : chararray,
                timestamp     : chararray,
                source        : chararray,
                severity      : chararray,
                eventid       : chararray,
                state         : chararray,
                fields
		);
-- DUMP formattedAvro;
DESCRIBE formattedAvro;

/* STORE */
STORE formattedAvro
	INTO '/openbus/data/normalized/$topicSchema/hourly/$currentFolder'
	USING AvroWriter();