SET mapreduce.job.user.classpath.first true;
SET mapreduce.fileoutputcommitter.marksuccessfuljobs false;

/* HBase */
REGISTER '$basePath/zookeeper-3.4.5.jar'
REGISTER '$basePath/hbase-server-0.98.6-hadoop2.jar'

/* ********** LIBs ********** */
-- REGISTER '$basePath/phoenix-4.3.1-client.jar'

/* ********** UDFs ********** */
DEFINE LinReg               br.com.produban.openbus.pig.udf.eval.LinRegLongDouble('$tenant');
DEFINE ProjectValues        br.com.produban.openbus.pig.udf.eval.ProjectValues();
DEFINE TruncateDate         br.com.produban.openbus.pig.udf.eval.TruncateDate();
DEFINE BagConcat            datafu.pig.bags.BagConcat();
DEFINE RedisStorage         br.com.produban.openbus.pig.udf.store.RedisStorage('stat', '$tenant');
DEFINE UnixToISO          org.apache.pig.piggybank.evaluation.datetime.convert.UnixToISO();

/* ***************************************** */
/* ********** LOAD & FILTER ********** */

hbase = LOAD 'hbase://$HBaseTable'
   USING org.apache.pig.backend.hadoop.hbase.HBaseStorage('metadata:tenant metadata:uid metadata:key metadata:hour tags:* aggregations:max aggregations:min aggregations:avg aggregations:p50 aggregations:p75 aggregations:p85 aggregations:p95 aggregations:dev', '-loadKey true -gte $weekAgo\\_$tenant* -lte $today\\_$tenant*')
   as (rowKey, tenant:chararray, uid:chararray, key:chararray, hour:long, tags:map[], max:double, min:double, avg:double, p50:double, p75:double, p85:double, p95:double, dev:double);
-- DUMP hbase;
 DESCRIBE hbase;

/* ************************************************* */
/* ********** LINEAR REGRESSION ********** */

/* Create bag of timestamp+AVG(values), where timestamp is "hour based"  */
prepareToGroup = FOREACH hbase GENERATE
   CONCAT('StatModel:',uid) as rowKey:chararray,
   TruncateDate('yyyy-MM-dd-HH', hour) as date:long,
   ((min IS NULL) ? 0 : min) as min:double,
   ((avg IS NULL) ? 0 : avg) as avg:double,
   ((max IS NULL) ? 0 : max) as max:double,
   ((p50 IS NULL) ? 0 : p50) as p50:double,
   ((p75 IS NULL) ? 0 : p75) as p75:double,
   ((p85 IS NULL) ? 0 : p85) as p85:double,
   ((p95 IS NULL) ? 0 : p95) as p95:double,
   ((dev IS NULL) ? 0 : dev) as dev:double
;
-- DUMP prepareToGroup;
 DESCRIBE prepareToGroup;

grouped = GROUP prepareToGroup BY (rowKey);
-- DUMP grouped;
 DESCRIBE grouped;

/* AVG by Day  */
groupedDay = FOREACH grouped GENERATE
  group as rowKey,
  prepareToGroup.(date, min) as mins,
  prepareToGroup.(date, avg) as avgs,
  prepareToGroup.(date, max) as maxs,
  prepareToGroup.(date, p50) as p50s,
  prepareToGroup.(date, p75) as p75s,
  prepareToGroup.(date, p85) as p85s,
  prepareToGroup.(date, p95) as p95s,
  prepareToGroup.(date, dev) as devs
;
-- DUMP groupedDay;
 DESCRIBE groupedDay;

/* Linear Regression */
lrCoef = FOREACH groupedDay GENERATE
   rowKey as rowKey:chararray,
   LinReg(mins) as min:tuple(minm:double, minb:double),
   LinReg(avgs) as avg:tuple(avgm:double, avgb:double),
   LinReg(maxs) as max:tuple(maxm:double, maxb:double),
   LinReg(p50s) as p50:tuple(p50m:double, p50b:double),
   LinReg(p75s) as p75:tuple(p75m:double, p75b:double),
   LinReg(p85s) as p85:tuple(p85m:double, p85b:double),
   LinReg(p95s) as p95:tuple(p95m:double, p95b:double),
   LinReg(devs) as dev:tuple(devm:double, devb:double)
;
-- DUMP lrCoef;
 DESCRIBE lrCoef;

/* **************************** */
/* ********** PROJECTION ********** */
/* Projection */
proj = FOREACH lrCoef GENERATE
    rowKey,
    ProjectValues((long)$today, (long)$qttProjectedDays, min.minm, min.minb, '$projectionType', $targetTimestamp) as mins:bag{T:tuple(date:long, min:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, avg.avgm, avg.avgb, '$projectionType', $targetTimestamp) as avgs:bag{T:tuple(date:long, avg:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, max.maxm, max.maxb, '$projectionType', $targetTimestamp) as maxs:bag{T:tuple(date:long, max:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, p50.p50m, p50.p50b, '$projectionType', $targetTimestamp) as p50s:bag{T:tuple(date:long, p50:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, p75.p75m, p75.p75b, '$projectionType', $targetTimestamp) as p75s:bag{T:tuple(date:long, p75:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, p85.p85m, p85.p85b, '$projectionType', $targetTimestamp) as p85s:bag{T:tuple(date:long, p85:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, p95.p95m, p95.p95b, '$projectionType', $targetTimestamp) as p95s:bag{T:tuple(date:long, p95:double)},
    ProjectValues((long)$today, (long)$qttProjectedDays, dev.devm, dev.devb, '$projectionType', $targetTimestamp) as devs:bag{T:tuple(date:long, dev:double)}
;
-- DUMP proj;
 DESCRIBE proj;

projFlattened = FOREACH proj GENERATE
    rowKey,
    FLATTEN(avgs.date) as date,
    FLATTEN(mins.min) as min,
    FLATTEN(avgs.avg) as avg,
    FLATTEN(maxs.max) as max,
    FLATTEN(p50s.p50) as p50,
    FLATTEN(p75s.p75) as p75,
    FLATTEN(p85s.p85) as p85,
    FLATTEN(p95s.p95) as p95,
    FLATTEN(devs.dev) as dev
;
-- DUMP projFlattened;
 DESCRIBE projFlattened;

projRedis = FOREACH projFlattened GENERATE
    rowKey,
    TOBAG(TOTUPLE('projectionDateTime', ToDate(date * 1000)), TOTUPLE('min', min), TOTUPLE('avg', avg), TOTUPLE('max', max), TOTUPLE('p50', p50), TOTUPLE('p75', p75), TOTUPLE('p85', p85), TOTUPLE('p95', p95), TOTUPLE('dev', dev))
;
-- DUMP projRedis;
 DESCRIBE projRedis;

STORE projRedis INTO 'statisticalModel' USING RedisStorage();