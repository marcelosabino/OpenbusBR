#!/bin/bash
today=$(date +"%s")
weekAgo=$(date --date="$1 days ago" +"%s")
humanProjectionForHour=$(date --date="+$2 days" +"%Y-%m-%d 23:00")
humanProjectionForDays=$(date --date="+$(($2+1)) days" +"%Y-%m-%d 00:00")
projectionForHour=$(date --date="$humanProjectionForHour" +"%s")
projectionForDays=$(date --date="$humanProjectionForDays" +"%s")

echo "today=$today"
echo "weekAgo=$weekAgo"
echo "qttProjectedDays=$2"
echo "projectionType=$3"

if [ "$3" == "HOUR" ]; then
  echo "targetTimestamp=$projectionForHour"
elif [ "$3" == "DAY" ]; then
  echo "targetTimestamp=$projectionForDays"
else
  echo "Illegal Argument for projection"
fi