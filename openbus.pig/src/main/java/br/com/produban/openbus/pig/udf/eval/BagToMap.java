package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

public class BagToMap extends EvalFunc<Map<Object, Object>> {

	public BagToMap() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Object, Object> exec(Tuple input) throws IOException {
		Map<Object, Object> map = new HashMap<Object, Object>();
		if (input == null || input.size() == 0) {
			return map;
		}
		try {
			Object noTypeValues = input.get(0);
			if (!(noTypeValues instanceof DataBag)) {
				throw new IOException("Excepted input to be a DataBag");
			}
			
			DataBag values = (DataBag)noTypeValues;
			
			for (Iterator<Tuple> it = values.iterator(); it.hasNext();) {
				Tuple t = it.next();
				map.put(t.get(0), t.get(1));
			}

			return map;
		} catch(Exception e) {
			throw new RuntimeException("BagToMap error", e);
		}
	}

	
    @Override
    public Schema outputSchema(Schema input) {
        return new Schema(new Schema.FieldSchema(null, DataType.MAP));
    }
}
