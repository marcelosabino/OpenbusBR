package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

public class BagToMapLongDouble extends EvalFunc<Map<Long, Double>> {

	public BagToMapLongDouble() {
	}

	@Override
	public Map<Long, Double> exec(Tuple input) throws IOException {
		Map<Long, Double> map = new TreeMap<Long, Double>();
		if (input == null || input.size() == 0) {
			return map;
		}
		try {
			Object noTypeValues = input.get(0);
			if (!(noTypeValues instanceof DataBag)) {
				throw new IOException("Excepted input to be a DataBag");
			}
			
			DataBag values = (DataBag)noTypeValues;
			
			for (Iterator<Tuple> it = values.iterator(); it.hasNext();) {
				Tuple t = it.next();
				Long key = (Long) t.get(0);
				Double value = (Double) t.get(1);
				map.put(key, value);
			}

			return map;
		} catch(Exception e) {
			throw new RuntimeException("BagToMap error", e);
		}
	}

	
    @Override
    public Schema outputSchema(Schema input) {
        return new Schema(new Schema.FieldSchema(null, DataType.MAP));
    }
}
