package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.util.UDFContext;

import br.com.produban.openbus.rules.Validator;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class CalculateThresholds extends EvalFunc<Tuple> {

	private final String tenantId;
	private final String redisPassword;
	private final Map<String, Integer> redisDatabasesPerTenant = new HashMap<>();
	private Properties props;
	private String timezoneID;
	private Calendar cal;
	
	
	private String redisHost;
	private int redisPort;

	private Validator validator;
	
	private Pattern patternUtil = Pattern.compile(".*utilization");
	private Pattern patternFree = Pattern.compile(".*free");
	private Pattern patternIdle = Pattern.compile(".*idle");
	
	public CalculateThresholds(String tenantId) {
		this.tenantId = tenantId;

		props = UDFContext.getUDFContext().getClientSystemProps();
		
		this.timezoneID = props.getProperty("timezone");
		if (timezoneID == null) {
			timezoneID = "America/Sao_Paulo";
		}
		
		cal = new GregorianCalendar(TimeZone.getTimeZone("America/Sao_Paulo"));
		
		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));
		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}
		
		validator = new Validator(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Tuple exec(Tuple input) throws IOException {
		// expected format: (uid, key, tag, max, min, avg, p50, dev)
		String uid = (String) input.get(0);
		String key = (String) input.get(1);
		
		Object noTypeValues = input.get(2);
		if (!(noTypeValues instanceof DataBag)) {
			throw new IOException("Input[2] is expected to be a DataBag");
		}
		
		DataBag values = (DataBag)noTypeValues;
		Map<String, String> tags = new HashMap<String, String>();
		
		for (Iterator<Tuple> it = values.iterator(); it.hasNext();) {
			Tuple t = it.next();
			tags.put(t.get(0).toString(), t.get(1).toString());
		}
		
		double max = (Double) input.get(3);
		double min = (Double) input.get(4);
		double avg = (Double) input.get(5);
		double p50 = (Double) input.get(6);
		double dev = (Double) input.get(7);
		
		Map<String, String> config = null;
		try {
			config = validator.getThresholdConfig(key, tags);
		} catch (BusinessException e) {
			e.printStackTrace();
		} catch (InfraException e) {
			e.printStackTrace();
		}

		if (config == null) {
			log.error("Could not find threshold config for: " + key + ", " + uid);
			return null;
		}
		
		double fatal = 0.0d, critical = 0.0d, warning = 0.0d;
		String cfgType = config.get("type");
		
		if (cfgType.equals("superior")) {
			// SUPERIOR
			
			//
			if (config.get("fatal").equals("auto")) {
				fatal = (double)(max + dev);
				
				Matcher matcher = patternUtil.matcher(key);
				if (matcher.find() && fatal > 95.0d) {
					fatal = 95.0d;
				}
			} else {
				fatal = Double.parseDouble(config.get("fatal"));
			}
			
			double base = (avg > p50)? avg : p50;
			double delta = fatal - base;

			//
			if (config.get("critical").equals("auto")) {
				critical = (delta * 0.7) + base;
			} else {
				critical = Double.parseDouble(config.get("critical"));
			}
			
			//
			if (config.get("warning").equals("auto")) {
				warning = (delta * 0.3) + base;
			} else {
				warning = Double.parseDouble(config.get("warning"));
			}
			
		} else {
			// INFERIOR
			
			//
			if (config.get("fatal").equals("auto")) {
				fatal = min - dev;
			
				Matcher matcherFree = patternFree.matcher(key);
				Matcher matcherIdle = patternIdle.matcher(key);
				if ( (matcherFree.find() || matcherIdle.find()) && fatal < 5.0) {
					fatal = 5.0d;
				}
			} else {
				fatal = Double.parseDouble(config.get("fatal"));
			}
				
			double base = (avg < p50)? avg : p50;
			double delta = base - fatal;
			
			//
			if (config.get("critical").equals("auto")) {
				critical = base - (delta * 0.7);
			} else {
				critical = Double.parseDouble(config.get("critical"));
			}
			
			//
			if (config.get("warning").equals("auto")) {
				warning = base - (delta * 0.3);
			} else {
				warning = Double.parseDouble(config.get("warning"));
			}
		}
		
		// output: 
		Tuple tFatal = TupleFactory.getInstance().newTuple(2);
		tFatal.set(0, "fatal");
		tFatal.set(1, fatal);
		
		Tuple tCritical = TupleFactory.getInstance().newTuple(2);
		tCritical.set(0, "critical");
		tCritical.set(1, critical);

		Tuple tWarn = TupleFactory.getInstance().newTuple(2);
		tWarn.set(0, "warning");
		tWarn.set(1, warning);
		
		DataBag thresholds = BagFactory.getInstance().newDefaultBag();
		thresholds.add(tFatal);
		thresholds.add(tCritical);
		thresholds.add(tWarn);
		
		Tuple output = TupleFactory.getInstance().newTuple(4);
		output.set(0, uid);
		output.set(1, key);
		output.set(2, cfgType);
		output.set(3, thresholds);
		
		return output;
	}

}

