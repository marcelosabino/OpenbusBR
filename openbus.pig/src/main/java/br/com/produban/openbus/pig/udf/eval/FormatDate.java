package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class FormatDate extends EvalFunc<String> {

	public FormatDate() {
	}

	/* (non-Javadoc)
	 * @see org.apache.pig.EvalFunc#exec(org.apache.pig.data.Tuple)
	 */
	@Override
	public String exec(Tuple input) throws IOException {
		
		String formatStr = (String) input.get(0);
		long clock = (Long) input.get(1);
		
		boolean milis = true;
		if (clock < 10000000000l) {
			clock = clock * 1000l;
			milis = false;
		}
		
		DateTime dt = new DateTime(clock);
		DateTimeFormatter fmt = DateTimeFormat.forPattern(formatStr);
		
		return fmt.print(dt);
	}
	
	
	public static void main(String[] args) throws IOException {
		FormatDate hash = new FormatDate();
		
		Tuple tuple = TupleFactory.getInstance().newTuple(2);
		tuple.set(0,"HH");
		tuple.set(1,(long)1429128000);
		
		System.out.println(hash.exec(tuple));
	}
}
