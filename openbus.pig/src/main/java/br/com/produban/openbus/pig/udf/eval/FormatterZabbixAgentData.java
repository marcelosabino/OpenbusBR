package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.enrichers.EnrichUID;
import br.com.produban.openbus.rules.services.filters.FilterServices;
import br.com.produban.openbus.rules.services.forkers.ForkerServices;
import org.apache.pig.EvalFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.util.UDFContext;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.model.pojo.ZabbixAgentDataBean;
import br.com.produban.openbus.rules.Transformer;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class FormatterZabbixAgentData extends EvalFunc<DataBag> {

	private final String tenantId;
	private final String redisPassword;
	private final Map<String, Integer> redisDatabasesPerTenant = new HashMap<>();
	private Properties props;
	private String redisHost;
	private int redisPort;
//TODO: Refactor
//  private EnrichmentDatabaseConfiguration enrichmentDatabaseConfiguration;
//	private Transformer transformer;
    private JedisResolver jedisResolver;
    private FilterServices fs;

	public FormatterZabbixAgentData(String tenantId) {

		this.tenantId = tenantId;

		props = UDFContext.getUDFContext().getClientSystemProps();

		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));

		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}

        //TODO: Refactor
		/*
		log.info(">>> FormatterZabbixAgentData: " + redisHost + ":" + redisPort + "/" + redisDatabasesPerTenant.get(tenantId));

		this.enrichmentDatabaseConfiguration = new EnrichmentDatabaseConfiguration();
		this.enrichmentDatabaseConfiguration.setUrl(props.getProperty("neo4j.url"));
		this.enrichmentDatabaseConfiguration.setUser(props.getProperty("neo4j.user"));
		this.enrichmentDatabaseConfiguration.setPassword(props.getProperty("neo4j.password"));
		this.enrichmentDatabaseConfiguration.setTraverseQuery(props.getProperty("neo4j.traversalQuery"));
		transformer = new Transformer(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword, enrichmentDatabaseConfiguration);
		*/

        this.jedisResolver = new JedisResolver(redisHost, redisPort, redisDatabasesPerTenant.get(this.tenantId), redisPassword);
        fs = new FilterServices(jedisResolver);
	}

	@Override
	public DataBag exec(Tuple tuple) throws IOException {
		ZabbixAgentData inputData = new ZabbixAgentData();
		inputData.setHost((String) tuple.get(0));
		inputData.setClock((String) tuple.get(1));
		inputData.setKey((String) tuple.get(2));
		inputData.setValue((String) tuple.get(3));
		
		DataBag outputBag = BagFactory.getInstance().newDefaultBag();
		
		List<DataBean> outputData = null;
		try {
            //TODO: Refactor
			//outputData = transformer.formatBean(inputData);

            if(!fs.isValidData(inputData)) {
                log.info("Invalid Zabbix Data: " + inputData);
                return outputBag;
            }

            // Fork
            outputData = new ForkerServices(jedisResolver).forkData(null, inputData);
            if (outputData == null) {
                return outputBag;
            }

			//TODO: Review on Rules Refactory
			EnrichUID eUID = new EnrichUID(jedisResolver);
			for (int i = 0; i < outputData.size(); i++) {
				ZabbixAgentDataBean data = (ZabbixAgentDataBean) outputData.get(i);
				eUID.enrichData(data);
				outputBag.add(zabbixAgentDataBeanToSimpleTuple(data));
			}

		} catch (InfraException e) {
            log.error("InfraException: ", e);
		} catch (BusinessException e) {
            log.error("BusinessException: ", e);
		}


		return outputBag;
	}
	
	
	private Tuple zabbixAgentDataBeanToSimpleTuple(ZabbixAgentDataBean zad) throws ExecException {
		DataBag tags = BagFactory.getInstance().newDefaultBag();
		Tuple tuple = TupleFactory.getInstance().newTuple(2);
		tuple.set(0, "host");
		tuple.set(1, zad.getHostname());
		tags.add(tuple);
		for (Entry<String, String> tag : zad.getExtraFields().entrySet()) {
			tuple = TupleFactory.getInstance().newTuple(2);
			tuple.set(0, tag.getKey());
			tuple.set(1, tag.getValue());
			tags.add(tuple);
		}

		tuple = TupleFactory.getInstance().newTuple(5);
		tuple.set(0, zad.getExtraField("uid"));
		tuple.set(1, zad.getBean().getKey());
		tuple.set(2, zad.getBean().getClock());
		tuple.set(3, Float.parseFloat(zad.getBean().getValue()));
		tuple.set(4, tags);
		
		return tuple;
	}
	
}
