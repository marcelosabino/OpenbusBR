package br.com.produban.openbus.pig.udf.eval;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.model.pojo.ZabbixAgentDataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.filters.FilterServices;
import br.com.produban.openbus.rules.services.forkers.ForkerServices;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.apache.pig.EvalFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.util.UDFContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class FormatterZabbixTupleToBag extends EvalFunc<DataBag> {

	private final String tenantId;
	private final String redisPassword;
	private final Map<String, Integer> redisDatabasesPerTenant = new HashMap<>();
	private Properties props;
	private String redisHost;
	private int redisPort;
//TODO: Refactor
//	private EnrichmentDatabaseConfiguration enrichmentDatabaseConfiguration;
//	private Transformer transformer;
	private JedisResolver jedisResolver;
    private FilterServices fs;

	public FormatterZabbixTupleToBag(String tenantId) {

		props = UDFContext.getUDFContext().getClientSystemProps();

		this.tenantId = tenantId;
		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));
		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}

        //TODO: Refactor
		/*
        log.info(">>> FormatterZabbixTupleToBag: " +"Loading Transformer...");

        this.enrichmentDatabaseConfiguration = new EnrichmentDatabaseConfiguration();
        this.enrichmentDatabaseConfiguration.setUrl(props.getProperty("neo4j.url"));
        this.enrichmentDatabaseConfiguration.setUser(props.getProperty("neo4j.user"));
        this.enrichmentDatabaseConfiguration.setPassword(props.getProperty("neo4j.password"));
        this.enrichmentDatabaseConfiguration.setTraverseQuery(props.getProperty("neo4j.traversalQuery"));

        transformer = new Transformer(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword, enrichmentDatabaseConfiguration);
		*/

		this.jedisResolver = new JedisResolver(redisHost, redisPort, redisDatabasesPerTenant.get(this.tenantId), redisPassword);
        fs = new FilterServices(jedisResolver);
	}

	@Override
	public DataBag exec(Tuple tuple) throws IOException {

		if (tuple.size() != 13) {
			log.info(">>> FormatterZabbixTupleToBag: number of fields should be 13");
			return null;
		}

		/**
		 * Expected Input Tuple:
		 * host, host_metadata, key, value, lastlogsize, mtime, timestamp, source, severity, eventid, state, clock, ns
		 */
		ZabbixAgentData inputData = tupleToZabbixAgentData(tuple);
		
		List<DataBean> outputData = null;
        DataBag outputBag = BagFactory.getInstance().newDefaultBag();
		try {
			//TODO: Refactor
			// outputData = transformer.formatBean(inputData);

            if(!fs.isValidData(inputData)) {
                log.info("Invalid Zabbix Data: " + inputData);
                return outputBag;
            }

			// Fork
			outputData = new ForkerServices(jedisResolver).forkData(null, inputData);
			if (outputData == null) {
                return outputBag;
            }

		} catch (InfraException e) {
            log.error("InfraException: ", e);
		} catch (BusinessException e) {
            log.error("BusinessException: ", e);
		}
		
		if (outputData == null) {
			return outputBag;
		}
		
		for (DataBean data : outputData) {
//			if (validOutput(inputData, data)) {
				outputBag.add(zabbixAgentDataToTuple((ZabbixAgentDataBean) data));
				log.info("      OUTPUT: " + data.getHostname() + " ; "
						+ data.getRawKey() + " ; "
						+ data.getBean().get("value") + " ; ");
//			}
		}
		
		return outputBag;
	}
	

	private ZabbixAgentData tupleToZabbixAgentData(Tuple tuple) throws ExecException {
		ZabbixAgentData zad = ZabbixAgentData.newBuilder()
				.setHost((String) tuple.get(0))
				.setHostMetadata((String) tuple.get(1))
				.setKey((String) tuple.get(2))
				.setValue((String) tuple.get(3))
				.setLastlogsize((String) tuple.get(4))
				.setMtime((String) tuple.get(5))
				.setTimestamp((String) tuple.get(6))
				.setSource((String) tuple.get(7))
				.setSeverity((String) tuple.get(8))
				.setEventid((String) tuple.get(9))
				.setState((String) tuple.get(10))
				.setClock((String) tuple.get(11))
				.setNs((String) tuple.get(12))
				.build();
		
		return zad;
	}
	
	
	private Tuple zabbixAgentDataToTuple(ZabbixAgentDataBean zad) throws ExecException {
		Tuple tuple = TupleFactory.getInstance().newTuple(9);
		tuple.set(0, zad.getBean().getHost());
		tuple.set(1, zad.getBean().getKey());
		tuple.set(2, zad.getBean().getValue());
		tuple.set(3, zad.getTimestamp());
		tuple.set(4, zad.getBean().getSource());
		tuple.set(5, zad.getBean().getSeverity());
		tuple.set(6, zad.getBean().getEventid());
		tuple.set(7, zad.getBean().getState());
		tuple.set(8, zad.getExtraFields());

		return tuple;
	}
	
//	private boolean validOutput(ZabbixAgentData inputData,
//			ZabbixAgentDataBean data) {
//		return ( !inputData.getKey().equalsIgnoreCase(data.getKey()) );
//	}
	
//	@Override
//	public Schema outputSchema(Schema inputSch) {
//		try {
//			return new Schema(
//					new Schema.FieldSchema(
//							getSchemaName(this.getClass().getName().toLowerCase(), inputSch),
//							inputSch, 
//							DataType.TUPLE)
//			);
//		} catch (FrontendException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
}