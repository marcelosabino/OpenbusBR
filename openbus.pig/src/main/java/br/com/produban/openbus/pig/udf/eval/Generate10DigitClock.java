package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

public class Generate10DigitClock extends EvalFunc<Long> {

	public Generate10DigitClock() {
	}

	/* (non-Javadoc)
	 * @see org.apache.pig.EvalFunc#exec(org.apache.pig.data.Tuple)
	 */
	@Override
	public Long exec(Tuple input) throws IOException {
		long clock = (Long) input.get(0);
		
		boolean milis = true;
		if (clock >= 10000000000l) 
			clock = clock / 1000l;
		
		return clock;
	}
	
	
	public static void main(String[] args) throws IOException {
		Generate10DigitClock hash = new Generate10DigitClock();
		
		Tuple tuple = TupleFactory.getInstance().newTuple(1);
		tuple.set(0,(long)1427832456l);
		System.out.println(hash.exec(tuple));
		
		tuple = TupleFactory.getInstance().newTuple(1);
		tuple.set(0,(long)1427832456123l);
		System.out.println(hash.exec(tuple));
	}
}
