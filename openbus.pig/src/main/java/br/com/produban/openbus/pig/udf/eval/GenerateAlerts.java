package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;

public class GenerateAlerts extends EvalFunc<DataBag> {

	public GenerateAlerts() {
	}

	@Override
	public DataBag exec(Tuple input) throws IOException {
		try {
			
			String type = (String) input.get(0);
			
			// thresholds
			Object noTypeValues = input.get(1);
			if (!(noTypeValues instanceof DataBag)) {
				throw new IOException("Excepted input to be a DataBag");
			}
			@SuppressWarnings("unchecked")
			DataBag thresholds = (DataBag)noTypeValues;
			
			Map<String, Double> mapThresholds = new HashMap<String, Double>();
			for (Iterator<Tuple> it = thresholds.iterator(); it.hasNext();) {
				Tuple t = it.next();
				String tLevel = (String) t.get(0);
				Double tValue = (Double) t.get(1);
				mapThresholds.put(tLevel, tValue);
			}
			
			// projected
			noTypeValues = input.get(2);
			if (!(noTypeValues instanceof DataBag)) {
				throw new IOException("Excepted input to be a DataBag");
			}
			@SuppressWarnings("unchecked")
			DataBag projected = (DataBag)noTypeValues;
			
			
			// - Search for the first value to breach threshold value
			// - Stops when every alert is created 
			//      OR when no more projected values are available
			Map<String, Long> mapAlerts = new HashMap<String, Long>();
			int alerts = 0;
			int limit = mapThresholds.size();
			for (Iterator<Tuple> it = projected.iterator(); it.hasNext();) {
				Tuple t = it.next();
				long ts = (Long) t.get(0);
				Double value = (Double) t.get(1);
				
				for(String threshold : mapThresholds.keySet()) {
					if (!mapAlerts.containsKey(threshold)) {
						if (type.equals("superior") &&  (value.doubleValue() > mapThresholds.get(threshold).doubleValue())) {
							mapAlerts.put(threshold, ts);
							alerts++;
						} else if (type.equals("inferior") &&  (value.doubleValue() < mapThresholds.get(threshold).doubleValue())) {
							mapAlerts.put(threshold, ts);
							alerts++;
						}
					}
				}
				
				if (alerts >= limit) {
					break;
				}
			}
			
			// transform Map to Databag for output
			DataBag output = BagFactory.getInstance().newDefaultBag();
			for (Entry<String, Long> alert : mapAlerts.entrySet()) {
				Tuple t = TupleFactory.getInstance().newTuple(2);
				t.set(0, alert.getKey());
				t.set(1, alert.getValue());
				output.add(t);
			}
			
			return output;
		} catch(Exception e) {
			throw new RuntimeException("GenerateAlert error ", e);
		}
	}


	public Schema outputSchema(Schema input) {
		try{
			Schema bagSchema = new Schema();
			bagSchema.add(new Schema.FieldSchema("level", DataType.CHARARRAY));
			bagSchema.add(new Schema.FieldSchema("date", DataType.LONG));
			
			return new Schema(new Schema.FieldSchema(getSchemaName(this.getClass().getName().toLowerCase(), input), bagSchema, DataType.BAG));
		}catch (Exception e){
			return null;
		}
	}
	
	
	public static void main(String[] args) throws IOException{
		GenerateAlerts gen = new GenerateAlerts();
		
		Tuple input = TupleFactory.getInstance().newTuple(3);

		// type
		input.set(0, "inferior");
		
		// thresholds
		DataBag thresholds = BagFactory.getInstance().newDefaultBag();
		
		Tuple limit = TupleFactory.getInstance().newTuple(2);
		limit.set(0, "warning");
		limit.set(1, 50.0);
		thresholds.add(limit);
		
		limit = TupleFactory.getInstance().newTuple(2);
		limit.set(0, "critical");
		limit.set(1, 75.0);
		thresholds.add(limit);
		
		limit = TupleFactory.getInstance().newTuple(2);
		limit.set(0, "fatal");
		limit.set(1, 90.0);
		thresholds.add(limit);
		
		input.set(1, thresholds);
		
		// projected values
		DataBag values = BagFactory.getInstance().newDefaultBag();
		
		for (int i = 0; i < 100; i++) {
			Tuple value = TupleFactory.getInstance().newTuple(2);
			
			Long ts = new Long(1433433000l + i);
			Double val = new Double(100-i);
			
			value.set(0, ts);
			value.set(1, val);
			values.add(value);
		}
		
		input.set(2, values);
		
		
		//
		DataBag output = gen.exec(input);
		
		for (Iterator<Tuple> it = output.iterator(); it.hasNext();) {
			Tuple t = it.next();
			
			System.out.println("Alert: " + (String)t.get(0) + " --> " + (Long) t.get(1));
		}
		
		
	}
}
