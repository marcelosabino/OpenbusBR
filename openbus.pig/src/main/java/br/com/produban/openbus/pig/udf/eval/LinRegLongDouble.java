package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.util.UDFContext;

import br.com.produban.openbus.rules.Validator;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class LinRegLongDouble extends EvalFunc<Tuple> {

	private final String tenantId;
	private final String redisPassword;
	private final Map<String, Integer> redisDatabasesPerTenant = new HashMap<>();
	private Properties props;
	private String timezoneID;
	private Calendar cal;
	
	private String redisHost;
	private int redisPort;

	private Validator validator;
	
	/**
	 * given a set of (x,y) tuples, the linear regression calculates (m, b):
	 *    y = m*x + b
	 */
	public LinRegLongDouble(String tenantId) {
		this.tenantId = tenantId;

		props = UDFContext.getUDFContext().getClientSystemProps();

		if(props != null) {
			this.timezoneID = props.getProperty("timezone");
		}

		if (timezoneID == null) {
			timezoneID = "America/Sao_Paulo";
		}
		
		cal = new GregorianCalendar(TimeZone.getTimeZone("America/Sao_Paulo"));
		
		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));
		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Tuple exec(Tuple input) throws IOException {
		if (validator == null) {
			log.info(">>> IsValidHour: Loading Validator...");
			validator = new Validator(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword);
		}
		
//		String key = (String) input.get(0);
//		
//		Object noTypeTags = input.get(1);
//		if (!(noTypeTags instanceof Map<?, ?>)) {
//			throw new IOException("Excepted input to be a Map");
//		}
//		Map<String, String> tags = new HashMap<String, String>();
//		for (Entry<String, Object> entry : ((Map<String, Object>)noTypeTags).entrySet()) {
//			tags.put(entry.getKey(), 
//					(entry.getValue() == null)? "null" : entry.getValue().toString());
//		}
		
		Object noTypeValues = input.get(0);
		if (!(noTypeValues instanceof DataBag)) {
			throw new IllegalArgumentException("Excepted input to be a DataBag");
		}
		DataBag values = (DataBag)noTypeValues;
		
		
		long numPoints = 0;
		double m, b, sumX, sumY, sumXY, sumXX, j;

		sumX = sumY = sumXY = sumXX = 0;

		for (Iterator<Tuple> it = values.iterator(); it.hasNext();) {
			Tuple t = it.next();
			
			Long ts = (Long)t.get(0);
			
			numPoints++;
			
			double x = ts.doubleValue();
			double y =  (Double)t.get(1);

			sumX += x;
			sumY += y;
			sumXY += x * y;
			sumXX += x * x;
		}

		m = b = 0;
		j = (numPoints * sumXX) - (sumX * sumX);

		if (j != 0) {
			m = ((numPoints * sumXY) - (sumX * sumY)) / j;
			b = ((sumY * sumXX) - (sumX * sumXY)) / j;
		}

		// output = (m, b) --> y = m*x + b
		Tuple output = TupleFactory.getInstance().newTuple(2);
		output.set(0, m);
		output.set(1, b);
		return output;
	}

}