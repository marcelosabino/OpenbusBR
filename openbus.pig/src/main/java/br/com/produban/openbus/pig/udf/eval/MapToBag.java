package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

public class MapToBag extends EvalFunc<DataBag> {

	public MapToBag() {
	}

	@Override
	public DataBag exec(Tuple input) throws IOException {
		try {
			Object noTypeValues = input.get(0);
			if (!(noTypeValues instanceof Map<?, ?>)) {
				throw new IOException("Excepted input to be a Map");
			}

			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>)noTypeValues;

			DataBag output = BagFactory.getInstance().newDefaultBag();

			for (Entry<String, Object> entry : map.entrySet()) {
				Tuple tuple = TupleFactory.getInstance().newTuple(2);
				tuple.set(0, entry.getKey());
				tuple.set(1, entry.getValue());
				output.add(tuple);
			}

			return output;
		} catch(Exception e) {
			throw new RuntimeException("MapToBag error", e);
		}
	}



}
