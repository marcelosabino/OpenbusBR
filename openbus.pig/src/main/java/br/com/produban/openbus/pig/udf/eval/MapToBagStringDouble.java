package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataByteArray;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;

public class MapToBagStringDouble extends EvalFunc<DataBag> {

	public MapToBagStringDouble() {
	}

	@Override
	public DataBag exec(Tuple input) throws IOException {
		try {
			Object noTypeValues = input.get(0);
			if (!(noTypeValues instanceof Map<?, ?>)) {
				throw new IOException("Excepted input to be a Map");
			}

			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>)noTypeValues;

			DataBag output = BagFactory.getInstance().newDefaultBag();

			for (Entry<String, Object> entry : map.entrySet()) {
				String key = (String) entry.getKey();
				double value = Double.parseDouble(entry.getValue().toString());
				
				Tuple tuple = TupleFactory.getInstance().newTuple(2);
				tuple.set(0, key);
				tuple.set(1, value);
				output.add(tuple);
			}

			return output;
		} catch(Exception e) {
			throw new RuntimeException("MapToBag error", e);
		}
	}



	public Schema outputSchema(Schema input) {
		try{
			Schema bagSchema = new Schema();
			bagSchema.add(new Schema.FieldSchema("level", DataType.LONG));
			bagSchema.add(new Schema.FieldSchema("value", DataType.DOUBLE));
			
			return new Schema(new Schema.FieldSchema(getSchemaName(this.getClass().getName().toLowerCase(), input), bagSchema, DataType.BAG));
		}catch (Exception e){
			return null;
		}
	}
}
