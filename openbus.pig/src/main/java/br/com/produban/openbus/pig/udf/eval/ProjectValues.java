package br.com.produban.openbus.pig.udf.eval;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.*;
import org.apache.pig.impl.logicalLayer.schema.Schema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ProjectValues extends EvalFunc<DataBag> {

	private final static long _DAY = 86400 * 1000;
	private final static long _HOUR = 3600 * 1000;
    private final static int EPOCH_START_INDEX = 5;

	public ProjectValues() {
	}

	@Override
	public DataBag exec(Tuple input) throws IOException {

		// expected format: ( ts_start, points, m, b )
		// y = m*x + b
		// output: {(x,y)}
		//   x: timestamp
		//   y: value
		Long ts_start = (Long) input.get(0);
		long points = (Long) input.get(1);
		double m = (Double) input.get(2);
		double b = (Double) input.get(3);

        List<Long> projectedDays = new ArrayList<>();

        long timeSegment = _DAY;
        String type = "DAY";
        if(input.size() > 4) {
            type = (String) input.get(4);
            if("HOUR".equals(type)) {
                timeSegment = _HOUR;
                points = (points + 1) * 24;
            } else if("DAY".equals(type)) {
                timeSegment = _DAY;
            } else {
                throw new IllegalArgumentException("Wrong segment time.");
            }
        }

		DataBag projectedValues = BagFactory.getInstance().newDefaultBag(new ArrayList<Tuple>());
		Long x = ts_start;
        if(ts_start != null && ts_start > 0 && ts_start.toString().length() < 11) {
            x = new java.util.Date(ts_start * 1000).getTime();
        }
		double y;
        Calendar c = Calendar.getInstance();
		for (int i = 0; i < points; i++) {
			x+= timeSegment;
			Tuple tuple = TupleFactory.getInstance().newTuple(3);
            c.setTimeInMillis(x);
            c.set(Calendar.MILLISECOND, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MINUTE, 0);
            if("DAY".equals(type)) {
                c.set(Calendar.HOUR, 0);
                c.set(Calendar.HOUR_OF_DAY, 0);
            }
            x = c.getTimeInMillis();

			y = (double) (m * x/1000 + b);

            tuple.set(0, c.getTimeInMillis() / 1000);
			tuple.set(1, y);
            tuple.set(2, i+1);

			projectedValues.add(tuple);
		}

        if(input.size() < 6) {
            return projectedValues;
        } else {
            for(int i = EPOCH_START_INDEX; i < input.size(); i++) {
                projectedDays.add(((Number) input.get(i)).longValue());
            }
        }

        DataBag values = BagFactory.getInstance().newDefaultBag(new ArrayList<Tuple>());
		for(Tuple t : projectedValues) {
			if(projectedDays.contains(t.get(0))) {
                values.add(t);
            }
		}

		// output = {(x,y)} --> y = m*x + b
		return values;
	}


	public Schema outputSchema(Schema input) {
		try{
			Schema bagSchema = new Schema();
			bagSchema.add(new Schema.FieldSchema("hour", DataType.LONG));
			bagSchema.add(new Schema.FieldSchema("value", DataType.DOUBLE));
			
			return new Schema(new Schema.FieldSchema(getSchemaName(this.getClass().getName().toLowerCase(), input), bagSchema, DataType.BAG));
		}catch (Exception e){
			return null;
		}
	}
}
