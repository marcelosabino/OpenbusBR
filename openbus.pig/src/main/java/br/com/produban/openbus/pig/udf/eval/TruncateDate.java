package br.com.produban.openbus.pig.udf.eval;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TruncateDate extends EvalFunc<Long> {

	public TruncateDate() {
	}

	/* (non-Javadoc)
	 * @see org.apache.pig.EvalFunc#exec(org.apache.pig.data.Tuple)
	 */
	@Override
	public Long exec(Tuple input) throws IOException {
		
		String formatStr = (String) input.get(0);
		long clock = (Long) input.get(1);
		
		boolean milis = true;
		if (clock < 10000000000l) {
			clock = clock * 1000l;
			milis = false;
		}
		
		DateTime dt = new DateTime(clock);
		DateTimeFormatter fmt = DateTimeFormat.forPattern(formatStr);
		dt = fmt.parseDateTime(fmt.print(dt));
		
		return (long) (milis? dt.getMillis() : (dt.getMillis() / 1000l));
	}
	
	
	public static void main(String[] args) throws IOException {
		TruncateDate hash = new TruncateDate();
		
		Tuple tuple = TupleFactory.getInstance().newTuple(2);
		tuple.set(0,"yyy-MM-dd-HH");
		tuple.set(1,(long)1427832456l);
		
		System.out.println(hash.exec(tuple));
	}
}
