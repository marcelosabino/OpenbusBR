package br.com.produban.openbus.pig.udf.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.pig.FilterFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.util.UDFContext;

import br.com.produban.openbus.rules.Validator;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class FilterKeys extends FilterFunc {

	private final String redisPassword;
	private final String tenantId;
	private Properties props;
	private String redisHost;
	private int redisPort;
	private Map<String,Integer> redisDatabasesPerTenant = new HashMap<>();
	
	private Validator validator;
	
	private ThreadLocal<Map<String, Boolean>> keys = new ThreadLocal<Map<String, Boolean>>();

	public FilterKeys(String tenantId) {

		props = UDFContext.getUDFContext().getClientSystemProps();

		this.tenantId = tenantId;
		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));
		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}
	}

	@Override
	public Boolean exec(Tuple tuple) throws IOException {
		if (validator == null) {
			log.info(">>> FilterKeys: Loading Validator...");
			validator = new Validator(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword);
		}
		
		log.info("> Validating: " + tuple.get(0).toString());
		String key = tuple.get(0).toString();
		
			try {
				return isValidKey(key);
			} catch (InfraException | BusinessException e) {
				throw new IOException(e);
			}
		
	}
	
	
	private boolean isValidKey(String key) throws InfraException, BusinessException{
		if (!getValidKeySet().containsKey(key)) {
			getValidKeySet().put(key, validator.keyHasTFMRules(key));
			log.info("\tValid Key: " + key + " -> " + getValidKeySet().get(key));
		}
		return getValidKeySet().get(key);
	}
	
	private Map<String, Boolean> getValidKeySet() {
		if (keys.get() == null) {
			keys.set(new HashMap<String, Boolean>());
		}
		return keys.get();
	}
}
