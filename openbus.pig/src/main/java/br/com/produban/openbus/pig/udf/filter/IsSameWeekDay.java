package br.com.produban.openbus.pig.udf.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.pig.FilterFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.util.UDFContext;
import org.joda.time.DateTime;

import br.com.produban.openbus.rules.Validator;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class IsSameWeekDay extends FilterFunc {
	
	public IsSameWeekDay() {
	}

	@Override
	public Boolean exec(Tuple tuple) throws IOException {
		log.info("> Validating: " + tuple.get(0) + " & " + tuple.get(1));
		long clock0 = Long.parseLong(tuple.get(0).toString()) * 1000;
		long clock1 = Long.parseLong(tuple.get(1).toString()) * 1000;
		
		DateTime dt0 = new DateTime(clock0);
		DateTime dt1 = new DateTime(clock1);
		
		return dt0.getDayOfWeek() == dt1.getDayOfWeek();
	}
	
	public static void main(String[] args) throws IOException {
		IsSameWeekDay teste = new IsSameWeekDay();
		
		Tuple tuple = TupleFactory.getInstance().newTuple(2);
		tuple.set(0, 1428055200l);
		tuple.set(1, 1429128000l);
		System.out.println(teste.exec(tuple));
	}
}
