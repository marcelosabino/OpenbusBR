package br.com.produban.openbus.pig.udf.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.pig.FilterFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.util.UDFContext;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.produban.openbus.rules.Validator;
import br.com.produban.openbus.security.exceptions.InfraException;

public class IsSpecifiedBusinessDay extends FilterFunc {

	private ThreadLocal<List<String>> businessDays = new ThreadLocal<List<String>>();
	private String fmtYYYYMM = "yyyy-MM";
	private DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");

	public IsSpecifiedBusinessDay(String tenantId, String diaUtil, String today, String n) throws IOException {
		Properties props = UDFContext.getUDFContext().getClientSystemProps();

		Validator validator = newValidator(tenantId, props);
		
		initBusinessDays(validator, diaUtil, Long.parseLong(today), Integer.parseInt(n));
	}
	
	private List<String> getBDays() {
		if (businessDays.get() == null) {
			businessDays.set(new ArrayList<String>());
		}
		return businessDays.get();
	}

	
	private Validator newValidator(String tenantId, Properties props) {
		String redisHost = props.getProperty("redis.host.ip");
		int redisPort = Integer.parseInt(props.getProperty("redis.host.port"));
		String redisPassword = props.getProperty("redis.rules.password");
		Map<String,Integer> redisDatabasesPerTenant = new HashMap<>();
		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");

		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}

		return new Validator(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword);
	}

	
	private void initBusinessDays(Validator validator,
			String diaUtil, long today, int n) throws IOException {
		
		// holydays
		Set<String> sHolydays = null;
		try {
			sHolydays = validator.getHolydays();
			if (sHolydays == null) {
				sHolydays = new HashSet<String>();
			}
		} catch (InfraException e) {
			throw new IOException(e);
		}
		
		// today
		if (today < 10000000000l) {
			today = today * 1000l;
		}
		DateTime dt = new DateTime(today);
		DateTimeFormatter fmt = DateTimeFormat.forPattern(fmtYYYYMM);
		dt = fmt.parseDateTime(fmt.print(dt));
		
		for (int i = 0; i <= n; i++) {
			getBDays().add(getNthBusinessDay(dt.minusMonths(i), diaUtil, sHolydays));
		}
	}

	
	private String getNthBusinessDay(DateTime date, String diaUtil, Set<String> sHolydays) throws IOException {
		int count = 0;
		int target = 0;
		try {
			target = Integer.parseInt(diaUtil);
			// max number of business days in a month is 23
			if (target > 23) target = 23;
		} catch (NumberFormatException e) {
			throw new IOException("Not a valid day count: " + diaUtil, e);
		}
		
		// lenient counter limits day count inside the original month 
		int lenientDayOfMonth = date.getDayOfMonth();
		int maxDayOfMonth = date.dayOfMonth().getMaximumValue();
		
		String targetDay = null;
		while (count < target && lenientDayOfMonth++ <= maxDayOfMonth) {
			
			// discard weekend & holydays
			if (date.getDayOfWeek() == 6 || date.getDayOfWeek() == 7 || sHolydays.contains(fmt.print(date))) {
				date = date.plusDays(1);
				continue;
			}
			
			targetDay = fmt.print(date);
			date = date.plusDays(1);
			count++;
		}
		
		return targetDay;
	}

	@Override
	public Boolean exec(Tuple tuple) throws IOException {
		long clock = (Long) tuple.get(0);
		
		if (clock < 10000000000l) {
			clock = clock * 1000l;
		}
		
		return getBDays().contains(fmt.print(new DateTime(clock)));
	}

	
	
//	public static void main(String[] args) throws IOException {
//		DateTimeFormatter fmtYM = DateTimeFormat.forPattern("yyyy-MM");
//		DateTimeFormatter fmtYMD = DateTimeFormat.forPattern("yyyy-MM-dd");
//		
//		IsSpecifiedBussinesDay teste = new IsSpecifiedBussinesDay("BR", "23", 1429901671000l, 5);
//		
//		Tuple tuple = TupleFactory.getInstance().newTuple(1);
//		tuple.set(0,1429901671000l);
//		System.out.println(teste.exec(tuple));
//		
//		
//		tuple = TupleFactory.getInstance().newTuple(1);
//		tuple.set(0,1430423184000l);
//		System.out.println(teste.exec(tuple));
//		
//		tuple = TupleFactory.getInstance().newTuple(1);
//		tuple.set(0,1419994800000l);
//		System.out.println(teste.exec(tuple));
//	}
}
