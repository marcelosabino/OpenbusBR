package br.com.produban.openbus.pig.udf.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.pig.FilterFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.util.UDFContext;

import br.com.produban.openbus.rules.Validator;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class IsValidHour extends FilterFunc {

	private final String redisPassword;
	private final Map<String, Integer> redisDatabasesPerTenant = new HashMap<>();
	private final String tenantId;
	private Properties props;
	private String redisHost;
	private int redisPort;

	private Validator validator;
	
	public IsValidHour(String tenantId) {
		props = UDFContext.getUDFContext().getClientSystemProps();

		this.tenantId = tenantId;
		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));
		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boolean exec(Tuple tuple) throws IOException {
		if (validator == null) {
			log.info(">>> IsValidHour: Loading Validator...");
			validator = new Validator(redisHost, redisPort, redisDatabasesPerTenant.get(tenantId), redisPassword);
		}
		
		log.info("> Validating: " + tuple.get(0) + " - " + tuple.get(1) + " - ");
		String key = tuple.get(0).toString();
		long clock = Long.parseLong(tuple.get(1).toString()) * 1000;
		
		Object noTypeValues = tuple.get(2);
		if (!(noTypeValues instanceof Map<?, ?>)) {
			throw new IOException("Excepted input to be a Map");
		}
		
		Map<String, String> tags = new HashMap<String, String>();
		for(Entry<Object, Object> entry : ((Map<Object, Object>)noTypeValues).entrySet() ) {
			tags.put(entry.getKey().toString(), entry.getValue().toString());
		}
		
		try {
			return validator.isValidHour(key, clock, tags);
		} catch (InfraException e) {
			log.error("", e);
		} catch (BusinessException e) {
			log.error("", e);
		}
		return true;
	}
}
