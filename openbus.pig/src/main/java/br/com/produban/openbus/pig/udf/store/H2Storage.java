package br.com.produban.openbus.pig.udf.store;

import br.com.produban.openbus.rules.repositories.h2.H2PoolClient;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.pig.StoreFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.util.UDFContext;

import java.io.IOException;
import java.util.Properties;

public class H2Storage extends StoreFunc {
	protected Log log = LogFactory.getLog(super.getClass());
	private Properties props;
	
	private String url;
	private String user;
	private String pswd;
	private int maxConn;
	private String table;
	private H2PoolClient h2Client = null;
	
	protected RecordWriter _writer;
	
	public H2Storage() {
	}
	
	private H2PoolClient getH2Client() {
		if (h2Client == null) {
			h2Client = new H2PoolClient(url, user, pswd, maxConn);
			log.info("New H2 client: " + user + ":" + pswd + "@" + url + " - " + maxConn);
		}
		return h2Client;
	}
	
	@Override
	public OutputFormat getOutputFormat() throws IOException {
		return new NullOutputFormat();
	}

	/**
	 * 
	 * @param input: expected tuple = (host, key, thresholdValue)
	 * @return
	 */
	@Override
	public void putNext(Tuple input) throws IOException {
		if (input.get(0) == null || input.get(1) == null || input.get(2) == null) {
			return;
		}

		String host = input.get(0).toString();
		String key = input.get(1).toString();
		float threshold = -1;
		try {
			threshold = Float.parseFloat(input.get(2).toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return;
		}

		insertThresholds(host, key, threshold);
	}
	
	@Override
	public void prepareToWrite(RecordWriter writer) throws IOException {
		_writer = writer;
	}

	@Override
	public void setStoreLocation(String location, Job job) throws IOException {
		System.out.println("> Location: " + location);
		
		props = UDFContext.getUDFContext().getClientSystemProps();
		
		this.url = props.getProperty("h2.url");
		this.user = props.getProperty("h2.user");
		this.pswd = props.getProperty("h2.pswd");
		this.maxConn = Integer.parseInt(props.getProperty("h2.maxConn"));
		this.table = props.getProperty("h2.table");
	}
	
	
	private void insertThresholds(String host, String key, float threshold){
		
		// INSERT INTO THRESHOLDS_DB.PIG_THRESHOLDS VALUES(host, key, value); 
		StringBuilder delBuilder = new StringBuilder(150);
		delBuilder.append("DELETE FROM ");
		delBuilder.append(table);
		delBuilder.append(" WHERE (host = '");
		delBuilder.append(host);
		delBuilder.append("' AND key = '");
		delBuilder.append(key);
		delBuilder.append("'); ");
		
		System.out.println("> " + delBuilder.toString());

		try {
			getH2Client().updateSql(delBuilder.toString());
		} catch (InfraException e) {
			e.printStackTrace();
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		
		// INSERT INTO THRESHOLDS_DB.PIG_THRESHOLDS VALUES(host, key, value); 
		StringBuilder insertBuilder = new StringBuilder(150);
		insertBuilder.append("INSERT INTO ");
		insertBuilder.append(table);
		insertBuilder.append(" VALUES('");
		insertBuilder.append(host);
		insertBuilder.append("', '");
		insertBuilder.append(key);
		insertBuilder.append("', ");
		insertBuilder.append(threshold);
		insertBuilder.append(");");
		
		System.out.println("> " + insertBuilder.toString());
		
		try {
			getH2Client().updateSql(insertBuilder.toString());
		} catch (InfraException e) {
			e.printStackTrace();
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}
}
