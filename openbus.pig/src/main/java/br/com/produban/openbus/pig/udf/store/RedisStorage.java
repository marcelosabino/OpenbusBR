package br.com.produban.openbus.pig.udf.store;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.pig.StoreFunc;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.util.UDFContext;

import br.com.produban.openbus.rules.repositories.jedis.JedisRepository;
import br.com.produban.openbus.security.exceptions.InfraException;

public class RedisStorage extends StoreFunc {
	protected Log log = LogFactory.getLog(super.getClass());
	private final String tenantId;
	private final String redisPassword;
	private final Map<String, Integer> redisDatabasesPerTenant = new HashMap<>();
	private Properties props;
	
	private String redisHost;
	private int redisPort;
	private int ttlValue = 7 * 24 * 3600;
	
	private JedisRepository jedis = null;
	
	protected RecordWriter _writer;

	public RedisStorage(String targetTTL, String tenantId) {
		this.tenantId = tenantId;

		props = UDFContext.getUDFContext().getClientSystemProps();

		this.redisHost = props.getProperty("redis.host.ip");
		this.redisPort = Integer.parseInt(props.getProperty("redis.host.port"));

		String ttlValue = props.getProperty("redis."+ targetTTL +".ttl");
		if (ttlValue != null) {
			try {
				this.ttlValue = Integer.parseInt(ttlValue);
			} catch (NumberFormatException e) {
				this.ttlValue = 7 * 24 * 3600;
			}
		}

		this.redisPassword = props.getProperty("redis.rules.password");

		String[] prefixArray = props.getProperty("redis.rules.databases").split(",");
		for (String s : prefixArray) {
			String[] split = s.split("=");
			redisDatabasesPerTenant.put(split[0],Integer.valueOf(split[1]));
		}
	}
	
	private JedisRepository getJedis() {
		if (jedis == null) {
			try {
				jedis = JedisRepository.newBuilder()
						   .withRedisHost(redisHost)
						   .withRedisPort(redisPort)
						   .withRedisPassword(redisPassword)
						   .withRedisDatabaseIndex(redisDatabasesPerTenant.get(tenantId))
						   .build();
				log.info("> New Jedis client: " + redisHost + ":" + redisPort + "/" + redisDatabasesPerTenant.get(tenantId));
			} catch (InfraException e) {
				e.printStackTrace();
			}
		}
		return jedis;
	}
	
	@Override
	public OutputFormat getOutputFormat() throws IOException {
		return new NullOutputFormat();
	}

	@Override
	public void putNext(Tuple f) throws IOException {
		if (f.get(0) == null) {
			return;
		}

		String key = f.get(0).toString();
		DataBag fields = (DataBag) f.get(1);
		
		log.info(">>> Hash: " + key);
		
		//
		try {
			getJedis().pipeInit();
			//
			for (Iterator<Tuple> it = fields.iterator(); it.hasNext();) {
				Tuple tuple = it.next();
				if (tuple.get(0) != null && tuple.get(1) != null) {
					String field = tuple.get(0).toString();
					String value = tuple.get(1).toString();
					getJedis().pipeHset(key, field, value, ttlValue);
					log.info(">>>\tvalue: " + field + " = " + value);
				}
			}
			
			//
			getJedis().pipeSend();
		} catch (InfraException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void prepareToWrite(RecordWriter writer) throws IOException {
		_writer = writer;
	}

	@Override
	public void setStoreLocation(String location, Job job) throws IOException {}
	

}
