package br.com.produban.openbus.pig;

import org.apache.pig.pigunit.PigTest;
import org.junit.Test;

import java.nio.file.Paths;

/**
 * To run this test, send the properties by vm arguments:
 * ex: -ea -Dredis.host.ip=localhost -Dredis.host.port=6379 -Dredis.rules.password=openbus -Dredis.rules.databases=Corporativo=0,BR=1,CH=2,ES=3
 */
public class AggregateZabbixMetricsTest {

    @Test
    public void run() {

        String[] args = {
                "tenant=BR",
                "topicSchema=BR.ZabbixAgentData",
                "basePath=/produtos/openbus.pig/scripts",
                "pastaAtual=2015/08/29/06",
                "HBaseTable=METRICS_AGGREGATED"
        };

        try {
            PigTest test = new PigTest(Paths.get("").toAbsolutePath() +"/scripts/aggregation_scriptAggregateMetricsH-1.pig", args);
            test.unoverride("STORE");
            test.runScript();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}