package br.com.produban.openbus.pig;

import org.apache.pig.impl.util.UDFContext;
import org.apache.pig.pigunit.PigTest;
import org.junit.Test;

import java.nio.file.Paths;

/**
 * To run this test, send the properties by vm arguments:
 * ex: -ea -Dredis.host.ip=localhost -Dredis.host.port=6379 -Dredis.rules.password=openbus -Dredis.rules.databases=Corporativo=0,BR=1,CH=2,ES=3
 */
public class FormatterZabbixTupleToBagTest {

    @Test
    public void run() {

        String[] args = {
                "tenantId=BR",
                "topicSchema=BR.ZabbixAgentData",
                "pastaAnterior=2015/08/29/06",
                "basePath=/produtos/openbus.pig/scripts"
        };

        try {
            PigTest test = new PigTest(Paths.get("").toAbsolutePath() +"/scripts/splunk_scriptFormatter.pig", args);
            test.unoverride("STORE");
            test.runScript();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}