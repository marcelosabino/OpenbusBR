package br.com.produban.openbus.pig;

import br.com.produban.openbus.pig.udf.eval.LinRegLongDouble;
import br.com.produban.openbus.pig.udf.eval.ProjectValues;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DefaultDataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.util.UDFContext;
import org.apache.pig.test.Util;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Properties;

/**
 * Created by inmetrics on 08/10/15.
 */
public class LinRegTest {

    @Test
    public void run() {

        try {

            Properties prop = new Properties();
            InputStream input = new FileInputStream(Paths.get("").toAbsolutePath() +"/src/test/resources/custom.properties");
            prop.load(input);

            UDFContext.getUDFContext().setClientSystemProps(prop);
            LinRegLongDouble lr = new LinRegLongDouble("BR");
            Calendar cal = Calendar.getInstance();
            DataBag db=new DefaultDataBag();
            db.add(Util.buildTuple(1444903200l,35.49763013605486));
            db.add(Util.buildTuple(1444906800l,35.547032126064956));
            db.add(Util.buildTuple(1444838400l,35.55043819032866));

            System.out.println(db);
            System.out.println("avg: "+ lr.exec(Util.buildTuple(db)));

            db=new DefaultDataBag();
            db.add(Util.buildTuple(1444903200l,35.5095100402832));
            db.add(Util.buildTuple(1444906800l,35.5454216003418));
            db.add(Util.buildTuple(1444838400l,35.558250427246094));

            System.out.println(db);
            System.out.println("p75: "+ lr.exec(Util.buildTuple(db)));

        } catch (ExecException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}