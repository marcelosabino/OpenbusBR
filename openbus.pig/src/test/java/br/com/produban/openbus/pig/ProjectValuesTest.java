package br.com.produban.openbus.pig;

import br.com.produban.openbus.pig.udf.eval.ProjectValues;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.Tuple;
import org.apache.pig.test.Util;
import org.junit.Test;

import java.io.IOException;
import java.util.Calendar;

/**
 * Created by inmetrics on 08/10/15.
 */
public class ProjectValuesTest {

    @Test
    public void run() {

        try {
            Long qttProjected = 90l;
            double avgm = 7.677339197567518E-7;
            double avgb = -1073.7538075673126;

            double p75m = 1.9402655854441026E-6;
            double p75b = -2790.599515513608;

            String projectionType = "HOUR";
            ProjectValues pv = new ProjectValues();
            Calendar cal = Calendar.getInstance();
            Tuple testTuple = Util.buildTuple(cal.getTime().getTime() / 1000, qttProjected, avgm, avgb, projectionType);
            System.out.println(testTuple);
            System.out.println(pv.exec(testTuple));
            testTuple = Util.buildTuple(cal.getTime().getTime() / 1000, qttProjected, p75m, p75b, projectionType);
            System.out.println(testTuple);
            System.out.println(pv.exec(testTuple));

            Calendar calAux = (Calendar) cal.clone();
            calAux.set(Calendar.MILLISECOND, 0);
            calAux.set(Calendar.SECOND, 0);
            calAux.set(Calendar.MINUTE, 0);
            calAux.set(Calendar.HOUR, 0);
            calAux.set(Calendar.HOUR_OF_DAY, 0);
            calAux.add(Calendar.DAY_OF_MONTH, 1);
            long plusOneDay = calAux.getTime().getTime();
            testTuple = Util.buildTuple(cal.getTime().getTime(), qttProjected, avgm, avgb, projectionType, plusOneDay);
            System.out.println(testTuple);
            System.out.println("plusOneDay: "+ pv.exec(testTuple));

            calAux.add(Calendar.DAY_OF_MONTH, 1);
            long plusTwoDays = calAux.getTime().getTime();
            testTuple = Util.buildTuple(cal.getTime().getTime(), qttProjected, avgm, avgb, projectionType, plusOneDay, plusTwoDays);
            System.out.println(testTuple);
            System.out.println("plusOneDay, plusTwoDays:"+ pv.exec(testTuple));

            calAux.add(Calendar.DAY_OF_MONTH, 88);
            long plus90Days = calAux.getTime().getTime();
            testTuple = Util.buildTuple(cal.getTime().getTime(), qttProjected, avgm, avgb, projectionType, plus90Days);
            System.out.println(testTuple);
            System.out.println("plus90Days: "+ pv.exec(testTuple));

            testTuple = Util.buildTuple(cal.getTime().getTime(), qttProjected, avgm, avgb, projectionType, 0l);
            System.out.println(testTuple);
            System.out.println("0l"+ pv.exec(testTuple));

            testTuple = Util.buildTuple(cal.getTime().getTime(), qttProjected, avgm, avgb, projectionType, -plusOneDay);
            System.out.println(testTuple);
            System.out.println("-plusOneDay"+ pv.exec(testTuple));
        } catch (ExecException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}