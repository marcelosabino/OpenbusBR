package br.com.produban.openbus.pig;

import br.com.produban.openbus.pig.udf.eval.LinRegLongDouble;
import br.com.produban.openbus.pig.udf.eval.TruncateDate;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DefaultDataBag;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.util.UDFContext;
import org.apache.pig.test.Util;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Properties;

/**
 * Created by inmetrics on 08/10/15.
 */
public class TruncateDateTest {

    @Test
    public void run() {

        try {
            Properties prop = new Properties();
            InputStream input = new FileInputStream(Paths.get("").toAbsolutePath() +"/src/test/resources/custom.properties");
            prop.load(input);

            UDFContext.getUDFContext().setClientSystemProps(prop);
            TruncateDate td = new TruncateDate();
            Tuple testTuple = Util.buildTuple("YYYY/MM/DD", Calendar.getInstance().getTimeInMillis() / 1000);
            System.out.println(td.exec(testTuple));

        } catch (ExecException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}