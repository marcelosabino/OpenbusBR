package br.com.openbus.publisher;

public class BasePublisher<T> {

    private MessageValidator<T>[] validators;

    public BasePublisher(MessageValidator<T>[] validators) {
        this.validators = validators;
    }

    protected boolean isValid(T data) {
        if (validators != null) {
            for (MessageValidator<T> validator : validators) {
                if (!validator.validate(data))
                    return false;
            }
        }

        return true;
    }

}
