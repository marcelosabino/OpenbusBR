package br.com.openbus.publisher;

public interface MessageValidator<T> {

    public boolean validate(T data);

}
