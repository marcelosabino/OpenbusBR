package br.com.openbus.publisher;

import java.util.List;

public interface Publisher<T> {

	void publish(String tool, T data, String topicName);
	void publish(T data, String topicName);
	void publishAll(String tool, List<T> dataList, String topicName);
	void publishAll(List<T> dataList, String topicName);
	void closePublisher();
}
