package br.com.openbus.publisher.kafka;

import br.com.openbus.publisher.MessageValidator;
import br.com.produban.openbus.avro.AvroEncoder;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;

import java.util.Properties;

public class KafkaAvroPublisher<T extends GenericRecord> extends KafkaBasePublisher<T> {

    public KafkaAvroPublisher(Properties properties, MessageValidator<T>... validators) {
        super(properties, validators);
    }

    public KafkaAvroPublisher(String brokerList,
                              boolean requiredAcks,
                              boolean isAsync,
                              int batchNumMessages,
                              MessageValidator<T>... validators) {
        super(brokerList, requiredAcks, isAsync, batchNumMessages, validators);
    }

    @Override
    public byte[] toByteArray(String tool, T data) {
        return AvroEncoder.toByteArray(tool, data);
    }
}
