package br.com.openbus.publisher.kafka;

import br.com.openbus.publisher.BasePublisher;
import br.com.openbus.publisher.MessageValidator;
import br.com.openbus.publisher.Publisher;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public abstract class KafkaBasePublisher<T> extends BasePublisher<T> implements Publisher<T> {

	private final static Logger LOGGER = LoggerFactory.getLogger(KafkaBasePublisher.class);
    private Producer<String, byte[]> producer;

	public KafkaBasePublisher(Properties properties, MessageValidator<T>... validators) {
        super(validators);
		producer = new Producer<String, byte[]>(new ProducerConfig(properties));
	}

	public KafkaBasePublisher(String brokerList,
                              boolean requiredAcks,
                              boolean isAsync, int batchNumMessages,
                              MessageValidator<T>... validators) {
        super(validators);
		Properties props = new Properties();
		props.put("metadata.broker.list", brokerList);
		props.put("request.required.acks", requiredAcks ? "1" : "0");
        props.put("producer.type", isAsync ? "async" : "sync");
		props.put("serializer.class", "kafka.serializer.DefaultEncoder");
		props.put("key.serializer.class", "kafka.serializer.StringEncoder");
        if (isAsync && batchNumMessages > 0)
            props.put("batch.num.messages",String.valueOf(batchNumMessages));

		producer = new Producer<String, byte[]>(new ProducerConfig(props));
	}

	@Override
	public void publish(T data, String topicName) {
		doPublish(null, data, topicName);
	}

	@Override
	public void publish(String tool, T data, String topicName) {
		doPublish(tool, data, topicName);
	}

	private void doPublish(String tool, T data, String topicName) {
		if (isValid(data))
			producer.send(new KeyedMessage<String, byte[]>(topicName, toByteArray(tool, data)));
	}

	@Override
	public void publishAll(List<T> dataList, String topicName) {
		doPublishAll(null, dataList, topicName);
	}

	@Override
	public void publishAll(String tool, List<T> dataList, String topicName) {
		doPublishAll(tool, dataList, topicName);
	}

	private void doPublishAll(String tool, List<T> dataList, String topicName) {
		if (dataList.size() > 0)
			LOGGER.info(Thread.currentThread().getName() + " processing " + dataList.size()
					+ " messages.");
		List<KeyedMessage<String, byte[]>> keyedMessages = new ArrayList<KeyedMessage<String, byte[]>>();
		for (T data : dataList) {
			if (isValid(data))
				keyedMessages.add(new KeyedMessage<String, byte[]>(topicName, toByteArray(tool, data)));
		}
		producer.send(keyedMessages);
	}

	@Override
	public void closePublisher() {
		producer.close();
	}

    public abstract byte[] toByteArray(String tool, T data);

}
