package br.com.openbus.publisher.kafka;

import br.com.openbus.publisher.MessageValidator;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

public class KafkaGenericPublisher extends KafkaBasePublisher<ByteArrayOutputStream> {

    public KafkaGenericPublisher(Properties properties, MessageValidator<ByteArrayOutputStream>... validators) {
        super(properties, validators);
    }

    public KafkaGenericPublisher(String brokerList,
                                 boolean requiredAcks,
                                 boolean isAsync,
                                 int batchNumMessages,
                                 MessageValidator<ByteArrayOutputStream>... validators) {
        super(brokerList, requiredAcks, isAsync, batchNumMessages, validators);
    }

    @Override
    public byte[] toByteArray(String tool, ByteArrayOutputStream data) {
        return data.toByteArray();
    }
}
