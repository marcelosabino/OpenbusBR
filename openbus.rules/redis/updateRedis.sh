## CONFIG ##
baixar=false
for i in $@; do
  if [ "$i" == "--force-download" ]; then
    baixar=true
  fi
done

# SRVBIGPVLBR10:/produtos/redisRules/
cmdRules=rules.redis
urlRules=https://raw.githubusercontent.com/Produban/OpenbusBR/master/openbus.rules/redis/$cmdRules
propPig=custom.properties
urlPig=https://raw.githubusercontent.com/Produban/OpenbusBR/master/openbus.pig/scripts/$propPig
propStorm=storm-multitenancy.properties
urlStorm=https://raw.githubusercontent.com/Produban/OpenbusBR/master/openbus.storm/$propStorm
##

## ENVIRONMENT ##
export http_proxy="http://spitc0bx:18z89jgt@prbc01.gsb:80"
export https_proxy=$http_proxy
redisCli="/produtos/storm/redis/redis-2.8.14/src/redis-cli"
##

## SCRIPT ##

#
if [ $baixar = true ]; then
  wget $urlRules --output-document=$cmdRules
  wget $urlPig   --output-document=$propPig
  wget $urlStorm --output-document=$propStorm 
fi

redisHostIp=`grep 'redis.host.ip' $propPig | cut -f2 -d'='`
redisHostPort=`grep 'redis.host.port' $propPig | cut -f2 -d'='`
redisHostPassword=`grep 'redis.rules.password' $propPig | cut -f2 -d'='`
if [ "$redisHostPassword" == "senha" ]; then
  redisHostPassword="openbus"
fi


echo ""

kvLine=$(grep 'redis.rules.databases' $propStorm | sed 's/^[^=]\+=//g')$(echo ",Corporativo=0")
kvList=$(echo $kvLine | tr "," "\n")
echo "Tenant List: "$kvList
echo ""

for e in $kvList
do 
   db=`echo $e | cut -f2 -d'='`
   echo "cat $cmdRules | grep -v -e '^#' | grep -v -e '^\s*$' | $redisCli -n $db -h $redisHostIp -p $redisHostPort -a $redisHostPassword"
   cat $cmdRules | grep -v -e '^#' | grep -v -e '^\s*$' | $redisCli -n $db -h $redisHostIp -p $redisHostPort -a $redisHostPassword
   echo ""
done


