package br.com.produban.openbus.rules;

import java.util.Arrays;
import java.util.List;

public class Combiner {
	
	private List<String> values;
	private int size;
	private long power;
	private long initialMask;
	private long binCombination;
	
	public Combiner(List<String> values) {
		this.values = values;
		if (values != null && values.size() > 0) {
			size = values.size();
		} else {
			size = -1;
		}
		power = (long) Math.pow(2, size);
		initialMask = power >> 1;
		reset();
	}
	
	public String next() {
		String next = null;
		if (binCombination >= 0) {
			next = combine();
			binCombination--;
		}
		return next;
	}
	
	public void reset() {
		binCombination = (long) Math.pow(2, size) - 1;
	}
	
	private String combine() {
		StringBuilder combination = new StringBuilder(512);
		long mask = initialMask;
		long bit = binCombination & mask;  
		
		if (bit > 0) {
			combination.append(values.get(0));
		} else {
			combination.append("*");
		}
		
		for (int i = 1; i < size; i++) {
			combination.append(";");

			mask = mask >> 1;
			bit = binCombination & mask;
			
			if (bit > 0) {
				combination.append(values.get(i));
			} else {
				combination.append("*");
			}
		}
		
		return combination.toString();
	}

	
	public static void main(String args[]) {
		Combiner combiner = new Combiner(Arrays.asList(args));
		
		String combination = null;
		while ( (combination = combiner.next()) != null ) {
			System.out.println(combination);
		}
	}
}
