package br.com.produban.openbus.rules;

import java.io.Serializable;
import java.util.List;

import org.apache.avro.specific.SpecificRecordBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.cleaners.CleanerServices;
import br.com.produban.openbus.rules.services.enrichers.EnricherServices;
import br.com.produban.openbus.rules.services.filters.FilterServices;
import br.com.produban.openbus.rules.services.forkers.ForkerServices;
import br.com.produban.openbus.rules.services.selectors.SelectorServices;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;


public class Selector implements Serializable {
	
	private static final long serialVersionUID = 3514604194760824994L;
	
	private JedisResolver jedisResolver;
	private SelectorServices selectorServices;

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	public Selector(String redisHost, int redisPort, int databaseIndex, String password) {
		this.jedisResolver = new JedisResolver(redisHost, redisPort, databaseIndex, password);
		this.selectorServices = new SelectorServices(jedisResolver);
	}

	public DataBean prepareToPersist(String persister, DataBean input) throws BusinessException, InfraException {
		return selectorServices.prepareToPersist(persister, input);
	}
}