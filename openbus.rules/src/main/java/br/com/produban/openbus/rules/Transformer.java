package br.com.produban.openbus.rules;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.enrichers.EnricherServices;
import br.com.produban.openbus.rules.services.filters.FilterServices;
import br.com.produban.openbus.rules.services.forkers.ForkerServices;
import br.com.produban.openbus.rules.services.selectors.SelectorServices;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.apache.avro.specific.SpecificRecordBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;


public class Transformer implements Serializable {

	private static final long serialVersionUID = -4725356276685231686L;
	
	private JedisResolver jedisResolver;

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	private EnrichmentDatabaseConfiguration enrichmentDatabaseConfiguration;

	public Transformer(String redisHost,
					   int redisPort,
					   int databaseIndex,
					   String password,
					   EnrichmentDatabaseConfiguration enrichmentDatabaseConfiguration) {
		this.enrichmentDatabaseConfiguration = enrichmentDatabaseConfiguration;
		this.jedisResolver = new JedisResolver(redisHost, redisPort, databaseIndex, password);
	}

	/**
	 * Format incoming "Avro Bean", based on rules persisted on redis
	 * @return DataBeans. Possible outputs:
	 * <ul>
	 * 		<li><i>null</i>: not nullable fields are null or host/key is blacklisted</li>
	 * 		<li><i>one bean</i>: original bean;
	 * 		<li><i>two beans</i>: original bean; +forked beans; 
	 * </ul>
	 * @throws BusinessException 
	 * @throws InfraException 
	 */
	public List<DataBean> formatBean(String tool, SpecificRecordBase inputData) throws InfraException, BusinessException {

		// Filter
		if ( !(new FilterServices(jedisResolver).isValidData(inputData)) )
			return null;
		
		// Fork
		List<DataBean> forkedBeans = new ForkerServices(jedisResolver).forkData(tool, inputData);
		if (forkedBeans == null)
			return null;
		
		// Enrich
		new EnricherServices(jedisResolver, enrichmentDatabaseConfiguration).enrichData(forkedBeans);
		
		return forkedBeans;
	}
    
	
	public DataBean prepareToPersist(String persister, DataBean input) throws BusinessException, InfraException {
		return new SelectorServices(jedisResolver).prepareToPersist(persister, input);
	}
	
	
	public void generateUID(DataBean bean) throws BusinessException, InfraException {
		new EnricherServices(jedisResolver, enrichmentDatabaseConfiguration).generateUID(bean);
	}

	public JedisResolver getJedisResolver() {
		return jedisResolver;
	}

	public void setJedisResolver(JedisResolver jedisResolver) {
		this.jedisResolver = jedisResolver;
	}
	
}