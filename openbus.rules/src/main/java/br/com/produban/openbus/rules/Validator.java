package br.com.produban.openbus.rules;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.forkers.ForkerZabbix;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class Validator implements Serializable {

	private static final long serialVersionUID = -1835064607879262698L;

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	private JedisResolver jedisResolver;
	private ForkerZabbix forkerZbx;
	
	private DateTimeFormatter dtf = DateTimeFormat.forPattern("yyy-MM-dd");
	
	public Validator(String redisHost, int redisPort, Integer databaseIndex, String redisPassword) {
		this.jedisResolver = new JedisResolver(redisHost, redisPort, databaseIndex, redisPassword);
		this.forkerZbx = new ForkerZabbix(jedisResolver);
	}	
	
	
	/* Key can be Forked? */
	
	public boolean keyHasTFMRules(String originalKey) throws InfraException, BusinessException {
		Map<String, String> patterns = forkerZbx.getMetricsKeyPatterns(originalKey);
		return (patterns != null) && (patterns.size() > 0);
	}

	
	/* PROFILES */
	
	private boolean serverProfileContainHour(String key, List<String> profileTagValues, int hour) throws InfraException, BusinessException {
		Combiner combiner = new Combiner(profileTagValues);
		String combination = null;
		List<Integer> profileHours = null;
		
		while ( ((combination = combiner.next()) != null) && (profileHours == null) ) {
			profileHours = jedisResolver.getJedis().getProfileHours(key, combination);
		}
		
		if (profileHours == null) {
			throw new BusinessException("Profile not found for " + key);
		}
		
		LOG.debug("\tProfileHours: " + key + "\t" + hour + "\t" + profileHours.toString());
		return profileHours.contains(hour);
	}
	
	
	private boolean serverProfileContainWeekDay(String key, List<String> profileTagValues, int weekDay) throws InfraException, BusinessException {
		Combiner combiner = new Combiner(profileTagValues);
		String combination = null;
		List<Integer> profileWdays = null;
		
		while ( ((combination = combiner.next()) != null) && (profileWdays == null) ) {
			profileWdays = jedisResolver.getJedis().getProfileWeekDays(key, combination);
		}

		if (profileWdays == null) {
			throw new BusinessException("Profile not found for " + key);
		}
		
		LOG.debug("\tProfileWeekDays: " + key + "\t" + weekDay + "\t" + profileWdays.toString());
		return profileWdays.contains(weekDay);
	}
	

	private boolean serverProfileContainMonthDay(String key, List<String> profileTagValues, int monthDay) throws InfraException, BusinessException {
		Combiner combiner = new Combiner(profileTagValues);
		String combination = null;
		List<Integer> profile = null;
		
		while ( ((combination = combiner.next()) != null) && (profile == null) ) {
			profile = jedisResolver.getJedis().getProfileMonthDays(key, combination);
		}

		if (profile == null) {
			throw new BusinessException("Profile not found for " + key);
		}
		
		LOG.debug("\tProfileMonthDays: " + key + "\t" + monthDay + "\t" + profile.toString());
		return profile.contains(monthDay);
	}
	

	private boolean serverProfileContainMonth(String key, List<String> profileTagValues, int month) throws InfraException, BusinessException {
		Combiner combiner = new Combiner(profileTagValues);
		String combination = null;
		List<Integer> profile = null;
		
		while ( ((combination = combiner.next()) != null) && (profile == null) ) {
			profile = jedisResolver.getJedis().getProfileMonths(key, combination);
		}

		if (profile == null) {
			throw new BusinessException("Profile not found for " + key);
		}
		
		LOG.debug("\tProfileMonthDays: " + key + "\t" + month + "\t" + profile.toString());
		return profile.contains(month);
	}
	
	
	private boolean serverProfileIsAHolyday(String key, List<String> profileTagValues, DateTime date) throws InfraException, BusinessException {
		Combiner combiner = new Combiner(profileTagValues);
		String combination = null;
		List<Long> profile = null;
		
		while ( ((combination = combiner.next()) != null) && (profile == null) ) {
			profile = jedisResolver.getJedis().getProfileHolydays(key, combination);
		}
		
		if (profile == null) {
			throw new BusinessException("Profile not found for " + key);
		}
		
		LOG.debug("\tProfileHolydays: " + key + "\t" + date + "\t" + profile.toString());
		return profile.contains(date.getMillis());
	}
	
	
	private boolean serverProfileIsUnderMaintainance(String key, List<String> profileTagValues, DateTime date) throws InfraException, BusinessException {
		Combiner combiner = new Combiner(profileTagValues);
		String combination = null;
		List<Interval> holydays = null;
		
		while ( ((combination = combiner.next()) != null) && (holydays == null) ) {
			holydays = jedisResolver.getJedis().getProfileMaintenance(key, combination);
		}

		if (holydays == null) {
			throw new BusinessException("Profile not found for " + key);
		}
		
		try {
			for (Interval interval : holydays) {
				LOG.debug("\tProfileHolydays: " + key + "\t" + date.toString() + "\t" 
							+ interval.getStartMillis() + "--" + interval.getEndMillis());
				if (interval.contains(date)) return true;
			}
		} catch (NumberFormatException e) {
			throw new BusinessException(e); 
		}
		return false;
	}
	
	
	public boolean isValidHour(String key, long clock, Map<String, String> tags) throws InfraException, BusinessException {
		DateTime date = new DateTime(clock);

		String lookupKey = key;
		List<String> profileTags = lookupProfile(key);
		if (profileTags == null) {
			lookupKey = "default";
			profileTags = lookupProfile(lookupKey);
		}
		
		List<String> profileTagValues = new ArrayList<String>();
		if (profileTags != null) {
			for (String tagK : profileTags) {
				String tagV = tags.get(tagK);
				if (tagV != null) {
					profileTagValues.add(tagV);
				} else {
					profileTagValues.add("null");
				}
			}
		}
		
		if (!serverProfileContainHour(lookupKey, profileTagValues, date.getHourOfDay())) return false;
		
		if (!serverProfileContainWeekDay(lookupKey, profileTagValues, date.getDayOfWeek())) return false;
		
		if (!serverProfileContainMonthDay(lookupKey, profileTagValues, date.getDayOfMonth())) return false;

		//FIXME: Adjust months on Redis if necessary
		//if (!serverProfileContainMonth(lookupKey, profileTagValues, date.getMonthOfYear())) return false;
		
		DateTime yyyyMMdd = dtf.parseDateTime(date.toString("yyyy-MM-dd"));
		if (serverProfileIsAHolyday(lookupKey, profileTagValues, yyyyMMdd)) return false;
		
		if (serverProfileIsUnderMaintainance(lookupKey, profileTagValues, yyyyMMdd)) return false;
		
		return true;
	}
	
	
	
	public List<String> lookupProfile(String key) throws InfraException {
		return jedisResolver.getJedis().getLookupProfile(key);
	}
	
	
	
	/* Thresholds */
	
	public Map<String, String> getThresholdConfig(String key, Map<String, String> tags) throws BusinessException, InfraException {
		
		List<String> capacityLookup = jedisResolver.getJedis().getCapacityLookup(key);
		
		List<String> selectedTags = new ArrayList<String>();
		for(String lookup : capacityLookup) {
			String tagValue = tags.get(lookup);
			selectedTags.add(tagValue == null? "null" : tagValue);
		}
		
		Combiner combiner = new Combiner(selectedTags);

		String combination = null;
		Map<String, String> thresholds = null;
		while ( (combination = combiner.next()) != null && thresholds == null) {
			thresholds = jedisResolver.getJedis().getCapacityThresholds(key, combination);
		}
		
		if (thresholds == null || thresholds.isEmpty()) {
			thresholds = jedisResolver.getJedis().getCapacityThresholds(key, "default");
			if (thresholds == null || thresholds.isEmpty()) {
				return null;
			}
		}
		
		return thresholds;
	}

	
	
	/* Holydays */
	public Set<String> getHolydays() throws InfraException {
		return jedisResolver.getJedis().getHolydays();
	}
	
}
