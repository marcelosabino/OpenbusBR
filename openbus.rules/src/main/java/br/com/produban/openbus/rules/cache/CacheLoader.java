package br.com.produban.openbus.rules.cache;

import br.com.produban.openbus.security.exceptions.InfraException;

import java.util.concurrent.Callable;

public interface CacheLoader<T> extends Callable<T> {

    String getCacheKey() throws InfraException;

}
