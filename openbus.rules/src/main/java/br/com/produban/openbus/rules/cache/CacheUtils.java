package br.com.produban.openbus.rules.cache;

import br.com.produban.openbus.security.exceptions.InfraException;
import com.google.common.cache.Cache;
import redis.clients.jedis.exceptions.JedisException;

import java.util.concurrent.ExecutionException;

public class CacheUtils {
    public static <T> T run(Cache<String,T> cache, CacheLoader<T> callable) throws InfraException {
        try {
            return (
                callable.getCacheKey() != null && !callable.getCacheKey().isEmpty() ?
                        (T) cache.get(callable.getCacheKey(), callable) :
                        null
            );
        } catch (ExecutionException e) {
            throw new InfraException(e);
        } catch (JedisException e) {
            throw new InfraException(e);
        }
    }
    
    public static boolean exits(String key, Cache<String,?> cache) {
    	return cache.getIfPresent(key) == null ? false : true;
    }
}
