package br.com.produban.openbus.rules.config;

import java.io.Serializable;

public class EnrichmentDatabaseConfiguration implements Serializable {

    private static final long serialVersionUID = 1389334810046822873L;

    private String url;
    private String user;
    private String password;
    private String traverseQuery;
    private String traverseQueryIn;
    private String traverseQueryOut;
    private String transactionalCypherPath;

    public String getTraverseQuery() {
        return traverseQuery;
    }

    public void setTraverseQuery(String traverseQuery) {
        this.traverseQuery = traverseQuery;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTransactionalCypherPath() {
        return transactionalCypherPath;
    }

    public void setTransactionalCypherPath(String transactionalCypherPath) {
        this.transactionalCypherPath = transactionalCypherPath;
    }

	public String getTraverseQueryIn() {
		return traverseQueryIn;
	}

	public void setTraverseQueryIn(String traverseQueryIn) {
		this.traverseQueryIn = traverseQueryIn;
	}

	public String getTraverseQueryOut() {
		return traverseQueryOut;
	}

	public void setTraverseQueryOut(String traverseQueryOut) {
		this.traverseQueryOut = traverseQueryOut;
	}
    
}
