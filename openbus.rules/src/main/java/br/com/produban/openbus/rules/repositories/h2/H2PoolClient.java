package br.com.produban.openbus.rules.repositories.h2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class H2PoolClient {

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	private String url;
	private String user;
	private String pswd;
	private int maxConn;
	private static JdbcConnectionPool pool = null;


	/**
	 * 
	 * @param url
	 * @param user
	 * @param pswd
	 * @param maxConn
	 */
	public H2PoolClient(String url, String user, String pswd, int maxConn) {
		this.url = url;
		this.user = user;
		this.pswd = pswd;
		this.maxConn = maxConn;

		init();
	}

	/* */

	private void init() {
		this.getPool();
	}

	private JdbcConnectionPool getPool() {
		if (pool == null) {
			LOG.info("> creating pool with "+maxConn+" connections");
			pool = JdbcConnectionPool.create(url, user, pswd);
			pool.setMaxConnections(maxConn);
		}
		return pool;
	}


	private Connection getConnection() throws InfraException {
		try {
			return getPool().getConnection();
		} catch (SQLException e) {
			throw new InfraException(e);
		}
	}


	public void disposePool() {
		if (pool != null) {
			pool.dispose();
		}
	}

	/* */

	public void updateSql(String sql) throws InfraException, BusinessException {
		Connection conn = null;
		PreparedStatement prepareStatement = null;
		ResultSet rs = null;

		LOG.debug("> connecting to: "+ url + ", " + user + ", " + pswd);
		conn = this.getConnection();
		try {
			prepareStatement = conn.prepareStatement(sql);
			prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new BusinessException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new InfraException(e);
				}
			}
			
			if (prepareStatement != null) {
				try {
					prepareStatement.close();
				} catch (SQLException e) {
					throw new InfraException(e);
				}
			}
			
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					throw new InfraException(e);
				}
			}
			
		}
	}

}
