package br.com.produban.openbus.rules.repositories.jedis;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.exceptions.JedisException;
import br.com.produban.openbus.security.exceptions.InfraException;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class JedisRepository implements Serializable {
	private static final long serialVersionUID = 2794481276513643388L;

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	// FILTER
	private static final String METRICS_HOSTBLACKLIST = "Metrics:HostBlackList";
	private static final String METRICS_KEYBLACKLIST = "Metrics:KeyBlackList";

	// FORK
	private static final String METRICS_KEY_PREFIX_TO_PATTERN = "Metrics:KeyPrefix_to_KeyPattern";
	private static final String METRICS_KEY_PREFIX_TO_TEMPLATE = "Metrics:KeyPrefix_to_KeyTemplate";
	private static final String METRICS_KEY_PREFIX_TO_TAGS_TEMPLATE = "Metrics:KeyPrefix_to_TagsTemplate";
	private static final String METRICS_KEY_PREFIX_TO_MATH_EXPRESSION = "Metrics:KeyPrefix_to_MathExpression";

	// ENRICH
	private static final String METRICS_TAG_VALUE_TO_FIELD = "Metrics:TagValue_to_FieldValue";
	private static final String IP_TO_HOSTNAME = "IpToHostname";
	private static final String LOOKUP_FOR_ADDITIONAL_INFO = "LookupForAdditionalInfo";

	// FIELD SELECTOR
	private static final String SELECTOR_WHITELIST_PREFIX = "FieldsWhitelist:";
	private static final String SELECTOR_BLACKLIST_PREFIX = "FieldsBlacklist:";

	// CLEAN
	private static final String HOST_CHARACTER_CLEAN = "HostCharacterClean";
	private static final String KEY_CHARACTER_CLEAN = "KeyCharacterClean";

	// SERVER PROFILE
	private static final String PROFILE_HOURS       = "ProfileHours:";
	private static final String PROFILE_WEEKDAYS    = "ProfileWeekDays:";
	private static final String PROFILE_MONTHDAYS   = "ProfileMonthDays:";
	private static final String PROFILE_MONTHS      = "ProfileMonths:";
	private static final String PROFILE_HOLYDAYS    = "ProfileHolydays:";
	private static final String PROFILE_MAINTENANCE = "ProfileMaintenance:";
	//
	private static final String PROFILE_LOOKUP = "ProfileLookup";

	// THRESHOLDS
	private static final String THRESHOLDS = "Thresholds:"; // Thresholds:key
	
	private static final String CAPACITY_LOOKUP = "CapacityLookup";
	private static final String CAPACITY_THRESHOLDS = "CapacityThresholds:";

	// BASELINE
	private static final String HOLYDAYS = "Holydays";

	// STATISTICAL MODEL
	private static final String STATISTICAL_MODEL = "StatModel:";
	
	private static Cache<String,Object> cache = CacheBuilder.newBuilder()
			.expireAfterWrite(5, TimeUnit.MINUTES)
			.build();


	private final Jedis jedis;
	private Pipeline pipeline;

	private String redisHost;
	private int redisPort;
	private String redisPassword;
	private int redisDatabaseIndex;

	private JedisRepository(Builder builder) throws InfraException {
		redisDatabaseIndex = builder.redisDatabaseIndex;
		redisHost = builder.redisHost;
		redisPort = builder.redisPort;
		redisPassword = builder.redisPassword;

		LOG.info(">>> JedisRepository: new Jedis client...");
		try {
			URI uri = new URI("redis",String.format("%s:%s","openbus",redisPassword),redisHost,redisPort,String.format("/%s", redisDatabaseIndex),null,null);
			jedis = new Jedis(uri);
			jedis.ping();
			LOG.info(">>> JedisRepository: ...connected!");
		} catch (JedisException e) {
			throw new InfraException(e);
		} catch (URISyntaxException e) {
			throw new InfraException(e);
		}
	}



	/* REDIS PIPELINE */

	public void pipeInit() throws InfraException {
		try {
			pipeline = jedis.pipelined();
			pipeline.multi();
		} catch (JedisException e) {
			throw new InfraException(e);
		}
	}

	public void pipeSend() throws InfraException {
		try {
			pipeline.exec();
			pipeline.sync();
		} catch (JedisException e) {
			throw new InfraException(e);
		}
	}

	public void pipeHset(String key, String field, String value, int ttl) throws InfraException {
		try {
			pipeline.hset(key, field, value);
			pipeline.expire(key, ttl);
		} catch (JedisException e) {
			throw new InfraException(e);
		}
	}



	/* BLACKLISTS */

	/**
	 * List of all blacklisted Hostnames for metrics
	 * @return
	 * @throws InfraException 
	 */
	public Set<String> getMetricsHostBlackList() throws InfraException {
		return runOnJedis(new JedisCallable<Set<String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_HOSTBLACKLIST;
			}

			@Override
			public Set<String> call() throws Exception {
				return jedis.smembers(getCacheKey());
			}
		});
	}

	public Set<String> getMetricsKeyBlackList() throws InfraException {
		return runOnJedis(new JedisCallable<Set<String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_KEYBLACKLIST;
			}

			@Override
			public Set<String> call() throws Exception {
				return jedis.smembers(getCacheKey());
			}
		});
	}



	/* FORK */

	public String getMetricsKeyPattern(String keyPrefix) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_KEY_PREFIX_TO_PATTERN;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(keyPrefix);
	}

	public String getMetricsKeyTemplate(String keyPrefix) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_KEY_PREFIX_TO_TEMPLATE;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(keyPrefix);
	}

	public String getMetricsTagsTemplate(String keyPrefix) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_KEY_PREFIX_TO_TAGS_TEMPLATE;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(keyPrefix);
	}

	public String getMetricsMathExpression(String keyPrefix) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_KEY_PREFIX_TO_MATH_EXPRESSION;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(keyPrefix);
	}



	/* ENRICH */

	public String getMetricsTagToField(String keyPrefix) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return METRICS_TAG_VALUE_TO_FIELD;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(keyPrefix);
	}
	
	
	public String getHostnameFromIp(String rawHostname) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return IP_TO_HOSTNAME;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(rawHostname);
	}


	public String getLookupForAdditionalInfo(String keyPrefix) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return LOOKUP_FOR_ADDITIONAL_INFO;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		}).get(keyPrefix);
	}



	public Map<String, String> hgetAll(final String hash) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,String>>() {
			@Override
			public String getCacheKey() {
				return hash;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		});
	}



	/* FIELDS SELECTOR */

	public List<Pattern> getFieldsWhitelistByPersisterAndKey(final String persister,
			final String key) throws InfraException {
		return runOnJedis(new JedisCallable<List<Pattern>>() {
			@Override
			public String getCacheKey() {
				return SELECTOR_WHITELIST_PREFIX+persister+":"+key;
			}

			@Override
			public List<Pattern> call() throws Exception {
				List<Pattern> patterns = new ArrayList<Pattern>();
				Set<String> patternsList = jedis.smembers(getCacheKey());
				
				if (patternsList == null || patternsList.isEmpty()) {
					return patterns;
				} else {
					Iterator<String> it = patternsList.iterator();
					while (it.hasNext()) {
						String pattern = it.next().toString();
						patterns.add(Pattern.compile(pattern));
					}
	
					return patterns;
				}
			}
		});
	}


	public List<Pattern> getFieldsBlacklistByPersisterAndKey(final String persister,
			final String key) throws InfraException {
		return runOnJedis(new JedisCallable<List<Pattern>>() {
			@Override
			public String getCacheKey() {
				return SELECTOR_BLACKLIST_PREFIX+persister+":"+key;
			}

			@Override
			public List<Pattern> call() throws Exception {
				List<Pattern> patterns = new ArrayList<Pattern>();
				Set<String> patternsList = jedis.smembers(getCacheKey());
				
				if (patternsList == null || patternsList.isEmpty()) {
					return patterns;
				} else {
					Iterator<String> it = patternsList.iterator();
					while (it.hasNext()) {
						String pattern = it.next().toString();
						patterns.add(Pattern.compile(pattern));
					}
	
					return patterns;
				}
			}
		});
	}


	
	/* CLEAN */

	public Map<String, String> getHostCharacterClean() throws InfraException {
		return runOnJedis(new JedisCallable<Map<String, String>>() {
			@Override
			public String getCacheKey() {
				return HOST_CHARACTER_CLEAN;
			}

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}
		});
	}

	/**
	 * Use "KeyCharacterClean" redis map, escapes all the characters and returns a regex map.<br>
	 * Eg.:
	 * <ul>KeyCharacterClean "," "."<br>
	 * KeyCharacterClean "=" "."<br>
	 * KeyCharacterClean ":" "."<br>
	 * KeyCharacterClean "[" "_"<br>
	 * KeyCharacterClean "]" "_"<br>
	 * KeyCharacterClean "(" "_"<br>
	 * KeyCharacterClean ")" "_"<br>
	 * ...</ul>
	 * 
	 * @return Eg.: { 
	 * <ul>"." : "[,=:]+",<br>
	 * "_" : "[\[\]\(\)]+",<br>
	 * ... }</ul>
	 * @throws InfraException
	 */
	public Map<String, String> getKeyCharacterClean() throws InfraException {
		return runOnJedis(new JedisCallable<Map<String, String>>() {
			@Override
			public String getCacheKey() {
				return KEY_CHARACTER_CLEAN;
			}

			@Override
			public Map<String, String> call() throws Exception {
				HashMap<String, String> patterns = new HashMap<String, String>();
				for (Entry<String, String> entry : jedis.hgetAll(getCacheKey()).entrySet()) {
					String pattern = patterns.get(entry.getValue());
					pattern = (pattern == null)? Pattern.quote(entry.getKey()) : pattern + Pattern.quote(entry.getKey());
					patterns.put(entry.getValue(), pattern);
				}

				HashMap<String, String> regexMap = new HashMap<String, String>();
				for (Entry<String, String> entry : patterns.entrySet()) {
					regexMap.put(entry.getKey(), "[" + entry.getValue() + "]+");
				}
				return regexMap;
			}
		});
	}


	/* PROFILES */

	// hours
	public List<Integer> getProfileHours(final String profile, String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,List<Integer>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_HOURS + profile;
			}

			@Override
			public Map<String, List<Integer>> call() throws Exception {
				Map<String, List<Integer>> profileHours = new HashMap<String, List<Integer>>();	

				Map<String, String> profileStrHours = jedis.hgetAll(getCacheKey());
				if (profileStrHours != null) {
					for (Entry <String, String> entry : profileStrHours.entrySet()) {
						List<Integer> hourList = new ArrayList<Integer>();
						for (String hora : entry.getValue().split(";")) {
							hourList.add(Integer.parseInt(hora));
						}
						profileHours.put(entry.getKey(), hourList);
					}
				}

				return profileHours;
			}
		}).get(key);
	}
	
	
	// Week Days
	public List<Integer> getProfileWeekDays(final String profile, String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,List<Integer>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_WEEKDAYS + profile;
			}

			@Override
			public Map<String, List<Integer>> call() throws Exception {
				Map<String, List<Integer>> profileWdays = new HashMap<String, List<Integer>>();	
				
				Map<String, String> profileStrWdays = jedis.hgetAll(getCacheKey());
				if (profileStrWdays != null) {
					for (Entry <String, String> entry : profileStrWdays.entrySet()) {
						List<Integer> dayList = new ArrayList<Integer>();
						for (String day : entry.getValue().split(";")) {
							dayList.add(Integer.parseInt(day));
						}
						profileWdays.put(entry.getKey(), dayList);
					}
				}

				return profileWdays;
			}
		}).get(key);
	}

	
	// Month Days
	public List<Integer> getProfileMonths(final String profile, String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,List<Integer>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_MONTHS + profile;
			}

			@Override
			public Map<String, List<Integer>> call() throws Exception {
				Map<String, List<Integer>> profileMonths = new HashMap<String, List<Integer>>();	
				
				Map<String, String> profileStrMonths = jedis.hgetAll(getCacheKey());
				if (profileStrMonths != null) {
					for (Entry <String, String> entry : profileStrMonths.entrySet()) {
						List<Integer> monthList = new ArrayList<Integer>();
						for (String month : entry.getValue().split(";")) {
							monthList.add(Integer.parseInt(month));
						}
						profileMonths.put(entry.getKey(), monthList);
					}
				}

				return profileMonths;
			}
		}).get(key);
	}
	
	
	// Month Days
	public List<Integer> getProfileMonthDays(final String profile, String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,List<Integer>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_MONTHDAYS + profile;
			}

			@Override
			public Map<String, List<Integer>> call() throws Exception {
				Map<String, List<Integer>> profileMdays = new HashMap<String, List<Integer>>();	
				
				Map<String, String> profileStrMdays = jedis.hgetAll(getCacheKey());
				if (profileStrMdays != null) {
					for (Entry <String, String> entry : profileStrMdays.entrySet()) {
						List<Integer> dayList = new ArrayList<Integer>();
						for (String day : entry.getValue().split(";")) {
							dayList.add(Integer.parseInt(day));
						}
						profileMdays.put(entry.getKey(), dayList);
					}
				}

				return profileMdays;
			}
		}).get(key);
	}
	
	
	// Holydays
	public List<Long> getProfileHolydays(final String profile, String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,List<Long>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_HOLYDAYS + profile;
			}

			@Override
			public Map<String, List<Long>> call() throws Exception {
				Map<String, List<Long>> profileHdays = new HashMap<String, List<Long>>();	
				
				Map<String, String> profileStrHdays = jedis.hgetAll(getCacheKey());
				if (profileStrHdays != null) {
					DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
					for (Entry <String, String> entry : profileStrHdays.entrySet()) {
						List<Long> dayList = new ArrayList<Long>();
						for (String holyday : entry.getValue().split(";")) {
							DateTime dtHolyday = dtf.parseDateTime(holyday);
							dayList.add(dtHolyday.getMillis());
						}
						profileHdays.put(entry.getKey(), dayList);
					}
				}

				return profileHdays;
			}
		}).get(key);
	}
	
	
	// Maintenance
	public List<Interval> getProfileMaintenance(final String profile, String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String,List<Interval>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_MAINTENANCE + profile;
			}

			@Override
			public Map<String, List<Interval>> call() throws Exception {
				Map<String, List<Interval>> profileHdays = new HashMap<String, List<Interval>>();	
				
				Map<String, String> profileStrHdays = jedis.hgetAll(getCacheKey());
				if (profileStrHdays != null) {
					DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm");
					for (Entry <String, String> entry : profileStrHdays.entrySet()) {
						List<Interval> dayList = new ArrayList<Interval>();
						for (String holyday : entry.getValue().split(";")) {
							String holydayInterval[] = holyday.split("--"); 
							DateTime start = dtf.parseDateTime(holydayInterval[0]);
							DateTime end = dtf.parseDateTime(holydayInterval[1]);
							dayList.add(new Interval(start, end));
						}
						profileHdays.put(entry.getKey(), dayList);
					}
				}

				return profileHdays;
			}
		}).get(key);
	}
	
	
	// Lookup Profile
	public List<String> getLookupProfile(String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String, List<String>>>() {
			@Override
			public String getCacheKey() {
				return PROFILE_LOOKUP;
			}

			@Override
			public Map<String, List<String>> call() throws Exception {
				Map<String, List<String>> profileTags = new HashMap<String, List<String>>();	
				
				Map<String, String> profiles = jedis.hgetAll(getCacheKey());
				if (profiles != null) {
					for (Entry <String, String> entry : profiles.entrySet()) {
						List<String> tags = new ArrayList<String>();
						if (entry.getValue() != null) {
							tags.addAll(Arrays.asList(entry.getValue().split(";")));
						}
						profileTags.put(entry.getKey(), tags);
					}
				}

				return profileTags;
			}
		}).get(key);
	}

	

	/* THRESHOLDS */

	public Map<String, String> getThresholdConfig(final String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String, String>>() {

			@Override
			public Map<String, String> call() throws Exception {
				return jedis.hgetAll(getCacheKey());
			}

			@Override
			public String getCacheKey() {
				return THRESHOLDS + key;
			}
		});
		
	}
	

	public List<String> getCapacityLookup(String key) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String, List<String>>>() {
			@Override
			public Map<String, List<String>> call() throws Exception {
				Map<String, List<String>> response = new HashMap<String, List<String>>();
				
				Map<String, String> hash = jedis.hgetAll(getCacheKey());
				for(Entry<String, String> entry : hash.entrySet()) {
					response.put(entry.getKey(), Arrays.asList(entry.getValue().split(";")));
				}
				
				return response;
			}

			@Override
			public String getCacheKey() {
				return CAPACITY_LOOKUP;
			}
		}).get(key);
	}


	public Map<String, String> getCapacityThresholds(final String key, String combination) throws InfraException {
		return runOnJedis(new JedisCallable<Map<String, Map<String, String>>>() {
			@Override
			public Map<String, Map<String, String>> call() throws Exception {
				Map<String, Map<String, String>> response = new HashMap<String, Map<String, String>>();
				
				Map<String, String> hash = jedis.hgetAll(getCacheKey()); // <-- Map<combination, threshold_configs>
				for(Entry<String, String> entry : hash.entrySet()) {
					Map<String, String> kvMap = new HashMap<String, String>();
					for (String value : entry.getValue().split(";")) { // <-- String[]: configName=configValue
						String[] kv = value.split("=");
						kvMap.put(kv[0],  kv[1]); // <-- Map<configName,configValue>
					}
					response.put(entry.getKey(), kvMap);
				}
				return response; // <-- Map<key, Map<combination, threshold_configs>>
			}

			@Override
			public String getCacheKey() {
				return CAPACITY_THRESHOLDS + key;
			}
		}).get(combination);
	}




	/* Holydays */
	public Set<String> getHolydays() throws InfraException {
		return runOnJedis(new JedisCallable<Set<String>>() {
			@Override
			public String getCacheKey() {
				return HOLYDAYS;
			}

			@Override
			public Set<String> call() throws Exception {
				return jedis.smembers(getCacheKey());
			}
		});
	}

    public Map<String, String> getStatisticalModel(final String uid) throws InfraException {
        return runOnJedis(new JedisCallable<Map<String, Map<String, String>>>() {
            @Override
            public Map<String, Map<String, String>> call() throws Exception {
                Map<String, Map<String, String>> response = new HashMap<String, Map<String, String>>();

                Map<String, String> hash = jedis.hgetAll(getCacheKey());
                response.put(uid, hash);
                return response;
            }

            @Override
            public String getCacheKey() {
                return STATISTICAL_MODEL + uid;
            }
        }).get(uid);
    }

	
	/* JEDIS BUILDER */

	public static Builder newBuilder() {
		return new Builder();
	}

	public static final class Builder {
		private int redisDatabaseIndex;
		private String redisHost;
		private int redisPort;
		private String redisPassword;

		private Builder() {
		}

		public Builder withRedisDatabaseIndex(int redisDatabaseIndex) {
			this.redisDatabaseIndex = redisDatabaseIndex;
			return this;
		}

		public Builder withRedisHost(String redisHost) {
			this.redisHost = redisHost;
			return this;
		}

		public Builder withRedisPort(int redisPort) {
			this.redisPort = redisPort;
			return this;
		}

		public Builder withRedisPassword(String redisPassword) {
			this.redisPassword = redisPassword;
			return this;
		}

		public JedisRepository build() throws InfraException {
			return new JedisRepository(this);
		}
	}



	/* CACHE */

	private <T> T runOnJedis(JedisCallable<T> callable) throws InfraException {
		try {
			return (T) cache.get(callable.getCacheKey(), callable);
		} catch (ExecutionException e) {
			throw new InfraException(e);
		} catch (JedisException e) {
			throw new InfraException(e);
		}
	}




}

interface JedisCallable<T> extends Callable<T> {
	public String getCacheKey();
}