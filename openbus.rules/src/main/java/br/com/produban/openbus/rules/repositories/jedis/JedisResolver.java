package br.com.produban.openbus.rules.repositories.jedis;

import java.io.Serializable;

import br.com.produban.openbus.security.exceptions.InfraException;

public class JedisResolver implements Serializable {
	private static ThreadLocal<JedisRepository> jedis = new ThreadLocal<>();
	
	private String redisHost;
	private int redisPort;
	private int databaseIndex;
	private String redisPassword;
	
	public JedisResolver(String redisHost, int redisPort, int databaseIndex, String password) {
		this.redisHost = redisHost;
		this.redisPort = redisPort;
		this.databaseIndex = databaseIndex;
		this.redisPassword = password;
	}
	
	public JedisRepository getJedis() throws InfraException {
		if (jedis.get() == null) {
			jedis.set(JedisRepository.newBuilder()
					.withRedisHost(redisHost)
					   .withRedisPort(redisPort)
					   .withRedisPassword(redisPassword)
					   .withRedisDatabaseIndex(databaseIndex)
					   .build());
		}
		return jedis.get();
	}
}
