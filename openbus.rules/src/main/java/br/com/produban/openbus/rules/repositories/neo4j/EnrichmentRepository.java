package br.com.produban.openbus.rules.repositories.neo4j;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.cache.CacheLoader;
import br.com.produban.openbus.rules.cache.CacheUtils;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.el.ELProcessor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static br.com.produban.openbus.rules.cache.CacheUtils.exits;
import static br.com.produban.openbus.rules.cache.CacheUtils.run;

import static br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTraversalModel.*;

public class EnrichmentRepository implements Serializable {

    protected final Logger LOG = LoggerFactory.getLogger(EnrichmentRepository.class);

    private static final String ENRICH_PREFIX = "ENRICH:%s";
    private static final String ENRICH_DATA = ENRICH_PREFIX + ":%s";

    private static final String VALUE_EXPRESSION_KEY = "valueExpression";
    private static final String VALUE_EXPRESSION_INDEXED_KEY = VALUE_EXPRESSION_KEY + ".%s";
    private static final String DISCOVER_QUERY = "discoverQuery";

    private static Cache<String,Optional<Map<String,String>>> dataCacheExits = CacheBuilder.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();
    
    private static Cache<String,Optional<Map<String,String>>> dataCacheNotExits = CacheBuilder.newBuilder()
            .expireAfterWrite(24, TimeUnit.HOURS)
            .build();

    private JedisResolver jedis;
    private final EnrichmentDatabaseConfiguration config;

    public EnrichmentRepository(JedisResolver jedis, EnrichmentDatabaseConfiguration config) {
        this.jedis = jedis;
        this.config = config;
    }

    public Map<String,String> getEnrichmentData(final DataBean bean) throws InfraException, BusinessException {
        final Map<String,String> ruleSet = getEnrichmentRuleSet(bean);

        if (ruleSet==null || ruleSet.isEmpty()) {
            if (LOG.isWarnEnabled())
                LOG.warn("Rule set {} cannot be found! Please verify",
                        resolveSchemaName(bean),
                        resolveEnrichmentRulesKey(bean));
            return null;
        } else {
        	String key = keyCache(bean);
        	Map<String, String> data = new HashMap<>();
        	
        	if (!exits(key, dataCacheExits) && !exits(key, dataCacheNotExits)) {
                Neo4JRepository repository = new Neo4JRepository(config);
                List<NodeData> nodes;
                nodes = repository.traverseNodePath(ruleSet.get(DISCOVER_QUERY),
                                                    resolveDataBeanLookupValue(bean));
                if (nodes == null) {
                	dataCacheNotExits.put(key, Optional.fromNullable(data));
                } else {
                    if (LOG.isDebugEnabled())
                        LOG.debug("Discovered data: {}",nodes);
                    data = joinPathData(nodes);
                    dataCacheExits.put(key, Optional.fromNullable(data));
                }
        	} else {
        		data = dataCacheExits.getIfPresent(key) != null ? 
        				dataCacheExits.getIfPresent(key).get() : 
        				(dataCacheNotExits.getIfPresent(key) != null ? dataCacheNotExits.getIfPresent(key).get() : null); 
        	}
        	return data;
        }
    }
    
    private String keyCache(DataBean bean) throws InfraException {
    	// Key for cache
        String key = resolveEnrichmentDataKey(bean);
        if (LOG.isDebugEnabled())
            LOG.debug("Enrichment data key: {}",key);

        if (key == null)
            if (LOG.isWarnEnabled())
                LOG.warn("Enrichment key is null for tool {} and bean {}",key,bean.getBean());
        return key;
    }

    private Map<String, String> joinPathData(List<NodeData> nodes) {
        if (nodes != null) {
            Map<String, String> response = new HashMap<String, String>();
            for (NodeData node : nodes) {
            	Map<String, String> map = node.getData();
            	
            	for (String key : map.keySet()) {
        			String value = response.get(key);

            		if (response.containsKey(key)) {
            			String newValue = map.get(key);
            			if (newValue.trim().equals(value)) {
            				value = newValue;
            			} else {
            				value += ";" + map.get(key);	
            			}
            		} else {
            			value = map.get(key);
            		}
            		
            		if (!"".equals(value)) {
            			response.put(key, value);	
            		}
            	}
            }
            return response;
        } else
            return null;
    }

    private Map<String,String> getEnrichmentRuleSet(final DataBean bean) throws InfraException {
        return run(dataCacheExits, new CacheLoader<Optional<Map<String, String>>>() {
            @Override
            public String getCacheKey() {
                return resolveEnrichmentRulesKey(bean);
            }

            @Override
            public Optional<Map<String, String>> call() throws Exception {
                return Optional.fromNullable(jedis.getJedis().hgetAll(getCacheKey()));
            }
        }).orNull();
    }

    private String[] resolveDataBeanLookupValue(DataBean bean) throws InfraException {
        Map<String,String> ruleSet = getEnrichmentRuleSet(bean);
        ELProcessor processor = new ELProcessor();
        processor.defineBean("bean",bean);
        int index = 0;
        List<String> result = new ArrayList<>();
        while (ruleSet.containsKey(String.format(VALUE_EXPRESSION_INDEXED_KEY,index))) {
            Object evaluatedValue = processor.eval(ruleSet.get(String.format(VALUE_EXPRESSION_INDEXED_KEY,index)));
            if (evaluatedValue != null && !evaluatedValue.toString().isEmpty())
                result.add(evaluatedValue.toString());

            index++;
        }

        if (result.isEmpty())
            return null;
        else
            return result.toArray(new String[result.size()]);
    }

    private String resolveEnrichmentDataKey(DataBean bean) throws InfraException {
        String[] values = resolveDataBeanLookupValue(bean);
        if (values != null)
            return String.format(ENRICH_DATA, resolveSchemaName(bean), Joiner.on("\t").join(values));
        else
            return null;
    }

    private String resolveEnrichmentRulesKey(DataBean bean) {
        return String.format(ENRICH_PREFIX, resolveSchemaName(bean));
    }

    private String resolveSchemaName(DataBean bean) {
        return bean.getTool();
    }
}
