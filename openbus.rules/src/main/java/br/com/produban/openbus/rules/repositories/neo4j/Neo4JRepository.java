package br.com.produban.openbus.rules.repositories.neo4j;

import static br.com.produban.openbus.rules.cache.CacheUtils.run;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.http.BasicAuthentication;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import br.com.produban.openbus.rules.cache.CacheLoader;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTransactionalCypherModel.RequestEntity;
import br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTransactionalCypherModel.RequestRootEntity;
import br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTransactionalCypherModel.ResponseRootEntity;
import br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTransactionalCypherModel.RestEntity;
import br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTraversalModel.NodeData;
import br.com.produban.openbus.rules.repositories.neo4j.parsing.Neo4JTraversalModel.NodeList;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class Neo4JRepository {

    protected final Logger LOG = LoggerFactory.getLogger(Neo4JRepository.class);

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new GsonFactory();

    private static HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
        @Override
        public void initialize(HttpRequest request) {
            request.setParser(new JsonObjectParser(JSON_FACTORY));
            request.setHeaders(new HttpHeaders().setAccept("application/json").setContentType("application/json"));
        }
    });

    private static Cache<String,Optional<RestEntity>> idCache = CacheBuilder.newBuilder()
            .expireAfterWrite(30, TimeUnit.MINUTES)
            .build();

    private EnrichmentDatabaseConfiguration config;

    private void init(EnrichmentDatabaseConfiguration config) {}

    public Neo4JRepository(EnrichmentDatabaseConfiguration config) {
        this.config = config;
        init(config);
    }

    public List<NodeData> traverseNodePath(final String cypherQuery, final String[] lookupValues) throws InfraException, BusinessException {
        final String encodedQuery = encodeCypherQuery(cypherQuery,lookupValues);
        Optional<RestEntity> discoveredNode = run(idCache, new CacheLoader<Optional<RestEntity>>() {
            @Override
            public String getCacheKey() throws InfraException {
                return encodedQuery;
            }

            @Override
            public Optional<RestEntity> call() throws Exception {
                if (LOG.isDebugEnabled())
                    LOG.debug("Trying to discover node for query: [{}]",encodedQuery);

                RestEntity node = getDiscoveredNode(encodedQuery);
                if (node!=null)
                    node.setTraverseUrl(node.getTraverseUrl().replaceAll("\\{.*\\}", "node"));

                if (LOG.isDebugEnabled())
                    LOG.debug("Discovered data for query [{}] is: {}", encodedQuery, node);

                return Optional.fromNullable(node);
            }
        });

        return discoveredNode.isPresent() ? getTraversalData(discoveredNode.get().getTraverseUrl()) : null;
    }
    
    private List<NodeData> getTraversalData(String traverseUrl) throws InfraException {
        try {
            GenericUrl traversalUrl = new GenericUrl(traverseUrl);
            HttpRequest traversalRequest = null;
            
            // Efetua execu��o de um traversal in
            traversalRequest = requestFactory.buildPostRequest(
                    traversalUrl,
                    ByteArrayContent.fromString(null, config.getTraverseQueryIn())
            );
            traversalRequest.setInterceptor(new BasicAuthentication(config.getUser(), config.getPassword()));
            List<NodeData> data = traversalRequest.execute().parseAs(NodeList.class);

            // Efetua execu��o de um traversal out
            traversalRequest = requestFactory.buildPostRequest(
                    traversalUrl,
                    ByteArrayContent.fromString(null, config.getTraverseQueryOut())
            );
            traversalRequest.setInterceptor(new BasicAuthentication(config.getUser(), config.getPassword()));
            data.addAll(traversalRequest.execute().parseAs(NodeList.class));
            
            if (LOG.isDebugEnabled())
                LOG.debug("Enrichment data: {}",data);

            return data;
        } catch (IOException e) {
            throw new InfraException(e);
        }
    }

    private RestEntity getDiscoveredNode(String encodedQuery) throws InfraException, BusinessException {
        ResponseRootEntity responseRoot = null;
        try {

            RequestEntity entity = new RequestEntity();
            entity.setStatement(encodedQuery);
            RequestRootEntity root = new RequestRootEntity();
            root.getStatements().add(entity);

            GenericUrl iddUrl = new GenericUrl(config.getUrl() + config.getTransactionalCypherPath());
            HttpRequest iddRequest = requestFactory.buildPostRequest(
                    iddUrl,
                    new JsonHttpContent(JSON_FACTORY, root)
            );
            iddRequest.setInterceptor(new BasicAuthentication(config.getUser(), config.getPassword()));
            responseRoot = iddRequest.execute().parseAs(ResponseRootEntity.class);

            if (responseRoot == null) {
                LOG.error(MessageFormat.format("Fail to parse response for query [{0}]", encodedQuery));
                return null;
            } else if (!responseRoot.getErrors().isEmpty()) {
                LOG.error(MessageFormat.format("Enrichment query has errors: {0}", responseRoot.getErrors()));
                return null;
            } else if (responseRoot.getResults().isEmpty()) {
                LOG.error(MessageFormat.format("Enrichment query [{0}] returns no results.",
                        encodedQuery));
                return null;
            } else if (responseRoot.getResults().get(0).getDataList().size() > 1) {
                LOG.error(MessageFormat.format("Enrichment query [{0}] returns more than one result.",
                        encodedQuery));
                return null;
            } else
                return responseRoot.getResults().get(0).getDataList().get(0).getRestList().get(0);
        } catch (IOException e) {
            throw new InfraException(e);
        } catch (IndexOutOfBoundsException e) {
            LOG.error("Fail to get rest entity from query [{}]: Received response - {}", encodedQuery, responseRoot);
            return null;
        }
    }

    private String encodeCypherQuery(String cypherQuery, String[] lookupValues) {
        return MessageFormat.format(cypherQuery, lookupValues);
    }
}
