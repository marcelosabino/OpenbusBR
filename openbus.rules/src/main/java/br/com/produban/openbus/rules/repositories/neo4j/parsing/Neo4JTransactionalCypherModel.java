package br.com.produban.openbus.rules.repositories.neo4j.parsing;

import com.google.api.client.util.Key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Neo4JTransactionalCypherModel {

    public static class RestEntity {
        @Key("traverse")
        private String traverseUrl;

        public String getTraverseUrl() {
            return traverseUrl;
        }

        public void setTraverseUrl(String traverseUrl) {
            this.traverseUrl = traverseUrl;
        }

        @Override
        public String toString() {
            return "RestEntity{" +
                    "traverseUrl='" + traverseUrl + '\'' +
                    '}';
        }
    }

    public static class DataEntity {
        @Key("rest")
        private List<RestEntity> restList;

        public List<RestEntity> getRestList() {
            return restList;
        }

        public void setRestList(List<RestEntity> restList) {
            this.restList = restList;
        }

        @Override
        public String toString() {
            return "DataEntity{" +
                    "restList=" + restList +
                    '}';
        }
    }

    public static class ResultEntity {
        @Key("data")
        private List<DataEntity> dataList;

        public List<DataEntity> getDataList() {
            return dataList;
        }

        public void setDataList(List<DataEntity> dataList) {
            this.dataList = dataList;
        }

        @Override
        public String toString() {
            return "ResultEntity{" +
                    "dataList=" + dataList +
                    '}';
        }
    }

    public static class ErrorEntity {
        @Key
        private String code;
        @Key
        private String message;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return "ErrorEntity{" +
                    "code='" + code + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }

    public static class ResponseRootEntity {
        @Key
        private List<ResultEntity> results;
        @Key
        private List<ErrorEntity> errors;

        public List<ResultEntity> getResults() {
            return results;
        }

        public void setResults(List<ResultEntity> results) {
            this.results = results;
        }

        public List<ErrorEntity> getErrors() {
            return errors;
        }

        public void setErrors(List<ErrorEntity> errors) {
            this.errors = errors;
        }

        @Override
        public String toString() {
            return "ResponseRootEntity{" +
                    "results=" + results +
                    ", errors=" + errors +
                    '}';
        }
    }

    public static class RequestEntity {
        @Key
        private String statement;
        @Key
        private List<String> resultDataContents = new ArrayList(Arrays.asList(new String[] {"REST"}));

        public String getStatement() {
            return statement;
        }

        public void setStatement(String statement) {
            this.statement = statement;
        }

        public List<String> getResultDataContents() {
            return resultDataContents;
        }

        public void setResultDataContents(List<String> resultDataContents) {
            this.resultDataContents = resultDataContents;
        }

        @Override
        public String toString() {
            return "RequestEntity{" +
                    "statement='" + statement + '\'' +
                    ", resultDataContents=" + resultDataContents +
                    '}';
        }
    }

    public static class RequestRootEntity {
        @Key
        private List<RequestEntity> statements = new ArrayList<>();

        public List<RequestEntity> getStatements() {
            return statements;
        }

        public void setStatements(List<RequestEntity> statements) {
            this.statements = statements;
        }

        @Override
        public String toString() {
            return "RequestRootEntity{" +
                    "statements=" + statements +
                    '}';
        }
    }
}
