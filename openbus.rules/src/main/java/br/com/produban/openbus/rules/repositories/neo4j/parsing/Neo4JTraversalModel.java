package br.com.produban.openbus.rules.repositories.neo4j.parsing;

import com.google.api.client.util.Key;

import java.util.ArrayList;
import java.util.Map;

public class Neo4JTraversalModel {

    public static class NodeData {
        @Key("traverse")
        private String traverseUrl;
        @Key("data")
        private Map<String,String> data;

        public String getTraverseUrl() {
            return traverseUrl;
        }

        public void setTraverseUrl(String traverseUrl) {
            this.traverseUrl = traverseUrl;
        }

        public Map<String, String> getData() {
            return data;
        }

        public void setData(Map<String, String> data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "NodeData{" +
                    "traverseUrl='" + traverseUrl + '\'' +
                    ", data=" + data +
                    '}';
        }
    }

    public static class NodeList extends ArrayList<NodeData> {}

}
