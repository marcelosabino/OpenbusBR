package br.com.produban.openbus.rules.services;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.produban.openbus.model.OpenbusTool;
import br.com.produban.openbus.model.avro.*;
import br.com.produban.openbus.model.factories.DataBeanFactory;
import br.com.produban.openbus.model.pojo.*;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.produban.openbus.rules.repositories.jedis.JedisRepository;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class BaseService implements Serializable {

	protected static final String IPADDRESS_PATTERN = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

	public enum ZabbixKeyResolver { NONE, SERVICE_MONITORING}
	protected static Pattern ipPattern = Pattern.compile(IPADDRESS_PATTERN);

//	private static ThreadLocal<JedisRepository> jedisRepository = new ThreadLocal<JedisRepository>();

	protected JedisResolver jedisResolver;

	protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

	public BaseService(JedisResolver jedisResolver) {
		this.jedisResolver = jedisResolver;
	}

	protected JedisRepository getJedis() throws InfraException {
		return jedisResolver.getJedis();
	}

	protected OpenbusTool resolveInputType(GenericRecord input) throws BusinessException {
		return OpenbusTool.resolveTool(input);
	}

	protected ZabbixKeyResolver resolveZabbixKeyType(String key) {
		if (key.startsWith("service_monitoring")) return ZabbixKeyResolver.SERVICE_MONITORING;
		return ZabbixKeyResolver.NONE;
	}


	/**
	 * Returns Hostname without FQDN or IP
	 * @param host
	 * @return
	 */
	protected String resolveHost(String host) {
		if (host != null) {
			Matcher matcher = ipPattern.matcher(host);
			String hostOrIp;
			if (matcher.find()) {
				hostOrIp = host;
			} else {
				hostOrIp = host.split("\\.")[0].toUpperCase();
			}

			return hostOrIp;
		} else return host;
	}


	/**
	 * Extracts key prefix from original key
	 * @param key eg.1 "system.cpu.util[,idle]"; eg.2 "system.uname".
	 * @return eg.1 "system.cpu.util"; eg.2 "system.uname"
	 */
	protected String resolveKeyPrefix(String key) {
		if (key == null) {
			return null;
		}
		int indexOfArray = key.indexOf("[");
		return indexOfArray > -1 ? key.substring(0, indexOfArray) : key;
	}

	protected boolean isInvalidData(String value) {
		return value == null || value.isEmpty();
	}

	/**
	 * Copy specific DataBean to a new Bean using its "specific bean builder"
	 * @param input
	 * @return
	 */
	protected DataBean copyBeanFrom(String tool, SpecificRecordBase input) throws BusinessException {
		DataBean newBean;
		try {
			newBean = DataBeanFactory.toBean(input);
			newBean.setTool(tool);
		} catch (Exception e) {
			throw new BusinessException("Fail to create data bean: ["+input.toString()+"]",e);
		}

		return newBean;
	}
}

