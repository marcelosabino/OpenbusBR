package br.com.produban.openbus.rules.services.cleaners;

import java.util.List;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.InfraException;

public class CleanerServices extends BaseService implements ICleaner {

	private ICleaner opentsdb;

	public CleanerServices(JedisResolver jedisResolver) {
		super(jedisResolver);
		opentsdb = new GenericCleaner(jedisResolver);
	}

	@Override
	public void cleanKey(DataBean input) throws InfraException {
		opentsdb.cleanKey(input);
	}

	@Override
	public void cleanExtraFields(DataBean input) throws InfraException {
		opentsdb.cleanExtraFields(input);
	}

	/**
	 * Clean Key Name and Field Names and Values for the whole list 
	 * @param forkedBeans
	 * @throws InfraException
	 */
	public void cleanData(List<DataBean> forkedBeans) throws InfraException {
		for (DataBean bean : forkedBeans) {
//			cleanKey(bean);
			cleanExtraFields(bean);
		}
	}

}
