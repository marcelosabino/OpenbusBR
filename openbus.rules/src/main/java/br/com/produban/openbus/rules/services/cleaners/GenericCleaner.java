package br.com.produban.openbus.rules.services.cleaners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.InfraException;

public class GenericCleaner extends BaseService implements ICleaner {

	public GenericCleaner(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public void cleanKey(DataBean input) throws InfraException {
		String key = input.getRawKey();

		String newKey = replaceCharacters(key);
		newKey = removeInvalidChar(newKey);

		input.setRawKey(newKey);
	}

	@Override
	public void cleanExtraFields(DataBean input) throws InfraException {
		Map<String, String> extraFieldsCopy = input.getExtraFieldsCopy();
		
		for (Entry<String, String> originalTuple : extraFieldsCopy.entrySet()) {
			String cleanKey = replaceCharacters(originalTuple.getKey());
			cleanKey = removeInvalidChar(cleanKey);

//			String cleanValue = replaceCharacters(originalTuple.getValue());
//			cleanValue = removeInvalidChar(cleanValue);

			input.removeExtraField(originalTuple.getKey());
			input.addExtraField(cleanKey, originalTuple.getValue());
		}
	}


	/**
	 * Replaces unwanted characters, using redis map KeyCharacterClean
	 * @param word
	 * @return
	 * @throws InfraException 
	 */
	private String replaceCharacters(String word) throws InfraException {
		if (word == null) {
			return null;
		}
		
		String newWord = word;

		for (Entry<String, String> entry : getJedis().getKeyCharacterClean().entrySet()) {
			newWord = newWord.replaceAll(entry.getValue(), entry.getKey());
		}

		return newWord;
	}

	/**
	 * OpenTSDB-considered invalid characters will be removed from 'word'.
	 * @param word
	 * @return
	 */
	private String removeInvalidChar(String word) {
		int n = word.length();
		StringBuilder newWord = new StringBuilder(n); 

		for (int i = 0; i < n; i++) {
			final char c = word.charAt(i);
			if ( ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') 
					|| ('0' <= c && c <= '9') || c == '-' || c == '_' || c == '.' 
					|| c == '/' || Character.isLetter(c) ) {
				newWord.append(c);
			}
		}

		return newWord.toString();
	}

}
