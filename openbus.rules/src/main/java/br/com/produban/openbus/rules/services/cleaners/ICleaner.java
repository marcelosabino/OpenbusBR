package br.com.produban.openbus.rules.services.cleaners;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.InfraException;

public interface ICleaner {
	
	/**
	 * clean invalid characters on Key
	 * @param input
	 */
	public abstract void cleanKey(DataBean input) throws InfraException;
	
	/**
	 * clean invalid characters on TagName and Values
	 * @param input
	 */
	public abstract void cleanExtraFields(DataBean input) throws InfraException;
	

}
