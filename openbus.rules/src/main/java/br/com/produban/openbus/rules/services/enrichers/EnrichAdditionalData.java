package br.com.produban.openbus.rules.services.enrichers;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.repositories.neo4j.EnrichmentRepository;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

import java.util.Map;

public class EnrichAdditionalData extends BaseService implements IEnricher {

	private final EnrichmentRepository enricher;

	public EnrichAdditionalData(JedisResolver jedisResolver, EnrichmentDatabaseConfiguration enrichmentConfig) {
		super(jedisResolver);
		this.enricher = new EnrichmentRepository(jedisResolver,enrichmentConfig);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException, InfraException {
		Map<String,String> additionalData = enricher.getEnrichmentData(input);

		if (LOG.isDebugEnabled())
			LOG.debug("Enrichment data for bean {} is: {}", input.getBean(), additionalData);

		if (additionalData!=null && !additionalData.isEmpty())
			input.addAllToExtraFields(additionalData);
	}
}
