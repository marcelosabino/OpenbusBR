package br.com.produban.openbus.rules.services.enrichers;

import java.util.Map;

import org.apache.avro.Schema.Field;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class EnrichCopyExtraFieldValueToFieldValue extends BaseService implements
		IEnricher {

	public EnrichCopyExtraFieldValueToFieldValue(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException,
			InfraException {

		String key = input.getKey();
		int index = 0;
		String prefixIndex = key + "." + index;
		String tagToField = null;
		
		Map<String, String> tags = input.getExtraFields();
		
		while ( ( tagToField = getJedis().getMetricsTagToField(prefixIndex) ) != null ) { 
			String tagToFieldArray[] = tagToField.split("->"); 
			
			// tag value
			String tag   = tagToFieldArray[0];
			String tagValue = tags.get(tag);
			if (tagValue == null) {
				continue;
			}
			
			// target field 
			String field = tagToFieldArray[1];
			
			// special case for "host" ("host", "hostname", "computerName", etc)
			if (field.equals("host")) {
				input.setHostname(tagValue);
			} else {
				SpecificRecordBase record = input.getBean();
				Field f = record.getSchema().getField(field);
				if (f != null) {
					record.put(f.pos(), tagValue);
				} else {
					throw new BusinessException("Schema " + record.getSchema().getName() + " does not contain field " + field);
				}
			}
			
			// is there a new rule?
			index++;
			prefixIndex = key + "." + index;
		}
	}

	
}
