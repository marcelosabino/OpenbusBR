package br.com.produban.openbus.rules.services.enrichers;

import java.util.Map;
import java.util.Map.Entry;

import br.com.produban.openbus.model.pojo.SNMPTrapBean;
import br.com.produban.openbus.model.pojo.SuperEventlogBean;
import br.com.produban.openbus.rules.services.cleaners.GenericCleaner;
import br.com.produban.openbus.rules.services.cleaners.ICleaner;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class EnrichExplodeComplexFields extends BaseService implements IEnricher {

	private ICleaner cleaner;

	public EnrichExplodeComplexFields(JedisResolver jedisResolver) {
		super(jedisResolver);
		cleaner = new GenericCleaner(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException,InfraException {
		if (input instanceof SuperEventlogBean || input instanceof SNMPTrapBean) {
			Map<String, String> parseFields = input.parseFields();
			if (parseFields != null) {
				for (Entry<String, String> entry : parseFields.entrySet()) {
					input.addExtraField(entry.getKey(), entry.getValue());
				}
			}

			cleaner.cleanExtraFields(input);
		}
	}

}
