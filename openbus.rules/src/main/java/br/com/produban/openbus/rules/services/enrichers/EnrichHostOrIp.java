package br.com.produban.openbus.rules.services.enrichers;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class EnrichHostOrIp extends BaseService implements IEnricher {

	public EnrichHostOrIp(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException,
			InfraException {
		input.setHostname(resolveHost(input.getHostname()));
	}

}
