package br.com.produban.openbus.rules.services.enrichers;

import java.util.regex.Matcher;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class EnrichIpToHostname extends BaseService implements IEnricher {

	public EnrichIpToHostname(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException,
			InfraException {
		String rawHostname = input.getHostname();

		if (rawHostname != null) {
			Matcher matcher = ipPattern.matcher(rawHostname);
			if (matcher.find()) {
				String hostname = getJedis().getHostnameFromIp(rawHostname);

				if (hostname != null) {
					input.setHostname(hostname);
				}
			}
		}
	}
	
}
