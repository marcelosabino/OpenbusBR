package br.com.produban.openbus.rules.services.enrichers;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EnrichStatisticalModel extends BaseService implements IEnricher {

	private static Map<String, String> uids = new ConcurrentHashMap<String, String>();

	public EnrichStatisticalModel(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException, InfraException {

		if(input == null || input.getExtraField("uid") == null) {
			LOG.debug("Illegal input/uid.");
			return;
		}

		Map<String, String> statisticalModel = jedisResolver.getJedis().getStatisticalModel(input.getExtraField("uid"));

		if (statisticalModel != null) {
			for (Map.Entry<String, String> entry : statisticalModel.entrySet()) {
                input.addExtraField(entry.getKey(), entry.getValue());
			    LOG.debug("Stats Model for '"+ input.getHostname() +" | "+ input.getKey() +"': "+ entry.getKey() +" = "+ entry.getValue());
			}
		}
	}
	
}