package br.com.produban.openbus.rules.services.enrichers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class EnrichUID extends BaseService implements IEnricher {
	
	private static Map<String, String> uids = new ConcurrentHashMap<String, String>();

	public EnrichUID(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException,
			InfraException {
		List<String> capacityLookup = jedisResolver.getJedis().getCapacityLookup(input.getKey());
		
		if (capacityLookup != null) {
			StringBuilder chunks = new StringBuilder(128);
			// by default, key is always used
			chunks.append(input.getKey());
			for (String field : capacityLookup) {
				if (field.equals("host")) {
					chunks.append(input.getHostname());
				} else {
					chunks.append(input.getExtraField(field));
				}
				chunks.append(";");
			}

			String hash = toHash(chunks.toString());
			LOG.debug("UID for '" + input.getKey() + "' : " + chunks.toString() + " -> " + hash);
			input.addExtraField("uid", hash);
		}
	}
	
	
	
	private String toHash(String chunks) {
		
		String uid = uids.get(chunks);
		if (uid != null) {
			return uid; 
		}
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		try {
			md.update(chunks.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		byte[] hash = md.digest();
		StringBuilder sb = new StringBuilder(2*hash.length);
		for (byte b : hash) {
			sb.append(String.format("%02x", b&0xff));
		}
		
		String hashStr = sb.toString();
		uids.put(chunks, hashStr);
		
		return hashStr;
	}
}

