package br.com.produban.openbus.rules.services.enrichers;

import br.com.produban.openbus.model.OpenbusTool;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

import java.util.List;

public class EnricherServices extends BaseService implements IEnricher {

	private IEnricher hostOrIp;
	private IEnricher tagToFields;
	private IEnricher lookupNewInfo;
	private IEnricher ipToHost;
	private IEnricher xplodeFields;
	private IEnricher generateUID;
    private IEnricher statisticalModel;

	private EnrichmentDatabaseConfiguration enrichmentDatabaseConfig;

	public EnricherServices(JedisResolver jedisResolver, EnrichmentDatabaseConfiguration rulesConfig) {
		super(jedisResolver);
		this.enrichmentDatabaseConfig = rulesConfig;
		hostOrIp = new EnrichHostOrIp(jedisResolver);
		tagToFields = new EnrichCopyExtraFieldValueToFieldValue(jedisResolver);
		ipToHost = new EnrichIpToHostname(jedisResolver);
		lookupNewInfo = new EnrichAdditionalData(jedisResolver, enrichmentDatabaseConfig);
		xplodeFields = new EnrichExplodeComplexFields(jedisResolver);
		generateUID = new EnrichUID(jedisResolver);
        statisticalModel = new EnrichStatisticalModel(jedisResolver);
	}

	@Override
	public void enrichData(DataBean input) throws BusinessException, InfraException {
		
		hostOrIp.enrichData(input);
		tagToFields.enrichData(input);
		ipToHost.enrichData(input);
		lookupNewInfo.enrichData(input);
		xplodeFields.enrichData(input);

		if (resolveInputType(input.getBean()).equals(OpenbusTool.ZABBIX)) {
			generateUID.enrichData(input);
            statisticalModel.enrichData(input);
		}
	}
	
	/**
	 * Enrich every bean of the list
	 * @param inputList
	 * @throws BusinessException
	 * @throws InfraException
	 */
	public void enrichData(List<DataBean> inputList) throws BusinessException, InfraException {
		for (DataBean bean : inputList) {
			enrichData(bean);
		}
	}
	
	
	public void generateUID(DataBean bean) throws BusinessException, InfraException {
		generateUID.enrichData(bean);
	}
}
