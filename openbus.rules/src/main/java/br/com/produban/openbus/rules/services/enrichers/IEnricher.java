package br.com.produban.openbus.rules.services.enrichers;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public interface IEnricher {

	public abstract void enrichData(DataBean input) throws BusinessException, InfraException;
	
}
