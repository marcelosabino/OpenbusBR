package br.com.produban.openbus.rules.services.filters;

import br.com.produban.openbus.model.avro.BigIp;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.apache.avro.specific.SpecificRecordBase;

public class FilterBigIpNullData extends BaseService implements IFilter {

    public FilterBigIpNullData(JedisResolver jedisResolver) {
        super(jedisResolver);
    }

    @Override
    public boolean isValidData(SpecificRecordBase genericInput) throws BusinessException, InfraException {

        BigIp input = (BigIp) genericInput;

        if (input.getTimestamp() == null || input.getTimestamp().isEmpty() ||
            input.getLoadBalancer() == null || input.getLoadBalancer().isEmpty() ||
            input.getVirtualServerName() == null || input.getVirtualServerName().isEmpty() ||
            input.getVirtualServerDestination() == null || input.getVirtualServerDestination().isEmpty() ||
            input.getPoolName() == null || input.getPoolName().isEmpty() ||
            input.getMemberAddress() == null || input.getMemberAddress().isEmpty() ||
            input.getMemberPort() == null || input.getMemberPort().isEmpty() ||
            input.getMetricName() == null || input.getMetricName().isEmpty() ||
            input.getValue() == null || input.getValue().isEmpty()) {
            LOG.debug(">>> FilterBigIpNullData: null || isEmpty()");
            return false;
        } else {
            try {
                Float.valueOf(input.getValue().toString());
                return true;
            } catch (NumberFormatException e) {
                LOG.debug(">>> FilterZabbixNullData: Not a Float value " + input.getValue());
                return false;
            }
        }
    }
}
