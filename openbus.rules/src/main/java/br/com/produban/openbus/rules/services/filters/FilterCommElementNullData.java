package br.com.produban.openbus.rules.services.filters;

import br.com.produban.openbus.model.avro.CommElement;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.apache.avro.specific.SpecificRecordBase;

public class FilterCommElementNullData extends BaseService implements IFilter {

    public FilterCommElementNullData(JedisResolver jedisResolver) {
        super(jedisResolver);
    }

    @Override
    public boolean isValidData(SpecificRecordBase input) throws BusinessException, InfraException {
        CommElement avro = (CommElement) input;

        if (isInvalidData(avro.getTimestamp()) ||
            isInvalidData(avro.getObjectId()) ||
            isInvalidData(avro.getPropertyName()) ||
            isInvalidData(avro.getPropertyValue())) {
            LOG.debug("SNMP based message is invalid");
            return false;
        } else {
            try {
                Float.valueOf(avro.getPropertyValue());
                return true;
            } catch (NumberFormatException e) {
                LOG.debug("Not a Float value " + avro.getPropertyValue());
                return false;
            }
        }
    }
}
