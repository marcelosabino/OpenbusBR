package br.com.produban.openbus.rules.services.filters;

import br.com.produban.openbus.model.avro.*;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class FilterHostBlackList extends BaseService implements IFilter {

	public FilterHostBlackList(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public boolean isValidData(SpecificRecordBase input) throws BusinessException, InfraException {
		String host = getHostname(input);
		if (host == null) {
			throw new BusinessException("FilterHostBlackList does not apply to input data type");
		}

		String hostOrIp = resolveHost(host);

		for (String hostRegex : getJedis().getMetricsHostBlackList()) {
			if (hostOrIp.matches(hostRegex)) {
				LOG.debug(">>> FilterHostBlackList: " + host + " is blackListed by condition: " + hostRegex);
				return false;
			}
		}
		return true;
	}

	
	private String getHostname(SpecificRecordBase input) throws BusinessException {
		switch (resolveInputType(input)) {
		case ACTIVEDIRECTORY:
			return ((ActiveDirectory)input).getComputerName();
		case EVENTLOG:
			return ((Eventlog)input).getComputerName();
		case FIREEYE:
			return ((FireEye)input).getHostname();
		case IPS:
			return ((IPS)input).getHostname();
		case PROXY:
			return ((Proxy)input).getHostname();
		case RADIUS:
			return ((Radius)input).getHostname();
		case SYSLOG5424:
			return ((Syslog5424)input).getHostname();
		case SNMPTRAP:
			return ((SNMPTrap)input).getSourceIP();
		case ZABBIX:
			return ((ZabbixAgentData)input).getHost();
		case FIREWALL:
			return ((Firewall)input).getOrigin();
		case ORACLE:
			return ((Oracle)input).getInstanceId();
		case BIGIP_POOL_MEMBERS:
			return ((BigIp)input).getMemberAddress().toString();
		case INTROSCOPE:
			return ((Introscope)input).getHost();
		case TACACS:
			return ((Tacacs)input).getHostname();
		case IDM:
			return ((IDM)input).getHostname();
		default:
			return null;
		}
	}

}
