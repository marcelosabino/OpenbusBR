package br.com.produban.openbus.rules.services.filters;

import br.com.produban.openbus.model.avro.*;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.apache.avro.specific.SpecificRecordBase;

public class FilterKeyBlackList extends BaseService implements IFilter {

	public FilterKeyBlackList(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public boolean isValidData(SpecificRecordBase input) throws BusinessException, InfraException {
		String key = getKey(input);
		if (key == null) {
			throw new BusinessException(String.format("Key cannot be null! [Data: %s]",input.toString()));
		}
		
		for (String keyRegex : getJedis().getMetricsKeyBlackList()) {
			if (key.matches(keyRegex)) {
				LOG.debug(">>> FilterHostBlackList: " + key + " is blackListed by condition: " + keyRegex);
				return false;
			}
		}
		return true;
	}

	private String getKey(SpecificRecordBase input) throws BusinessException {
		switch (resolveInputType(input)) {
		case ACTIVEDIRECTORY:
			return ((ActiveDirectory)input).getEventIdentifier();
		case EVENTLOG:
			return ((Eventlog)input).getEventIdentifier();
		case FIREEYE:
			return ((FireEye)input).getApplicationName();
		case IPS:
			return ((IPS)input).getApplicationName();
		case PROXY:
			return ((Proxy)input).getApplicationName();
		case RADIUS:
			return ((Radius)input).getApplicationName();
		case SYSLOG5424:
			return ((Syslog5424)input).getApplicationName();
		case SNMPTRAP:
			return ((SNMPTrap)input).getTrapOID();
		case ZABBIX:
			return ((ZabbixAgentData)input).getKey();
		case FIREWALL:
			return ((Firewall)input).getProduct();
		case ORACLE:
			return ((Oracle)input).getMetricName();
		case BIGIP_POOL_MEMBERS:
			return ((BigIp)input).getMetricName();
		case WEBSPHERE:
			return ((WebSphere)input).getAppServerName();
		case INTROSCOPE:
			return ((Introscope)input).getResource();
		case TACACS:
			return ((Tacacs)input).getApplicationName();
		case IDM:
			return ((IDM)input).getLogin();
		default:
			return null;
		}
	}
}
