package br.com.produban.openbus.rules.services.filters;

import br.com.produban.openbus.model.avro.DB2;
import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class FilterServices extends BaseService implements IFilter {

	public FilterServices(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public boolean isValidData(SpecificRecordBase input)
			throws BusinessException, InfraException {
		switch (resolveInputType(input)) {
			case ZABBIX:
				return new FilterZabbixNullData(jedisResolver).isValidData(input);
			case BIGIP_POOL_MEMBERS:
				return new FilterBigIpNullData(jedisResolver).isValidData(input);
			case COMM_ELEMENT:
				return new FilterCommElementNullData(jedisResolver).isValidData(input);
			default:
				return true;
		}
	}

}
