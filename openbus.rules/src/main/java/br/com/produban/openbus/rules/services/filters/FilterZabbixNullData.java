package br.com.produban.openbus.rules.services.filters;

import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class FilterZabbixNullData extends BaseService implements IFilter {

	public FilterZabbixNullData(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public boolean isValidData(SpecificRecordBase genericInput) throws BusinessException, InfraException {
		if (genericInput == null || !(genericInput instanceof ZabbixAgentData)) {
			LOG.debug(">>> FilterZabbixNullData: genericInput == null || !(genericInput instanceof ZabbixAgentData)");
			return false;
		}
		
		ZabbixAgentData input = (ZabbixAgentData) genericInput; 

        if (input.getHost() == null || input.getHost().isEmpty() ||
            input.getKey() == null || input.getKey().isEmpty() ||
            input.getValue() == null || input.getValue().isEmpty() ||
            input.getClock() == null || input.getClock().isEmpty()) {
        	LOG.debug(">>> FilterZabbixNullData: null || isEmpty()");
            return false;
        } else {
            try {
                Float.valueOf(input.getValue());
                return true;
            } catch (NumberFormatException e) {
            	LOG.debug(">>> FilterZabbixNullData: Not a Float value " + input.getValue());
                return false;
            }
        }
	}
	
	
}
