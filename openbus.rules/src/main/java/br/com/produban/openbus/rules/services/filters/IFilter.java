package br.com.produban.openbus.rules.services.filters;

import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public interface IFilter {

	boolean isValidData(SpecificRecordBase input) throws BusinessException, InfraException;
	
}
