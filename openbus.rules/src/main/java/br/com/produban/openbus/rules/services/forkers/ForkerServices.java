package br.com.produban.openbus.rules.services.forkers;

import java.util.List;

import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class ForkerServices extends BaseService implements IForker {

	public ForkerServices(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public List<DataBean> forkData(String tool, SpecificRecordBase input)
			throws BusinessException, InfraException {
		List<DataBean> forked = null;
		
		switch(resolveInputType(input)) {
			case ZABBIX:
				forked = new ForkerZabbix(jedisResolver).forkData(tool, input);
				break;
			default:
				forked = new GenericForker(jedisResolver).forkData(tool, input);
		}
		
		return forked;
	}

}
