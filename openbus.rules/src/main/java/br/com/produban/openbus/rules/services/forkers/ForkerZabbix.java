package br.com.produban.openbus.rules.services.forkers;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Joiner;
import org.apache.avro.specific.SpecificRecordBase;

import com.udojava.evalex.Expression;
import com.udojava.evalex.Expression.ExpressionException;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.avro.ZabbixAgentData.Builder;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.model.pojo.ZabbixAgentDataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class ForkerZabbix extends GenericForker {

	public ForkerZabbix(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public List<DataBean> forkData(String tool, SpecificRecordBase input) throws BusinessException, InfraException {

		List<DataBean> forkedData = super.forkData(tool, input);

		if (input instanceof ZabbixAgentData) {
			String key = ((ZabbixAgentData) input).getKey();
			String value = ((ZabbixAgentData) input).getValue();

			/* Look for compatible Key Patterns(Regex) */
			Map<String, String> mapPrefixPattern = getMetricsKeyPatterns(key);

			/* Iterate over compatible patterns */
			Builder newZabbixBuilder = ZabbixAgentData.newBuilder((ZabbixAgentData) input);
			for (Entry<String, String> prefixPattern : mapPrefixPattern.entrySet()) {
				String lookup = prefixPattern.getKey();
				String keyPattern = prefixPattern.getValue();

				/* Extract groups from original key */
				List<String> groups = extractRegexMatchedGroups(key, keyPattern);

				/* Apply Key Template */
				String keyTemplate = getJedis().getMetricsKeyTemplate(lookup);
				String newKey = applyKeyTemplate(keyTemplate, groups);

				/* Apply Tags Template */
				String tagsTemplate = getJedis().getMetricsTagsTemplate(prefixPattern.getKey());
				Map<String, String> tags = applyTagsTemplate(tagsTemplate, groups);

				/* Apply Math Transformation */
				String expression = getJedis().getMetricsMathExpression(lookup);
				Float newValue = applyMathExpression(expression, value);

				/* Create forked bean */
				ZabbixAgentData zad = newZabbixBuilder.build();
				zad.setKey(newKey);
				zad.setValue(newValue.toString());

				DataBean forkedBean = copyBeanFrom(tool, zad);
				forkedBean.addAllToExtraFields(tags);

				forkedData.add(forkedBean);
			}

			if (forkedData.size() > 1) {
                //Do not forward original avro message but just the fork
                forkedData.remove(0);
            }

			return forkedData;
		} else {
			throw new BusinessException("ForkerZabbix only accepts ZabbixAgentData as input.");
		}
	}


	/**
	 * Retrieves all compatible key patterns
	 * @param key = keyPrefix
	 * @return keyPrefix.[index], keyPattern
	 * @throws InfraException
	 */
	public Map<String, String> getMetricsKeyPatterns(String key) throws InfraException {
		TreeMap<String, String> keyPatterns = new TreeMap<String, String>();

		int index = 0;
		String prefix = resolveKeyPrefix(key);
		String prefixIndex = prefix + "." + index;
		String pattern = null;
		LOG.debug(">>> getMetricsKeyPatterns: prefix = " + prefix );
		while ((pattern = getJedis().getMetricsKeyPattern(prefixIndex)) != null) {
			LOG.debug(">>> getMetricsKeyPatterns: prefix.index = " + prefixIndex );
			LOG.debug(">>> getMetricsKeyPatterns: pattern = " + pattern );
			if (key.matches(pattern)) {
				keyPatterns.put(prefixIndex, pattern);
				LOG.debug(">>> getMetricsKeyPatterns: " + key + " : " + prefixIndex + " -> " + pattern);
			}
			index++;
			prefixIndex = prefix + "." + index;
		}

		return keyPatterns;
	}


	/**
	 * Applies regex to the key and - if they match - returns a list of extracted groups.
	 * @param key
	 * @param regex
	 * @return List of extracted groups. First group is a "key"'s copy, if they match.
	 */
	private List<String> extractRegexMatchedGroups(String key, String regex) {
		List<String> groups = new ArrayList<String>();

		Pattern pattern = Pattern.compile(regex);
		Matcher keyMatcher = pattern.matcher(key);

		if (keyMatcher.matches()) {
			for (int i = 0; i <= keyMatcher.groupCount(); i++) {
				groups.add(keyMatcher.group(i));
				LOG.debug(">>> extractRegexMatchedGroups: " + i + " : " + keyMatcher.group(i));
			}
		}

		return groups;
	}

	/**
	 * Replaces every group tag "{#ID}" by the group indexed by "ID" 
	 * @param template eg. "newKey.{#2}-{#5}.{#2}"
	 * @param groups eg. { "value0", "value1", "value2", "value3", "value4", "value5" }
	 * @return "newKey.value2-value5.value2"
	 */
	private String applyKeyTemplate(String template, List<String> groups) {
		String newKey = template;
		if ((newKey != null) && (groups.size() > 1)) {
			for (int i = 1; i < groups.size(); i++) {
				newKey = newKey.replaceAll(Pattern.quote("{#"+i+"}"), groups.get(i));
			}
		}
		LOG.debug(">>> applyKeyTemplate: " + template + " ---> " + newKey);
		return newKey;
	}

	/**
	 * Creates a map of tags based on the Template
	 * @param tagsTemplate eg. "tagName1:1,tagName2:3,tagNameA:4"
	 * @param groups eg. { "value0", "value1", "value2", "value3", "value4", "value5" }
	 * @return {
	 * <ul>"tagName1": "value1",<br>
	 * "tagName2": "value3",<br>
	 * "tagNameA": "value4"</ul>
	 * } 
	 * @throws BusinessException 
	 */
	private Map<String, String> applyTagsTemplate(String tagsTemplate, List<String> groups) throws BusinessException {
		Map<String, String> tags = new HashMap<String, String>();

		if (tagsTemplate != null) {
			LOG.debug(">>> applyTagsTemplate: " + tagsTemplate);
			try {
				for (String tagTemplate : tagsTemplate.split(",")) {
					String tag[] = tagTemplate.split(":");
					int tagIndex = Integer.parseInt(tag[1]);
					tags.put(tag[0], groups.get(tagIndex));
					LOG.debug(">>> applyTagsTemplate: \\--> " + tag[0] + ":" + groups.get(tagIndex));
				}
			} catch (IndexOutOfBoundsException e) {
				throw new BusinessException("Fail to apply tags template: ["+tagsTemplate+","+ Joiner.on("|").join(groups)+"]",e);
			}
		}

		return tags;
	}


	/**
	 * Calculates a new value based on the function "MathExpression" and the original "value"
	 * @param mathExpression eg. "100 - value"
	 * @param value eg. "12.34"
	 * @return eg. "87.66"
	 * @throws BusinessException
	 */
	public Float applyMathExpression(String mathExpression, String value) throws BusinessException {
		if (mathExpression == null)
			return Float.parseFloat(value);
		try {
			if (mathExpression.contains("value"))
				return new Expression(mathExpression).with("value", value).eval().floatValue();
			else
				return new Expression(mathExpression).eval().floatValue();
		} catch(ExpressionException e) {
			throw new BusinessException(e);
		} catch(EmptyStackException e) {
			throw new BusinessException(e);
		} catch(RuntimeException e) {
			throw new BusinessException(e);
		}
	}
}
