package br.com.produban.openbus.rules.services.forkers;

import java.util.ArrayList;
import java.util.List;

import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.avro.ActiveDirectory;
import br.com.produban.openbus.model.avro.Eventlog;
import br.com.produban.openbus.model.avro.FireEye;
import br.com.produban.openbus.model.avro.IPS;
import br.com.produban.openbus.model.avro.Proxy;
import br.com.produban.openbus.model.avro.SNMPTrap;
import br.com.produban.openbus.model.avro.Syslog5424;
import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.pojo.ActiveDirectoryBean;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.model.pojo.EventlogBean;
import br.com.produban.openbus.model.pojo.FireEyeBean;
import br.com.produban.openbus.model.pojo.IPSBean;
import br.com.produban.openbus.model.pojo.ProxyBean;
import br.com.produban.openbus.model.pojo.SNMPTrapBean;
import br.com.produban.openbus.model.pojo.Syslog5424Bean;
import br.com.produban.openbus.model.pojo.ZabbixAgentDataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class GenericForker extends BaseService implements IForker {

	public GenericForker(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public List<DataBean> forkData(String tool, SpecificRecordBase input) throws BusinessException, InfraException {
		List<DataBean> forkedData = new ArrayList<DataBean>();
		forkedData.add(copyBeanFrom(tool, input));
		return forkedData;
	}
	
}
