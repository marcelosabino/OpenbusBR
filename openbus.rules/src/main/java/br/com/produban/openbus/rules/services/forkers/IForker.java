package br.com.produban.openbus.rules.services.forkers;

import java.util.List;

import org.apache.avro.specific.SpecificRecordBase;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public interface IForker {

	public abstract List<DataBean> forkData(String tool, SpecificRecordBase input) throws BusinessException, InfraException;
	
}
