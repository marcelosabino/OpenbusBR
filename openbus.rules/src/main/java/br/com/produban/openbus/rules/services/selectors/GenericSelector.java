package br.com.produban.openbus.rules.services.selectors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import br.com.produban.openbus.security.exceptions.BusinessException;
import redis.clients.jedis.exceptions.JedisException;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.InfraException;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class GenericSelector extends BaseService implements IFieldsSelector {

	private static final long serialVersionUID = 9018811686565770790L;

	public GenericSelector(JedisResolver jedisResolver) {
		super(jedisResolver);
	}

	@Override
	public Map<String, String> selectFields(String persister, DataBean input) throws BusinessException, InfraException {
		String key = input.getKey();
		LOG.debug("SelectFields: '" + persister + "' + '" + key + "'");

		List<Pattern> whitelist = getJedis().getFieldsWhitelistByPersisterAndKey(persister, key);
		if (whitelist.isEmpty()) {
			LOG.debug("   Null Whitelist, using default");
			whitelist = getJedis().getFieldsWhitelistByPersisterAndKey(persister, DEFAULT_KEY);
		}

		List<Pattern> blacklist = getJedis().getFieldsBlacklistByPersisterAndKey(persister, key);
		if (blacklist.isEmpty()) {
			LOG.debug("   Null Blacklist, using default");
			blacklist = getJedis().getFieldsBlacklistByPersisterAndKey(persister, DEFAULT_KEY);
		}

		Map<String, String> fields = input.getExtraFieldsCopy();
		Map<String, String> finalMap = new HashMap<String, String>();

		for (Entry<String, String> field : fields.entrySet()) {
			LOG.debug("   Testing: " + field.getKey());
			if (isValidField(field.getKey(), whitelist, blacklist)) {
				finalMap.put(field.getKey(), field.getValue());
				LOG.debug("      Accepted");
			}
		}

		return finalMap;
	}

	private boolean isValidField(String key, List<Pattern> whitelist, List<Pattern> balcklist) {
		boolean white = false;
		for (Pattern pattern : whitelist) {
			LOG.debug("      + " + pattern.toString());
			if (pattern.matcher(key).matches()) {
				// partially accepted
				white = true;
				break;
			}
		}
		// if it's not even partially accepted, it'll be invalid
		if (!white) {
			return false;
		}

		for (Pattern pattern : balcklist) {
			LOG.debug("      - " + pattern.toString());
			if (pattern.matcher(key).matches()) {
				// balcklisted --> invalid
				return false;
			}
		}

		// only in whitelist --> accepted
		return true;
	}
}