package br.com.produban.openbus.rules.services.selectors;

import java.util.Map;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public interface IFieldsSelector {
	
	public static final String DEFAULT_KEY = "default";
	
	public abstract Map<String, String> selectFields(String persister, DataBean input) throws BusinessException, InfraException;
	
}
