package br.com.produban.openbus.rules.services.selectors;

import java.util.Map;

import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.BaseService;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class SelectorServices extends BaseService implements IFieldsSelector {

	private static final long serialVersionUID = -7269838400710170162L;

	private IFieldsSelector genericSelector;
	
	public SelectorServices(JedisResolver jedisResolver) {
		super(jedisResolver);
		genericSelector = new GenericSelector(jedisResolver);
	}

	@Override
	public Map<String, String> selectFields(String persister, DataBean input) throws BusinessException, InfraException {
		 return genericSelector.selectFields(persister, input);
	}

	public DataBean prepareToPersist(String persister, DataBean input) throws BusinessException, InfraException {
		DataBean output = copyBeanFrom(input.getTool(), input.getBean());

		// copy only selected fields 
		output.addAllToExtraFields(selectFields(persister, input));
		
		return output;
	}
}
