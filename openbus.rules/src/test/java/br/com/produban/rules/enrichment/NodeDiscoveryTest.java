package br.com.produban.rules.enrichment;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.Key;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class NodeDiscoveryTest {

    private static final JsonFactory JSON_FACTORY = new GsonFactory();

    public static class RestEntity {
        @Key("traverse")
        private String traverseUrl;

        public String getTraverseUrl() {
            return traverseUrl;
        }

        public void setTraverseUrl(String traverseUrl) {
            this.traverseUrl = traverseUrl;
        }
    }

    public static class DataEntity {
        @Key("rest")
        private List<RestEntity> restList;

        public List<RestEntity> getRestList() {
            return restList;
        }

        public void setRestList(List<RestEntity> restList) {
            this.restList = restList;
        }
    }

    public static class ResultEntity {
        @Key("data")
        private List<DataEntity> dataList;

        public List<DataEntity> getDataList() {
            return dataList;
        }

        public void setDataList(List<DataEntity> dataList) {
            this.dataList = dataList;
        }
    }

    public static class ResponseRootEntity {
        @Key
        private List<ResultEntity> results;

        public List<ResultEntity> getResults() {
            return results;
        }

        public void setResults(List<ResultEntity> results) {
            this.results = results;
        }
    }

    public static class RequestEntity {
        @Key
        private String statement;
        @Key
        private List<String> resultDataContents = new ArrayList(Arrays.asList(new String[] {"REST"}));

        public String getStatement() {
            return statement;
        }

        public void setStatement(String statement) {
            this.statement = statement;
        }

        public List<String> getResultDataContents() {
            return resultDataContents;
        }

        public void setResultDataContents(List<String> resultDataContents) {
            this.resultDataContents = resultDataContents;
        }
    }

    public static class RequestRootEntity {
        @Key
        private List<RequestEntity> statements = new ArrayList<>();

        public List<RequestEntity> getStatements() {
            return statements;
        }

        public void setStatements(List<RequestEntity> statements) {
            this.statements = statements;
        }
    }

    @Test
    public void testResponseDeserialization() throws Exception {

        String jsonFile = new String(Files.readAllBytes(
                Paths.get(getClass().getClassLoader().getResource("nodeCypherDiscoveryResponse.json").toURI())));
        ResponseRootEntity root = JSON_FACTORY.fromString(jsonFile, ResponseRootEntity.class);

        assertThat(root,notNullValue());
        assertThat(root.getResults().get(0).getDataList(),hasSize(1));

        String expectedTraverseUrl = "http://srvbigpvlbr10:7474/db/data/node/21600/traverse/{returnType}";
        String actualTraverseUrl = root.getResults().get(0).getDataList().get(0).getRestList().get(0).getTraverseUrl();

        assertThat(actualTraverseUrl,equalToIgnoringCase(expectedTraverseUrl));
    }

    @Test
    public void testRequestSerialization() throws Exception {

        String expectedJsonFile = new String(Files.readAllBytes(
                Paths.get(getClass().getClassLoader().getResource("nodeCypherDiscoveryRequest.json").toURI())));

        JsonElement expectedElement = new JsonParser().parse(expectedJsonFile);

        String cypher = "MATCH (n:Node) WHERE n.property = \"value\" RETURN n";
        RequestEntity entity = new RequestEntity();
        entity.setStatement(cypher);

        RequestRootEntity root = new RequestRootEntity();
        root.getStatements().add(entity);

        String actualJson = JSON_FACTORY.toString(root);
        JsonElement actualElement = new JsonParser().parse(actualJson);

        assertThat(actualElement,equalTo(expectedElement));
    }
}
