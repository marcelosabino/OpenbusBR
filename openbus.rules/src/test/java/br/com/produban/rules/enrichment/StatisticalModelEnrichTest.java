package br.com.produban.rules.enrichment;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.model.pojo.ZabbixAgentDataBean;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;
import br.com.produban.openbus.rules.services.enrichers.EnricherServices;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Random;

public class StatisticalModelEnrichTest {

    private JedisResolver jedisResolver;
    private String redisHost = "localhost";
    private int redisPort = 6379;
    private int databaseIndex = 1;
    private String password = "openbus";

    @Before
    public void runBefore() {
        this.jedisResolver = new JedisResolver(redisHost, redisPort, databaseIndex, password);
    }
    @Test
    public void statsModelTest() {

        DataBean dataBean;
        try {
            dataBean = createZabbixAgentData();
            new EnricherServices(jedisResolver, null).enrichData(dataBean);
            System.out.println(dataBean.getExtraFields());

        } catch (BusinessException e) {
            e.printStackTrace();
        } catch (InfraException e) {
            e.printStackTrace();
        }

    }

    private static DataBean createZabbixAgentData() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, ((new Random().nextInt() % 2 == 0) ? -2 : (new Random().nextInt() % 2 == 0) ? -3 : -4));
        long timestamp = c.getTimeInMillis();
        String host = "SRVJLAHVWBR05";

        ZabbixAgentData zbx = new ZabbixAgentData();
        zbx.setHost(host);
        zbx.setHostMetadata("host_metadata");
        zbx.setKey("system.cpu.utilization");
        zbx.setValue("value");
        zbx.setLastlogsize("lastlogsize");
        zbx.setMtime("mtime");
        zbx.setTimestamp(String.valueOf(timestamp));
        zbx.setSource("source");
        zbx.setSeverity("severity");
        zbx.setEventid("eventiId");
        zbx.setClock(String.valueOf(timestamp));
        zbx.setNs(String.valueOf(timestamp));

        DataBean dataBean = new ZabbixAgentDataBean();
        dataBean.setBean(zbx);
        dataBean.setTool("Zabbix");
        dataBean.setHostname(host);
        dataBean.setTimestamp(timestamp);

        return dataBean;
    }
}