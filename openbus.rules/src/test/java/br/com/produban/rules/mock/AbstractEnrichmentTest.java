package br.com.produban.rules.mock;

import java.io.OutputStreamWriter;
import java.util.Properties;

import org.apache.avro.generic.GenericRecord;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.mockito.Mock;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.rules.repositories.jedis.JedisRepository;
import br.com.produban.openbus.rules.repositories.jedis.JedisResolver;

public abstract class AbstractEnrichmentTest {
	
	protected static Logger logger = Logger.getLogger(Class.class); 
	
	static {
		ConsoleAppender consoleAppender = new ConsoleAppender();
		consoleAppender.setWriter(new OutputStreamWriter(System.out));
		consoleAppender.setLayout(new PatternLayout("%-5p: %m%n"));
		logger.addAppender(consoleAppender);
	}
	
	@Mock
	protected JedisRepository jedisRepository;

	@Mock
	protected JedisResolver jedisResolver;
	
	protected GenericRecord getZabbixAgentExists() throws Exception {
		Properties properties = new Properties();
		properties.load(getClass().getResourceAsStream("/zabbix_exists.properties"));
		
		GenericRecord genericRecord = ZabbixAgentData.newBuilder()
			                .setValue(properties.getProperty("value"))
			                .setTimestamp(properties.getProperty("timestamp"))
			                .setClock(properties.getProperty("clock"))
			                .setHost(properties.getProperty("host"))
			                .setKey(properties.getProperty("key"))
			                .setHostMetadata(properties.getProperty("host_metadata"))
			                .setLastlogsize(properties.getProperty("lastlogsize"))
			                .setEventid(properties.getProperty("eventid"))
			                .setMtime(properties.getProperty("mtime"))
			                .setNs(properties.getProperty("ns"))
			                .setSeverity(properties.getProperty("severity"))
			                .setSource(properties.getProperty("source"))
			                .setState(properties.getProperty("state"))
			            .build();
		
		return genericRecord;
	}
	
	protected GenericRecord getZabbixAgentNotExists() throws Exception {
		Properties properties = new Properties();
		properties.load(getClass().getResourceAsStream("/zabbix_not_exists.properties"));
		
		GenericRecord genericRecord = ZabbixAgentData.newBuilder()
			                .setValue(properties.getProperty("value"))
			                .setTimestamp(properties.getProperty("timestamp"))
			                .setClock(properties.getProperty("clock"))
			                .setHost(properties.getProperty("host"))
			                .setKey(properties.getProperty("key"))
			                .setHostMetadata(properties.getProperty("host_metadata"))
			                .setLastlogsize(properties.getProperty("lastlogsize"))
			                .setEventid(properties.getProperty("eventid"))
			                .setMtime(properties.getProperty("mtime"))
			                .setNs(properties.getProperty("ns"))
			                .setSeverity(properties.getProperty("severity"))
			                .setSource(properties.getProperty("source"))
			                .setState(properties.getProperty("state"))
			            .build();
		
		return genericRecord;
	}	
	
	protected EnrichmentDatabaseConfiguration getEnrichmentDatabaseConfiguration() throws Exception {
		Properties properties = new Properties();
		properties.load(getClass().getResourceAsStream("/neo4j-conf.properties"));
		
	    EnrichmentDatabaseConfiguration enrichmentDatabaseConfiguration = new EnrichmentDatabaseConfiguration();
	    enrichmentDatabaseConfiguration.setUser(properties.getProperty("user"));
	    enrichmentDatabaseConfiguration.setPassword(properties.getProperty("password"));
	    enrichmentDatabaseConfiguration.setTransactionalCypherPath(properties.getProperty("transactionalCypherPath"));
	    enrichmentDatabaseConfiguration.setTraverseQuery(properties.getProperty("traversalQuery"));
	    enrichmentDatabaseConfiguration.setTraverseQueryIn(properties.getProperty("traversalQueryIn"));
	    enrichmentDatabaseConfiguration.setTraverseQueryOut(properties.getProperty("traversalQueryOut"));
	    enrichmentDatabaseConfiguration.setUrl(properties.getProperty("url"));
	    
	    return enrichmentDatabaseConfiguration;
	}

}
