package br.com.produban.rules.mock;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecordBase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.Transformer;

//@Ignore
@RunWith(MockitoJUnitRunner.class)
public class ZabbixEnrichmentMockTest extends AbstractEnrichmentTest {
	
	@Mock
	private ZabbixAgentData zabbixAgentData;

	@Test
	public void testZabbixAgent() {
		try {
			GenericRecord genericRecordExists = getZabbixAgentExists();
			GenericRecord genericRecordNotExists = getZabbixAgentNotExists();
	        
			Transformer transformer = new Transformer(null, 0, 0, null, getEnrichmentDatabaseConfiguration());
			transformer.setJedisResolver(jedisResolver);
			
			Map<String, String> values = new HashMap<>();
			values.put("discoverQuery", "MATCH (ls:LogicalServer) WHERE ls.hostname = \"{0}\" RETURN ls");
			values.put("valueExpression.0", "bean.getField('host')");

			when(jedisResolver.getJedis()).thenReturn(jedisRepository);
			when(jedisRepository.hgetAll("ENRICH:Zabbix")).thenReturn(values);
			
			for (int i=0;i<=3;i++) {
				List<DataBean> dataBeans = transformer.formatBean("Zabbix", (SpecificRecordBase) genericRecordExists);
				
				for (DataBean dataBean : dataBeans) {
					for (String key : dataBean.getAvroMapCopy().keySet()) {
						logger.info(key + "=" + dataBean.getExtraField(key));
					}
					
					for (String key : dataBean.getExtraFields().keySet()) {
						logger.info(key + "=" + dataBean.getExtraField(key));
					}
				}
			}
			
			for (int i=0;i<=3;i++) {
				List<DataBean> dataBeans = transformer.formatBean("Zabbix", (SpecificRecordBase) genericRecordNotExists);
				
				for (DataBean dataBean : dataBeans) {
					for (String key : dataBean.getAvroMapCopy().keySet()) {
						logger.info(key + "=" + dataBean.getExtraField(key));
					}
					
					for (String key : dataBean.getExtraFields().keySet()) {
						logger.info(key + "=" + dataBean.getExtraField(key));
					}
				}
			}			
			
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			fail(ex.getMessage());
		}
	}

}
