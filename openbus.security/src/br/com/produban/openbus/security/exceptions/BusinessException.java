package br.com.produban.openbus.security.exceptions;


public class BusinessException extends Exception {

	private static final long serialVersionUID = -7788061799557452462L;

	public BusinessException() {
		super();
	}
	
	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public BusinessException(Throwable cause) {
		super(cause);
	}
	
	public BusinessException(Exception e) {
		this(e.getMessage(), e);
	}
}
