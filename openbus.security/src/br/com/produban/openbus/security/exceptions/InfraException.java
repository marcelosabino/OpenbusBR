package br.com.produban.openbus.security.exceptions;

public class InfraException extends Exception {

	private static final long serialVersionUID = -6612011362429567590L;

	public InfraException() {
		super();
	}
	
	public InfraException(String message) {
		super(message);
	}
	
	public InfraException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public InfraException(Throwable cause) {
		super(cause);
	}
	
	public InfraException(Exception e) {
		super(e.getMessage(), e);
	}
}
