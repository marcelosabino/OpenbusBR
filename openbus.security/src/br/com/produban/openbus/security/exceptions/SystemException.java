package br.com.produban.openbus.security.exceptions;


public class SystemException extends Exception {
	
	private static final long serialVersionUID = -4237318762130087991L;

	public SystemException() {
		super();
	}
	
	public SystemException(String message) {
		super(message);
	}
	
	public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public SystemException(Throwable cause) {
		super(cause);
	}
	
	public SystemException(Exception e) {
		this(e.getMessage(), e);
	}
}
