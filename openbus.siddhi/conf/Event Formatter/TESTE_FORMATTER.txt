<?xml version="1.0" encoding="UTF-8"?>
<eventFormatter name="Teste_Formatter" statistics="enable"
  trace="disable" xmlns="http://wso2.org/carbon/eventformatter">
  <from streamName="TESTE_QL_OUTPUT" version="0.0.1"/>
  <mapping customMapping="disable" type="json"/>
  <to eventAdaptorName="OutQpidActiveMQ" eventAdaptorType="jms">
    <property name="transport.jms.Header"/>
    <property name="transport.jms.Destination">TESTE</property>
  </to>
</eventFormatter>
                                    