{
  "name": "StormToSiddhiStream_ZabbixMetrics",
  "version": "0.0.1",
  "nickName": "",
  "description": "",
  "payloadData": [
    {
      "name": "host",
      "type": "STRING"
    },
    {
      "name": "key",
      "type": "STRING"
    },
    {
      "name": "value",
      "type": "FLOAT"
    },
    {
      "name": "clock",
      "type": "LONG"
    },
    {
      "name": "ns",
      "type": "LONG"
    },
    {
      "name": "vdc",
      "type": "STRING"
    },
    {
      "name": "company",
      "type": "STRING"
    },
    {
      "name": "businessService",
      "type": "STRING"
    },
    {
      "name": "technicalService",
      "type": "STRING"
    },
    {
      "name": "serviceComponent",
      "type": "STRING"
    },
    {
      "name": "logicalService",
      "type": "STRING"
    },
    {
      "name": "platform",
      "type": "STRING"
    },
    {
      "name": "function",
      "type": "STRING"
    },
    {
      "name": "environment",
      "type": "STRING"
    },
    {
      "name": "status",
      "type": "STRING"
    },
    {
      "name": "cpu",
      "type": "STRING"
    },
    {
      "name": "interface",
      "type": "STRING"
    },
    {
      "name": "disk",
      "type": "STRING"
    },
    {
      "name": "tool",
      "type": "STRING"
    }
  ]
}
                                    