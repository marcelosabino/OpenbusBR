{
  "name": "TESTE_QL_OUTPUT",
  "version": "0.0.1",
  "nickName": "",
  "description": "",
  "payloadData": [
    {
      "name": "host",
      "type": "STRING"
    },
    {
      "name": "key",
      "type": "STRING"
    },
    {
      "name": "robotId",
      "type": "STRING"
    },
    {
      "name": "systemName",
      "type": "STRING"
    },
    {
      "name": "useCase",
      "type": "STRING"
    },
    {
      "name": "uid",
      "type": "STRING"
    },
    {
      "name": "level",
      "type": "STRING"
    },
    {
      "name": "threshold",
      "type": "FLOAT"
    },
    {
      "name": "minValue",
      "type": "FLOAT"
    },
    {
      "name": "maxValue",
      "type": "FLOAT"
    },
    {
      "name": "lastClock",
      "type": "LONG"
    },
    {
      "name": "occurrences",
      "type": "LONG"
    }
  ]
}
                                    