// H2
define table t_baseline (
  tenant string, 
  uid string, 
  hour string, 
  startTS long, 
  endTS long, 
  supCritical double, 
  supWarning double, 
  baseValue double, 
  infWarning double, 
  infCritical double)
from ('datasource.name'='BR_BASELINE_DB', 'database.name'='BR_BASELINE_DB', 'table.name'='BASELINE');


// "Superior Critical"
from servicemonitoring_input[key=='service_monitoring.latency']
  inner join t_baseline
  on (servicemonitoring_input.uid == t_baseline.uid)
     and (servicemonitoring_input.clock >= t_baseline.startTS)
	 and (servicemonitoring_input.clock < t_baseline.endTS)
     and (servicemonitoring_input.value > t_baseline.supCritical )
select 
  servicemonitoring_input.host as host, 
  servicemonitoring_input.key as key,
  servicemonitoring_input.robotId as robotId,
  servicemonitoring_input.systemName as systemName,
  servicemonitoring_input.useCase as useCase,
  servicemonitoring_input.uid as uid,
  servicemonitoring_input.value as value, 
  'Sup Critical' as level,
  convert(t_baseline.supCritical, float) as threshold,
  servicemonitoring_input.clock as clock
insert into outgoing;


// "Superior Warning"
from servicemonitoring_input[key=='service_monitoring.latency']
  inner join t_baseline
  on (servicemonitoring_input.uid == t_baseline.uid) 
     and (servicemonitoring_input.clock >= t_baseline.startTS)
	 and (servicemonitoring_input.clock < t_baseline.endTS)
     and (servicemonitoring_input.value <= t_baseline.supCritical) 
	 and (servicemonitoring_input.value > t_baseline.supWarning)
select 
  servicemonitoring_input.host as host, 
  servicemonitoring_input.key as key,
  servicemonitoring_input.robotId as robotId,
  servicemonitoring_input.systemName as systemName,
  servicemonitoring_input.useCase as useCase,
  servicemonitoring_input.uid as uid,
  servicemonitoring_input.value as value, 
  'Sup Warning' as level,
  convert(t_baseline.supWarning, float) as threshold,
  servicemonitoring_input.clock as clock
insert into outgoing;


// "Inferior Warning"
from servicemonitoring_input[key=='service_monitoring.latency']
  inner join t_baseline
  on (servicemonitoring_input.uid == t_baseline.uid) 
     and (servicemonitoring_input.clock >= t_baseline.startTS)
	 and (servicemonitoring_input.clock < t_baseline.endTS)
     and (servicemonitoring_input.value < t_baseline.infWarning) 
	 and (servicemonitoring_input.value >= t_baseline.infCritical)
select 
  servicemonitoring_input.host as host, 
  servicemonitoring_input.key as key,
  servicemonitoring_input.robotId as robotId,
  servicemonitoring_input.systemName as systemName,
  servicemonitoring_input.useCase as useCase,
  servicemonitoring_input.uid as uid,
  servicemonitoring_input.value as value, 
  'Inferior Warning' as level,
  convert(t_baseline.infWarning, float) as threshold,
  servicemonitoring_input.clock as clock
insert into outgoing;


// "Inferior Critical"
from servicemonitoring_input[key=='service_monitoring.latency']
  inner join t_baseline
  on (servicemonitoring_input.uid == t_baseline.uid) 
     and (servicemonitoring_input.clock >= t_baseline.startTS)
	 and (servicemonitoring_input.clock < t_baseline.endTS)
     and (servicemonitoring_input.value < t_baseline.infCritical)
select 
  servicemonitoring_input.host as host, 
  servicemonitoring_input.key as key,
  servicemonitoring_input.robotId as robotId,
  servicemonitoring_input.systemName as systemName,
  servicemonitoring_input.useCase as useCase,
  servicemonitoring_input.uid as uid,
  servicemonitoring_input.value as value, 
  'Inf Critical'  as level,
  convert(t_baseline.infCritical, float)  as threshold,
  servicemonitoring_input.clock as clock
insert into outgoing;
