<?xml version="1.0" encoding="UTF-8"?>
<outputEventAdaptor name="OutQpidActiveMQ" statistics="enable"
  trace="disable" type="jms" xmlns="http://wso2.org/carbon/eventadaptormanager">
  <property name="java.naming.provider.url">repository/conf/jndi.properties</property>
  <property name="java.naming.security.principal">admin</property>
  <property name="java.naming.security.credentials">admin</property>
  <property name="java.naming.factory.initial">org.apache.qpid.jndi.PropertiesFileInitialContextFactory</property>
  <property name="transport.jms.ConnectionFactoryJNDIName">QueueConnectionFactory</property>
  <property name="transport.jms.DestinationType">queue</property>
</outputEventAdaptor>
                                    