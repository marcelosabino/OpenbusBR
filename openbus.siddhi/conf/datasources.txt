-- DATASOURCE

Data Source Type		RDBMS
Name					BR_BASELINE_DB
Description				DB populated via Pig
Data Source Provider	default
Driver					org.h2.Driver
URL						jdbc:h2:repository/database/BR_BASELINE_DB;DB_CLOSE_ON_EXIT=FALSE
User Name				openbus
Password				openbus


-- JNDI

Name					jdbc/BR_BASELINE_DS


-- H2

CREATE SCHEMA BR_BASELINE_DB;

CREATE TABLE BR_BASELINE_DB.BASELINE (
	tenant VARCHAR NOT NULL,
	uid VARCHAR NOT NULL,
	hour VARCHAR NOT NULL,
	startTS BIGINT,
	endTS BIGINT,
	supCritical DOUBLE,
	supWarning DOUBLE,
	baseValue DOUBLE,
	infWarning DOUBLE,
	infCritical DOUBLE
);

ALTER TABLE BR_BASELINE_DB.BASELINE ADD CONSTRAINT pk PRIMARY KEY (tenant, uid, hour);


MERGE INTO BR_BASELINE_DB.BASELINE KEY (tenant, uid, hour) VALUES('TESTE', 'MYUID', '01', 1234667890, 1234667900, 90.0, 70.0, 50.0, 30.0, 10.0);

MERGE INTO BR_BASELINE_DB.BASELINE KEY (tenant, uid, hour) VALUES('AB', 'MyUid', '01', 1234567880, 1234567890, 90.0, 75.0, 50.0, 25.0, 10.0);
