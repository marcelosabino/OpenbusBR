package br.com.produban.openbus.siddhi.extensions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.wso2.siddhi.core.config.SiddhiContext;
import org.wso2.siddhi.core.event.Event;
import org.wso2.siddhi.core.event.StreamEvent;
import org.wso2.siddhi.core.event.in.InEvent;
import org.wso2.siddhi.core.event.in.InListEvent;
import org.wso2.siddhi.core.event.remove.RemoveEvent;
import org.wso2.siddhi.core.event.remove.RemoveListEvent;
import org.wso2.siddhi.core.query.QueryPostProcessingElement;
import org.wso2.siddhi.core.query.processor.window.RunnableWindowProcessor;
import org.wso2.siddhi.core.query.processor.window.WindowProcessor;
import org.wso2.siddhi.core.snapshot.ThreadBarrier;
import org.wso2.siddhi.core.util.collection.queue.scheduler.ISchedulerSiddhiQueue;
import org.wso2.siddhi.core.util.collection.queue.scheduler.SchedulerSiddhiQueue;
import org.wso2.siddhi.core.util.collection.queue.scheduler.SchedulerSiddhiQueueGrid;
import org.wso2.siddhi.query.api.definition.AbstractDefinition;
import org.wso2.siddhi.query.api.expression.Expression;
import org.wso2.siddhi.query.api.expression.Variable;
import org.wso2.siddhi.query.api.expression.constant.IntConstant;
import org.wso2.siddhi.query.api.expression.constant.LongConstant;
import org.wso2.siddhi.query.api.extension.annotation.SiddhiExtension;

@SiddhiExtension(namespace = "openbus", function = "inactive")
public class TimeWindowInactivity extends WindowProcessor implements RunnableWindowProcessor {

	static final Logger log = Logger.getLogger(TimeWindowInactivity.class);

	private ScheduledExecutorService eventRemoverScheduler;
	private ScheduledFuture<?> lastSchedule;
	private long timeToKeep;
	private String variable = "";
	int variablePosition = 0;
	private Map<Object, InEvent> newEventMap;
	private List<RemoveEvent> oldEventList;
	private Set<Object> cachedKeys;
	private ThreadBarrier threadBarrier;
	private ISchedulerSiddhiQueue<StreamEvent> window;
	private AbstractDefinition streamDefinition;

	protected void processEvent(InEvent event) {
		acquireLock();
		try {
			Object key = event.getData(variablePosition);
			this.newEventMap.put(key, event);
			cachedKeys.add(key);
		} finally {
			releaseLock();
		}
	}

	protected void processEvent(InListEvent listEvent) {
		acquireLock();
		try {
			System.out.println(listEvent);
			int i = 0;
			for (int size = listEvent.getActiveEvents(); i < size; ++i) {
				Event event = listEvent.getEvent(i);
				Object key = event.getData(variablePosition);
				this.newEventMap.put(key, (InEvent) event);
				cachedKeys.add(key);
			}
		} finally {
			releaseLock();
		}
	}

	public Iterator<StreamEvent> iterator() {
		return this.window.iterator();
	}

	public Iterator<StreamEvent> iterator(String predicate) {
		if (this.siddhiContext.isDistributedProcessingEnabled()) {
			return ((SchedulerSiddhiQueueGrid<StreamEvent>) this.window).iterator(predicate);
		}
		return this.window.iterator();
	}

	public void run() {
		acquireLock();
		try {
			long scheduledTime = System.currentTimeMillis();
			try {
				this.oldEventList.clear();
				while (true) {
					this.threadBarrier.pass();
					RemoveEvent removeEvent = (RemoveEvent) this.window.poll();
					if (removeEvent == null) {
						if (this.oldEventList.size() > 0) {
							this.nextProcessor.process(new RemoveListEvent(this.oldEventList
									.toArray(new RemoveEvent[this.oldEventList.size()])));
							this.oldEventList.clear();
						}

						if (this.newEventMap.size() > 0) {
							Set<Object> keySet = newEventMap.keySet();
							ArrayList<InEvent> processEvents = new ArrayList<InEvent>();
							ArrayList<InEvent> windowRemoveEvents = (ArrayList<InEvent>) newEventMap.values();

							for (Object key : cachedKeys) {
								if (!keySet.contains(key)) {
									Object[] data = new Object[streamDefinition.getAttributeList().size()];
									data[variablePosition] = key;
									InEvent inEvent = new InEvent(streamDefinition.getId(), System.currentTimeMillis(),
											data);
									processEvents.add(inEvent);
								}
							}

							for (InEvent inEvent : windowRemoveEvents) {
								this.window.put(new RemoveEvent(inEvent, -1L));
							}
							this.nextProcessor.process(new InListEvent(processEvents.toArray(new InEvent[processEvents
									.size()])));

							this.newEventMap.clear();
						}

						long diff = this.timeToKeep - (System.currentTimeMillis() - scheduledTime);
						if (diff > 0L) {
							try {
								if (this.lastSchedule != null) {
									this.lastSchedule.cancel(false);
								}
								this.lastSchedule = this.eventRemoverScheduler.schedule(this, diff,
										TimeUnit.MILLISECONDS);
							} catch (RejectedExecutionException ex) {
								log.warn("scheduling cannot be accepted for execution: elementID " + this.elementId);
							}
							break;
						}
						scheduledTime = System.currentTimeMillis();
					} else {
						this.oldEventList.add(new RemoveEvent(removeEvent, System.currentTimeMillis()));
					}
				}
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
			}
		} finally {
			releaseLock();
		}
	}

	protected Object[] currentState() {
		return new Object[] { this.window.currentState(), this.oldEventList, this.newEventMap, this.cachedKeys };
	}

	@SuppressWarnings("unchecked")
	protected void restoreState(Object[] data) {
		this.window.restoreState(data);
		this.window.restoreState((Object[]) (Object[]) data[0]);
		this.oldEventList = ((ArrayList<RemoveEvent>) data[1]);
		this.newEventMap = ((HashMap<Object, InEvent>) data[2]);
		this.cachedKeys = ((HashSet<Object>) data[3]);
		this.window.reSchedule();
	}

	protected void init(Expression[] parameters, QueryPostProcessingElement nextProcessor,
			AbstractDefinition streamDefinition, String elementId, boolean async, SiddhiContext siddhiContext) {
		if (parameters.length != 2) {
			log.error("Parameters count is not matching, there should be two parameters.");
		}
		if (parameters[0] instanceof IntConstant)
			this.timeToKeep = ((IntConstant) parameters[0]).getValue().intValue();
		else {
			this.timeToKeep = ((LongConstant) parameters[0]).getValue().longValue();
		}
		variable = ((Variable) parameters[1]).getAttributeName();
		variablePosition = streamDefinition.getAttributePosition(variable);
		this.streamDefinition = streamDefinition;

		this.oldEventList = new ArrayList<RemoveEvent>();
		if (this.siddhiContext.isDistributedProcessingEnabled())
			this.newEventMap = this.siddhiContext.getHazelcastInstance().getMap(elementId + "-newEventMap");
		else {
			this.newEventMap = new HashMap<Object, InEvent>();
		}
		this.cachedKeys = new HashSet<Object>();

		if (this.siddhiContext.isDistributedProcessingEnabled())
			this.window = new SchedulerSiddhiQueueGrid<StreamEvent>(elementId, this, this.siddhiContext, this.async);
		else {
			this.window = new SchedulerSiddhiQueue<StreamEvent>(this);
		}

		this.window.schedule();
	}

	public void schedule() {
		if (this.lastSchedule != null) {
			this.lastSchedule.cancel(false);
		}
		this.lastSchedule = this.eventRemoverScheduler.schedule(this, this.timeToKeep, TimeUnit.MILLISECONDS);
	}

	public void scheduleNow() {
		if (this.lastSchedule != null) {
			this.lastSchedule.cancel(false);
		}
		this.lastSchedule = this.eventRemoverScheduler.schedule(this, 0L, TimeUnit.MILLISECONDS);
	}

	public void setScheduledExecutorService(ScheduledExecutorService scheduledExecutorService) {
		this.eventRemoverScheduler = scheduledExecutorService;
	}

	public void destroy() {
	}

	public void setThreadBarrier(ThreadBarrier threadBarrier) {
		this.threadBarrier = threadBarrier;
	}
}
