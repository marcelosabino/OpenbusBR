package integration.kafka;

import br.com.openbus.publisher.Publisher;
import br.com.openbus.publisher.kafka.KafkaAvroPublisher;
import integration.KafkaMessage;
import org.apache.avro.generic.GenericRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaIntegrationTask<A extends GenericRecord> implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaIntegrationTask.class);

    private static final ThreadLocal<Publisher<? extends GenericRecord>> publisherThreadLocal = new ThreadLocal<>();

    private String kafkaBrokerList;
    private String topicName;
    private KafkaMessage<A> message;

    public KafkaIntegrationTask(String kafkaBrokerList, String topicName) {
        this.kafkaBrokerList = kafkaBrokerList;
        this.topicName = topicName;
    }

    public void setMessage(KafkaMessage<A> message) {
        this.message = message;
    }

    private Publisher<A> getPublisher() {
        Publisher publisher = publisherThreadLocal.get();

        if (publisher == null) {
            publisher = new KafkaAvroPublisher<A>(kafkaBrokerList, false, true, 10000);
            publisherThreadLocal.set(publisher);
        }

        return publisher;
    }

    @Override
    public void run() {
        try {
            getPublisher().publishAll(message.getTool(), message.toAvro(), topicName);
        } catch (Exception e) {
            LOGGER.error("Fail to send collected data to Kafka",e);
            return;
        }
    }
}
