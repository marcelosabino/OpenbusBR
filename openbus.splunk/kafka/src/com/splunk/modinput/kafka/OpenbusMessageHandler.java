package com.splunk.modinput.kafka;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.avro.Schema.Field;
import org.apache.avro.generic.GenericRecord;
import org.apache.log4j.Logger;

import br.com.produban.openbus.avro.AvroDecoder;

import com.splunk.modinput.SplunkLogEvent;
import com.splunk.modinput.Stream;
import com.splunk.modinput.StreamEvent;
import com.splunk.modinput.kafka.KafkaModularInput.MessageReceiver;

public class OpenbusMessageHandler extends AbstractMessageHandler {

	private static Logger logger = Logger.getLogger(OpenbusMessageHandler.class);

	private String schemaFilter = null;
	private AvroDecoder avroDecoder;

	@Override
	public Stream handleMessage(byte[] messageContents, MessageReceiver context) throws Exception {

		Stream stream = new Stream();
		ArrayList<StreamEvent> list = new ArrayList<StreamEvent>();

		try {
			GenericRecord record = avroDecoder.toRecord(messageContents);

			if (!record.getSchema().getName().equals(schemaFilter)) {
				return null;
			}

			SplunkLogEvent splunkEvent = buildCommonEventMessagePart(context);
			for (Field field : record.getSchema().getFields()) {
				String fieldName = Character.toString(field.name().charAt(0)).toUpperCase() + field.name().substring(1);
				splunkEvent.addPair(fieldName, record.get(field.pos()));
				logger.debug("KEY: " + field.name() + " VALUE: " + record.get(field.pos()));
			}

			StreamEvent event = new StreamEvent();
			event.setUnbroken("1");
			event.setData(splunkEvent.toString());
			event.setStanza(context.stanzaName);
			event.setDone(" ");

			list.add(event);

		} catch (Exception e) {
			logger.error("Error handling message" + new String(messageContents), e);
		}

		stream.setEvents(list);

		return stream;
	}

	/**
	 * Properties example:
	 * kafka.message.coder.schema.registry.class=br.com.produban.openbus.avro.schemaregistry.AvroLocalSchemaRegistry
	 * kafka.registry.schemaPackage=br.com.produban.openbus.model.avro
	 * splunk.schema.filter=Eventlog
	 */
	@Override
	public void setParams(Map<String, String> params) {
		Properties properties = new Properties();
		for (Entry<String, String> entry : params.entrySet())
			properties.setProperty(entry.getKey(), entry.getValue());

		logger.debug("Package scannig: " + properties.getProperty("kafka.registry.schemaPackage"));
		avroDecoder = new AvroDecoder(properties);

		schemaFilter = properties.getProperty("splunk.schema.filter");
		if (schemaFilter == null) {
			throw new IllegalArgumentException("Property splunk.schema.filter must be filled.");
		}
	}

}
