package br.com.produban.openbus.sql2kafka.sender;

import java.util.List;

import org.apache.avro.specific.SpecificRecordBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.openbus.publisher.MessageValidator;
import br.com.openbus.publisher.kafka.KafkaAvroPublisher;

public class KafkaSender extends SenderJob {

	private Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Override
	public void send() {

		MessageValidator<SpecificRecordBase> messageValidator = new MessageValidator<SpecificRecordBase>() {
			@Override
			public boolean validate(SpecificRecordBase paramT) {
				return true;
			}
		};

		@SuppressWarnings("unchecked")
		KafkaAvroPublisher<SpecificRecordBase> kafkaPublisher = new KafkaAvroPublisher<>(
				sql2KafkaProperties.getKafkaBrokerList(), Boolean.valueOf(sql2KafkaProperties.getKafkaRequiredAcks()),
				false, 100, messageValidator);

		try {
			List<SpecificRecordBase> specificRecordBaseList = sendQueues.getSpecificRecordBaseList();
			if (specificRecordBaseList != null && !specificRecordBaseList.isEmpty())
				kafkaPublisher.publishAll(collectProperties.getTool(), specificRecordBaseList, sql2KafkaProperties.getKafkaTopicName());

		}  finally {
			kafkaPublisher.closePublisher();
		}
	}
}
