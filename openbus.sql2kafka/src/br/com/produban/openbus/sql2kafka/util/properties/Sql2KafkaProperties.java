package br.com.produban.openbus.sql2kafka.util.properties;

public class Sql2KafkaProperties {

	private String collectorListResultsExit;
	private String collectorName;
	private String kafkaBrokerList;
	private String kafkaRequiredAcks;
	private String kafkaTopicName;

	public String getCollectorListResultsExit() {
		return collectorListResultsExit;
	}

	public void setCollectorListResultsExit(String collectorListResultsExit) {
		this.collectorListResultsExit = collectorListResultsExit;
	}

	public String getCollectorName() {
		return collectorName;
	}

	public void setCollectorName(String collectorName) {
		this.collectorName = collectorName;
	}

	public String getKafkaBrokerList() {
		return kafkaBrokerList;
	}

	public void setKafkaBrokerList(String kafkaBrokerList) {
		this.kafkaBrokerList = kafkaBrokerList;
	}

	public String getKafkaRequiredAcks() {
		return kafkaRequiredAcks;
	}

	public void setKafkaRequiredAcks(String kafkaRequiredAcks) {
		this.kafkaRequiredAcks = kafkaRequiredAcks;
	}

	public String getKafkaTopicName() {
		return kafkaTopicName;
	}

	public void setKafkaTopicName(String kafkaTopicName) {
		this.kafkaTopicName = kafkaTopicName;
	}

}
