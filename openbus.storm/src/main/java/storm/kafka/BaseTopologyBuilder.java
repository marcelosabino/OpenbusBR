package storm.kafka;

import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import com.typesafe.config.Config;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseTopologyBuilder {

    private String tenantId = "";
    private Config configuration;
    private TopologyBuilder builder = new TopologyBuilder();

    // Kafka's zookeeper
    private BrokerHosts brokerHosts;
    private int zkPort;
    // Storm zookeeper
    private List<String> stormZkHosts;

    public BaseTopologyBuilder(Config configuration) {
        this.configuration = configuration;
        init();
    }

    public BaseTopologyBuilder(String tenantId, Config configuration) {
        this.tenantId = tenantId;
        this.configuration = configuration;
        init();
    }

    private void init() {

        Config kafkaConfig = configuration.getConfig("kafka");

        brokerHosts = new ZkHosts(kafkaConfig.getString("kafka.zookeeper"));

        stormZkHosts = new ArrayList<>();
        zkPort = 2181;
        for (String hostPort : kafkaConfig.getString("storm.zookeeper").split(",")) {
            String[] temp = hostPort.split(":");
            stormZkHosts.add(temp[0]);
            if (temp[1] != null) {
                zkPort = Integer.parseInt(temp[1]);
            }
        }

        Config ramifications = configuration.getConfig("topologies.transformation.ramifications");

        if (!ramifications.getBoolean("metrics.enabled") &&
            !ramifications.getBoolean("logs.enabled") &&
            !ramifications.getBoolean("snmp.enabled"))
            throw new IllegalStateException("At least one transformation topology must be enabled");

        if (!ramifications.getBoolean("qpid.enabled") &&
            !ramifications.getBoolean("splunk.enabled") &&
            !ramifications.getBoolean("elasticsearch.enabled"))
            throw new IllegalStateException("At least one persistence topology must be enabled");
    }

    public abstract StormTopology build();

    public String getTenantId() {
        return tenantId;
    }

    public Config getConfiguration() {
        return configuration;
    }

    public TopologyBuilder getBuilder() {
        return builder;
    }

    public BrokerHosts getBrokerHosts() {
        return brokerHosts;
    }

    public int getZkPort() {
        return zkPort;
    }

    public List<String> getStormZkHosts() {
        return stormZkHosts;
    }
}