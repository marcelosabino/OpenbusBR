package storm.kafka;

public interface Constants {
	public static final String STREAM_OPENTSDB = "stream_opentsdb";
	public static final String STREAM_KAFKA_MESSAGE = "stream_kafka_message";
	public static final String STREAM_SPLUNK = "stream_splunk";

	public static final String PERSISTER_OPENTSDB = "opentsdb";
	public static final String PERSISTER_KAFKA_MESSAGE = "qpid";
	public static final String PERSISTER_SPLUNK = "splunk";
}
