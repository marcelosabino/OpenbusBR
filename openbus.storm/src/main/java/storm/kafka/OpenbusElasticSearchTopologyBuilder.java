package storm.kafka;

import backtype.storm.generated.StormTopology;
import backtype.storm.spout.RawScheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.elasticsearch.storm.EsBolt;
import storm.kafka.bolts.DataBeanEmitterBolt;

import java.util.HashMap;
import java.util.Map;

public class OpenbusElasticSearchTopologyBuilder extends BaseTopologyBuilder {

    private Config elasticSearchTopologyConfig;
    private Config ramificationsConfig;
    private Config executorsConfig;
    private Config kafkaConfig;
    private Map<String,String> elasticSearchConfig;

    public OpenbusElasticSearchTopologyBuilder(Config configuration) {
        super(configuration);

        this.elasticSearchTopologyConfig = getConfiguration().getConfig("topologies.elasticsearch");
        this.ramificationsConfig = getConfiguration().getConfig("topologies.transformation.ramifications");
        this.executorsConfig = elasticSearchTopologyConfig.getConfig("executors.count");
        this.kafkaConfig = getConfiguration().getConfig("kafka");

        elasticSearchConfig = new HashMap<>();

        for (Map.Entry<String,ConfigValue> entry : getConfiguration().getConfig("elasticsearch").entrySet()) {
            elasticSearchConfig.put(entry.getKey(),entry.getValue().unwrapped().toString());
        }
    }

    @Override
    public StormTopology build() {
        return withPersistenceElasticSearch().getBuilder().createTopology();
    }

    private OpenbusElasticSearchTopologyBuilder withPersistenceElasticSearch() {

//        if (elasticSearchTopologyConfig.getBoolean("enabled")) {
            // ************************************** ElasticSearch configuration **************************************
            // Kafka Topic for ElasticSearch
            String topicElasticSearch = ramificationsConfig.getString("elasticsearch.topic.name");
            String rootPathElasticSearch = kafkaConfig.getString("zookeeper.root.path") + "_ElasticSearch";
            String zkConsumerIdElasticSearch = kafkaConfig.getString("zookeeper.consumer.id") + "_ElasticSearch";
            // Spout ElasticSearch Config
            SpoutConfig spoutElasticSearchConfig = new SpoutConfig(getBrokerHosts(), topicElasticSearch, rootPathElasticSearch,
                    zkConsumerIdElasticSearch);
            // ZK to Storm
            spoutElasticSearchConfig.zkServers = getStormZkHosts();
            spoutElasticSearchConfig.zkPort = getZkPort();
            // Schema
            spoutElasticSearchConfig.scheme = new SchemeAsMultiScheme(new RawScheme());

            getBuilder().setSpout("topicElasticSearch", new KafkaSpout(spoutElasticSearchConfig),
                    executorsConfig.getInt("spout"))
                    .setNumTasks(executorsConfig.getInt("spout"));

            getBuilder().setBolt("deserializerElasticSearchBolt",
                    new DataBeanEmitterBolt(), executorsConfig.getInt("bolt.deserializer"))
                    .setNumTasks(executorsConfig.getInt("bolt.deserializer"))
                    .shuffleGrouping("topicElasticSearch");

            getBuilder().setBolt("persisterElasticSearchBolt",
                    new EsBolt("openbus_index_{tenant}_{route}/main",elasticSearchConfig), executorsConfig.getInt("bolt.persister"))
                    .setNumTasks(executorsConfig.getInt("bolt.persister"))
                    .addConfiguration(backtype.storm.Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 5)
                    .shuffleGrouping("deserializerElasticSearchBolt");
//        }

        return this;
    }
}
