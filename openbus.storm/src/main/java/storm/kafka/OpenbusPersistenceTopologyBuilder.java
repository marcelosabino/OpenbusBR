package storm.kafka;

import backtype.storm.Config;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.RawScheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import org.elasticsearch.storm.EsBolt;
import storm.kafka.bolts.*;

public class OpenbusPersistenceTopologyBuilder extends BaseTopologyBuilder {

    public OpenbusPersistenceTopologyBuilder(com.typesafe.config.Config configuration) {
        super(configuration);
    }

    @Override
    public StormTopology build() {
        return null;
    }

//    public OpenbusPersistenceTopologyBuilder(String tenantId, StormLauncher.TopologyProperties properties) {
//        super(tenantId, properties);
//    }
//
//    public StormTopology build() {
//        return withPersistenceOpenTsdb()
//                .withPersistenceQpid()
//                .withPersistenceSplunk()
//                .withPersistenceElasticSearch()
//                .withPersistenceWSO2()
//                .getBuilder()
//                .createTopology();
//    }
//
//    private OpenbusPersistenceTopologyBuilder withPersistenceOpenTsdb() {
//
//        if (getProperties().isOpenTsdbEnabled()) {
//            // ************************************** OpenTSDB configuration **************************************
//            // Kafka Topic for OpenTSDB
//            String topicOpenTsdb = getProperties().getKafkaTopicOpenTSDB();
//            String rootPathOpenTsdb = getProperties().getKafkaZookeeperRootPath() + "_OpenTsdb";
////            String zkConsumerIdOpenTsdb = getTenantId() + "_" + getProperties().getKafkaZookeeperConsumerId() + "_OpenTsdb";
//            String zkConsumerIdOpenTsdb = getProperties().getKafkaZookeeperConsumerId() + "_OpenTsdb";
//            // Spout OpenTSDB Config
//            SpoutConfig spoutOpenTsdbConfig = new SpoutConfig(getBrokerHosts(), topicOpenTsdb, rootPathOpenTsdb,
//                    zkConsumerIdOpenTsdb);
//            // ZK to Storm
//            spoutOpenTsdbConfig.zkServers = getStormZkHosts();
//            spoutOpenTsdbConfig.zkPort = getZkPort();
//            // Schema
//            spoutOpenTsdbConfig.scheme = new SchemeAsMultiScheme(new RawScheme());
//
//            getBuilder().setSpout("topicOpenTsbd", new KafkaSpout(spoutOpenTsdbConfig),
//                    getProperties().getSpoutOpenTsdbExecutorCount())
//                    .setNumTasks(getProperties().getSpoutOpenTsdbExecutorCount());
//
//            //OpenTSDB
//            getBuilder().setBolt("deserializerOpenTsdbBolt",
//                    new DeserializerKafkaMessageBolt(), getProperties().getDeserializerOpentsdbExecutorCount())
//                    .setNumTasks(getProperties().getDeserializerOpentsdbExecutorCount())
//                    .shuffleGrouping("topicOpenTsbd");
//            getBuilder().setBolt("persisterOpenTsdbBolt",
//                    new PersisterOpenTsdbBolt(getProperties()), getProperties().getPersisterOpentsdbExecutorCount())
//                    .setNumTasks(getProperties().getPersisterOpentsdbExecutorCount())
//                    .shuffleGrouping("deserializerOpenTsdbBolt");
//        }
//
//        return this;
//    }
//
//    private OpenbusPersistenceTopologyBuilder withPersistenceQpid() {
//
//        if (getProperties().isQpidEnabled()) {
//            // ************************************** Qpid configuration **************************************
//            // Kafka Topic for Qpid
//            String topicQpid = getProperties().getKafkaTopicQpid();
//            String rootPathQpid = getProperties().getKafkaZookeeperRootPath() + "_Qpid";
////            String zkConsumerIdQpid = getTenantId() + "_" + getProperties().getKafkaZookeeperConsumerId() + "_Qpid";
//            String zkConsumerIdQpid = getProperties().getKafkaZookeeperConsumerId() + "_Qpid";
//            // Spout Qpid Config
//            SpoutConfig spoutQpidConfig = new SpoutConfig(getBrokerHosts(), topicQpid, rootPathQpid,
//                    zkConsumerIdQpid);
//            // ZK to Storm
//            spoutQpidConfig.zkServers = getStormZkHosts();
//            spoutQpidConfig.zkPort = getZkPort();
//            // Schema
//            spoutQpidConfig.scheme = new SchemeAsMultiScheme(new RawScheme());
//
//            getBuilder().setSpout("topicQpid", new KafkaSpout(spoutQpidConfig),
//                    getProperties().getSpoutQpidExecutorCount())
//                    .setNumTasks(getProperties().getSpoutQpidExecutorCount());
//
//            //Qpid
//            getBuilder().setBolt("deserializerQpidBolt",
//                    new DeserializerKafkaMessageBolt(), getProperties().getDeserializerQpidExecutorCount())
//                    .setNumTasks(getProperties().getDeserializerQpidExecutorCount())
//                    .shuffleGrouping("topicQpid");
//            getBuilder().setBolt("persisterQpidBolt",
//                    new PersisterQpidBolt(getProperties()), getProperties().getPersisterQpidExecutorCount())
//                    .setNumTasks(getProperties().getPersisterQpidExecutorCount())
//                    .shuffleGrouping("deserializerQpidBolt");
//        }
//
//        return this;
//
//    }
//
//    private OpenbusPersistenceTopologyBuilder withPersistenceSplunk() {
//
//        if (getProperties().isSplunkEnabled()) {
//            // ************************************** Splunk configuration **************************************
//            // Kafka Topic for Splunk
//            String topicSplunk = getProperties().getKafkaTopicSplunk();
//            String rootPathSplunk = getProperties().getKafkaZookeeperRootPath() + "_Splunk";
////            String zkConsumerIdSnmp = getTenantId() + "_" + getProperties().getKafkaZookeeperConsumerId() + "_Splunk";
//            String zkConsumerIdSplunk = getProperties().getKafkaZookeeperConsumerId() + "_Splunk";
//            // Spout Splunk Config
//            SpoutConfig spoutSplunkConfig = new SpoutConfig(getBrokerHosts(), topicSplunk, rootPathSplunk,
//                    zkConsumerIdSplunk);
//            // ZK to Storm
//            spoutSplunkConfig.zkServers = getStormZkHosts();
//            spoutSplunkConfig.zkPort = getZkPort();
//            // Schema
//            spoutSplunkConfig.scheme = new SchemeAsMultiScheme(new RawScheme());
//
//            getBuilder().setSpout("topicSplunk", new KafkaSpout(spoutSplunkConfig),
//                    getProperties().getSpoutSplunkExecutorCount())
//                    .setNumTasks(getProperties().getSpoutSplunkExecutorCount());
//
//            //Splunk
//            getBuilder().setBolt("deserializerSplunkBolt",
//                    new DeserializerKafkaMessageBolt(), getProperties().getDeserializerSplunkExecutorCount())
//                    .setNumTasks(getProperties().getDeserializerSplunkExecutorCount())
//                    .shuffleGrouping("topicSplunk");
//            getBuilder().setBolt("persisterSplunkBolt",
//                    new PersisterSplunkBolt(getProperties()), getProperties().getPersisterSplunkExecutorCount())
//                    .setNumTasks(getProperties().getPersisterSplunkExecutorCount())
//                    .shuffleGrouping("deserializerSplunkBolt");
//            getBuilder().setBolt("persisterSplunkCepBolt",
//                    new PersisterSplunkCepBolt(getProperties()), getProperties().getPersisterSplunkExecutorCount())
//                    .setNumTasks(getProperties().getPersisterSplunkExecutorCount())
//                    .shuffleGrouping("deserializerSplunkBolt");
//        }
//
//        return this;
//    }
//
//    private OpenbusPersistenceTopologyBuilder withPersistenceElasticSearch() {
//
//        if (getProperties().isElasticSearchEnabled()) {
//            // ************************************** ElasticSearch configuration **************************************
//            // Kafka Topic for ElasticSearch
//            String topicElasticSearch = getProperties().getKafkaTopicQpid();
//            String rootPathElasticSearch = getProperties().getKafkaZookeeperRootPath() + "_ElasticSearch";
//            String zkConsumerIdElasticSearch = getProperties().getKafkaZookeeperConsumerId() + "_ElasticSearch";
//            // Spout ElasticSearch Config
//            SpoutConfig spoutElasticSearchConfig = new SpoutConfig(getBrokerHosts(), topicElasticSearch, rootPathElasticSearch,
//                    zkConsumerIdElasticSearch);
//            // ZK to Storm
//            spoutElasticSearchConfig.zkServers = getStormZkHosts();
//            spoutElasticSearchConfig.zkPort = getZkPort();
//            // Schema
//            spoutElasticSearchConfig.scheme = new SchemeAsMultiScheme(new RawScheme());
//
//            getBuilder().setSpout("topicElasticSearch", new KafkaSpout(spoutElasticSearchConfig),
//                    getProperties().getSpoutEsExecutorCount())
//                    .setNumTasks(getProperties().getSpoutEsExecutorCount());
//
//            getBuilder().setBolt("deserializerElasticSearchBolt",
//                    new DataBeanEmitterBolt(getProperties()), getProperties().getDeserializerElasticSearchExecutorCount())
//                    .setNumTasks(getProperties().getDeserializerElasticSearchExecutorCount())
//                    .shuffleGrouping("topicElasticSearch");
//
//            getBuilder().setBolt("persisterElasticSearchBolt",
//                    new EsBolt("openbus_index_{tenant}_{route}/main"), getProperties().getPersisterElasticSearchExecutorCount())
//                    .setNumTasks(getProperties().getPersisterElasticSearchExecutorCount())
//                    .addConfiguration(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 5)
//                    .shuffleGrouping("deserializerElasticSearchBolt");
//        }
//
//        return this;
//    }
//
//    private OpenbusPersistenceTopologyBuilder withPersistenceWSO2() {
//
//        if (getProperties().isWso2Enabled()) {
//            // ************************************** WSO2 configuration **************************************
//            // Kafka Topic for WSO2
//            String topicWSO2 = getProperties().getKafkaTopicQpid();
//            String rootPathWSO2 = getProperties().getKafkaZookeeperRootPath() + "_WSO2";
////            String zkConsumerIdWSO2 = getTenantId() + "_" + getProperties().getKafkaZookeeperConsumerId() + "_WSO2";
//            String zkConsumerIdWSO2 = getProperties().getKafkaZookeeperConsumerId() + "_WSO2";
//            // Spout WSO2 Config
//            SpoutConfig spoutWSO2Config = new SpoutConfig(getBrokerHosts(), topicWSO2, rootPathWSO2,
//                    zkConsumerIdWSO2);
//            // ZK to Storm
//            spoutWSO2Config.zkServers = getStormZkHosts();
//            spoutWSO2Config.zkPort = getZkPort();
//            // Schema
//            spoutWSO2Config.scheme = new SchemeAsMultiScheme(new RawScheme());
//
//            getBuilder().setSpout("topicWSO2", new KafkaSpout(spoutWSO2Config),
//                    getProperties().getSpoutQpidExecutorCount())
//                    .setNumTasks(getProperties().getSpoutQpidExecutorCount());
//
//            //WSO2
//            getBuilder().setBolt("deserializerWSO2Bolt",
//                    new DeserializerKafkaMessageBolt(), getProperties().getDeserializerQpidExecutorCount())
//                    .setNumTasks(getProperties().getDeserializerQpidExecutorCount())
//                    .shuffleGrouping("topicWSO2");
//            getBuilder().setBolt("persisterWSO2Bolt",
//                    new PersisterWso2CepBolt(getProperties()), getProperties().getPersisterQpidExecutorCount())
//                    .setNumTasks(getProperties().getPersisterQpidExecutorCount())
//                    .shuffleGrouping("deserializerWSO2Bolt");
//        }
//
//        return this;
//
//    }
}