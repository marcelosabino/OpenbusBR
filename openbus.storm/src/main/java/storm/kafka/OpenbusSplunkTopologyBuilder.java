package storm.kafka;

import backtype.storm.generated.StormTopology;
import backtype.storm.spout.RawScheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import com.typesafe.config.Config;
import storm.kafka.bolts.DeserializerKafkaMessageBolt;
import storm.kafka.bolts.PersisterSplunkBolt;
import storm.kafka.bolts.PersisterSplunkCepBolt;

public class OpenbusSplunkTopologyBuilder extends BaseTopologyBuilder {

    private Config splunkTopologyConfig;
    private Config ramificationsConfig;
    private Config executorsConfig;
    private Config kafkaConfig;

    public OpenbusSplunkTopologyBuilder(Config configuration) {
        super(configuration);

        this.splunkTopologyConfig = getConfiguration().getConfig("topologies.splunk");
        this.ramificationsConfig = getConfiguration().getConfig("topologies.transformation.ramifications");
        this.executorsConfig = splunkTopologyConfig.getConfig("executors.count");
        this.kafkaConfig = getConfiguration().getConfig("kafka");
    }

    @Override
    public StormTopology build() {
        return withPersistenceSplunk().getBuilder().createTopology();
    }

    private OpenbusSplunkTopologyBuilder withPersistenceSplunk() {

//        if (splunkTopologyConfig.getBoolean("enabled")) {
            // ************************************** Splunk configuration **************************************
            // Kafka Topic for Splunk
            String topicSplunk = ramificationsConfig.getString("splunk.topic.name");
            String rootPathSplunk = kafkaConfig.getString("zookeeper.root.path") + "_Splunk";
            String zkConsumerIdSplunk = kafkaConfig.getString("zookeeper.consumer.id") + "_Splunk";
            // Spout Splunk Config
            SpoutConfig spoutSplunkConfig = new SpoutConfig(getBrokerHosts(), topicSplunk, rootPathSplunk,
                    zkConsumerIdSplunk);
            // ZK to Storm
            spoutSplunkConfig.zkServers = getStormZkHosts();
            spoutSplunkConfig.zkPort = getZkPort();
            // Schema
            spoutSplunkConfig.scheme = new SchemeAsMultiScheme(new RawScheme());

            getBuilder().setSpout("topicSplunk", new KafkaSpout(spoutSplunkConfig),
                    executorsConfig.getInt("spout"))
                    .setNumTasks(executorsConfig.getInt("spout"));

            //Splunk
            getBuilder().setBolt("deserializerSplunkBolt",
                    new DeserializerKafkaMessageBolt(), executorsConfig.getInt("bolt.deserializer"))
                    .setNumTasks(executorsConfig.getInt("bolt.deserializer"))
                    .shuffleGrouping("topicSplunk");
            getBuilder().setBolt("persisterSplunkBolt",
                    new PersisterSplunkBolt(getConfiguration()), executorsConfig.getInt("bolt.persister"))
                    .setNumTasks(executorsConfig.getInt("bolt.persister"))
                    .shuffleGrouping("deserializerSplunkBolt");
//            getBuilder().setBolt("persisterSplunkCepBolt",
//                    new PersisterSplunkCepBolt(getProperties()), getProperties().getPersisterSplunkExecutorCount())
//                    .setNumTasks(getProperties().getPersisterSplunkExecutorCount())
//                    .shuffleGrouping("deserializerSplunkBolt");
//        }

        return this;
    }
}
