package storm.kafka;

import backtype.storm.generated.StormTopology;
import backtype.storm.spout.RawScheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.BoltDeclarer;
import com.typesafe.config.Config;
import storm.kafka.bolts.*;

import java.text.MessageFormat;

public class OpenbusTransformationTopologyBuilder extends BaseTopologyBuilder {

    private static final String TOPIC_NAME_TEMPLATE = "{0}.{1}";

    private Config transformationConfig;
    private Config ramificationsConfig;
    private Config executorsConfig;
    private Config kafkaConfig;

    public OpenbusTransformationTopologyBuilder(String tenantId, Config configuration) {
        super(tenantId, configuration);

        this.transformationConfig = getConfiguration().getConfig("topologies.transformation");
        this.ramificationsConfig = transformationConfig.getConfig("ramifications");
        this.executorsConfig = transformationConfig.getConfig("executors.count");
        this.kafkaConfig = getConfiguration().getConfig("kafka");
    }

    public StormTopology build() {
        return withTransformationMetrics()
                .withTransformationLogs()
                .withTransformationSnmp()
                .withKafkaPersisters()
                .getBuilder()
                .createTopology();
    }

    private Config getTransformationConfig() {
        return transformationConfig;
    }

    private Config getRamificationsConfig() {
        return ramificationsConfig;
    }

    private Config getExecutorsConfig() {
        return executorsConfig;
    }

    private Config getKafkaConfig() {
        return kafkaConfig;
    }

    private OpenbusTransformationTopologyBuilder withTransformationMetrics() {

        if (getRamificationsConfig().getBoolean("metrics.enabled")) {
            // ************************************** Metrics configuration **************************************
            // Kafka Topic for Metrics
            String topicMetrics = MessageFormat.format(TOPIC_NAME_TEMPLATE,getTenantId(),getRamificationsConfig().getString("metrics.topic.name"));
            String rootPathMetrics = getKafkaConfig().getString("zookeeper.root.path") + "_Metrics";
            String zkConsumerIdMetrics = getTenantId() + "_" + getKafkaConfig().getString("zookeeper.consumer.id") + "_Metrics";
            // Spout Metrics Config
            SpoutConfig spoutMetricsConfig = new SpoutConfig(getBrokerHosts(), topicMetrics, rootPathMetrics,
                    zkConsumerIdMetrics);
            // ZK to Storm
            spoutMetricsConfig.zkServers = getStormZkHosts();
            spoutMetricsConfig.zkPort = getZkPort();
            // Schema
            spoutMetricsConfig.scheme = new SchemeAsMultiScheme(new RawScheme());

            getBuilder().setSpout("topicMetrics", new KafkaSpout(spoutMetricsConfig),
                    getExecutorsConfig().getInt("spout.metrics"))
                             .setNumTasks(getExecutorsConfig().getInt("spout.metrics"));

            //Metrics
            getBuilder().setBolt("deserializerMetricsBolt",
                    new DeserializerAvroBolt(getConfiguration()), getExecutorsConfig().getInt("bolt.deserializer.metrics"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.deserializer.metrics"))
                    .shuffleGrouping("topicMetrics");
            getBuilder().setBolt("transformerMetricsBolt",
                    new TransformerMetricsBolt(getConfiguration(), getTenantId()), getExecutorsConfig().getInt("bolt.transformer.metrics"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.transformer.metrics"))
                    .shuffleGrouping("deserializerMetricsBolt");
        }

        return this;
    }

    private OpenbusTransformationTopologyBuilder withTransformationLogs() {

        if (getRamificationsConfig().getBoolean("logs.enabled")) {
            // ************************************** Logs configuration **************************************
            // Kafka Topic for Logs
            String topicLogs = MessageFormat.format(TOPIC_NAME_TEMPLATE,getTenantId(),getRamificationsConfig().getString("logs.topic.name"));
            String rootPathLogs = getKafkaConfig().getString("zookeeper.root.path") + "_Logs";
            String zkConsumerIdLogs = getTenantId() + "_" + getKafkaConfig().getString("zookeeper.consumer.id") + "_Logs";
            // Spout Logs Config
            SpoutConfig spoutLogsConfig = new SpoutConfig(getBrokerHosts(), topicLogs, rootPathLogs, zkConsumerIdLogs);
            // ZK to Storm
            spoutLogsConfig.zkServers = getStormZkHosts();
            spoutLogsConfig.zkPort = getZkPort();
            // Schema
            spoutLogsConfig.scheme = new SchemeAsMultiScheme(new RawScheme());

            getBuilder().setSpout("topicLogs", new KafkaSpout(spoutLogsConfig),
                    getExecutorsConfig().getInt("spout.logs"))
                             .setNumTasks(getExecutorsConfig().getInt("spout.logs"));

            //Logs
            getBuilder().setBolt("deserializerLogsBolt",
                    new DeserializerAvroBolt(getConfiguration()), getExecutorsConfig().getInt("bolt.deserializer.logs"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.deserializer.logs"))
                    .shuffleGrouping("topicLogs");
            getBuilder().setBolt("transformerLogsBolt",
                    new TransformerLogsBolt(getConfiguration(), getTenantId()), getExecutorsConfig().getInt("bolt.transformer.logs"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.transformer.logs"))
                    .shuffleGrouping("deserializerLogsBolt");
        }

        return this;
    }

    private OpenbusTransformationTopologyBuilder withTransformationSnmp() {

        if (getRamificationsConfig().getBoolean("snmp.enabled")) {
            // ************************************** SNMP configuration **************************************
            // Kafka Topic for SNMP
            String topicSnmp = MessageFormat.format(TOPIC_NAME_TEMPLATE,getTenantId(),getRamificationsConfig().getString("snmp.topic.name"));
            String rootPathSnmp = getKafkaConfig().getString("zookeeper.root.path") + "_Snmp";
            String zkConsumerIdSnmp = getTenantId() + "_" + getKafkaConfig().getString("zookeeper.consumer.id") + "_Snmp";
            // Spout SNMP Config
            SpoutConfig spoutSnmpConfig = new SpoutConfig(getBrokerHosts(), topicSnmp, rootPathSnmp,
                    zkConsumerIdSnmp);
            // ZK to Storm
            spoutSnmpConfig.zkServers = getStormZkHosts();
            spoutSnmpConfig.zkPort = getZkPort();
            // Schema
            spoutSnmpConfig.scheme = new SchemeAsMultiScheme(new RawScheme());

            getBuilder().setSpout("topicSnmp", new KafkaSpout(spoutSnmpConfig),
                    getExecutorsConfig().getInt("spout.snmp"))
                             .setNumTasks(getExecutorsConfig().getInt("spout.snmp"));

            //Snmp
            getBuilder().setBolt("deserializerSnmpBolt",
                    new DeserializerAvroBolt(getConfiguration()), getExecutorsConfig().getInt("bolt.deserializer.snmp"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.deserializer.snmp"))
                    .shuffleGrouping("topicSnmp");
            getBuilder().setBolt("transformerSnmpBolt",
                    new TransformerSnmpBolt(getConfiguration(), getTenantId()), getExecutorsConfig().getInt("bolt.transformer.snmp"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.transformer.snmp"))
                    .shuffleGrouping("deserializerSnmpBolt");
        }

        return this;
    }

    private OpenbusTransformationTopologyBuilder withKafkaPersisters() {

//        BoltDeclarer persisterKafkaOpenTsdbBolt = null;
//        if (getProperties().isOpenTsdbEnabled()) {
//            persisterKafkaOpenTsdbBolt = getBuilder().setBolt("persisterKafkaOpenTsdbBolt",
//                    new PersisterKafkaBolt(getTenantId(), getProperties(), getProperties().getKafkaTopicOpenTSDB()),
//                    getProperties().getPersisterKafkaOpentsdbExecutorCount())
//                    .setNumTasks(getProperties().getPersisterKafkaOpentsdbExecutorCount());
//        }

        BoltDeclarer persisterKafkaMessageBolt = null;
        if (getRamificationsConfig().getBoolean("qpid.enabled") || getRamificationsConfig().getBoolean("elasticsearch.enabled")) {
            persisterKafkaMessageBolt = getBuilder().setBolt("persisterKafkaMessageBolt",
                    new PersisterKafkaBolt(getTenantId(), getKafkaConfig(), getRamificationsConfig().getString("qpid.topic.name")),
                    getExecutorsConfig().getInt("bolt.persister.kafka.qpid"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.persister.kafka.qpid"));
        }

        BoltDeclarer persisterKafkaSplunkBolt = null;
        if (getRamificationsConfig().getBoolean("splunk.enabled")) {
            persisterKafkaSplunkBolt = getBuilder().setBolt("persisterKafkaSplunkBolt",
                    new PersisterKafkaBolt(getTenantId(), getKafkaConfig(), getRamificationsConfig().getString("splunk.topic.name")),
                    getExecutorsConfig().getInt("bolt.persister.kafka.splunk"))
                    .setNumTasks(getExecutorsConfig().getInt("bolt.persister.kafka.splunk"));
        }


        if (getRamificationsConfig().getBoolean("metrics.enabled")) {
//            if (persisterKafkaOpenTsdbBolt != null)
//                persisterKafkaOpenTsdbBolt.shuffleGrouping("transformerMetricsBolt", Constants.STREAM_OPENTSDB);
            if (persisterKafkaMessageBolt != null)
                persisterKafkaMessageBolt.shuffleGrouping("transformerMetricsBolt", Constants.STREAM_KAFKA_MESSAGE);
            if (persisterKafkaSplunkBolt != null)
                persisterKafkaSplunkBolt.shuffleGrouping("transformerMetricsBolt", Constants.STREAM_SPLUNK);
        }

        if (getRamificationsConfig().getBoolean("logs.enabled")) {
//            if (persisterKafkaOpenTsdbBolt != null)
//                persisterKafkaOpenTsdbBolt.shuffleGrouping("transformerLogsBolt", Constants.STREAM_OPENTSDB);
            if (persisterKafkaMessageBolt != null)
                persisterKafkaMessageBolt.shuffleGrouping("transformerLogsBolt", Constants.STREAM_KAFKA_MESSAGE);
            if (persisterKafkaSplunkBolt != null)
                persisterKafkaSplunkBolt.shuffleGrouping("transformerLogsBolt", Constants.STREAM_SPLUNK);
        }

        if (getRamificationsConfig().getBoolean("snmp.enabled")) {
//            if (persisterKafkaOpenTsdbBolt != null)
//                persisterKafkaOpenTsdbBolt.shuffleGrouping("transformerSnmpBolt", Constants.STREAM_OPENTSDB);
            if (persisterKafkaMessageBolt != null)
                persisterKafkaMessageBolt.shuffleGrouping("transformerSnmpBolt", Constants.STREAM_KAFKA_MESSAGE);
            if (persisterKafkaSplunkBolt != null)
                persisterKafkaSplunkBolt.shuffleGrouping("transformerSnmpBolt", Constants.STREAM_SPLUNK);
        }


        return this;
    }
}
