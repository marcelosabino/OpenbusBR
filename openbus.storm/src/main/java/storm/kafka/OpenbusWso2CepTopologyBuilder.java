package storm.kafka;

import backtype.storm.generated.StormTopology;
import backtype.storm.spout.RawScheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import com.typesafe.config.Config;
import storm.kafka.bolts.DeserializerKafkaMessageBolt;
import storm.kafka.bolts.PersisterWso2CepBolt;

public class OpenbusWso2CepTopologyBuilder extends BaseTopologyBuilder {

    private Config cepTopologyConfig;
    private Config ramificationsConfig;
    private Config executorsConfig;
    private Config kafkaConfig;

    public OpenbusWso2CepTopologyBuilder(Config configuration) {
        super(configuration);

        this.cepTopologyConfig = getConfiguration().getConfig("topologies.cep");
        this.ramificationsConfig = getConfiguration().getConfig("topologies.transformation.ramifications");
        this.executorsConfig = cepTopologyConfig.getConfig("executors.count");
        this.kafkaConfig = getConfiguration().getConfig("kafka");
    }

    @Override
    public StormTopology build() {
        return withPersistenceWSO2().getBuilder().createTopology();
    }

    private OpenbusWso2CepTopologyBuilder withPersistenceWSO2() {

//        if (cepTopologyConfig.getBoolean("enabled")) {
            // ************************************** WSO2 configuration **************************************
            // Kafka Topic for WSO2
            String topicWSO2 = ramificationsConfig.getString("qpid.topic.name");
            String rootPathWSO2 = kafkaConfig.getString("zookeeper.root.path") + "_WSO2";
            String zkConsumerIdWSO2 = kafkaConfig.getString("zookeeper.consumer.id") + "_WSO2";
            // Spout WSO2 Config
            SpoutConfig spoutWSO2Config = new SpoutConfig(getBrokerHosts(), topicWSO2, rootPathWSO2,
                    zkConsumerIdWSO2);
            // ZK to Storm
            spoutWSO2Config.zkServers = getStormZkHosts();
            spoutWSO2Config.zkPort = getZkPort();
            // Schema
            spoutWSO2Config.scheme = new SchemeAsMultiScheme(new RawScheme());

            getBuilder().setSpout("topicWSO2", new KafkaSpout(spoutWSO2Config),
                    executorsConfig.getInt("spout"))
                    .setNumTasks(executorsConfig.getInt("spout"));

            //WSO2
            getBuilder().setBolt("deserializerWSO2Bolt",
                    new DeserializerKafkaMessageBolt(), executorsConfig.getInt("bolt.deserializer"))
                    .setNumTasks(executorsConfig.getInt("bolt.deserializer"))
                    .shuffleGrouping("topicWSO2");
            getBuilder().setBolt("persisterWSO2Bolt",
                    new PersisterWso2CepBolt(getConfiguration()), executorsConfig.getInt("bolt.persister"))
                    .setNumTasks(executorsConfig.getInt("bolt.persister"))
                    .shuffleGrouping("deserializerWSO2Bolt");
//        }

        return this;
    }
}
