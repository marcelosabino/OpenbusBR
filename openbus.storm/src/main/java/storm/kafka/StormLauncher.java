package storm.kafka;

import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import br.com.produban.openbus.model.pojo.DataBean;
import com.typesafe.config.ConfigFactory;
import org.apache.avro.specific.AvroGenerated;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import storm.kafka.api.KafkaMessage;

import java.io.File;
import java.util.*;

public class StormLauncher {

    public static void main(String[] args) throws Exception {

        String configFileLocation = args[0];

        if (configFileLocation == null || configFileLocation.isEmpty())
            throw new IllegalStateException("Config file location is mandatory!");
        else if (!(new File(configFileLocation).exists()))
            throw new IllegalStateException("Config file location is mandatory!");

        System.setProperty("config.file", configFileLocation);

        com.typesafe.config.Config openbusConfig = ConfigFactory.load().getConfig("openbus");

        Set<Class<?>> scannedClasses = loadClasspathRegistry();

        com.typesafe.config.Config tenantConfig = openbusConfig.getConfig("tenants");
        com.typesafe.config.Config transformationTopologyConfig = openbusConfig.getConfig("topologies.transformation");

        for (String tenantId : tenantConfig.root().keySet()) {
            Config stormTopologyConfig = createNewConfig(transformationTopologyConfig.getInt("num.workers"),
                    openbusConfig,
                    scannedClasses);

            OpenbusTransformationTopologyBuilder transformationTopologyBuilder = new OpenbusTransformationTopologyBuilder(tenantId, openbusConfig);
            StormTopology stormTransformationTopology = transformationTopologyBuilder.build();
            System.out.println(">>> Transformation topology... ");
            String tranformationTopologyId = "openbus_topology_" + tenantId.toLowerCase();
            System.out.println(">>> Submit Topology... ");
            StormSubmitter.submitTopology(tranformationTopologyId, stormTopologyConfig, stormTransformationTopology);
        }

        if (openbusConfig.getBoolean("topologies.elasticsearch.enabled")) {
            Config elasticSearchTopologyConfig = createNewConfig(openbusConfig.getInt("topologies.elasticsearch.num.workers"),
                    openbusConfig,
                    scannedClasses);

            OpenbusElasticSearchTopologyBuilder elasticSearchTopologyBuilder = new OpenbusElasticSearchTopologyBuilder(openbusConfig);
            StormTopology elasticSearchStormTopology = elasticSearchTopologyBuilder.build();
            System.out.println(">>> Elasticsearch topology... ");
            String elasticSearchTopologyId = "elasticsearch_topology";
            System.out.println(">>> Submit Topology... ");
            StormSubmitter.submitTopology(elasticSearchTopologyId, elasticSearchTopologyConfig, elasticSearchStormTopology);
        }

        if (openbusConfig.getBoolean("topologies.cep.enabled")) {
            Config cepTopologyConfig = createNewConfig(openbusConfig.getInt("topologies.cep.num.workers"),
                    openbusConfig,
                    scannedClasses);

            OpenbusWso2CepTopologyBuilder wso2CepTopologyBuilder = new OpenbusWso2CepTopologyBuilder(openbusConfig);
            StormTopology wso2StormTopology = wso2CepTopologyBuilder.build();
            System.out.println(">>> Elasticsearch topology... ");
            String wso2TopologyId = "wso2cep_topology";
            System.out.println(">>> Submit Topology... ");
            StormSubmitter.submitTopology(wso2TopologyId, cepTopologyConfig, wso2StormTopology);
        }

        if (openbusConfig.getBoolean("topologies.splunk.enabled")) {
            Config splunkTopologyConfig = createNewConfig(openbusConfig.getInt("topologies.splunk.num.workers"),
                    openbusConfig,
                    scannedClasses);

            OpenbusSplunkTopologyBuilder splunkTopologyBuilder = new OpenbusSplunkTopologyBuilder(openbusConfig);
            StormTopology splunkStormTopology = splunkTopologyBuilder.build();
            System.out.println(">>> Splunk topology... ");
            String splunkTopologyId = "splunk_topology";
            System.out.println(">>> Submit Topology... ");
            StormSubmitter.submitTopology(splunkTopologyId, splunkTopologyConfig, splunkStormTopology);
        }
    }

    private static void registerSerializationClasses(Config topologyConfig, Set<Class<?>> scannedClasses) {
        for (Class<?> avroClass : scannedClasses) {
            topologyConfig.registerSerialization(avroClass);
        }
    }

    private static Set<Class<?>> loadClasspathRegistry() {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forClassLoader())
                .filterInputsBy(new FilterBuilder().includePackage("br.com.produban.openbus.model")));

        Set<Class<?>> scannedClasses = new HashSet<>();

        scannedClasses.addAll(reflections.getTypesAnnotatedWith(AvroGenerated.class));
        scannedClasses.addAll(reflections.getSubTypesOf(DataBean.class));
        scannedClasses.add(KafkaMessage.class);
        scannedClasses.add(AvroTool.class);

        return scannedClasses;
    }

    private static Config createNewConfig(int numWorkers, com.typesafe.config.Config openbusConfig, Set<Class<?>> scannedClasses) {
        Config topologyConfig = new Config();

        topologyConfig.setMaxTaskParallelism(openbusConfig.getInt("storm.max.task.parallelism"));
        topologyConfig.setMaxSpoutPending(openbusConfig.getInt("storm.max.spout.pending"));
        topologyConfig.setNumAckers(openbusConfig.getInt("storm.acker.executors"));
        topologyConfig.setNumWorkers(numWorkers);

        registerSerializationClasses(topologyConfig, scannedClasses);

        return topologyConfig;
    }
}



