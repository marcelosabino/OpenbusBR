package storm.kafka.api;

import br.com.produban.openbus.model.pojo.DataBean;

import java.util.Map;

public class KafkaMessage {

    private String tenantId;
    private DataBean dataBean;

    public KafkaMessage() {
    }

    public KafkaMessage(String tenantId, DataBean dataBean) {
        this.tenantId = tenantId;
        this.dataBean = dataBean;
    }

    public String getTenantId() {
        return tenantId;
    }

    public DataBean getDataBean() {
        return dataBean;
    }
}
