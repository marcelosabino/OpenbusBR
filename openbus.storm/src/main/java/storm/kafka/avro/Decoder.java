package storm.kafka.avro;

import br.com.produban.openbus.avro.AvroDecoder;
import com.typesafe.config.Config;

import java.io.Serializable;

public class Decoder implements Serializable {

	private static final long serialVersionUID = -3741618327016833840L;

	private static final ThreadLocal<AvroDecoder> decoderThreadLocal = new ThreadLocal<>();
	private Config avroConfiguration;

	public Decoder(Config avroConfiguration) {
		this.avroConfiguration = avroConfiguration;
	}

	public AvroDecoder getAvroDecoder() {
		if (decoderThreadLocal.get() == null) {
			decoderThreadLocal.set(new AvroDecoder(avroConfiguration));
		}
		return decoderThreadLocal.get();
	}

}
