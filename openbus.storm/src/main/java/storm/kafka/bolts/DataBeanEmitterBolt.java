package storm.kafka.bolts;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import br.com.produban.openbus.model.OpenbusTool;
import br.com.produban.openbus.model.avro.DB2;
import br.com.produban.openbus.security.exceptions.BusinessException;
import storm.kafka.StormLauncher;
import storm.kafka.api.KafkaMessage;

import java.text.ParseException;

public class DataBeanEmitterBolt extends DeserializerKafkaMessageBolt {

    public DataBeanEmitterBolt() {
    }

    @Override
    protected void doEmit(Tuple tuple, KafkaMessage message) throws BusinessException {
        String tool = message.getDataBean().getTool();
        try {
            getCollector().emit(tuple,
                    new Values(message.getTenantId().toLowerCase(),
                            tool.replaceAll(" ","").toLowerCase(),
                            tool,
                            message.getDataBean().getCompleteDataBeanMapCopy(),
                            message.getDataBean().getTimestamp()));
        } catch (ParseException e) {
            throw new BusinessException(String.format("Invalid timestamp for tool %s",tool),e);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tenant","route","tool","bean","timestamp"));
    }
}
