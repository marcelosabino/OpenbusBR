package storm.kafka.bolts;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.TupleImpl;
import backtype.storm.tuple.Values;
import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import br.com.produban.openbus.security.exceptions.SystemException;
import com.typesafe.config.Config;
import storm.kafka.avro.Decoder;
import storm.kafka.bolts.base.OpenbusBaseBolt;

public class DeserializerAvroBolt extends OpenbusBaseBolt {

    private static final long serialVersionUID = -6292083689206223198L;

    private Decoder decoder;

	public DeserializerAvroBolt(Config configuration) {
		decoder = new Decoder(configuration.getConfig("avro"));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("record"));
	}

    @Override
    protected void doExecute(Tuple tuple) throws Exception {
        byte[] message = (byte[]) ((TupleImpl) tuple).get("bytes");

        AvroTool decodedMessage = decoder.getAvroDecoder().toRecord(message);

        if (decodedMessage == null)
            throw new SystemException("Avro decoder failed.");
        else {

            if (getLogger().isInfoEnabled())
                getLogger().info("A message has arrived: Tool -> {} | Avro -> {}",
                        decodedMessage.getToolName(),
                        decodedMessage.getRecord());

            getCollector().emit(tuple, new Values(decodedMessage));
        }
    }
}
