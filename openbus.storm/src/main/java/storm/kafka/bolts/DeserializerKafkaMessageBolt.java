package storm.kafka.bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.TupleImpl;
import backtype.storm.tuple.Values;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.SystemException;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.OpenbusBaseBolt;

import java.io.ByteArrayInputStream;
import java.util.Map;

public class DeserializerKafkaMessageBolt extends OpenbusBaseBolt {

    private Kryo kryo;

    @Override
    protected void doExecute(Tuple tuple) throws Exception {
        byte[] messageBytes = (byte[]) ((TupleImpl) tuple).get("bytes");

        Input in = new Input(new ByteArrayInputStream(messageBytes));
        KafkaMessage message = kryo.readObject(in, KafkaMessage.class);
        in.close();

        if (message == null)
            throw new SystemException("Kafka message deserialization failed.");
        else
            doEmit(tuple, message);
    }

    protected void doEmit(Tuple tuple, KafkaMessage message) throws BusinessException {
        getCollector().emit(tuple, new Values(message));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("message"));
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        kryo = new Kryo();
        super.prepare(map, topologyContext, outputCollector);
    }
}
