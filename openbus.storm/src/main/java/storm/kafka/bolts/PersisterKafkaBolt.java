package storm.kafka.bolts;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import br.com.openbus.publisher.Publisher;
import br.com.openbus.publisher.kafka.KafkaGenericPublisher;
import br.com.produban.openbus.model.pojo.DataBean;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import com.typesafe.config.Config;
import storm.kafka.StormLauncher;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.OpenbusBaseBolt;

import java.io.ByteArrayOutputStream;
import java.util.Map;

public class PersisterKafkaBolt extends OpenbusBaseBolt {

    private String tenantId;
    private String kafkaTopic;
    private Config kafkaConfig;

    private Publisher<ByteArrayOutputStream> publisher;
    private Kryo kryo;

    public PersisterKafkaBolt(String tenantId, Config kafkaConfig,
                              String kafkaTopic) {
        this.tenantId = tenantId;
        this.kafkaTopic = kafkaTopic;
        this.kafkaConfig = kafkaConfig;
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        this.publisher = new KafkaGenericPublisher(kafkaConfig.getString("broker.list"),false,true,1000);
        this.kryo = new Kryo();

        super.prepare(map, topologyContext, outputCollector);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {}

    @Override
    public void cleanup() {
        publisher.closePublisher();
    }

    @Override
    protected void doExecute(Tuple tuple) throws Exception {
        DataBean dataBean = (DataBean) tuple.getValueByField("dataBean");
        KafkaMessage message = new KafkaMessage(tenantId,dataBean);

        Output out = new Output(new ByteArrayOutputStream());
        kryo.writeObject(out,message);
        out.close();

        publisher.publish((ByteArrayOutputStream) out.getOutputStream(),kafkaTopic);
    }
}