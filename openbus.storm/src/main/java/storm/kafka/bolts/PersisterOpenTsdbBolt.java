package storm.kafka.bolts;

import br.com.produban.openbus.model.avro.ZabbixAgentData;
import br.com.produban.openbus.security.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import storm.kafka.StormLauncher;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.OpenbusBaseBolt;
import storm.kafka.opentsdb.OpentsdbLogger;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.InfraException;

public class PersisterOpenTsdbBolt extends OpenbusBaseBolt {

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private OpentsdbLogger opentsdbLogger;

//    public PersisterOpenTsdbBolt(StormLauncher.TopologyProperties topologyProperties) {
//        opentsdbLogger = new OpentsdbLogger(topologyProperties);
//    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {}

    @Override
    protected void doExecute(Tuple tuple) throws Exception {
        KafkaMessage message = (KafkaMessage) tuple.getValueByField("message");

        if (message.getDataBean().getKey().length() >= 256)
            throw new BusinessException(String.format("OpenTSDB key is too long [key=%s]. Data will be discarded!",
                                                      message.getDataBean().getKey()));

//        opentsdbLogger.log(message.getDataBean().getKey(),
//                           message.getDataBean().getTimestamp()/1000L,
//                           getTsValue(message.getDataBean()),
//                           message.getDataBean().getHostname(),
//                           message.getDataBean().getExtraFields());
    }

    protected String getTsValue(DataBean dataBean) {
        return dataBean.getBean()
                       .getSchema()
                       .getName()
                       .equals(ZabbixAgentData.getClassSchema().getName()) ?
                        ((ZabbixAgentData)dataBean.getBean()).getValue() :
                        "1";
    }
}
