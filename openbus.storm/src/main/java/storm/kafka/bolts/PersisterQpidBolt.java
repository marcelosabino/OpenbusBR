package storm.kafka.bolts;

import java.util.Map;

import storm.kafka.StormLauncher;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.JsonOutputPersisterBaseBolt;
import storm.kafka.qpid.QpidSender;
import backtype.storm.topology.OutputFieldsDeclarer;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class PersisterQpidBolt extends JsonOutputPersisterBaseBolt {

	private static final long serialVersionUID = -1810394715650759735L;

	private QpidSender qpidSender;

//	public PersisterQpidBolt(StormLauncher.TopologyProperties topologyProperties) {
//		qpidSender = new QpidSender(topologyProperties);
//	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {}

	@Override
	protected String resolveDestination(KafkaMessage message) throws BusinessException {
		return message.getTenantId() + "." + super.resolveDestination(message);
	}
	
    @Override
    protected Map<String, String> buildOutputMap(DataBean dataBean, String destination) {
		Map<String, String> map = dataBean.getCompleteDataBeanMapCopy();
		map.put("tool", destination);
        return map;
    }

    @Override
    protected String buildJson(Map<String, String> dataMap) {
    	return "{\"event\": { \"payloadData\": " + getDataMapAsJson(dataMap) + "} }";
    }
	
	@Override
	protected void deliver(String destRef, String json) throws InfraException, BusinessException {
		qpidSender.send(destRef,json);
	}
}
