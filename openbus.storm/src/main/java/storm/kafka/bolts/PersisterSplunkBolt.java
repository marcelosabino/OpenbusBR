package storm.kafka.bolts;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import br.com.produban.openbus.security.exceptions.SystemException;
import com.typesafe.config.Config;
import storm.kafka.StormLauncher;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.JsonOutputPersisterBaseBolt;
import storm.kafka.splunk.SplunkSender;

import java.util.HashMap;
import java.util.Map;

public class PersisterSplunkBolt extends JsonOutputPersisterBaseBolt {

    private static final long serialVersionUID = 6499301813260640939L;

    private SplunkSender splunkSender;

    public PersisterSplunkBolt(Config config) {

        Map<String,String> tenantPortPrefix = new HashMap<>();
        for (String tenant : config.getConfig("tenants").root().keySet())
            tenantPortPrefix.put(tenant, config.getString("tenants."+tenant+".splunk.basePort"));

        splunkSender = new SplunkSender(
            config.getString("topologies.splunk.host"),
            tenantPortPrefix
        );
    }

    @Override
    protected String resolveDestination(KafkaMessage message) throws BusinessException {
        return message.getTenantId() + "." + super.resolveDestination(message);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {}

    @Override
    protected Map<String, String> buildOutputMap(DataBean dataBean, String destination) {
    	return dataBean.getAvroMapCopy();
    }

    @Override
    protected String buildJson(Map<String, String> dataMap) {
        return getDataMapAsJson(dataMap);
    }

    @Override
    protected void deliver(String destRef, String json) throws InfraException, BusinessException {
        splunkSender.send(destRef, json);
    }
}
