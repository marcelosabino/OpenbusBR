package storm.kafka.bolts;

import br.com.produban.openbus.model.OpenbusTool;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import storm.kafka.StormLauncher;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.JsonOutputPersisterBaseBolt;
import storm.kafka.splunk.SplunkSender;

import java.util.Map;

public class PersisterSplunkCepBolt extends JsonOutputPersisterBaseBolt {

    private static final long serialVersionUID = 6499301813260640939L;

    private SplunkSender splunkSender;

//    public PersisterSplunkCepBolt(StormLauncher.TopologyProperties topologyProperties) {
//        splunkSender = new SplunkSender(
//            topologyProperties.getSplunkCepHost(),
//            topologyProperties.getSplunkCepPort()
//        );
//    }

    @Override
    protected String resolveDestination(KafkaMessage message) throws BusinessException {
        OpenbusTool tool = OpenbusTool.resolveTool(message.getDataBean().getBean());

        if (tool.getToolType().equals(OpenbusTool.ToolType.METRICS))
            return "CEP."+message.getDataBean().getTool();
        else
            return null;
    }

    @Override
    protected Map<String, String> buildOutputMap(DataBean dataBean, String destination) {
    	return dataBean.getCompleteDataBeanMapCopy();
    }

    @Override
    protected String buildJson(Map<String, String> dataMap) {
        return getDataMapAsJson(dataMap);
    }

    @Override
    protected void deliver(String destRef, String json) throws InfraException, BusinessException {
        splunkSender.send(destRef, json);
    }
}
