package storm.kafka.bolts;

import backtype.storm.topology.OutputFieldsDeclarer;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import com.typesafe.config.Config;
import storm.kafka.api.KafkaMessage;
import storm.kafka.bolts.base.JsonOutputPersisterBaseBolt;
import storm.kafka.http.HTTPSender;

import java.util.HashMap;
import java.util.Map;

public class PersisterWso2CepBolt extends JsonOutputPersisterBaseBolt {

	private static final long serialVersionUID = -1810394715650759735L;

	private HTTPSender httpSender;
	private Config configuration;
	private Map<String,String> domains;

	public PersisterWso2CepBolt(Config configuration) {
		httpSender = new HTTPSender(configuration.getConfig("wso2.cep"));
		this.configuration = configuration;

		this.domains = new HashMap<>();
		for (String tenant : configuration.getConfig("tenants").root().keySet()) {
			domains.put(tenant, configuration.getString("tenants."+tenant+".cep.domain"));
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {}

	@Override
	protected String resolveDestination(KafkaMessage message) throws BusinessException {
		return domains.get(message.getTenantId()) + "/" + super.resolveDestination(message);
	}
	
    @Override
    protected Map<String, String> buildOutputMap(DataBean dataBean, String destination) {
		Map<String, String> map = dataBean.getCompleteDataBeanMapCopy();
		map.put("tool", destination);
        return map;
    }

    @Override
    protected String buildJson(Map<String, String> dataMap) {
    	return "{\"event\": { \"payloadData\": " + getDataMapAsJson(dataMap) + "} }";
    }
	
	@Override
	protected void deliver(String destRef, String json) throws InfraException, BusinessException {
		httpSender.send(destRef,json);
	}
}
