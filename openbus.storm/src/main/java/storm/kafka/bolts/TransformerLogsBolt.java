package storm.kafka.bolts;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import com.typesafe.config.Config;
import org.apache.avro.specific.SpecificRecordBase;
import storm.kafka.Constants;
import storm.kafka.StormLauncher;
import storm.kafka.bolts.base.TransformerBaseBolt;

import java.util.List;

public class TransformerLogsBolt extends TransformerBaseBolt {

    private static final long serialVersionUID = 1618806826355571487L;

    public TransformerLogsBolt(Config configuration, String tenantId) {
        super(configuration, tenantId);
    }

    @Override
    protected void doExecute(Tuple tuple) throws InfraException, BusinessException {
        AvroTool record = (AvroTool)tuple.getValueByField("record");

        List<DataBean> beanList = getTransformer().formatBean(record.getToolName(), (SpecificRecordBase) record.getRecord());

        if (beanList != null) {
            DataBean toBeEmmited = beanList.get(0);
            doEmit(tuple, toBeEmmited);
        } else {
            logAndDiscard(record.getRecord());
        }
    }

    @Override
    protected void doEmit(Tuple tuple, DataBean bean) throws InfraException, BusinessException {
        if (getRamificationsConfig().getBoolean("splunk.enabled")) {
            getCollector().emit(
                    Constants.STREAM_SPLUNK,
                    //tuple,
                    new Values(getTransformer().prepareToPersist(Constants.PERSISTER_SPLUNK, bean)));
        }
    }
}
