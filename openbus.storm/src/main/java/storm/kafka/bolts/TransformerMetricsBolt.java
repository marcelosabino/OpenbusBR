package storm.kafka.bolts;

import java.util.List;

import br.com.produban.openbus.avro.schemaregistry.AvroTool;
import com.typesafe.config.Config;
import org.apache.avro.specific.SpecificRecordBase;

import storm.kafka.StormLauncher;
import storm.kafka.bolts.base.TransformerBaseBolt;
import backtype.storm.tuple.Tuple;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;

public class TransformerMetricsBolt extends TransformerBaseBolt {

	private static final long serialVersionUID = -2989898710478126044L;

    public TransformerMetricsBolt(Config configuration, String tenantId) {
        super(configuration, tenantId);
    }

	@Override
	protected void doExecute(Tuple tuple) throws InfraException, BusinessException {
		AvroTool record = (AvroTool)tuple.getValueByField("record");

		List<DataBean> beanList = getTransformer().formatBean(record.getToolName(), (SpecificRecordBase) record.getRecord());

		if (beanList != null) {
			for (DataBean toBeEmmited : beanList) {
				doEmit(tuple, toBeEmmited);
			}
		} else {
			logAndDiscard(record.getRecord());
		}
	}
}
