package storm.kafka.bolts.base;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import br.com.produban.openbus.model.OpenbusTool;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import com.google.gson.GsonBuilder;
import storm.kafka.api.KafkaMessage;

import java.util.Map;

public abstract class JsonOutputPersisterBaseBolt extends OpenbusBaseBolt {

	private static final long serialVersionUID = 6992736563417780011L;

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {}

	@Override
    protected void doExecute(Tuple tuple) throws Exception {
        KafkaMessage message = (KafkaMessage) tuple.getValueByField("message");
        String destRef = resolveDestination(message);

        if (destRef != null) {
            Map<String, String> dataMap = buildOutputMap(message.getDataBean(), destRef);
            String json = buildJson(dataMap);

            deliver(destRef, json);
        }
    }


    protected String resolveDestination(KafkaMessage message) throws BusinessException {
//        OpenbusTool tool = OpenbusTool.resolveTool(message.getDataBean().getBean());
//
//        if (tool != null)
//            return tool.getToolName();
//        else
//            throw new BusinessException("Error while resolving destination for: " + message.getDataBean().toString());
        return message.getDataBean().getTool();
    }


    protected abstract Map<String, String> buildOutputMap(DataBean dataBean, String destination);

    protected String getDataMapAsJson(Map<String, String> dataMap) {
        return new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(dataMap);
    }

    protected abstract String buildJson(Map<String, String> dataMap);

    protected abstract void deliver(String destRef, String json) throws InfraException, BusinessException;

}