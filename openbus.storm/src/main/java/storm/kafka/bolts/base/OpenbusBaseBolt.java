package storm.kafka.bolts.base;

import java.util.Map;

import org.apache.avro.generic.GenericRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import br.com.produban.openbus.security.exceptions.SystemException;

public abstract class OpenbusBaseBolt extends BaseRichBolt {

	private OutputCollector collector;

	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
		collector = outputCollector;
	}

	public OutputCollector getCollector() {
		return collector;
	}

	public Logger getLogger() {
		return LOGGER;
	}

	@Override
	public void execute(Tuple tuple) {
		try {
			
			doExecute(tuple);
			getCollector().ack(tuple);
			
		} catch (InfraException e) {
			LOGGER.error("Tuple will be replayed.\n" + e.getMessage(), e);
			getCollector().fail(tuple);
		} catch (BusinessException e) {
			LOGGER.error("Tuple discarded.\n" + e.getMessage(), e);
			getCollector().ack(tuple);
		} catch (SystemException e) {
			LOGGER.error("Tuple discarded.\n" + e.getMessage(), e);
			getCollector().ack(tuple);
		} catch (Exception exception) {
			LOGGER.error("Exception during bolt execution", exception);
			getCollector().ack(tuple);
		}

	}

	protected void logAndDiscard(GenericRecord record) {
		getLogger().info("Discarding an invalid record: [{}]", record);
	}

	protected abstract void doExecute(Tuple tuple) throws Exception;
}
