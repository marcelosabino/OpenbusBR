package storm.kafka.bolts.base;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import br.com.produban.openbus.model.pojo.DataBean;
import br.com.produban.openbus.rules.Transformer;
import br.com.produban.openbus.rules.config.EnrichmentDatabaseConfiguration;
import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import com.typesafe.config.Config;
import storm.kafka.Constants;
import storm.kafka.StormLauncher;

public abstract class TransformerBaseBolt extends OpenbusBaseBolt {

    private Config configuration;
    private Config ramificationsConfig;
    private Transformer transformer;

    public TransformerBaseBolt(Config configuration, String tenantId) {
        this.configuration = configuration;
        this.ramificationsConfig = configuration.getConfig("topologies.transformation.ramifications");

        Config redisConfig = configuration.getConfig("redis");
        Config tenantConfig = configuration.getConfig("tenants").getConfig(tenantId);
        Config neo4jConfig = configuration.getConfig("neo4j");

        EnrichmentDatabaseConfiguration enrichmentDatabaseConfiguration = new EnrichmentDatabaseConfiguration();
        enrichmentDatabaseConfiguration.setUrl(neo4jConfig.getString("url"));
        enrichmentDatabaseConfiguration.setUser(neo4jConfig.getString("user"));
        enrichmentDatabaseConfiguration.setPassword(neo4jConfig.getString("password"));
        enrichmentDatabaseConfiguration.setTraverseQuery(neo4jConfig.getString("traversalQuery"));
        enrichmentDatabaseConfiguration.setTraverseQueryIn(neo4jConfig.getString("traversalQueryIn"));
        enrichmentDatabaseConfiguration.setTraverseQueryOut(neo4jConfig.getString("traversalQueryOut"));
        enrichmentDatabaseConfiguration.setTransactionalCypherPath(neo4jConfig.getString("transactionalCypherPath"));

        this.transformer = new Transformer(redisConfig.getString("host"),
                redisConfig.getInt("port"),
                tenantConfig.getInt("redis.database"),
                redisConfig.getString("password"),
                enrichmentDatabaseConfiguration);
    }

    public Config getRamificationsConfig() {
        return ramificationsConfig;
    }

    public Transformer getTransformer() {
        return transformer;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
//        if (getTopologyProperties().isOpenTsdbEnabled())
//            declarer.declareStream(Constants.STREAM_OPENTSDB, new Fields("dataBean"));
        if (getRamificationsConfig().getBoolean("qpid.enabled") || getRamificationsConfig().getBoolean("elasticsearch.enabled"))
            declarer.declareStream(Constants.STREAM_KAFKA_MESSAGE, new Fields("dataBean"));
        if (getRamificationsConfig().getBoolean("splunk.enabled"))
            declarer.declareStream(Constants.STREAM_SPLUNK, new Fields("dataBean"));
    }

    protected void doEmit(Tuple tuple, DataBean bean) throws InfraException, BusinessException {

        if (getLogger().isInfoEnabled())
            getLogger().info("Sending data bean: {}", bean);

//        if (getTopologyProperties().isOpenTsdbEnabled()) {
//            getCollector().emit(
//                    Constants.STREAM_OPENTSDB,
//                    tuple,
//                    new Values(getTransformer().prepareToPersist(Constants.PERSISTER_OPENTSDB, bean)));
//        }
        if (getRamificationsConfig().getBoolean("qpid.enabled") || getRamificationsConfig().getBoolean("elasticsearch.enabled")) {
            getCollector().emit(
                    Constants.STREAM_KAFKA_MESSAGE,
                    tuple,
                    new Values(getTransformer().prepareToPersist(Constants.PERSISTER_KAFKA_MESSAGE, bean)));
        }
        if (getRamificationsConfig().getBoolean("splunk.enabled")) {
            getCollector().emit(
                    Constants.STREAM_SPLUNK,
                    //tuple,
                    new Values(getTransformer().prepareToPersist(Constants.PERSISTER_SPLUNK, bean)));
        }
    }
}
