package storm.kafka.http;

import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import com.google.api.client.http.*;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.typesafe.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;

/**
 * Created by renato on 14/08/15.
 */
public class HTTPSender implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(HTTPSender.class);

    private Config config;
    private String host;
    private int port;

    private static final ThreadLocal<HttpRequest> httpConnectionThreadLocal = new ThreadLocal<HttpRequest>() {
        @Override
        public HttpRequest initialValue() {

            HttpTransport HTTP_TRANSPORT = new ApacheHttpTransport();

            try {
                return HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) {
                        request.setHeaders(new HttpHeaders().setAccept("application/json").setContentType("application/json"));
                    }
                }).buildPostRequest(null, null);
            } catch (IOException e) {
                logger.error("Error building HTTP Request to WSO2 CEP.", e);
            };
            return null;
        }
    };

    public HTTPSender(Config cepConfig) {
        this.config = cepConfig;

        this.host = config.getString("host");
        this.port = config.getInt("port");
    }

    public void send(String destRef, String json) throws InfraException, BusinessException {
        GenericUrl genericUrl = null;
        HttpResponse result = null;
        try {
            genericUrl = new GenericUrl(new URL("http://" + host + ":" + port + "/endpoints/t/" + destRef));
            HttpContent postData = new ByteArrayContent("application/json", json.getBytes());

            HttpRequest request = httpConnectionThreadLocal.get();
            request.setUrl(genericUrl);
            request.setContent(postData);

            result = request.execute();

        } catch (HttpResponseException e) {
            if (e.getStatusCode() >= 400 && e.getStatusCode() < 500) {
                throw new BusinessException("Endpoint failed for destination: " + genericUrl.toString(), e);
            } else
                throw new InfraException("Error calling WSO2 CEP, for URL: " + genericUrl.toString(), e);
        } catch (IOException e) {
            throw new InfraException("Error calling WSO2 CEP, for URL: " + genericUrl.toString(), e);
        } finally {
            try {
                if (result != null)
                    result.disconnect();
            } catch (IOException e) {
                throw new InfraException(e);
            }
        }
    }
}
