package storm.kafka.network;

import redis.clients.jedis.Jedis;

import java.util.concurrent.Callable;

public abstract class JedisCallable<V> implements Callable<V> {

    private Jedis jedis;

    public Jedis getJedis() {
        return jedis;
    }

    public void setJedis(Jedis jedis) {
        this.jedis = jedis;
    }
}
