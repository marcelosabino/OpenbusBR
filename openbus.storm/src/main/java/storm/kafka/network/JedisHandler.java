package storm.kafka.network;

import br.com.produban.openbus.security.exceptions.InfraException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;
import storm.kafka.StormLauncher;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class JedisHandler {

//    private static final Logger logger = LoggerFactory.getLogger(JedisHandler.class);
//    private final String tenantId;
//    private final String redisHost;
//    private final Integer redisPort;
//    private final Integer redisDatabaseIndex;
//    private final String redisPassword;
//
//    private Jedis jedis;
//
//    public JedisHandler(String tenantId, StormLauncher.TopologyProperties topologyProperties) {
//        this.tenantId = tenantId;
//        this.redisHost = topologyProperties.getRedisRulesHost();
//        this.redisPort = topologyProperties.getRedisRulesPort();
//        this.redisDatabaseIndex = Integer.valueOf(topologyProperties.getRedisDatabasesPerTenant().get(tenantId));
//        this.redisPassword = topologyProperties.getRedisPassword();
//    }
//
//    private Jedis getJedis() throws InfraException {
//        if (jedis==null) {
//            try {
//                URI uri = new URI("redis",String.format("%s:%s","openbus",redisPassword),redisHost,redisPort,String.format("/%s", redisDatabaseIndex),null,null);
//                jedis = new Jedis(uri);
//                jedis.ping();
//            } catch (URISyntaxException e) {
//                throw new InfraException("Fail to create a Redis connection",e);
//            } catch (JedisException e) {
//                throw new InfraException("Fail to connect to Redis",e);
//            }
//        }
//
//        return jedis;
//    }
//
//    private void recycleJedis() {
//        jedis.close();
//        jedis = null;
//    }
//
//    public <T> T runOnJedis(JedisCallable<T> callable) throws InfraException {
//        try {
//            callable.setJedis(getJedis());
//            return (T) callable.call();
//        } catch (JedisException e) {
//            recycleJedis();
//            throw new InfraException("Fail to run a statement on Redis",e);
//        } catch (Exception e) {
//            throw new InfraException("Fail to run a statement on Redis",e);
//        }
//    }
}