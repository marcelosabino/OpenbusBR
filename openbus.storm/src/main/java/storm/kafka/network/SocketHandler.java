package storm.kafka.network;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.produban.openbus.security.exceptions.InfraException;

public class SocketHandler implements Serializable {

    private static final long serialVersionUID = 5206545536762515307L;
    
    private static final Logger logger = LoggerFactory.getLogger(SocketHandler.class);

    private Socket socket;

    private String host;
    private int port;

    private static int RECYCLE_MAX_RETRIES = 3;
    private int recycleRetryCount;
    private boolean failFast;

    public SocketHandler(String host, int port, boolean failFast) {
        this.host = host;
        this.port = port;
        this.failFast = failFast;
    }

    public Socket getConnection() throws IOException, InfraException {
        if (socket == null) {
            Socket newSocket = createConnection(host,port);

            if (newSocket == null) {
            	if (failFast) {
            		throw new IOException("Failed to get Socket. Fail Fast enabled.");
            	}
                newSocket = retryConnection(host,port);
            }

            socket = newSocket;
        }

        return socket;
    }

    private Socket createConnection(String host, int port) {
        Socket socket = null;
        try {
            socket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(host, port);
            socket.setKeepAlive(false);
            socket.setSoTimeout(0);
            socket.connect(socketAddress, 500);

        } catch (IOException e) {
            logger.error("Error when creating a new connection", e);
            socket = null;
        }
        return socket;
    }

    public void destroy() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
            	logger.error("Error when destroying a stale connection", e);
            }
        }
    }

    private Socket retryConnection(String host, int port) throws InfraException {
        Socket socket = createConnection(host, port);

        while (socket == null && recycleRetryCount < RECYCLE_MAX_RETRIES) {
            try {
                Thread.sleep(3000);
                socket = createConnection(host, port);
            } catch (InterruptedException e) {
            	logger.error("Thread interrupted when recycling a connection", e);
            }

            recycleRetryCount++;
        }

        if (socket == null) {
            throw new InfraException("Fail to recycle a stale socket [host="+host+" | port="+port+"] after "+RECYCLE_MAX_RETRIES+" attempts");
        } else {
            resetRecycleRetryCount();
            return socket;
        }
    }

    private void resetRecycleRetryCount() {
        this.recycleRetryCount = 0;
    }
}
