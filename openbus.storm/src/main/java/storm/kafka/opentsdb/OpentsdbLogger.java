package storm.kafka.opentsdb;

import java.io.Serializable;

public class OpentsdbLogger implements Serializable {

//	private static final long serialVersionUID = 2452059096405358081L;
//
//	private static final Logger logger = LoggerFactory.getLogger(OpentsdbLogger.class);
//
//	private static final ThreadLocal<SocketHandler> openTsdbConnectionThreadLocal = new ThreadLocal<>();
//
//	private TopologyProperties topologyProperties;
//
//	public OpentsdbLogger(TopologyProperties topologyProperties) {
//		this.topologyProperties = topologyProperties;
//	}
//
//    public void log(String key, Long clock, String value, String host, Map<String,String> tags) throws InfraException {
//
//        StringBuffer lines = new StringBuffer();
//
//        lines.append("put ").append(key)
//        		.append(" ").append(clock)
//        		.append(" ").append(value)
//        		.append(" host=").append(host);
//
//        for (String tag : tags.keySet()) {
//        	String tagValue = tags.get(tag);
//        	if (tagValue != null && !tagValue.isEmpty()) {
//        		lines.append(" " + tag + "=" + tags.get(tag));
//        	}
//        }
//
//        lines.append("\n");
//
//        logToOpentsdb(lines);
//    }
//
//	private void logToOpentsdb(StringBuffer lines) throws InfraException {
//		String msg = lines.toString();
//		logger.info("Writing [{}] to opentsdb", msg);
//
//		SocketHandler socketHandler = getSocket();
//		try {
//			Writer writer = new OutputStreamWriter(socketHandler.getConnection().getOutputStream());
//			writer.write(msg);
//			writer.flush();
//		} catch (IOException e) {
//			invalidateSocket();
//			throw new InfraException(e);
//		}
//	}
//
//	private SocketHandler getSocket() {
//        SocketHandler socketHandler = openTsdbConnectionThreadLocal.get();
//
//        if (socketHandler == null) {
//            openTsdbConnectionThreadLocal.set(new SocketHandler(topologyProperties.getOpentsdbSeriesHost(), topologyProperties.getOpentsdbSeriesPort(), false));
//        }
//
//        return openTsdbConnectionThreadLocal.get();
//    }
//
//	private void invalidateSocket() {
//        SocketHandler staleSocket = openTsdbConnectionThreadLocal.get();
//        staleSocket.destroy();
//        openTsdbConnectionThreadLocal.set(null);
//    }

}
