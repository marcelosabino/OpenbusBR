package storm.kafka.qpid;

import br.com.produban.openbus.security.exceptions.InfraException;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.NamingException;
import java.io.Serializable;
import java.util.HashMap;

public class QpidSender implements Serializable {

	private static final long serialVersionUID = -2518941647393909274L;

	private static final ThreadLocal<Connection> qpidConnectionThreadLocal = new ThreadLocal<Connection>();
	private static final ThreadLocal<HashMap<String, Destination>> qpidDestinationThreadLocal = new ThreadLocal<HashMap<String, Destination>>();
//	private TopologyProperties topologyProperties;

//	public QpidSender(TopologyProperties topologyProperties) {
//		this.topologyProperties = topologyProperties;
//	}

	private Connection getQpidConnection() throws InfraException {
		Context context = null;
		ConnectionFactory connectionFactory = null;
		Connection connection = qpidConnectionThreadLocal.get();

		if (connection == null) {
			try {
//				context = new InitialContext(topologyProperties.getRaw());
				connectionFactory = (ConnectionFactory) context.lookup("qpidConnectionfactory");
				connection = connectionFactory.createConnection();
			} catch (NamingException | JMSException e) {
				throw new InfraException(e);
			}
			qpidConnectionThreadLocal.set(connection);
		}
		
		try {
			connection.getMetaData();
		} catch (JMSException e) {
			qpidConnectionThreadLocal.set(null);
			throw new InfraException(e);
		}
		return connection;
	}
	
	private Destination getQpidDestination(String destRef) throws InfraException {
		Destination destination = getDestinationMap().get(destRef);
		if (destination == null) {
//			try {
//				Context context = new InitialContext(topologyProperties.getRaw());
//				destination = (Destination) context.lookup(destRef);
//			} catch (NamingException e) {
//				throw new InfraException(e);
//			}
			getDestinationMap().put(destRef, destination);
		}
		return destination;
	}
	
	private HashMap<String, Destination> getDestinationMap() {
		HashMap<String, Destination> destinationMap = qpidDestinationThreadLocal.get();
		
		if (destinationMap == null) {
			destinationMap = new HashMap<String, Destination>();
			qpidDestinationThreadLocal.set(destinationMap);
		}
		return destinationMap;
	}
	
	
	public void send(String destRef, String message) throws InfraException {
		Session session = null;

		try {
			getQpidConnection().start();

			// Create a Session
			session = getQpidConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = getQpidDestination(destRef);

			// Create a MessageProducer from the Session to the Topic or Queue
			MessageProducer producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

			// Create a messages
			TextMessage textMessage = session.createTextMessage(message);

			// Tell the producer to send the message
			System.out.println("Sent message: " + textMessage);
			producer.send(textMessage);

		} catch (JMSException e) {
			throw new InfraException(e);
		} finally {
			// Clean up
			try {
				if (session != null)
					session.close();
			} catch (JMSException e) {
				throw new InfraException(e);
			}
		}
	}
}
