package storm.kafka.splunk;

import br.com.produban.openbus.security.exceptions.BusinessException;
import br.com.produban.openbus.security.exceptions.InfraException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.kafka.StormLauncher;
import storm.kafka.network.SocketHandler;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class SplunkSender implements Serializable {

    private static final long serialVersionUID = 4815613130392606806L;

    private static final Logger logger = LoggerFactory.getLogger(SplunkSender.class);

    private static final ThreadLocal<Map<String,SocketHandler>> splunkSocketThreadLocal = new ThreadLocal<Map<String,SocketHandler>>() {
        @Override
        protected Map<String, SocketHandler> initialValue() {
            return new HashMap<>();
        }
    };

    private String host;
    private int port;
    private Map<String, String> tenantPortPrefixMap;

    public SplunkSender(String host, Map<String,String> tenantPortPrefixMap) {
        this.host = host;
        this.tenantPortPrefixMap = tenantPortPrefixMap;
    }

    public SplunkSender(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private SocketHandler getSocket(String destRef) throws BusinessException, InfraException {
        Map<String,SocketHandler> socketHandlerMap = splunkSocketThreadLocal.get();

        if (!socketHandlerMap.containsKey(destRef)) {
            socketHandlerMap.put(destRef,initializeSocketHandler(destRef));
        }

        return socketHandlerMap.get(destRef);
    }

    private SocketHandler initializeSocketHandler(String destRef) throws BusinessException, InfraException {
        if (tenantPortPrefixMap != null && !tenantPortPrefixMap.isEmpty())
            return new SocketHandler(host, SplunkTcpRouter.resolve(destRef,tenantPortPrefixMap), true);
        else if (port != 0)
            return new SocketHandler(host,port,true);
        else
            throw new InfraException("There's no way to resolve Splunk TCP port!");
    }

    public void send(String destRef, String message) throws InfraException, BusinessException {
        logger.info("Writing [{}] to Splunk [DestRef: {}]", message, destRef);

        SocketHandler socketHandler = getSocket(destRef);
        try {
            Writer writer = new OutputStreamWriter(socketHandler.getConnection().getOutputStream(), StandardCharsets.UTF_8);
            writer.write(message);
            writer.flush();
        } catch (IOException e) {
            invalidateSocket(destRef);
            throw new InfraException(e);
        }
    }

    private void invalidateSocket(String destRef) {
        SocketHandler staleSocket = splunkSocketThreadLocal.get().remove(destRef);
        staleSocket.destroy();
    }
}
