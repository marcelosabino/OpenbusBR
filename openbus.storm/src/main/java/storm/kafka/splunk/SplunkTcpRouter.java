package storm.kafka.splunk;

import br.com.produban.openbus.security.exceptions.BusinessException;

import java.util.Map;

public class SplunkTcpRouter {
    public static int resolve(String destRef, Map<String, String> tenantPortPrefixMap) throws BusinessException {

        String[] destinationSplit = destRef.split("\\.");
        String portPrefix = tenantPortPrefixMap.get(destinationSplit[0]);
        String tool = destinationSplit[1];

        String toolPort;

        switch (tool) {
        	case "Firewall" :
        		toolPort = "14";
        		break;
        	case "Radius" :
        		toolPort = "13";
        		break;
        	case "ActiveDirectory" :
                toolPort = "00";
                break;
            case "Proxy" :
                toolPort = "01";
                break;
            case "IPS" :
                toolPort = "02";
                break;
            case "CommElement":
                toolPort = "12";
                break;
            case "SNMPTrap" :
                toolPort = "03";
                break;
            case "FireEye" :
                toolPort = "04";
                break;
            case "Syslog" :
                toolPort = "05";
                break;
            case "Zabbix":
            case "PoolMembers":
            case "Robots":
            case "Introscope":
                toolPort = "06";
                break;
            case "Eventlog" :
                toolPort = "07";
                break;
            case "Oracle" :
                toolPort = "08";
                break;
            case "WebSphere" :
                toolPort = "09";
                break;
            case "WebsphereMQ":
                toolPort = "10";
                break;
            case "Tacacs" :
                toolPort = "16";
                break;
            case "IDM" :
                toolPort = "21";
                break;

            default:
                throw new BusinessException("Error while resolving destination for: " + destRef);
        }

        return Integer.valueOf(portPrefix + toolPort);
    }
}
